#!/bin/bash

echo Building DELTA3 with new configuration ...

sudo add-apt-repository ppa:webupd8team/java

sudo apt-get update

echo ===================================================

#sudo apt-get install oracle-java8-installer

sudo apt install openjdk-8-jdk

#export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64


echo $JAVA_HOME

echo ===================================================

echo Setting up ant build ...

sudo apt-get -u install ant

ant -Dj2ee.server.home=/var/lib/tomcat9/webapps