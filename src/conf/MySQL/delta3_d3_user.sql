-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_user`
--

DROP TABLE IF EXISTS `d3_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d3_user` (
  `idUser` int(11) NOT NULL,
  `GUID` varchar(45) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `alias` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `passExpirationTS` timestamp NULL DEFAULT NULL,
  `passChangedTS` timestamp NULL DEFAULT NULL,
  `passFailureCount` int(11) DEFAULT NULL,
  `passFailureMax` int(11) DEFAULT NULL,
  `securityHandler` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `locale` varchar(45) DEFAULT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `emailAddress1` varchar(256) DEFAULT NULL,
  `emailAddress2` varchar(256) DEFAULT NULL,
  `uri1` varchar(256) DEFAULT NULL,
  `uri2` varchar(256) DEFAULT NULL,
  `graphPackage` varchar(45) DEFAULT 'd3',
  `phoneNumber1` varchar(45) DEFAULT NULL,
  `phoneNumber2` varchar(45) DEFAULT NULL,
  `alertPreference1` varchar(45) DEFAULT NULL,
  `alertPreference2` varchar(45) DEFAULT NULL,
  `Organization_idOrganization` int(11) NOT NULL,
  `Person_idPerson` int(11) NOT NULL,
  `Group_idGroup` int(11) NOT NULL,
  PRIMARY KEY (`idUser`,`Organization_idOrganization`,`Person_idPerson`,`Group_idGroup`),
  KEY `fk_User_Organization1_idx` (`Organization_idOrganization`),
  CONSTRAINT `fk_User_Organization1` FOREIGN KEY (`Organization_idOrganization`) REFERENCES `d3_organization` (`idOrganization`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_user`
--

LOCK TABLES `d3_user` WRITE;
/*!40000 ALTER TABLE `d3_user` DISABLE KEYS */;
INSERT INTO `d3_user` VALUES (1,'4a48decd-232e-49cc-ae7b-0bf827de6061',2,'2013-10-03 20:08:53',1,'2017-01-19 01:36:23','csoadmin','b00f66d87bb00ca2ccb207bfa3de4110b0da88b9','2020-10-10 04:00:00','2015-03-25 17:52:33',0,3,'1',1,'','OADMIN','eng_US','John','Smith','company@xyzsystems.com','','','','d3','','','','',1,1,1),(2,'4a48decd-232e-49cc-ae7b-0bf827de6061',2,'2013-12-31 18:37:29',1,'2017-01-19 01:36:44','cssadmin','b00f66d87bb00ca2ccb207bfa3de4110b0da88b9','2020-10-10 04:00:00','2015-03-25 17:51:58',0,3,'1',1,'',NULL,'eng_US','System','Admin','cssadmin@xyzsystems.com','','','','d3','','','','',1,0,0),(3,'9c653ac2-c5ba-4623-9f6c-6e66a28c060d',1,'2017-01-24 21:36:49',1,'2017-01-24 21:37:25','dbadmin','b00f66d87bb00ca2ccb207bfa3de4110b0da88b9','2020-01-24 05:00:00','2017-01-25 14:04:35',0,3,'1',1,'','USER','eng_US','DB','Admin','','','','','d3','','','','',1,0,0),(4,'d0b72650-4eda-4543-ad40-abfda11033c8',1,'2017-01-24 22:29:37',1,'2017-01-24 22:30:01','deltauser','b00f66d87bb00ca2ccb207bfa3de4110b0da88b9','2020-01-24 05:00:00','2017-01-25 14:03:29',0,3,'1',1,'','USER','eng_US','Delta','User','','','','','d3','','','','',1,0,0);
/*!40000 ALTER TABLE `d3_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-25  9:05:51
