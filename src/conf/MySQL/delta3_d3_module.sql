-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_module`
--

DROP TABLE IF EXISTS `d3_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d3_module` (
  `idModule` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `parentId` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `Organization_idOrganization` int(11) NOT NULL,
  PRIMARY KEY (`idModule`,`Organization_idOrganization`),
  KEY `fk_Module_Organization1_idx` (`Organization_idOrganization`),
  CONSTRAINT `fk_Module_Organization1` FOREIGN KEY (`Organization_idOrganization`) REFERENCES `d3_organization` (`idOrganization`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_module`
--

LOCK TABLES `d3_module` WRITE;
/*!40000 ALTER TABLE `d3_module` DISABLE KEYS */;
INSERT INTO `d3_module` VALUES (1,1,'2013-10-30 14:00:00',1,'2013-10-30 16:00:00','Administration','All administration tasks','ADMIN','0',1,1),(2,1,'2014-01-21 17:00:00',1,NULL,'Model Builder','Model Builder','APP','0',1,1),(3,1,'2014-07-03 16:00:00',1,NULL,'Study Manager','Module used to create Studies','APP','0',1,1),(4,1,'2014-07-03 16:00:01',1,NULL,'Logistic Regression','Logistic Regression Formula Builder','APP','0',1,1),(5,1,'2014-09-12 16:00:00',1,NULL,'Events','Events, Alerts and Notifications Module','ADMIN','0',1,1),(6,1,'2014-09-25 16:00:00',1,NULL,'Statistics Manager','Interaction with statistical packages, data management.','APP','0',1,1),(7,1,'2016-02-19 17:00:00',1,NULL,'Health Survey','External Health Survey Module running in iFrame','APP','0',1,1),(8,1,'2018-04-23 14:59:00',1,NULL,'Chart Review','Ambulatory Chart Review','APP','0',1,1),(9,1,'2018-06-28 13:40:00',1,NULL,'LHODE Valueset','LHODE Valueset Manager ','APP','0',1,1);
/*!40000 ALTER TABLE `d3_module` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-05 10:56:46
