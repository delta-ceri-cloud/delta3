-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_alert`
--

DROP TABLE IF EXISTS `d3_alert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d3_alert` (
  `idAlert` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `retryMax` int(11) DEFAULT NULL,
  `timeOut` int(11) DEFAULT NULL COMMENT 'in seconds',
  `ackRequired` tinyint(1) DEFAULT NULL,
  `sendEmail` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `EventTemplate_idEventTemplate` int(11) NOT NULL,
  `User_idUser` int(11) NOT NULL,
  PRIMARY KEY (`idAlert`),
  KEY `fk_Alert_EventTemplae1_idx` (`EventTemplate_idEventTemplate`),
  KEY `fk_Alert_Subscriber1_idx` (`User_idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_alert`
--

LOCK TABLES `d3_alert` WRITE;
/*!40000 ALTER TABLE `d3_alert` DISABLE KEYS */;
INSERT INTO `d3_alert` VALUES (9,'Job Submit','Request submitted','1',0,0,0,0,1,1,'2014-09-24 14:22:29',1,'2015-10-09 19:53:26',1,1),(11,'Value Below Zero','Value of one of the results is below zero','2',0,0,0,0,1,1,'2015-10-09 19:53:10',1,'2015-10-09 20:03:34',9,1),(12,'Value Above Primary CI','Value of one of the results is above primary confidence level','2',0,0,0,0,1,1,'2015-10-09 19:54:00',1,'2015-10-09 20:03:42',10,1),(13,'Value Above Secondary CI','Value of one of the results is above secondary confidence level','2',0,0,0,0,1,1,'2015-10-09 19:54:28',1,'2015-10-09 20:03:51',11,1),(14,'Value Below Secondary CI','Value of one of the results is below secondary confidence level','2',0,0,0,0,1,1,'2015-10-09 19:54:56',1,'2015-10-09 20:03:59',13,1),(15,'Value Below Zero','Value of one of the results is below zero','2',0,0,0,0,1,1,'2015-10-09 19:56:47',1,'2015-10-09 20:04:08',9,1),(16,'Value Above Secondary CI','Value of one of the results is above secondary confidence level','2',0,0,0,0,1,1,'2015-10-09 19:57:21',1,'2015-10-09 20:04:14',11,1);
/*!40000 ALTER TABLE `d3_alert` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-19 11:38:37
