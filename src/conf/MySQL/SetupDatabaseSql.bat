@echo off

REM -------------------------------------------------------------------------------------------------------------------------------------------------
REM -- This is a batch file to process building the databases and sample users, grant access DELTA3 SQL, code version 3.62 Maintainer @NehaKhairnar
REM -----------------------------------------------------------------------------------------------------------------------------------------------


REM --
REM --mysql -u root -p -e "CREATE DATABASE delta3";

REM --mysql -u root -p -e "CREATE DATABASE delta3_data";

REM --mysql -u root -p -e "CREATE DATABASE d3_test_datasets";

REM --mysql -u root -p -e "CREATE USER 'CSUser'@'localhost' IDENTIFIED BY '123'";

REM --mysql -u root -p -e "GRANT ALTER, CREATE, CREATE TEMPORARY TABLES, LOCK TABLES, SELECT, INSERT, UPDATE, DELETE, DROP ON delta3.* TO 'CSUser'@'localhost'";

REM --mysql -u root -p -e "CREATE USER 'DeltaUser'@'localhost' IDENTIFIED BY 'deltadata135'";

REM --mysql -u root -p -e "GRANT ALTER, CREATE, CREATE TEMPORARY TABLES, LOCK TABLES, SELECT, INSERT, UPDATE, DELETE, DROP ON delta3_data.* TO 'DeltaUser'@'localhost'";

REM --mysql -u root -p -e "CREATE USER 'd3TestUser'@'localhost' IDENTIFIED BY '123'";

REM --mysql -u root -p -e "GRANT SELECT, INSERT, UPDATE, DELETE, ALTER, DROP ON d3_test_datasets.* TO 'd3TestUser'@'localhost'";




mysql -uroot -p%1 -e "CREATE DATABASE delta3";

mysql -uroot -p%1 -e "CREATE DATABASE delta3_data";

mysql -uroot -p%1 -e "CREATE DATABASE d3_test_datasets";

mysql -uroot -p%1 -e "CREATE USER 'CSUser'@'localhost' IDENTIFIED BY '123'";

mysql -uroot -p%1 -e "GRANT ALTER, CREATE, CREATE TEMPORARY TABLES, LOCK TABLES, SELECT, INSERT, UPDATE, DELETE, DROP ON delta3.* TO 'CSUser'@'localhost'";

mysql -uroot -p%1 -e "CREATE USER 'DeltaUser'@'localhost' IDENTIFIED BY 'deltadata135'";

mysql -uroot -p%1 -e "GRANT ALTER, CREATE, CREATE TEMPORARY TABLES, LOCK TABLES, SELECT, INSERT, UPDATE, DELETE, DROP ON delta3_data.* TO 'DeltaUser'@'localhost'";

mysql -uroot -p%1 -e "CREATE USER 'd3TestUser'@'localhost' IDENTIFIED BY '123'";

mysql -uroot -p%1 -e "GRANT SELECT, INSERT, UPDATE, DELETE, ALTER, DROP ON d3_test_datasets.* TO 'd3TestUser'@'localhost'";