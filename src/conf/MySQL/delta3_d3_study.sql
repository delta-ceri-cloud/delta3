-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_study`
--

DROP TABLE IF EXISTS `d3_study`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d3_study` (
  `idStudy` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `status` varchar(1024) DEFAULT NULL,
  `idOrganization` int(11) NOT NULL,
  `idGroup` int(11) DEFAULT NULL,
  `idModel` int(11) NOT NULL,
  `ModelColumn_idOutcome` int(11) DEFAULT NULL,
  `ModelColumn_idTreatment` int(11) DEFAULT NULL,
  `ModelColumn_idKey` int(11) DEFAULT NULL,
  `ModelColumn_idDate` int(11) DEFAULT NULL,
  `Stat_idStat` int(11) DEFAULT NULL,
  `Method_idMethod` int(11) DEFAULT NULL,
  `Filter_idFilter` int(11) DEFAULT NULL,
  `methodParams` varchar(10000) DEFAULT NULL,
  `isMissingDataUsed` tinyint(1) DEFAULT NULL,
  `isGenericId` tinyint(1) DEFAULT NULL,
  `isChunking` tinyint(1) DEFAULT NULL,
  `chunkingBy` varchar(45) DEFAULT NULL,
  `dumpData` tinyint(1) DEFAULT '0',
  `sendAllColumns` tinyint(1) DEFAULT '0',
  `outcomePositive` tinyint(1) DEFAULT '0',
  `autoExecute` tinyint(1) NOT NULL DEFAULT '1',
  `startTS` timestamp NULL DEFAULT NULL,
  `endTS` timestamp NULL DEFAULT NULL,
  `Alert_idAlert` int(11) DEFAULT NULL,
  `statPackageVer` varchar(45) DEFAULT NULL,
  `originalStudyId` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `confidenceInterval` double DEFAULT NULL,
  `confidenceInterval2` double DEFAULT NULL,
  `overwriteResults` tinyint(1) DEFAULT NULL,
  `localStatPackage` tinyint(1) DEFAULT NULL,
  `remoteStatPackage` tinyint(1) DEFAULT NULL,
  `LastProcess_idProcess` int(11) DEFAULT NULL,
  `submittedTS` varchar(45) DEFAULT NULL,
  `processedTS` timestamp NULL DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idStudy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_study`
--

LOCK TABLES `d3_study` WRITE;
/*!40000 ALTER TABLE `d3_study` DISABLE KEYS */;
/*!40000 ALTER TABLE `d3_study` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-11 11:10:55
