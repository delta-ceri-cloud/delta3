@echo off

REM ------------------------------------------------------------------------------------
REM -- This is a batch file to process building the DELTA3 SQL, code version 1.85
REM ------------------------------------------------------------------------------------

mysql -uroot -p%1 -B delta3 < delta3_d3_alert.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_audit.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_configuration.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_event.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_eventtemplate.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_filter.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_filter_field.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_filter_has_field.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_group_has_entity.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_group_has_user.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_group_table.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_location.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_logregr.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_logregr_date.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_logregr_field.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_method.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_model.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_model_column.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_model_key.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_model_relationship.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_model_table.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_module.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_notification.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_notificationtemplate.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_organization.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_permission.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_permissiontemplate.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_person.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_process.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_role.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_sequence.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_study.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_study_has_category.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_subscriber.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_user.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_user_has_role.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_task.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_session.sql
mysql -uroot -p%1 -B delta3 < delta3_d3_metadata.sql
mysql -uroot -p%1 -B delta3 < sample_data1_v200.sql

