/* 
 * (C) 2016 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Sep 14, 2018
 */

CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateOrgUserStat`(
 IN orgName VARCHAR(25),
 IN orgDesc VARCHAR(50), 
 IN userAlias VARCHAR(50), 
 OUT idOrg INT)
BEGIN
DECLARE idOrg INT DEFAULT 0;
DECLARE idUser INT DEFAULT 0;

    SELECT seq_count
    INTO idOrg
    FROM d3_sequence
    WHERE seq_name = 'ORGANIZATION_ID';

    INSERT INTO `d3_organization` VALUES (idOrg,UUID(),2,CURRENT_TIMESTAMP(),2,CURRENT_TIMESTAMP(),orgName,orgDesc,'IT',0,'vaJXZZhtpLXECudR7JcZz9fXDDjGMRuQTLEpaIge',NULL,1);
    SET idOrg = idOrg + 1;

    UPDATE d3_sequence 
    SET seq_count = idOrg
    WHERE seq_name = 'ORGANIZATION_ID';
    
    SET idOrg = idOrg -1;
    
    CALL CreateUser_OADMIN(idOrg,userAlias,@idUser);   
    
    CALL AssignDELTAlyticsStatisticPackage(idOrg);

END

