/* 
 * (C) 2016 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Sep 17, 2018
 */
CREATE DEFINER=`root`@`localhost` PROCEDURE `AssignDELTAlyticsStatisticPackage`(
 IN idOrg INT)
BEGIN
  DECLARE idStat INT DEFAULT 0;
  -- Declare variables to hold diagnostics area information
  DECLARE code CHAR(5) DEFAULT '00000';
  DECLARE msg TEXT;
  DECLARE rows INT;
  DECLARE result TEXT;
  -- Declare exception handler for failed insert
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1
        code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
    END;
    

	SELECT seq_count
	INTO idStat
	FROM d3_sequence
	WHERE seq_name = 'STAT_ID';
    
	SELECT 
		@idStat := 1,
		@idOrg := 1,     
		@shortName := `shortName`,
		@nameN := `name`,
		@description := `description`,
		@configVersion := `configVersion`,
		@useProxy := `useProxy`,
		@configuration := `configuration`,
		@active := `active`,
		@createdBy := `createdBy`,
		@createdTs := CURRENT_TIMESTAMP(),
		@updatedBy :=`updatedBy`,
		@updatedTs := CURRENT_TIMESTAMP()   
	FROM d3_stat
	WHERE idOrganization = 1 AND idStat = 1;   
    
  -- Check whether the select was successful
   
    
	INSERT d3_stat (
		`idStat`,
		`idOrganization`,     
		`shortName`,
		`name`,
		`description`,
		`configVersion`,
		`useProxy`,
		`configuration`,
		`active`,
		`createdBy`,
		`createdTS`,
		`updatedBy`,
		`updatedTS`
        )
    VALUES (
		idStat,
		idOrg,     
		@shortName,
		@nameN,
		@description,
		@configVersion,
		@useProxy,
		@configuration,
		@active,
		@createdBy,
		@createdTs,
		@updatedBy,
		@updatedTs      
        );
 
    
	  -- Check whether the insert was successful
	IF code = '00000' THEN
		GET DIAGNOSTICS rows = ROW_COUNT;
		SET result = CONCAT('insert succeeded, row count = ',rows);
	ELSE
		SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
	END IF;
    
	SET idStat = idStat + 1;
 
	UPDATE d3_sequence 
	SET seq_count = idStat
	WHERE seq_name = 'STAT_ID';
    
    -- SET idStat = idStat - 1;
    
    SELECT result;    

END
