-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_model_column`
--

DROP TABLE IF EXISTS `d3_model_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d3_model_column` (
  `idModelColumn` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `physicalName` varchar(255) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `defaultValue` varchar(1024) DEFAULT NULL,
  `defaultValueType` varchar(12) DEFAULT NULL,
  `keyField` tinyint(1) DEFAULT '0',
  `insertable` tinyint(1) DEFAULT '1',
  `atomic` tinyint(1) DEFAULT '1',
  `sub` tinyint(1) DEFAULT '0',
  `virtualField` tinyint(1) DEFAULT '0',
  `virtualOrder` tinyint(1) DEFAULT '0',
  `rollup` tinyint(4) DEFAULT NULL,
  `formula` varchar(1024) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT '0',
  `fieldClass` varchar(20) DEFAULT NULL,
  `fieldKind` varchar(20) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `ModelTable_idModelTable` int(11) DEFAULT NULL,
  `Model_idModel` int(11) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idModelColumn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_model_column`
--

LOCK TABLES `d3_model_column` WRITE;
/*!40000 ALTER TABLE `d3_model_column` DISABLE KEYS */;
INSERT INTO `d3_model_column` VALUES (1,'married','','married','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(2,'gender_x','','gender_x','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(3,'risk1_enum','','risk1_enum','INT(11)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Continuous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(4,'risk2_enum','','risk2_enum','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Continuous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(5,'risk3_enum','','risk3_enum','INT(11)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Continuous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(6,'age','','age','INT(11)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Continuous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(7,'duration','','duration','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Continuous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(8,'treatment1','','treatment1','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Treatment','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(9,'treatment2','','treatment2','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Treatment','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(10,'duration_gt24','','duration_gt24','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(11,'race1','','race1','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(12,'race2','','race2','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(13,'race3','','race3','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(14,'race4','','race4','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(15,'insurance1','','insurance1','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(16,'insurance2','','insurance2','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(17,'insurance3','','insurance3','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(18,'insurance4','','insurance4','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(19,'risk4_dich','','risk4_dich','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(20,'risk5_dich','','risk5_dich','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(21,'risk6_dich','','risk6_dich','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(22,'month','','month','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Risk Factor','Continuous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(23,'outcome1','','outcome1','SMALLINT(6)',NULL,NULL,0,1,1,0,0,0,0,'',0,'Outcome','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(24,'caseid','','caseid','VARCHAR(50)',NULL,NULL,1,1,1,0,0,0,0,'',0,'Case ID','Continuous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(25,'durationgte12','duration >=12','virtual','INTEGER(11)',NULL,NULL,0,1,0,0,1,0,0,'duration>=12',1,'Risk Factor','Dichotomous',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21'),(26,'ctrdate','','ctrdate','DATE',NULL,NULL,0,1,1,0,0,0,0,'',0,'Sequencer','Date',1,1,1,3,'2017-01-24 21:49:21',3,'2017-01-24 21:49:21');
/*!40000 ALTER TABLE `d3_model_column` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-24 17:03:51
