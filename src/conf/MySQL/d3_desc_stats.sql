CREATE TABLE `delta3`.`d3_desc_stats` ( 
    `idStats` int NOT NULL, 
    `idOrganization` int NOT NULL, 
    `idModel` int NOT NULL, 
    `name` varchar(256) NULL, 
    `stats` mediumtext NULL, 
    `status` varchar(50) NULL, 
    `flat` tinyint(1) NULL, 
    `active` tinyint(1) NULL, 
    `createdBy` int NULL, 
    `createdTS` timestamp NULL, 
    `updatedBy` int NULL, 
    `updatedTS` timestamp NULL,  
    PRIMARY KEY (`idStats`)
) ENGINE=InnoDB  COLLATE=utf8_general_ci;