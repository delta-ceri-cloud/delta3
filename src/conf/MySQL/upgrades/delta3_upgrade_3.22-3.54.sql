/* 
 * (C) 2016 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Jun 29, 2018
 */

INSERT INTO `delta3`.`d3_module` (`idModule`, `createdBy`, `createdTS`, `updatedBy`, `name`, `description`, `type`, `parentId`, `active`, `Organization_idOrganization`) VALUES ('9', '1', '2018-06-28 13:40:00', '1', 'LHODE Valueset', 'LHODE Valueset Manager ', 'APP', '0', '1', '1');
INSERT INTO `delta3`.`d3_permissiontemplate` (`idPermissionTemplate`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `name`, `description`, `type`, `tag`, `sequence`, `active`, `Module_idModule`) VALUES ('33', '1', '2018-06-28 13:46:00', '1', '2018-06-28 13:46:00', 'Valueset Admin', 'administer valuesets', 'app', 'ValuesetAdmin', '1044', '1', '9');
INSERT INTO `delta3`.`d3_permissiontemplate` (`idPermissionTemplate`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `name`, `description`, `type`, `tag`, `sequence`, `active`, `Module_idModule`) VALUES ('34', '1', '2018-06-28 13:46:00', '1', '2018-06-28 13:46:00', 'Valueset Reviewer', 'review valuesets', 'app', 'ValuesetReviewer', '1046', '1', '9');
UPDATE `delta3`.`d3_sequence` SET `SEQ_COUNT`='35' WHERE `SEQ_NAME`='PERMTEMPLATE_ID';

ALTER TABLE `delta3`.`d3_role` 
ADD COLUMN `moduleId` INT(11) NULL DEFAULT NULL AFTER `parentId`;

/* set moduleId in d3_role table accordingly */
UPDATE `delta3`.`d3_role` SET `moduleId`='1' WHERE `idRole`='4';
UPDATE `delta3`.`d3_role` SET `moduleId`='1' WHERE `idRole`='3';
UPDATE `delta3`.`d3_role` SET `moduleId`='1' WHERE `idRole`='2';
UPDATE `delta3`.`d3_role` SET `moduleId`='1' WHERE `idRole`='1';

ALTER TABLE `delta3`.`d3_session` 
ADD COLUMN `apps` VARCHAR(256) NULL DEFAULT NULL AFTER `permissions`;




