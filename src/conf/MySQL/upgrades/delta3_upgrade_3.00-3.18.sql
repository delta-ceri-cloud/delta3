/* 
 * (C) 2016 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Mar 21, 2018
 */

CREATE TABLE `d3_session` (
  `idSession` int(11) NOT NULL,
  `cookie` varchar(100) DEFAULT NULL,
  `userAlias` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `locale` varchar(45) DEFAULT NULL,
  `emailAddress1` varchar(256) DEFAULT NULL,
  `emailAddress2` varchar(256) DEFAULT NULL,
  `misc` varchar(1024) DEFAULT NULL,  
  `permissions` MEDIUMTEXT NULL DEFAULT NULL,
  `userAgent` VARCHAR(256) NULL DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `timeoutTS` timestamp NULL DEFAULT NULL,  
  `idOrganization` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idSUser` int(11) NOT NULL,  
  `idApp` int(11) NOT NULL,    
  PRIMARY KEY (`idSession`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `delta3`.`d3_sequence` (`SEQ_NAME`, `SEQ_COUNT`) VALUES ('SESSSION_ID', '1');
