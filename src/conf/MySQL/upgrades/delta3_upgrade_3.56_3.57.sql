/* 
 * (C) 2018 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Aug 2, 2018
 */

CREATE TABLE `d3_metadata` (
  `idMetadata` int(11) NOT NULL,
  `idModel` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `data` MEDIUMTEXT DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idMetadata`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `delta3`.`d3_sequence` (`SEQ_NAME`, `SEQ_COUNT`) VALUES ('METADATA_ID', '1');


