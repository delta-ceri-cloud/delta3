/* 
 * (C) 2016 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Mar 15, 2017
 */

INSERT INTO `delta3`.`d3_permissiontemplate` (`idPermissionTemplate`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `name`, `description`, `type`, `tag`, `sequence`, `active`, `Module_idModule`) VALUES ('30', '1', '2017-03-15 10:00:00', '1', '2017-03-15 10:00:00', 'Log Level', 'adjust logging cerver logging level', 'button', 'Logging', '1030', '1', '1');
UPDATE `delta3`.`d3_sequence` SET `SEQ_COUNT`='31' WHERE `SEQ_NAME`='PERMTEMPLATE_ID';

ALTER TABLE `delta3`.`d3_process` 
CHANGE COLUMN `input` `input` MEDIUMTEXT NULL DEFAULT NULL AFTER `remoteStatPackage`,
CHANGE COLUMN `manualExecution` `manualExecution` TINYINT(1) NULL DEFAULT NULL AFTER `input`,
ADD COLUMN `stepNumber` INT(11) NULL AFTER `manualExecution`;




