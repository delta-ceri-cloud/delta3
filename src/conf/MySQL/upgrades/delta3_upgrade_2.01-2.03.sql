/* 
 * (C) 2016 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Feb 22, 2017
 */

ALTER TABLE `delta3`.`d3_user_has_role` 
ADD COLUMN `idGroup` INT(11) NULL DEFAULT 0 AFTER `Role_idRole`;
