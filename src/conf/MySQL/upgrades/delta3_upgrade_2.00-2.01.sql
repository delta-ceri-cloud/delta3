/* 
 * (C) 2016 CERI-Lahey
 * 
 */
CREATE SCHEMA `delta3_data` DEFAULT CHARACTER SET utf8 ;

DROP TABLE IF EXISTS `d3_stat`;

CREATE TABLE `delta3`.`d3_stat` (
  `idStat` int(11) NOT NULL,
  `idOrganization` int(11) NOT NULL,
  `shortName` varchar(45) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `configVersion` varchar(45) DEFAULT NULL,  
  `configuration` text DEFAULT NULL,  
  `useProxy` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idStat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `delta3`.`d3_sequence` (`SEQ_NAME`, `SEQ_COUNT`) VALUES ('STAT_ID', '1');

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_eventtemplate`
--

DROP TABLE IF EXISTS `delta3`.`d3_eventtemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delta3`.`d3_eventtemplate` (
  `idEventTemplate` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `actionClass` varchar(200) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `Module_idModule` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEventTemplate`),
  KEY `fk_EventTemplae_Module1_idx` (`Module_idModule`),
  CONSTRAINT `fk_EventTemplae_Module1` FOREIGN KEY (`Module_idModule`) REFERENCES `d3_module` (`idModule`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_eventtemplate`
--

LOCK TABLES `delta3`.`d3_eventtemplate` WRITE;
/*!40000 ALTER TABLE `d3_eventtemplate` DISABLE KEYS */;
INSERT INTO `delta3`.`d3_eventtemplate` VALUES (1,'Job Submit','Request submitted','1',NULL,1,1,'2014-09-15 17:39:41',1,'2014-09-15 17:39:41',6),(2,'Results Ready','Results received','1',NULL,1,1,'2014-09-15 17:52:52',1,'2014-09-15 17:52:52',6),(3,'Stat Method Update','Statistical method configuration was updated','1',NULL,1,1,'2014-12-23 17:00:00',1,'2014-12-23 17:00:00',1),(4,'Data Source Update','Data Source configuration was updated','1',NULL,1,1,'2014-12-23 17:00:00',1,'2014-12-23 17:00:00',1),(5,'Stat Method Created','Statistical method configuration was created','1',NULL,1,1,'2015-04-23 16:00:00',1,'2015-04-23 16:00:00',1),(6,'Data Source Created','Data Source configuration was created','1',NULL,1,1,'2015-04-23 16:00:00',1,'2015-04-23 16:00:00',1),(7,'Study Full Processing Start','Generate Flat Table and process study','1','com.ceri.delta.server.processor.ProcessStudyAll',1,6,'2015-08-11 17:41:35',1,'2015-08-12 18:00:24',6),(8,'Study Processing Start','Process study','1','com.ceri.delta.server.processor.ProcessStudy',1,1,'2015-08-13 17:38:35',1,'2015-08-13 17:38:35',6),(9,'Value Below Zero','Value of one of the results is below zero','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(10,'Value Above Primary CI','Value of one of the results is above primary confidence level','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(11,'Value Above Secondary CI','Value of one of the results is above secondary confidence level','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(12,'Value Below Primary CI','Value of one of the results is below primary confidence level','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(13,'Value Below Secondary CI','Value of one of the results is below secondary confidence level','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(14,'Trending Up','3 values of the results are increasing ','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(15,'Trending Down','3 values of the results are decreasing','2',NULL,1,1,'2015-09-10 16:00:00',1,'2015-09-10 16:00:00',6),(16,'Stat Package Created','Statistical package configuration was created','1','',1,1,'2017-01-30 21:10:43',1,'2017-01-30 21:10:43',1),(17,'Stat Package Updated','Statistical Package configuration was updated','1','',1,1,'2017-01-30 21:11:40',1,'2017-01-30 21:11:40',1);
/*!40000 ALTER TABLE `d3_eventtemplate` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
UPDATE `delta3`.`d3_sequence` SET `SEQ_COUNT`='18' WHERE `SEQ_NAME`='EVENTTEMPLATE_ID';

ALTER TABLE `delta3`.`d3_method` 
ADD COLUMN `idStat` INT(11) NULL DEFAULT NULL AFTER `idOrganization`;

ALTER TABLE `delta3`.`d3_study` 
ADD COLUMN `statPackageVer` VARCHAR(45) NULL DEFAULT NULL AFTER `Alert_idAlert`;

ALTER TABLE `delta3`.`d3_study` 
ADD COLUMN `Stat_idStat` INT(11) NULL DEFAULT NULL AFTER `ModelColumn_idDate`;

--update `delta3`.`d3_study` set Stat_idStat = 4 where d3_study.idOrganization = 7;
----update d3_study set idStat = 4 where d3_study.idOrganization = 7 and Method_idMethod = (
----SELECT Method_idMethod FROM delta3.d3_study where Method_idMethod > 0 and (select active from d3_method where idMethod = Method_idMethod));

update delta3.d3_permissiontemplate set type = 'button' where tag = 'Methods' and idPermissionTemplate = 11;
INSERT INTO `delta3`.`d3_permissiontemplate` (`idPermissionTemplate`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `name`, `description`, `type`, `tag`, `sequence`, `active`, `Module_idModule`) VALUES ('29', '6', '2017-02-06 12:12:00', '6', '2017-02-06 12:12:00', 'Statistics Configurator', 'statistical package configuration', 'menu', 'Stats', '84', '1', '1');
UPDATE `delta3`.`d3_sequence` SET `SEQ_COUNT`='30' WHERE `SEQ_NAME`='PERMTEMPLATE_ID';

/* Following steps have to be done using UI: */
/* add "Statistics Configurator" to appropriate Role */
/* open Statistics Configurator from main menu after relogin with newly created permission */
/* add OCEANS and write down given system ID further called n1, find org ID (m1) and execute query after disabling "safe" mode in "Preferences.."*/

update delta3.d3_study set Stat_idstat = n1 where idOrganization = m1;
update delta3.d3_method set idstat = n1 where idOrganization = m1;

/* verify that OCEANS appears in Study table */
/* add DELTAlytics as second package */
/* initialize both packes from Statistical Configurator and verify version number shown in Study table for each OCEANS study */

ALTER TABLE `delta3`.`d3_event` 
CHANGE COLUMN `description` `description` TEXT NULL DEFAULT NULL ;