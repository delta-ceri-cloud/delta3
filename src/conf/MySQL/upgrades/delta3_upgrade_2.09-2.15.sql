/* 
 * (C) 2016 CERI-Lahey
 * 
 */
/**
 * Author:  mxm29
 * Created: Jun 16, 2017
 */
ALTER TABLE `delta3`.`d3_method` 
ADD COLUMN `sequenceNumber` INT(11) NULL DEFAULT 0 AFTER `description`;
