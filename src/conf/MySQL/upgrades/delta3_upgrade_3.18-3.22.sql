UPDATE `delta3`.`d3_sequence` SET `SEQ_COUNT`='33' WHERE `SEQ_NAME`='PERMTEMPLATE_ID';

INSERT INTO `delta3`.`d3_module` (`idModule`, `createdBy`, `createdTS`, `updatedBy`, `name`, `description`, `type`, `parentId`, `active`, `Organization_idOrganization`) VALUES ('8', '1', '2018-04-23 10:59:00.000', '1', 'Chart Review', 'Ambulatory Chart Review', 'APP', '0', '1', '1');

INSERT INTO `delta3`.`d3_permissiontemplate` (`idPermissionTemplate`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `name`, `description`, `type`, `tag`, `sequence`, `active`, `Module_idModule`) VALUES ('31', '1', '2018-04-23 11:00:00.000', '1', '2018-04-23 11:00:00.000', 'Chart Admin', 'administer chart reviews', 'app', 'ChartAdmin', '1040', '1', '8');
INSERT INTO `delta3`.`d3_permissiontemplate` (`idPermissionTemplate`, `createdBy`, `createdTS`, `updatedBy`, `updatedTS`, `name`, `description`, `type`, `tag`, `sequence`, `active`, `Module_idModule`) VALUES ('32', '1', '2018-04-23 11:00:00.000', '1', '2018-04-23 11:00:00.000', 'Chart Reviewer', 'review ambulatory charts', 'app', 'ChartReviewer', '1042', '1', '8');


