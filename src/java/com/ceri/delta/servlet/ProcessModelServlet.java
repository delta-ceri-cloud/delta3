/*
 * CERI Lahey, 2016
 */

package com.ceri.delta.servlet;

import com.ceri.delta.server.processor.ProcessAtomic;
import com.ceri.delta.security.Auth;
import com.ceri.delta.server.processor.ProcessFlatTable;
import com.ceri.delta.server.processor.ProcessFlatTableNoMissing;
import com.ceri.delta.server.processor.ProcessMissing;
import com.ceri.delta.server.processor.ProcessVirtual0;
import com.ceri.delta.server.processor.ProcessVirtual1;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns="/ProcessModelServlet", asyncSupported=true)

public class ProcessModelServlet extends HttpServlet {
    private Map<String, Future> threadsInPool = new HashMap<String, Future>();
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String requestType = "Unknown";
        try {
            String payload = req.getParameter("payload");
            requestType = req.getParameter("requestType");            
            Integer userId = Auth.getUserIdFromSession(req);
            String decodedContent = URLDecoder.decode(payload, "UTF-8");   
            AsyncContext asyncContext = req.startAsync(req, resp);
            asyncContext.setTimeout(1 * 60 * 1000);   
            asyncContext.addListener(new AsyncListener() {
                public void onComplete(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessModelServlet.class.getName()).log(Level.SEVERE, "ProcessAtomicServlet onComplete");                  
                }

                @Override
                public void onTimeout(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessModelServlet.class.getName()).log(Level.SEVERE, "ProcessAtomicServlet onTimeout");
                }

                @Override
                public void onError(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessModelServlet.class.getName()).log(Level.SEVERE, "ProcessAtomicServlet onError");
                }

                @Override
                public void onStartAsync(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessModelServlet.class.getName()).log(Level.SEVERE, "ProcessAtomicServlet onStartAsync");
                }
            });
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());    
            ThreadPoolExecutor executor = (ThreadPoolExecutor) req.getServletContext().getAttribute("executor");
            Future future = null;
            String responseString = "";
            switch ( requestType ) {
                case "ProcessAtomic":
                    future = executor.submit(new ProcessAtomic(asyncContext, decodedContent, currentTimestamp.toString(), userId));   
                    responseString = "Processing source fields.";
                    break;
                case "ProcessFlatTable":
                    future = executor.submit(new ProcessFlatTable(asyncContext, decodedContent, currentTimestamp.toString(), userId));   
                    responseString = "Processing Flat Table.";   
                    break;
                case "ProcessFlatTableNoMissing":
                    future = executor.submit(new ProcessFlatTableNoMissing(asyncContext, decodedContent, currentTimestamp.toString(), userId));   
                    responseString = "Processing Flat Table, no missing fields processed.";   
                    break;     
                case "ProcessVirtual0":
                    future = executor.submit(new ProcessVirtual0(asyncContext, decodedContent, currentTimestamp.toString(), userId));    
                    responseString = "Processing primary virtual fields.";
                    break;
                case "ProcessVirtual1":
                    future = executor.submit(new ProcessVirtual1(asyncContext, decodedContent, currentTimestamp.toString(), userId));    
                    responseString = "Processing secondary virtual fields.";
                    break;      
                case "ProcessMissing":
                    future = executor.submit(new ProcessMissing(asyncContext, decodedContent, currentTimestamp.toString(), userId));    
                    responseString = "Processing missing fields.";
                    break;                       
            }

            threadsInPool.put(currentTimestamp.toString(),future); // entries in this map stay forever causing small memory leak
            response(resp, responseString);
         
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ProcessModelServlet.class.getName()).log(Level.SEVERE, "Exception in POST method of " + requestType, ex);
        }
    }
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            String payload = req.getParameter("payload");
            Integer userId = Auth.getUserIdFromSession(req);
            if ( userId > 0 ) {
                String decodedContent = URLDecoder.decode(payload, "UTF-8");   
                Future future = threadsInPool.get(decodedContent);
                if ( future == null ) {
                    Logger.getLogger(ProcessModelServlet.class.getName()).log(Level.SEVERE, 
                            "Processing not cancelled by ProcessModelServlet. Task not found: " + decodedContent);   
                    response(resp, "Request ignored. Task not found: " + decodedContent);
                }
                future.cancel(true);
                threadsInPool.remove(decodedContent);
                Logger.getLogger(ProcessModelServlet.class.getName()).log(Level.SEVERE, "Processing of model cancelled by ProcessModelServlet");
                response(resp, "Processing of model cancelled.");
            } else {
                response(resp, "Requester not authenticated. Request ignored.");
            }
         
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ProcessModelServlet.class.getName()).log(Level.SEVERE, "Exception in GET method of ProcessModelServlet  ", ex);
        }
    }
    
    private void response(HttpServletResponse resp, String msg) throws IOException {
            PrintWriter out = resp.getWriter();
            out.println(msg);
    }    
}
