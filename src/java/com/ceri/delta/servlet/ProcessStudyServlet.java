/*
 * CERI Lahey, 2018
 */

package com.ceri.delta.servlet;

import com.ceri.delta.server.processor.ProcessStudyBatch;
import com.ceri.delta.security.Auth;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns="/ProcessStudyServlet", asyncSupported=true)

public class ProcessStudyServlet extends HttpServlet {
    private Map<String, Future> threadsInPool = new HashMap<String, Future>();
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String requestType = "Unknown";
        try {
            String payload = req.getParameter("payload");
            requestType = req.getParameter("requestType");            
            Integer userId = Auth.getUserIdFromSession(req);
            String decodedContent = URLDecoder.decode(payload, "UTF-8");   
            AsyncContext asyncContext = req.startAsync(req, resp);
            
            
            asyncContext.setTimeout(1 * 60 * 1000);   
            asyncContext.addListener(new AsyncListener() {
                public void onComplete(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessStudyServlet.class.getName()).log(Level.SEVERE, "ProcessStudyServlet onComplete");                  
                }

                @Override
                public void onTimeout(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessStudyServlet.class.getName()).log(Level.SEVERE, "ProcessStudyServlet onTimeout");
                }

                @Override
                public void onError(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessStudyServlet.class.getName()).log(Level.SEVERE, "ProcessStudyServlet onError");
                }

                @Override
                public void onStartAsync(AsyncEvent event) throws IOException {
                    Logger.getLogger(ProcessStudyServlet.class.getName()).log(Level.SEVERE, "ProcessStudyServlet onStartAsync");
                }
            });
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());    
            ThreadPoolExecutor executor = (ThreadPoolExecutor) req.getServletContext().getAttribute("executor");
            Future future = null;
            String responseString = "";
            switch ( requestType ) {
                case "ProcessStudyBatch":
                    future = executor.submit(new ProcessStudyBatch(asyncContext, decodedContent, currentTimestamp.toString(), userId));   
                    responseString = "Processing Study batch.";
                    break;                      
            }

            threadsInPool.put(currentTimestamp.toString(),future); // entries in this map stay forever causing small memory leak
            response(resp, responseString);
         
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ProcessStudyServlet.class.getName()).log(Level.SEVERE, "Exception in POST method of " + requestType, ex);
        }
    }
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            String payload = req.getParameter("payload");
            Integer userId = Auth.getUserIdFromSession(req);
            if ( userId > 0 ) {
                String decodedContent = URLDecoder.decode(payload, "UTF-8");   
                Future future = threadsInPool.get(decodedContent);
                if ( future == null ) {
                    Logger.getLogger(ProcessStudyServlet.class.getName()).log(Level.SEVERE, 
                            "Processing not cancelled by ProcessStudyServlet. Task not found: " + decodedContent);   
                    response(resp, "Request ignored. Task not found: " + decodedContent);
                }
                future.cancel(true);
                threadsInPool.remove(decodedContent);
                Logger.getLogger(ProcessStudyServlet.class.getName()).log(Level.SEVERE, "Processing of study cancelled by ProcessStudyServlet");
                response(resp, "Processing of study batch cancelled.");
            } else {
                response(resp, "Requester not authenticated. Request ignored.");
            }
         
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ProcessStudyServlet.class.getName()).log(Level.SEVERE, "Exception in GET method of ProcessStudyServlet  ", ex);
        }
    }
    
    private void response(HttpServletResponse resp, String msg) throws IOException {
            PrintWriter out = resp.getWriter();
            out.println(msg);
    }    
}
