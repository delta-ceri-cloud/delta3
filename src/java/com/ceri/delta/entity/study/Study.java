/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.entity.study;

import com.ceri.delta.entity.GroupEntity;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_study")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Study.findByName", query = "SELECT s FROM Study s WHERE s.name = :name")    
    /*@NamedQuery(name = "Study.findAll", query = "SELECT s FROM Study s"),
    @NamedQuery(name = "Study.findByIdStudy", query = "SELECT s FROM Study s WHERE s.idStudy = :idStudy"),
    @NamedQuery(name = "Study.findByIdModel", query = "SELECT s FROM Study s WHERE s.idModel = :idModel"),
    @NamedQuery(name = "Study.findByModelColumnidOutcome", query = "SELECT s FROM Study s WHERE s.idOutcome = :idOutcome"),
    @NamedQuery(name = "Study.findByModelColumnidTreatment", query = "SELECT s FROM Study s WHERE s.idTreatment = :idTreatment"),
    @NamedQuery(name = "Study.findByMethodidMethod", query = "SELECT s FROM Study s WHERE s.idMethod = :idMethod"),
    @NamedQuery(name = "Study.findByMethodParams", query = "SELECT s FROM Study s WHERE s.methodParams = :methodParams"),
    @NamedQuery(name = "Study.findByIsMissingDataUsed", query = "SELECT s FROM Study s WHERE s.isMissingDataUsed = :isMissingDataUsed"),
    @NamedQuery(name = "Study.findByAlertidAlert", query = "SELECT s FROM Study s WHERE s.idAlert = :idAlert"),
    @NamedQuery(name = "Study.findByActive", query = "SELECT s FROM Study s WHERE s.active = :active"),
    @NamedQuery(name = "Study.findByConfidenceInterval", query = "SELECT s FROM Study s WHERE s.confidenceInterval = :confidenceInterval"),
    @NamedQuery(name = "Study.findByCreatedBy", query = "SELECT s FROM Study s WHERE s.createdBy = :createdBy"),
    @NamedQuery(name = "Study.findByCreatedTS", query = "SELECT s FROM Study s WHERE s.createdTS = :createdTS"),
    @NamedQuery(name = "Study.findByUpdatedBy", query = "SELECT s FROM Study s WHERE s.updatedBy = :updatedBy"),
    @NamedQuery(name = "Study.findByUpdatedTS", query = "SELECT s FROM Study s WHERE s.updatedTS = :updatedTS")*/})
public class Study implements Serializable, GroupEntity {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="STUDY_ID")
    @TableGenerator(name="STUDY_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="STUDY_ID", allocationSize=1)     
    private Integer idStudy;
    @Basic(optional = false)
    @Column(name = "idOrganization")
    private int idOrganization;    
    @Column(name = "idGroup")
    private int idGroup;    
    @Column(name = "idModel")
    private int idModel;
    @Column(name = "ModelColumn_idOutcome")
    private Integer idOutcome;
    @Column(name = "ModelColumn_idTreatment")
    private Integer idTreatment;
    @Column(name = "ModelColumn_idKey")
    private Integer idKey;      
    @Column(name = "ModelColumn_idDate")
    private Integer idDate;    
    @Column(name = "Stat_idStat")
    private Integer idStat;    
    @Column(name = "Method_idMethod")
    private Integer idMethod;
    @Column(name = "Filter_idFilter")
    private Integer idFilter;    
    private String name;
    private String description;
    private String status;    
    private String methodParams;
    private String chunkingBy;
    private Boolean isMissingDataUsed;
    private Boolean isChunking;    
    private Boolean isGenericId;     
    private Boolean dumpData;
    private Boolean sendAllColumns;   
    private Boolean outcomePositive;
    @Column(name = "localStatPackage")    
    private Boolean localStatPackage;   
    @Column(name = "remoteStatPackage")   
    private Boolean remoteStatPackage;    
    @Column(name = "Alert_idAlert")
    private Integer idAlert;
    private String statPackageVer;    
    @Column(name = "originalStudyId")
    private Integer originalStudyId; 
    @Column(name = "LastProcess_idProcess")
    private Integer idLastProcess;      
    private Timestamp startTS;
    private Timestamp endTS;    
    private Boolean active;
    private Double confidenceInterval;
    private Double confidenceInterval2;    
    private Boolean overwriteResults; 
    private Boolean autoExecute;
    private Timestamp submittedTS;        
    private Timestamp processedTS;    
    private Integer createdBy;
    private Timestamp createdTS;
    private Integer updatedBy;
    private Timestamp updatedTS;

    public Study() {
    }

    public Study(Integer idStudy) {
        this.idStudy = idStudy;
    }

    public Study(Integer idStudy, int idModel, int idOrganization) {
        this.idStudy = idStudy;
        this.idOrganization = idOrganization;
        this.idModel = idModel;
    }

    public Integer getIdStudy() {
        return idStudy;
    }

    public void setIdStudy(Integer idStudy) {
        this.idStudy = idStudy;
    }  

    public int getIdOrganization() {
        return idOrganization;
    }

    public void setIdOrganization(int idOrganization) {
        this.idOrganization = idOrganization;
    }    

    public int getIdModel() {
        return idModel;
    }

    public void setIdModel(int idModel) {
        this.idModel = idModel;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }
    
    public Integer getIdOutcome() {
        return idOutcome;
    }

    public void setIdOutcome(Integer idOutcome) {
        this.idOutcome = idOutcome;
    }

    public Integer getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(Integer idTreatment) {
        this.idTreatment = idTreatment;
    }

    public Integer getIdKey() {
        return idKey;
    }

    public void setIdKey(Integer idDate) {
        this.idDate = idKey;
    }    
    
    public Integer getIdDate() {
        return idDate;
    }

    public void setIdDate(Integer idDate) {
        this.idDate = idDate;
    }

    public Integer getIdStat() {
        return idStat;
    }

    public void setIdStat(Integer idStat) {
        this.idStat = idStat;
    }
    
    public Integer getIdMethod() {
        return idMethod;
    }

    public void setIdMethod(Integer idMethod) {
        this.idMethod = idMethod;
    }
    
    public Integer getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(Integer idFilter) {
        this.idFilter = idFilter;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }    

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }    
    
    public String getMethodParams() {
        return methodParams;
    }

    public void setMethodParams(String methodParams) {
        this.methodParams = methodParams;
    }
    
    public String getChunkingBy() {
        return chunkingBy;
    }

    public void setChunkingBy(String chunkingBy) {
        this.chunkingBy = chunkingBy;
    }

    public Boolean getDumpData() {
        return dumpData;
    }

    public void setDumpData(Boolean d) {
        this.dumpData = d;
    }
 
    public Boolean getSendAllColumns() {
        return sendAllColumns;
    }

    public void setSendAllColumns(Boolean sac) {
        this.sendAllColumns = sac;
    }
    
 
    public Boolean getOutcomePositive() {
        return outcomePositive;
    }

    public void setOutcomePositive(Boolean op) {
        this.outcomePositive = op;
    }
    
    public Boolean getIsMissingDataUsed() {
        return isMissingDataUsed;
    }

    public void setIsMissingDataUsed(Boolean isMissingDataUsed) {
        this.isMissingDataUsed = isMissingDataUsed;
    }

    public Boolean getIsChunking() {
        return isChunking;
    }

    public void setIsChunking(Boolean isChunking) {
        this.isChunking = isChunking;
    }

    public Boolean getLocalStatPackage() {
        return localStatPackage;
    }

    public void setLocalStatPackage(Boolean l) {
        this.localStatPackage = l;
    }
 
    public Boolean getRemoteStatPackage() {
        return remoteStatPackage;
    }

    public void setRemoteStatPackage(Boolean r) {
        this.remoteStatPackage = r;
    }
    
    public Boolean getIsGenericId() {
        return isGenericId;
    }

    public void setIsGenericId(Boolean isGenericId) {
        this.isGenericId = isGenericId;
    }
    
    public Integer getIdAlert() {
        return idAlert;
    }

    public void setIdAlert(Integer idAlert) {
        this.idAlert = idAlert;
    }
    
    public String getStatPackageVer() {
        return statPackageVer;
    }

    public void setStatPackageVer(String statPackageVer) {
        this.statPackageVer = statPackageVer;
    }     

    public Integer getOriginalStudyId() {
        return originalStudyId;
    }

    public void setOriginalStudyId(Integer originalStudyId) {
        this.originalStudyId = originalStudyId;
    }

    public Timestamp getSubmittedTS() {
        return submittedTS;
    }

    public void setSubmittedTS(Timestamp submittedTS) {
        this.submittedTS = submittedTS;
    }
    
    public Timestamp getProcessedTS() {
        return processedTS;
    }

    public void setProcessedTS(Timestamp processedTS) {
        this.processedTS = processedTS;
    }
    
    public Timestamp getStartTS() {
        return startTS;
    }

    public void setStartTS(Timestamp startTS) {
        this.startTS = startTS;
    }

    public Timestamp getEndTS() {
        return endTS;
    }

    public void setEndTS(Timestamp endTS) {
        this.endTS = endTS;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getAutoExecute() {
        return autoExecute;
    }

    public void setAutoExecute(Boolean autoExecute) {
        this.autoExecute = autoExecute;
    }
    
    public Double getConfidenceInterval() {
        return confidenceInterval;
    }

    public void setConfidenceInterval(Double confidenceInterval) {
        this.confidenceInterval = confidenceInterval;
    }

    public Double getConfidenceInterval2() {
        return confidenceInterval2;
    }

    public void setConfidenceInterval2(Double confidenceInterval) {
        this.confidenceInterval2 = confidenceInterval;
    }


    public Boolean getOverwriteResults() {
        return overwriteResults;
    }

    public void setOverwriteResults(Boolean overwriteResults) {
        this.overwriteResults = overwriteResults;
    }
    
    public Integer getIdLastProcess() {
        return idLastProcess;
    }

    public void setIdLastProcess(Integer idLastProcess) {
        this.idLastProcess = idLastProcess;
    }    
    
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStudy != null ? idStudy.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Study)) {
            return false;
        }
        Study other = (Study) object;
        if ((this.idStudy == null && other.idStudy != null) || (this.idStudy != null && !this.idStudy.equals(other.idStudy))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ceri.delta.entity.study.Study[ idStudy=" + idStudy + " ]";
    }

    @Override
    public Integer getIdEntity() {
        return this.idStudy;
    }

    @Override
    public void setIdEntity(Integer i) {
        this.idStudy = i;
    }
    
}
