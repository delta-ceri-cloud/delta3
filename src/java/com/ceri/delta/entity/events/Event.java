/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ceri.delta.entity.events;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Boston Advanced Analytics
 */
@Entity
@Table(name = "d3_event")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Event.findByUpdatedNull", query = "SELECT e FROM Event e WHERE e.updatedBy is null"),
    @NamedQuery(name = "Event.findByUpdatedAndExecuteNull", query = "SELECT e FROM Event e WHERE e.updatedBy is null and e.executeBy is null"),
    @NamedQuery(name = "Event.findByExecute", query = "SELECT e FROM Event e WHERE e.executeBy is not null"),
    @NamedQuery(name = "Event.findByExecuteAndUpdatedNull", query = "SELECT e FROM Event e WHERE e.executeBy is not null and e.updatedBy is null")    
})
public class Event implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="EVENT_ID")
    @TableGenerator(name="EVENT_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="EVENT_ID", allocationSize=1)    
    @Column(name = "idEvent")    
    private Integer idEvent;    
    @Column(name = "description")
    private String description;
    @Column(name = "objectId")    
    private Integer objectId;        
    @Column(name = "objectType")    
    private String objectType;            
    @Column(name = "executeBy")
    private Integer executeBy;
    @Column(name = "executeTS")
    private Timestamp executeTS;     
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "createdTS")
    private Timestamp createdTS;    
    @Column(name = "updatedBy")
    private Integer updatedBy;
    @Column(name = "updatedTS")
    private Timestamp updatedTS;  
    @Column(name = "Organization_idOrganization")
    private Integer idOrganization;
    @Column(name = "EventTemplate_idEventTemplate")
    private Integer idEventTemplate;        
    @Column(name = "Task_idTask")
    private Integer idTask;   
    public Event() {
    }

    public Event(Integer idEvent) {
        this.idEvent = idEvent;
    }

    public Integer getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(Integer idEvent) {
        this.idEvent = idEvent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String data) {
        this.description = data;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }
    
    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public Integer getExecuteBy() {
        return executeBy;
    }

    public void setExecuteBy(Integer executeBy) {
        this.executeBy = executeBy;
    }

    public Date getExecuteTS() {
        return executeTS;
    }

    public void setExecuteTS(Timestamp executeTS) {
        this.executeTS = executeTS;
    }
    
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    public Integer getIdOrganization() {
        return idOrganization;
    }

    public void setIdOrganization(Integer idOrganization) {
        this.idOrganization = idOrganization;
    }

    public Integer getIdEventTemplate() {
        return idEventTemplate;
    }

    public void setIdEventTemplate(Integer idEventTemplate) {
        this.idEventTemplate = idEventTemplate;
    }
 
    public Integer getIdTask() {
        return idTask;
    }

    public void setIdTask(Integer idTask) {
        this.idTask = idTask;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEvent != null ? idEvent.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Event)) {
            return false;
        }
        Event other = (Event) object;
        if ((this.idEvent == null && other.idEvent != null) || (this.idEvent != null && !this.idEvent.equals(other.idEvent))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ceri.delta.entity.events.Event[ idEvent=" + idEvent + " ]";
    }
    
}
