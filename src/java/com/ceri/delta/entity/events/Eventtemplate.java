package com.ceri.delta.entity.events;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "d3_eventtemplate")
@XmlRootElement
public class Eventtemplate implements Serializable {
  public static int SUBMIT_EVENT = 1;
  
  public static int RESULT_EVENT = 2;
  
  public static int CONFIG_METHOD_UPDATE_EVENT = 3;
  
  public static int CONFIG_DB_UPDATE_EVENT = 4;
  
  public static int CONFIG_METHOD_CREATE_EVENT = 5;
  
  public static int CONFIG_DB_CREATE_EVENT = 6;
  
  public static int START_FULL_STUDY_EVENT = 7;
  
  public static int START_STUDY_EVENT = 8;
  
  public static int RESULT_BELOW_ZERO = 9;
  
  public static int RESULT_ABOVE_PRIMARY_CL = 10;
  
  public static int RESULT_ABOVE_SECONDARY_CL = 11;
  
  public static int RESULT_BELOW_PRIMARY_CL = 12;
  
  public static int RESULT_BELOW_SECONDARY_CL = 13;
  
  public static int RESULT_TRENDING_UP = 14;
  
  public static int RESULT_TRENDING_DOWN = 15;
  
  public static int CONFIG_STAT_CREATE_EVENT = 16;
  
  public static int CONFIG_STAT_UPDATE_EVENT = 17;
  
  public static int START_FT_EVENT = 18;
  
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "EVENTTEMPLATE_ID")
  @TableGenerator(name = "EVENTTEMPLATE_ID", table = "d3_sequence", pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT", pkColumnValue = "EVENTTEMPLATE_ID", allocationSize = 1)
  @Column(name = "idEventTemplate")
  private Integer idEventTemplate;
  
  @Column(name = "name")
  private String name;
  
  @Column(name = "description")
  private String description;
  
  @Column(name = "actionClass")
  private String actionClass;
  
  @Column(name = "priority")
  private String priority;
  
  @Column(name = "active")
  private Boolean active;
  
  @Column(name = "createdBy")
  private Integer createdBy;
  
  @Column(name = "createdTS")
  private Timestamp createdTS;
  
  @Column(name = "updatedBy")
  private Integer updatedBy;
  
  @Column(name = "updatedTS")
  private Timestamp updatedTS;
  
  @Column(name = "Module_idModule")
  private Integer idModule;
  
  public Eventtemplate() {}
  
  public Eventtemplate(Integer idEventTemplate) {
    this.idEventTemplate = idEventTemplate;
  }
  
  public Integer getIdEventTemplate() {
    return this.idEventTemplate;
  }
  
  public void setIdEventTemplate(Integer idEventTemplate) {
    this.idEventTemplate = idEventTemplate;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getDescription() {
    return this.description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public String getActionClass() {
    return this.actionClass;
  }
  
  public void setActionClass(String action) {
    this.actionClass = action;
  }
  
  public String getPriority() {
    return this.priority;
  }
  
  public void setPriority(String priority) {
    this.priority = priority;
  }
  
  public Boolean getActive() {
    return this.active;
  }
  
  public void setActive(Boolean active) {
    this.active = active;
  }
  
  public Integer getCreatedBy() {
    return this.createdBy;
  }
  
  public void setCreatedBy(Integer createdBy) {
    this.createdBy = createdBy;
  }
  
  public Date getCreatedTS() {
    return this.createdTS;
  }
  
  public void setCreatedTS(Timestamp createdTS) {
    this.createdTS = createdTS;
  }
  
  public Integer getUpdatedBy() {
    return this.updatedBy;
  }
  
  public void setUpdatedBy(Integer updatedBy) {
    this.updatedBy = updatedBy;
  }
  
  public Date getUpdatedTS() {
    return this.updatedTS;
  }
  
  public void setUpdatedTS(Timestamp updatedTS) {
    this.updatedTS = updatedTS;
  }
  
  public Integer getIdModule() {
    return this.idModule;
  }
  
  public void setIdModule(Integer idModule) {
    this.idModule = idModule;
  }
  
  public int hashCode() {
    int hash = 0;
    hash += (this.idEventTemplate != null) ? this.idEventTemplate.hashCode() : 0;
    return hash;
  }
  
  public boolean equals(Object object) {
    if (!(object instanceof Eventtemplate))
      return false; 
    Eventtemplate other = (Eventtemplate)object;
    if ((this.idEventTemplate == null && other.idEventTemplate != null) || (this.idEventTemplate != null && !this.idEventTemplate.equals(other.idEventTemplate)))
      return false; 
    return true;
  }
  
  public String toString() {
    return "com.ceri.delta.entity.events.Eventtemplate[ idEventTemplate=" + this.idEventTemplate + " ]";
  }
}
