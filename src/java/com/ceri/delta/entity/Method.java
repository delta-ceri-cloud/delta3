/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Coping Systems Inc.
 */
@Entity
@Table(name = "d3_method")
@XmlRootElement
/*@NamedQueries({
    @NamedQuery(name = "Method.findAll", query = "SELECT m FROM Method m"),
    @NamedQuery(name = "Method.findByIdMethod", query = "SELECT m FROM Method m WHERE m.idMethod = :idMethod"),
    @NamedQuery(name = "Method.findByIdOrganization", query = "SELECT m FROM Method m WHERE m.idOrganization = :idOrganization"),
    @NamedQuery(name = "Method.findByName", query = "SELECT m FROM Method m WHERE m.name = :name"),
    @NamedQuery(name = "Method.findByDescription", query = "SELECT m FROM Method m WHERE m.description = :description"),
    @NamedQuery(name = "Method.findByMethodParams", query = "SELECT m FROM Method m WHERE m.methodParams = :methodParams"),
    @NamedQuery(name = "Method.findByActive", query = "SELECT m FROM Method m WHERE m.active = :active"),
    @NamedQuery(name = "Method.findByCreatedBy", query = "SELECT m FROM Method m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "Method.findByCreatedTS", query = "SELECT m FROM Method m WHERE m.createdTS = :createdTS"),
    @NamedQuery(name = "Method.findByUpdatedBy", query = "SELECT m FROM Method m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "Method.findByUpdatedTS", query = "SELECT m FROM Method m WHERE m.updatedTS = :updatedTS")})*/
public class Method implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.TABLE, generator="METHOD_ID")
    @TableGenerator(name="METHOD_ID", table="d3_sequence", pkColumnName="SEQ_NAME",
        valueColumnName="SEQ_COUNT", pkColumnValue="METHOD_ID", allocationSize=1)     
    private Integer idMethod;
    @Basic(optional = false)
    private int idOrganization;
    private int idStat;
    private String name;
    private String shortName;
    private String description;
    private int sequenceNumber;
    private String methodParams;
    private Boolean active;
    private Integer createdBy;
    private Timestamp createdTS;
    private Integer updatedBy;
    private Timestamp updatedTS;

    public Method() {
    }

    public Method(Integer idMethod) {
        this.idMethod = idMethod;
    }

    public Method(Integer idMethod, int idOrganization) {
        this.idMethod = idMethod;
        this.idOrganization = idOrganization;
    }

    public Integer getIdMethod() {
        return idMethod;
    }

    public void setIdMethod(Integer idMethod) {
        this.idMethod = idMethod;
    }

    public int getIdOrganization() {
        return idOrganization;
    }

    public void setIdOrganization(int idOrganization) {
        this.idOrganization = idOrganization;
    }

    public int getIdStat() {
        return idStat;
    }

    public void setIdStat(int idStat) {
        this.idStat = idStat;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer seqNumber) {
        this.sequenceNumber = seqNumber;
    }
    
    public String getMethodParams() {
        return methodParams;
    }

    public void setMethodParams(String methodParams) {
        this.methodParams = methodParams;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedTS() {
        return createdTS;
    }

    public void setCreatedTS(Timestamp createdTS) {
        this.createdTS = createdTS;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTS() {
        return updatedTS;
    }

    public void setUpdatedTS(Timestamp updatedTS) {
        this.updatedTS = updatedTS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMethod != null ? idMethod.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Method)) {
            return false;
        }
        Method other = (Method) object;
        if ((this.idMethod == null && other.idMethod != null) || (this.idMethod != null && !this.idMethod.equals(other.idMethod))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ceri.delta.entity.study.Method[ idMethod=" + idMethod + " ]";
    }
    
}
