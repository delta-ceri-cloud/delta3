/*
 * (c) 2018 CERI-Lahey
 */
package com.ceri.delta.server;

import com.ceri.delta.jpa.model.ModelColumnJpaController;
import com.ceri.delta.jpa.process.ProcessJpaController;
import com.ceri.delta.jpa.study.StudyJpaController;
import com.ceri.delta.util.JSON.JSONArray;
import com.ceri.delta.util.JSON.JSONObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImportServiceImpl {

    private static final Logger logger = Logger.getLogger(ImportServiceImpl.class.getName());
    private static StudyJpaController studyController;
    private static ModelColumnJpaController modelColumnController;
    private static ProcessJpaController processController;
    private static AdminServiceImpl adminService;
    private static StudyServiceImpl studyService;
    private static ModelServiceImpl modelService;    

    public ImportServiceImpl() {
        if (studyController == null) {
            studyController = new StudyJpaController();
        }
        if (modelColumnController == null) {
            modelColumnController = new ModelColumnJpaController();
        }
        if (processController == null) {
            processController = new ProcessJpaController();
        }
    }

    public String importObjects(String JSONString, Integer currentOrgId, Integer currentUserId) {
        String result = "Objects(s) not imported";
        String dataString = "";  
        String objectType = "object";
        JSONObject impObject;
        
        try {
            JSONArray objectArray =  new JSONArray(JSONString);
            for (int i=0; i<objectArray.length(); i++) {
                impObject =  (JSONObject) objectArray.get(0);
                objectType = impObject.getString("objectType");     
                //String objectVersion = impObject.getString("version");                    
                logger.log(Level.INFO, "Importing object type " + objectType);
                switch ( objectType ) {
                    case "Model":
                        if ( modelService == null ) {
                            modelService = new ModelServiceImpl();
                        } 
                        dataString = modelService.importModelWithOverwrite(impObject, currentOrgId, currentUserId);
                        break;
                    case "Study":
                        if ( studyService == null ) {
                            studyService = new StudyServiceImpl();
                        } 
                        dataString = studyService.importStudyWithOverwrite(impObject, currentOrgId, currentUserId);
                        break;                        
                    }
                }
            dataString = "[" + dataString + "]";
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while importing new Object: {0} {1}", new Object[]{"Object", ex});
            result = "DA0108 Exception while importing "  + objectType + ": " + ex.getMessage();
        }   
        return wrapAPI(objectType.toLowerCase()+"s", dataString, objectType + " imported", result);
    }    
    
    /* delegete this work to common method in admin service */
    public String wrapAPI(String objectId, String result, String msgSuccess, String msgFailure) {
        if (adminService == null) {
            adminService = new AdminServiceImpl();
        }        
        return adminService.wrapAPI(objectId, result, msgSuccess, msgFailure);
    }
    
      
}
