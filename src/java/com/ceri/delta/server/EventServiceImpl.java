package com.ceri.delta.server;

import com.ceri.delta.entity.User;
import com.ceri.delta.entity.events.Alert;
import com.ceri.delta.entity.events.Event;
import com.ceri.delta.entity.events.Eventtemplate;
import com.ceri.delta.entity.events.Notification;
import com.ceri.delta.entity.events.Task;
import com.ceri.delta.entity.model.Model;
import com.ceri.delta.entity.study.Study;
import com.ceri.delta.entity.process.Process;
import com.ceri.delta.jpa.UserJpaController;
import com.ceri.delta.jpa.event.AlertJpaController;
import com.ceri.delta.jpa.event.EventJpaController;
import com.ceri.delta.jpa.event.EventtemplateJpaController;
import com.ceri.delta.jpa.event.NotificationJpaController;
import com.ceri.delta.jpa.event.TaskJpaController;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.model.ModelJpaController;
import com.ceri.delta.jpa.process.ProcessJpaController;
import com.ceri.delta.jpa.study.StudyJpaController;
import com.ceri.delta.security.AuditLogger;
import com.ceri.delta.util.JSON.JSONArray;
import com.ceri.delta.util.JSON.JSONException;
import com.ceri.delta.util.JSON.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EventServiceImpl {
  private static final Logger logger = Logger.getLogger(EventServiceImpl.class.getName());
  
  private static EventJpaController eventController;
  
  private static EventtemplateJpaController eventtemplateController;
  
  private static AlertJpaController alertController;
  
  private static NotificationJpaController notificationController;
  
  private static UserJpaController userController;
  
  private static TaskJpaController taskController;
  
  private static AdminServiceImpl adminService;
  
  private static StudyJpaController studyController;
  
  private static ModelJpaController modelController;
   
  private static ProcessJpaController processController;
  
  
   
  
  public Integer logEvent(Integer currentOrgId, Integer currentUserId, Integer idEventtemplate, String data) {
    Integer returnInt = Integer.valueOf(0);
    if (eventtemplateController == null)
      eventtemplateController = new EventtemplateJpaController(); 
    if (eventController == null)
      eventController = new EventJpaController(); 
    Eventtemplate et = eventtemplateController.findEventtemplate(idEventtemplate);
    if (et.getActive().booleanValue() == true) {
      Event event = new Event();
      event.setIdEvent(Integer.valueOf(0));
      event.setIdEventTemplate(idEventtemplate);
      event.setDescription(data);
      try {
        eventController.createObjects(event, currentOrgId, currentUserId);
      } catch (Exception ex) {
        logger.log(Level.SEVERE, "EA0001 Exception while storing event: ", ex);
      } 
    } 
    return returnInt;
  }
  
  public Integer logEvent(Integer currentOrgId, Integer currentUserId, Integer idEventtemplate, String data, Integer objectId, String objectType) {
    Integer returnInt = Integer.valueOf(0);
    if (eventtemplateController == null)
      eventtemplateController = new EventtemplateJpaController(); 
    if (eventController == null)
      eventController = new EventJpaController(); 
    Eventtemplate et = eventtemplateController.findEventtemplate(idEventtemplate);
    if (et.getActive().booleanValue() == true) {
      Event event = new Event();
      event.setIdEvent(Integer.valueOf(0));
      event.setIdEventTemplate(idEventtemplate);
      event.setDescription(data);
      event.setObjectId(objectId);
      event.setObjectType(objectType);
      try {
        eventController.createObjects(event, currentOrgId, currentUserId);
      } catch (Exception ex) {
        logger.log(Level.SEVERE, "EA0001 Exception while storing event: ", ex);
      } 
    } 
    return returnInt;
  }
  public Integer scheduleEvent(Integer currentOrgId, Integer currentUserId, Integer idEventtemplate, String data) {
    Integer returnInt = Integer.valueOf(0);
    if (eventtemplateController == null)
      eventtemplateController = new EventtemplateJpaController(); 
    if (eventController == null)
      eventController = new EventJpaController(); 
    Eventtemplate et = eventtemplateController.findEventtemplate(idEventtemplate);
    if (et.getActive().booleanValue() == true) {
      Event event = new Event();
      event.setIdEvent(Integer.valueOf(0));
      event.setIdEventTemplate(idEventtemplate);
      event.setDescription(data);
      try {
        eventController.createObjects(event, currentOrgId, currentUserId);
      } catch (Exception ex) {
        logger.log(Level.SEVERE, "EA0001 Exception while storing event: ", ex);
      } 
    } 
    return returnInt;
  }
  
  public void processEvents() {
    if (eventController == null)
      eventController = new EventJpaController(); 
    if (eventtemplateController == null)
      eventtemplateController = new EventtemplateJpaController(); 
    if (alertController == null)
      alertController = new AlertJpaController(); 
    if (notificationController == null)
      notificationController = new NotificationJpaController(); 
    if (userController == null)
      userController = new UserJpaController();     
     if (studyController == null)
      studyController = new StudyJpaController(); 

    List<Object> eventtemplates = eventtemplateController.findObjectEntities();
    List<Object> events = eventController.findNewEventEntities(Boolean.TRUE.booleanValue(), 0, 0);
    for (Object e : events) {
      Event updatedEvent = (Event)e;
      if (checkEventActive((Event)e, eventtemplates) == true) {
        List<Object> users = userController.findOrgUserEntities(((Event)e).getIdOrganization());
        for (Object user : users) {
          List<Object> alerts = alertController.findUserAlertEntities(Integer.valueOf(((User)user).getIdUser()));
          for (Object al : alerts) {
            if (Objects.equals(((Alert)al).getIdEventTemplate(), ((Event)e).getIdEventTemplate()) && (
              (Alert)al).getActive().booleanValue() == true) {
  
                String objectType = ((Event)e).getObjectType();
                Integer search_id = ((Event)e).getObjectId();

                
                
              Notification n = new Notification();
              n.setAckRequired(((Alert)al).getAckRequired());
              n.setIdUser(((Alert)al).getIdUser());
              n.setTimeOut(((Alert)al).getTimeOut());
              n.setName(((Alert)al).getName());
              n.setDescription(((Alert)al).getDescription());
              n.setAcknowledged(Boolean.valueOf(false));
              n.setRetryCount(Integer.valueOf(0));
              n.setEventData(((Event)e).getDescription());
              n.setEventObjectId(((Event)e).getObjectId());
              n.setEventObjectType(((Event)e).getObjectType());             
              n.setIdEvent(((Event)e).getIdEvent());
              n.setActive(Boolean.TRUE);
              Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
              n.setCreatedTS(currentTimestamp);
              n.setCreatedBy(Integer.valueOf(0));
              
              if("studies. Submission remote: DELTAlytics".equals(objectType)){
                    // System.out.println("comparing string for studies");
                     Study studydetail = (Study) studyController.findStudy(search_id);
                     studydetail.getIdGroup();
                     n.setIdGroup(studydetail.getIdGroup());
                    //System.out.println("idproject of study"+ studydetail.getIdGroup());
                    
                }
              
              if("models".equals(objectType)){
                    //System.out.println("comparing string for models");
                     Model modeldetail = (Model) modelController.findModel(search_id);
                     modeldetail.getIdGroup();
                    //System.out.println("idproject of models"+ modeldetail.getIdGroup());
                     n.setIdGroup(modeldetail.getIdGroup());
                }
              
              if("results".equals(objectType)){
                    //System.out.println("comparing string for results");
                     Process processdetail = (Process) processController.findProcess(search_id);
                     processdetail.getIdGroup();
                    //System.out.println("idproject of result :"+ processdetail.getIdGroup());
                     n.setIdGroup(processdetail.getIdGroup());
                }
              
              
              try {
                notificationController.create(n);
              } catch (Exception ex) {
                Logger.getLogger(EventServiceImpl.class.getName()).log(Level.SEVERE, "Exception while creating notification for event " + 
                    
                    Integer.toString(updatedEvent.getIdEvent().intValue()), ex);
              } 
            } 
          } 
        } 
      } 
      try {
        eventController.updateObjects(updatedEvent, Integer.valueOf(0));
      } catch (Exception ex) {
        logger.log(Level.SEVERE, "Exception while updating event " + Integer.toString(updatedEvent.getIdEvent().intValue()), ex);
      } 
    } 
    logger.log(Level.INFO, "Processed {0} events.", Integer.toString(events.size()));
  }
  
  public void processAllEvents() {
    //System.out.println("This is processAll Events ------------------------------------");
    if (eventController == null)
      eventController = new EventJpaController(); 
    if (eventtemplateController == null)
      eventtemplateController = new EventtemplateJpaController(); 
    if (alertController == null)
      alertController = new AlertJpaController(); 
    if (notificationController == null)
      notificationController = new NotificationJpaController(); 
    if (userController == null)
      userController = new UserJpaController(); 
    if (studyController == null)
      studyController = new StudyJpaController(); 
    if (processController == null)
      processController = new ProcessJpaController(); 
    if (modelController == null)
      modelController = new ModelJpaController(); 
    List<Object> eventtemplates = eventtemplateController.findObjectEntities();
    List<Object> events = eventController.findScheduledEventEntities(Boolean.TRUE.booleanValue(), 0, 0);
    
    for (Object e : events) {
    System.out.println("This is e Events ------------------------------------");      
        if (checkEventRipe((Event)e, eventtemplates) == true) {
            //System.out.println("Event e :"+e.toString());
            //System.out.println("eventtemplates :"+eventtemplates.toString());
            
            Event updatedEvent = (Event)e;
            try {
            if (checkEventActive((Event)e, eventtemplates))
                executeEvent((Event)e, eventtemplates);
                eventController.updateObjects(updatedEvent, Integer.valueOf(0));        
            } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception while executing/updating event " + Integer.toString(updatedEvent.getIdEvent().intValue()), ex);
            } 
        } 
        } 
    
    events = eventController.findNewEventEntities(Boolean.TRUE.booleanValue(), 0, 0);
    for (Object e : events) {
      Event updatedEvent = (Event)e;
      if (checkEventActive((Event)e, eventtemplates) == true) {
        List<Object> users = userController.findOrgUserEntities(((Event)e).getIdOrganization());
        
        for (Object user : users) {
          List<Object> alerts = alertController.findUserAlertEntities(Integer.valueOf(((User)user).getIdUser()));
          for (Object al : alerts) {
//            System.out.println("This is alert Object ------------------------------------");
//            System.out.println("alerts object al :"+al.toString());
//            System.out.println("eventTemplate al :"+((Alert)al).getIdEventTemplate());
//            System.out.println("eventTemplate e  :"+((Event)e).getIdEventTemplate());
//            System.out.println("alerts active al :"+ ((Alert)al).getActive().booleanValue());
         
         if (Objects.equals(((Alert)al).getIdEventTemplate(), ((Event)e).getIdEventTemplate()) && 
                    ((Alert)al).getActive().booleanValue() == true) {
             
//              System.out.println("Event data :" + ((Event)e).getDescription());
//              System.out.println("Event type :" + ((Event)e).getObjectType());
//              System.out.println("Event objectId :" + ((Event)e).getObjectId());
              
              String objectType = ((Event)e).getObjectType();
              Integer search_id = ((Event)e).getObjectId();

              Notification n = new Notification();
              n.setAckRequired(((Alert)al).getAckRequired());
              n.setIdUser(((Alert)al).getIdUser());
              n.setTimeOut(((Alert)al).getTimeOut());
              n.setName(((Alert)al).getName());
              n.setPriority(((Alert)al).getPriority());
              n.setDescription(((Alert)al).getDescription());
              n.setAcknowledged(Boolean.valueOf(false));
              n.setRetryCount(Integer.valueOf(0));
              n.setEventData(((Event)e).getDescription());
              n.setEventObjectId(((Event)e).getObjectId());
              n.setEventObjectType(((Event)e).getObjectType());
              n.setIdEvent(((Event)e).getIdEvent());
              n.setActive(Boolean.TRUE);              
              Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
              n.setCreatedTS(currentTimestamp);
              n.setCreatedBy(Integer.valueOf(0));
              
              
              if("studies. Submission remote: DELTAlytics".equals(objectType)){
                    // System.out.println("comparing string for studies");
                     Study studydetail = (Study) studyController.findStudy(search_id);
                     studydetail.getIdGroup();
                     n.setIdGroup(studydetail.getIdGroup());
                    //System.out.println("idproject of study"+ studydetail.getIdGroup());
                    
                }
              
              if("models".equals(objectType)){
                    //System.out.println("comparing string for models");
                     Model modeldetail = (Model) modelController.findModel(search_id);
                     modeldetail.getIdGroup();
                    //System.out.println("idproject of models"+ modeldetail.getIdGroup());
                     n.setIdGroup(modeldetail.getIdGroup());
                }
              
              if("results".equals(objectType)){
                    //System.out.println("comparing string for results");
                     Process processdetail = (Process) processController.findProcess(search_id);
                     processdetail.getIdGroup();
                    //System.out.println("idproject of result :"+ processdetail.getIdGroup());
                     n.setIdGroup(processdetail.getIdGroup());
                }
              
              try {                  
                notificationController.create(n);
              } catch (Exception ex) {
                Logger.getLogger(EventServiceImpl.class.getName()).log(Level.SEVERE, "Exception while creating notification for event " + 
                    Integer.toString(updatedEvent.getIdEvent().intValue()), ex);
              } 
            } 
          } 
        } 
      } 
      try {
        eventController.updateObjects(updatedEvent, Integer.valueOf(0));
      } catch (Exception ex) {
        logger.log(Level.SEVERE, "Exception while updating event " + Integer.toString(updatedEvent.getIdEvent().intValue()), ex);
      } 
    } 
    logger.log(Level.INFO, "Processed {0} events.", Integer.toString(events.size()));
  }
  
  private boolean checkEventRipe(Event e, List<Object> eventtemplates) {
    //System.out.println("This is e checkEventRipe ------------------------------------");
    for (Object et : eventtemplates) {
        //System.out.println("Eventtemplate et :"+((Eventtemplate)et).getIdEventTemplate());
        //System.out.println("event e :"+e.getIdEventTemplate());
            
      if (Objects.equals(((Eventtemplate)et).getIdEventTemplate(), e.getIdEventTemplate())) {
        Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
        if (e.getExecuteTS().before(currentTimestamp))
          return true; 
      } 
    
    } 
    return false;
  }
  
  private boolean checkAlertActive(Event e, List<Object> alerts) {
    for (Object al : alerts) {
      if (Objects.equals(((Alert)al).getIdEventTemplate(), e.getIdEventTemplate()) && (
        (Alert)al).getActive().booleanValue() == true)
        return true; 
    } 
    return false;
  }
  
  private boolean checkEventActive(Event e, List<Object> eventtemplates) {
    for (Object et : eventtemplates) {
      if (Objects.equals(((Eventtemplate)et).getIdEventTemplate(), e.getIdEventTemplate()) && (
        (Eventtemplate)et).getActive().booleanValue() == true)
        return true; 
    } 
    return false;
  }
  
  private boolean executeEvent(Event e, List<Object> eventtemplates) throws Exception {
    for (Object et : eventtemplates) {
      if (Objects.equals(((Eventtemplate)et).getIdEventTemplate(), e.getIdEventTemplate())) {
        Class[] param2Int = new Class[2];
        param2Int[0] = int.class;
        param2Int[1] = int.class;
        Class[] noparams = new Class[0];
        Class<?> cls = Class.forName(((Eventtemplate)et).getActionClass());
        Object obj = cls.newInstance();
        Method method = cls.getDeclaredMethod("init", param2Int);
        method.invoke(obj, new Object[] { e.getObjectId(), e.getExecuteBy() });
        method = cls.getDeclaredMethod("run", noparams);
        method.invoke(obj, new Object[0]);
        return true;
      } 
    } 
    return false;
  }
  
  public String createTaskAndSchedule(String JSONString, Integer currentOrgId, Integer currentUserId) {
    String result = "Task(s) not created";
    String dataString = "";
    String objectType = "object";
    int i = 0;
    Integer eventtemplateId = Integer.valueOf(0);
    if (taskController == null)
      taskController = new TaskJpaController(); 
    try {
      JSONObject tasksAndSchedules = new JSONObject(JSONString);
      JSONArray taskArray = tasksAndSchedules.getJSONArray("tasks");
      JSONArray scheduleArray = tasksAndSchedules.getJSONArray("schedules");
      for (; i < taskArray.length(); i++) {
        Task task = new Task();
        JSONObject taskObject = (JSONObject)taskArray.get(i);
        JSONObject scheduleObject = (JSONObject)scheduleArray.get(i);
        Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
        int id = taskObject.getInt("idTask");
        if (id > 0) {
          task.setIdTask(Integer.valueOf(id));
          deleteTaskEvents(task.getIdTask(), currentUserId);
          task.setUpdatedTS(currentTimestamp);
          task.setUpdatedBy(currentUserId);
        } 
        task.setCreatedTS(currentTimestamp);
        task.setCreatedBy(currentUserId);
        String type = taskObject.getString("type");
        task.setType(type);
        objectType = taskObject.getString("objectType");
        task.setObjectType(objectType);
        Integer objectId = Integer.valueOf(taskObject.getInt("objectId"));
        task.setObjectId(objectId);
        task.setParameters(scheduleObject.toString());
        task.setActive(Boolean.TRUE);
        switch (type) {
          case "generateFTrunStudy":
            task.setName("Full Study Run");
            task.setDescription("Generate Flat Table and submit Study");
            eventtemplateId = Integer.valueOf(Eventtemplate.START_FULL_STUDY_EVENT);
            break;
          case "runStudy":
            task.setName("Study Run");
            task.setDescription("Submit Study");
            eventtemplateId = Integer.valueOf(Eventtemplate.START_STUDY_EVENT);
            break;
          case "generateFT":
            task.setName("Generate FT");
            task.setDescription("Generate Flat Table");
            eventtemplateId = Integer.valueOf(Eventtemplate.START_FT_EVENT);
            break;
            
        } 
        task.setOrganizationidOrganization(currentOrgId.intValue());
        task = taskController.edit(task);
        AuditLogger.log(currentUserId.intValue(), task.getIdTask().intValue(), "create", task);
        scheduleEvents(task, scheduleObject, eventtemplateId, currentOrgId, currentUserId);
        dataString = dataString + (new JSONObject(task)).toString();
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while scheduling: {0} {1}", new Object[] { objectType, ex.getMessage() });
      result = "EA0108 Exception while scheduling " + objectType + ": " + ex.getMessage();
    } 
    return wrapAPI("tasks", dataString, Integer.toString(i) + " task(s) scheduled", result);
  }
  
  private String scheduleEvents(Task task, JSONObject scheduleObject, Integer eventtemplateId, Integer currentOrgId, Integer currentUserId) throws Exception {
    int eventCounter = 0;
    String dataString = "";
    int recNumber = scheduleObject.getInt("reccurenceNumber");
    JSONObject reccurence = scheduleObject.getJSONObject("reccurence");
    int recType = reccurence.getInt("type");
    int period = reccurence.getInt("period");
    if (eventController == null)
      eventController = new EventJpaController(); 
    String ts = scheduleObject.getString("startYYYY") + "-" + scheduleObject.getString("startMM") + "-" + scheduleObject.getString("startDD") + " " + scheduleObject.getString("startHH") + ":" + scheduleObject.getString("startmm") + ":0.0";
    Timestamp startTS = Timestamp.valueOf(ts);
    if (recType == 0)
      recNumber = 1; 
    for (; eventCounter < recNumber; eventCounter++) {
      JSONObject eventJSON;
      int internalEventCounter;
      Calendar calOriginal;
      Event event = new Event();
      event.setIdTask(task.getIdTask());
      event.setIdEventTemplate(eventtemplateId);
      event.setExecuteBy(currentUserId);
      event.setDescription(task.getDescription());
      event.setObjectId(task.getObjectId());
      event.setObjectType(task.getObjectType());
      Calendar cal = Calendar.getInstance();
      cal.setTime(startTS);
      switch (recType) {
        case 0:
          event.setExecuteTS(startTS);
          eventJSON = new JSONObject(eventController.createObjects(event, currentOrgId, currentUserId));
          if (eventCounter > 0)
            dataString = dataString + ","; 
          dataString = dataString + eventJSON.toString();
          break;
        case 1:
          if (eventCounter > 0) {
            cal.add(7, period);
            startTS.setTime(cal.getTime().getTime());
          } 
          event.setExecuteTS(startTS);
          eventJSON = new JSONObject(eventController.createObjects(event, currentOrgId, currentUserId));
          if (eventCounter > 0)
            dataString = dataString + ","; 
          dataString = dataString + eventJSON.toString();
          break;
        case 2:
          if (eventCounter > 0)
            cal.add(7, 7 * period); 
          internalEventCounter = 0;
          calOriginal = cal;
          if (reccurence.getBoolean("monday") == true) {
            cal.set(7, 2);
            startTS.setTime(cal.getTime().getTime());
            event.setExecuteTS(startTS);
            eventJSON = new JSONObject(eventController.createObjects(event, currentOrgId, currentUserId));
            if (eventCounter > 0 || internalEventCounter > 0)
              dataString = dataString + ","; 
            dataString = dataString + eventJSON.toString();
            internalEventCounter++;
            cal = calOriginal;
          } 
          if (reccurence.getBoolean("tuesday") == true) {
            cal.set(7, 3);
            startTS.setTime(cal.getTime().getTime());
            event.setExecuteTS(startTS);
            eventJSON = new JSONObject(eventController.createObjects(event, currentOrgId, currentUserId));
            if (eventCounter > 0 || internalEventCounter > 0)
              dataString = dataString + ","; 
            dataString = dataString + eventJSON.toString();
            internalEventCounter++;
            cal = calOriginal;
          } 
          if (reccurence.getBoolean("wednesday") == true) {
            cal.set(7, 4);
            startTS.setTime(cal.getTime().getTime());
            event.setExecuteTS(startTS);
            eventJSON = new JSONObject(eventController.createObjects(event, currentOrgId, currentUserId));
            if (eventCounter > 0 || internalEventCounter > 0)
              dataString = dataString + ","; 
            dataString = dataString + eventJSON.toString();
            internalEventCounter++;
            cal = calOriginal;
          } 
          if (reccurence.getBoolean("thursday") == true) {
            cal.set(7, 5);
            startTS.setTime(cal.getTime().getTime());
            event.setExecuteTS(startTS);
            eventJSON = new JSONObject(eventController.createObjects(event, currentOrgId, currentUserId));
            if (eventCounter > 0 || internalEventCounter > 0)
              dataString = dataString + ","; 
            dataString = dataString + eventJSON.toString();
            internalEventCounter++;
            cal = calOriginal;
          } 
          if (reccurence.getBoolean("friday") == true) {
            cal.set(7, 6);
            startTS.setTime(cal.getTime().getTime());
            event.setExecuteTS(startTS);
            eventJSON = new JSONObject(eventController.createObjects(event, currentOrgId, currentUserId));
            if (eventCounter > 0 || internalEventCounter > 0)
              dataString = dataString + ","; 
            dataString = dataString + eventJSON.toString();
            internalEventCounter++;
            cal = calOriginal;
          } 
          if (reccurence.getBoolean("saturday") == true) {
            cal.set(7, 7);
            startTS.setTime(cal.getTime().getTime());
            event.setExecuteTS(startTS);
            eventJSON = new JSONObject(eventController.createObjects(event, currentOrgId, currentUserId));
            if (eventCounter > 0 || internalEventCounter > 0)
              dataString = dataString + ","; 
            dataString = dataString + eventJSON.toString();
            internalEventCounter++;
            cal = calOriginal;
          } 
          if (reccurence.getBoolean("sunday") == true) {
            cal.set(7, 1);
            startTS.setTime(cal.getTime().getTime());
            event.setExecuteTS(startTS);
            eventJSON = new JSONObject(eventController.createObjects(event, currentOrgId, currentUserId));
            if (eventCounter > 0 || internalEventCounter > 0)
              dataString = dataString + ","; 
            dataString = dataString + eventJSON.toString();
            internalEventCounter++;
            cal = calOriginal;
          } 
          break;
        case 3:
          if (eventCounter > 0) {
            cal.add(2, period);
            startTS.setTime(cal.getTime().getTime());
          } 
          event.setExecuteTS(startTS);
          eventJSON = new JSONObject(eventController.createObjects(event, currentOrgId, currentUserId));
          if (eventCounter > 0)
            dataString = dataString + ","; 
          dataString = dataString + eventJSON.toString();
          break;
        case 4:
          if (eventCounter > 0) {
            cal.add(1, period);
            startTS.setTime(cal.getTime().getTime());
          } 
          event.setExecuteTS(startTS);
          eventJSON = new JSONObject(eventController.createObjects(event, currentOrgId, currentUserId));
          if (eventCounter > 0)
            dataString = dataString + ","; 
          dataString = dataString + eventJSON.toString();
          break;
      } 
    } 
    return dataString;
  }
  
  public String getOrgTasksByGroup(int start, int limit, int idGroup, Integer currentOrgId) {
    String dataString = "";
    String result = "No records found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    List<Task> tasks = null;
    if (taskController == null)
      taskController = new TaskJpaController(); 
    if (adminService == null)
      adminService = new AdminServiceImpl(); 
    int recordCounter = 0;
    logger.log(Level.INFO, "Getting list of task for group {0} starting at {1}", new Object[] { Integer.toString(idGroup), Integer.toString(start) });
    if (limit == 0) {
      tasks = taskController.findOrgTaskEntities(currentOrgId);
    } else {
      tasks = taskController.findOrgTaskEntities(limit, start, currentOrgId);
    } 
    int max = tasks.size();
    int hitCounter = 0;
    for (; recordCounter < max; recordCounter++) {
      if (idGroup == 0 || adminService.checkGroupHasEntityExists(idGroup, ((Task)tasks.get(recordCounter)).getObjectId().intValue(), ((Task)tasks.get(recordCounter)).getObjectType())) {
        dataString = dataString + (new JSONObject(tasks.get(recordCounter))).toString();
        hitCounter++;
        if (recordCounter != max - 1)
          dataString = dataString.concat(","); 
      } 
    } 
    dataString = "[" + dataString + "]";
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(hitCounter) + " tasks retrieved");
        success.put("totalCount", Integer.valueOf(hitCounter));
        success.put("tasks", new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getObjects]";
      return result;
    } 
  }
  
  public String deleteTasks(String JSONString, Integer currentUserId) {
    String result = "Tasks not deleted";
    Task task = null;
    String dataString = "";
    try {
      JSONObject JSONContentRoot = new JSONObject(JSONString);
      JSONArray anArray = JSONContentRoot.getJSONArray("tasks");
      for (int i = 0; i < anArray.length(); i++) {
        logger.log(Level.INFO, "Deleting Tasks {0}", anArray.getJSONObject(i).getString("name"));
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.u").create();
        String jString = anArray.getJSONObject(i).toString();
        
        //System.out.println("jString"+jString);
 
        task = (Task)gson.fromJson(jString, Task.class);
        //System.out.println("task"+task.toString());
        
        deleteTaskAndEvents(task.getIdTask(), currentUserId);
        
        AuditLogger.log(currentUserId.intValue(), task.getIdTask().intValue(), "delete", task);
       
        
        if (i > 0)
          dataString = dataString.concat(","); 
        dataString = dataString + (new JSONObject(task)).toString();
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while deleting Task: {0} {1}", new Object[] { task.getName(), ex });
      result = "EA0109 Exception while deleting tasks: " + ex.getMessage();
    } 
    return wrapAPI("tasks", dataString, "Tasks deleted", result);
  }
  
  public String getOrgEventsByGroup(int start, int limit, int idGroup, Integer currentOrgId) {
    int totalCount;
    String dataString = "";
    String result = "No records found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    if (eventController == null)
      eventController = new EventJpaController(); 
    List<Object> events = null;
    if (limit == 0) {
      if (idGroup == 0) {
        events = eventController.findObjectEntities(currentOrgId);
      } else {
        events = eventController.findObjectEntitiesByOrgGroup(currentOrgId, Integer.valueOf(idGroup));
      } 
    } else if (idGroup == 0) {
      events = eventController.findObjectEntities(limit, start, currentOrgId);
    } else {
      events = eventController.findObjectEntitiesByOrgGroup(limit, start, currentOrgId, Integer.valueOf(idGroup));
    } 
    int max = events.size();
    int recordCounter = 0;
    for (; recordCounter < max; recordCounter++) {
      if (recordCounter > 0)
        dataString = dataString + ","; 
      dataString = dataString + (new JSONObject(events.get(recordCounter))).toString();
    } 
    dataString = "[" + dataString + "]";
    if (idGroup == 0) {
      totalCount = eventController.getObjectCountByOrg(currentOrgId);
    } else {
      totalCount = eventController.getObjectCountByOrgGroup(currentOrgId, Integer.valueOf(idGroup));
    } 
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(recordCounter) + " events retrieved");
        success.put("totalCount", Integer.valueOf(totalCount));
        success.put("events", new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [event.getOrgEventsByGroup]: ", (Throwable)ex);
      result = "JSON error in WS method [event.getOrgEventsByGroup]";
      return result;
    } 
  }
  
  public String getUserNotificationsByGroup(int start, int limit, int idGroup, Integer userId, String sort) {
    int totalCount;
    String dataString = "";
    String result = "No records found";
    String orderBy = "";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    //System.out.println("THIS IS GET NOTIFICATION BY GROUP :"+idGroup);
    
    if (notificationController == null)
      notificationController = new NotificationJpaController(); 
    try {
      JSONArray sortArray = new JSONArray(sort);
      orderBy = sortArray.getJSONObject(0).getString("property") + " " + sortArray.getJSONObject(0).getString("direction");
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSONArray parsing error in WS method [event.getUserNotificationsByGroup]: ", (Throwable)ex);
    } 
    List<Object> notifications = null;
    if (limit == 0) {
      if (idGroup == 0) {
        //System.out.println("This is at JPA limit 0 Group 0");
        notifications = notificationController.findObjectEntities(userId);
      } else {
        //System.out.println("This is at JPA limit 0 Group NON 0");
        notifications = notificationController.findObjectEntitiesByOrgGroup(userId, Integer.valueOf(idGroup), orderBy);
      } 
    } else if (idGroup == 0) {
      //System.out.println("This is at JPA limit Non 0 Group 0 limit start userId"+limit+ start+ userId);
      notifications = notificationController.findObjectEntities(limit, start, userId);
    } else {
      
      //System.out.println("This is at JPA limit Non 0 Group NON 0");
      notifications = notificationController.findObjectEntitiesByOrgGroup(limit, start, userId, Integer.valueOf(idGroup), orderBy);
    } 
    int max = notifications.size();
    int recordCounter = 0;
    for (; recordCounter < max; recordCounter++) {
      if (recordCounter > 0)
        dataString = dataString + ","; 
      dataString = dataString + (new JSONObject(notifications.get(recordCounter))).toString();
    } 
    dataString = "[" + dataString + "]";
    if (idGroup == 0) {
      totalCount = notificationController.getObjectCountByOrg(userId);
    } else {
      totalCount = notificationController.getObjectCountByOrgGroup(userId, Integer.valueOf(idGroup));
    } 
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(recordCounter) + " notifications retrieved");
        success.put("totalCount", Integer.valueOf(totalCount));
        success.put("notifications", new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [event.getUserNotificationsByGroup]: ", (Throwable)ex);
      result = "JSON error in WS method [event.getUserNotificationByGroup]";
      return result;
    } 
  }
  
  private Task deleteTaskAndEvents(Integer idTask, Integer currentUserId) throws NonexistentEntityException {
    //System.out.println("This is at delete task AND event"+idTask);
    deleteTaskEvents(idTask, currentUserId);
    if (taskController == null)
      taskController = new TaskJpaController(); 
    return taskController.destroy(idTask);
  }
  
  private void deleteTaskEvents(Integer idTask, Integer currentUserId) throws NonexistentEntityException {
   //System.out.println("This is at deleteTaskEvents"+idTask);
      if (eventController == null)
      eventController = new EventJpaController(); 
    List<Object> events = eventController.findTaskEventEntities(idTask);
    for (Object event : events)
      eventController.deleteObject(event, currentUserId); 
  }
  
  private String wrapAPI(String objectId, String result, String msgSuccess, String msgFailure) {
    if (adminService == null)
      adminService = new AdminServiceImpl(); 
    return adminService.wrapAPI(objectId, result, msgSuccess, msgFailure);
  }
}
