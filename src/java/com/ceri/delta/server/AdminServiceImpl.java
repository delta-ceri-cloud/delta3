package com.ceri.delta.server;

import com.ceri.delta.entity.GroupHasEntity;
import com.ceri.delta.entity.GroupHasEntityPK;
import com.ceri.delta.entity.GroupTable;
import com.ceri.delta.entity.Organization;
import com.ceri.delta.entity.Permission;
import com.ceri.delta.entity.PermissionPK;
import com.ceri.delta.entity.Permissiontemplate;
import com.ceri.delta.entity.PermissiontemplateComp;
import com.ceri.delta.entity.Role;
import com.ceri.delta.entity.User;
import com.ceri.delta.entity.UserHasRole;
import com.ceri.delta.entity.UserHasRolePK;
import com.ceri.delta.jpa.GroupHasEntityJpaController;
import com.ceri.delta.jpa.GroupJpaController;
import com.ceri.delta.jpa.OrganizationJpaController;
import com.ceri.delta.jpa.PermissionJpaController;
import com.ceri.delta.jpa.PermissiontemplateJpaController;
import com.ceri.delta.jpa.PersistenceHelper;
import com.ceri.delta.jpa.RoleJpaController;
import com.ceri.delta.jpa.UserHasProjectRoleJpaController;
import com.ceri.delta.jpa.UserHasRoleJpaController;
import com.ceri.delta.jpa.UserJpaController;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.security.AuditLogger;
import com.ceri.delta.security.Auth;
import com.ceri.delta.security.HashEncrypter;
import com.ceri.delta.server.exceptions.EntityExistsException;
import com.ceri.delta.util.JSON.JSONArray;
import com.ceri.delta.util.JSON.JSONException;
import com.ceri.delta.util.JSON.JSONObject;
import com.ceri.delta.util.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

public class AdminServiceImpl {
  private static final Logger logger = Logger.getLogger(AdminServiceImpl.class.getCanonicalName());
  
  private static UserJpaController userController;
  
  private static OrganizationJpaController organizationController;
  
  private static RoleJpaController roleController;
  
  private static UserHasRoleJpaController userhasroleController;
  
  private static UserHasProjectRoleJpaController userhasprojectroleController;
  
  private static GroupJpaController groupController;
  
  private static GroupHasEntityJpaController grouphasentityController;
  
  private static PermissionJpaController permissionController;
  
  private static PermissiontemplateJpaController permissiontemplateController;
  
  
  
  
  
  
  
  
  public String createObjects(String JSONString, Integer currentUserId, PersistenceHelper controller, Object subject) {
    String result = "Objects(s) not created";
    String dataString = "";
    try {
      JSONArray objectArray = new JSONArray(JSONString);
      for (int i = 0; i < objectArray.length(); i++) {
        logger.log(Level.INFO, "Creating Object {0} " + Integer.toString(i), controller.getObjectType());
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = objectArray.getJSONObject(i).toString();
        subject = gson.fromJson(jString, subject.getClass());
        subject = controller.createObjects(subject, currentUserId);
        
        if (i > 0)
          dataString = dataString.concat(","); 
        dataString = dataString + (new JSONObject(subject)).toString();
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "DA0111 Exception while creating new Object: {0} {1}", new Object[] { controller.getObjectType(), ex });
      result = "DA0111 Exception while creating new " + controller.getObjectType() + ": " + ex.getMessage();
    } 
    return wrapAPI(controller.getObjectType(), dataString, "Object(s) created", result);
  }
  
  
  public String projectALL(String JSONString, Integer currentUserId,Integer currentOrgId) 
  {
      
      
    String result = "Objects(s) added to all projects";
    String result1="test";
    String dataString = "";
    try {
        System.out.println("This is new method projectALL AdminServiceImpl JSONString"+JSONString);
        System.out.println("This is new method projectALL AdminServiceImpl currentUserId"+currentUserId);
        System.out.println("This is new method projectALL AdminServiceImpl currentOrgId"+currentOrgId);
        
        if (groupController == null)
         groupController = new GroupJpaController(); 
        result1 = groupController.projectALL(JSONString, currentUserId,currentOrgId);
        System.out.println("result1"+result1);
        
    } catch (Exception ex) {
      
    } 
    
    
    return wrapAPI(groupController.getObjectType(), dataString, "Objects(s) added to all projects", result1);
  }
  
  
  public String updateObjects(String JSONString, Integer currentUserId, PersistenceHelper controller, Object subject) {
    String result = "Objects(s) not updated";
    String dataString = "";
    try {
      JSONArray objectArray = new JSONArray(JSONString);
      for (int i = 0; i < objectArray.length(); i++) {
        logger.log(Level.INFO, "Updating Object {0} " + Integer.toString(i), controller.getObjectType());
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = objectArray.getJSONObject(i).toString();
        subject = gson.fromJson(jString, subject.getClass());
        subject = controller.updateObjects(subject, currentUserId);
        if (i > 0)
          dataString = dataString.concat(","); 
        dataString = dataString + (new JSONObject(subject)).toString();
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while updating new Object: {0} {1}", new Object[] { controller.getObjectType(), ex });
      result = "DA0008 Exception while updating " + controller.getObjectType() + ": " + ex.getMessage();
    } 
    return wrapAPI(controller.getObjectType(), dataString, "Object(s) updated", result);
  }
  
  public String deleteObjects(String JSONString, Integer currentUserId, PersistenceHelper controller, Object subject) {
    String result = "Objects(s) not deleted";
    String dataString = "";
    try {
      JSONArray objectArray = new JSONArray(JSONString);
      for (int i = 0; i < objectArray.length(); i++) {
        logger.log(Level.INFO, "Deleting Object {0} iteration " + Integer.toString(i), controller.getObjectType());
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = objectArray.getJSONObject(i).toString();
        subject = gson.fromJson(jString, subject.getClass());
        controller.deleteObject(subject, currentUserId);
        if (i > 0)
          dataString = dataString.concat(","); 
        dataString = dataString + (new JSONObject(subject)).toString();
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while deleting Object: {0} {1}", new Object[] { controller.getObjectType(), ex });
      result = "DA0008 Exception while deleting " + controller.getObjectType() + ": " + ex.getMessage();
    } 
    return wrapAPI(controller.getObjectType(), dataString, "Object(s) deleted", result);
  }
  
  public String getObjects(int start, int limit, PersistenceHelper controller, Object subject) {
    String dataString = "";
    String result = "No records found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    List<Object> subjects = null;
    int recordCounter = 0;
    logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[] { controller.getObjectType(), Integer.toString(start) });
    if (limit == 0) {
      subjects = controller.findObjectEntities();
    } else {
      subjects = controller.findObjectEntities(limit, start);
    } 
    int max = subjects.size();
    for (; recordCounter < max; recordCounter++) {
      dataString = dataString + (new JSONObject(subjects.get(recordCounter))).toString();
      if (recordCounter != max - 1)
        dataString = dataString.concat(","); 
    } 
    dataString = "[" + dataString + "]";
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");
        success.put("totalCount", Integer.valueOf(controller.getObjectCount()));
        success.put(controller.getObjectType(), new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getObjects]";
      return result;
    } 
  }
  
  public String getObjectsExtended(int start, int limit, String filter, String sort, PersistenceHelper controller, Object subject) {
    String dataString = "";
    String result = "No records found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    List<Object> subjects = null;
    int recordCounter = 0;
    String whereClause = constructWhereClause(filter, sort);
    logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[] { controller.getObjectType(), Integer.toString(start) });
    if (limit == 0) {
      subjects = controller.findObjectEntitiesExtended(whereClause);
    } else {
      subjects = controller.findObjectEntitiesExtended(limit, start, whereClause);
    } 
    int max = subjects.size();
    for (; recordCounter < max; recordCounter++) {
      dataString = dataString + (new JSONObject(subjects.get(recordCounter))).toString();
      if (recordCounter != max - 1)
        dataString = dataString.concat(","); 
    } 
    dataString = "[" + dataString + "]";
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");
        success.put("totalCount", Integer.valueOf(controller.getObjectCountExtended(whereClause)));
        success.put(controller.getObjectType(), new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getObjects]";
      return result;
    } 
  }
  
  public String getObjectsById(int start, int limit, int id, PersistenceHelper controller, Object subject) {
    String dataString = "";
    String result = "No records found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    List<Object> subjects = null;
    int recordCounter = 0;
    logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[] { controller.getObjectType(), Integer.toString(start) });
    if (limit == 0) {
      subjects = controller.findObjectEntitiesById(limit, start, id);
    } else {
      subjects = controller.findObjectEntitiesById(limit, start, id);
    } 
    int max = subjects.size();
    for (; recordCounter < max; recordCounter++) {
      dataString = dataString + (new JSONObject(subjects.get(recordCounter))).toString();
      if (recordCounter != max - 1)
        dataString = dataString.concat(","); 
    } 
    dataString = "[" + dataString + "]";
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");
        success.put("totalCount", Integer.valueOf(controller.getObjectCount(id)));
        success.put(controller.getObjectType(), new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjectsById]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getObjectsById]: " + ex.getMessage();
      return result;
    } 
  }
  
  public String createOrgObjects(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Object subject) {
    String result = "Objects(s) not created";
    String dataString = "";
    try {
      JSONArray objectArray = new JSONArray(JSONString);
      //System.out.println("This is object string"+JSONString);
      //System.out.println("This is objectArray"+objectArray);
        
      
      for (int i = 0; i < objectArray.length(); i++) {
        logger.log(Level.INFO, "Creating Object {0} " + Integer.toString(i), controller.getObjectType());
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = objectArray.getJSONObject(i).toString();
        //System.out.println("This is jString"+jString);
        subject = gson.fromJson(jString, subject.getClass());
        subject = controller.createObjects(subject, currentOrgId, currentUserId);
        
        if (i > 0)
          dataString = dataString.concat(","); 
          dataString = dataString + (new JSONObject(subject)).toString();
        } 
       
      dataString = "[" + dataString + "]";
      //System.out.println("This is dataString"+dataString);
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "DA0112 Exception while creating new Object: {0} {1}", new Object[] { controller.getObjectType(), ex });
      result = "DA0112 Exception while creating new " + controller.getObjectType() + ": " + ex.getMessage();
    } 
    return wrapAPI(controller.getObjectType(), dataString, "Object(s) created", result);
  }
  
  public String updateOrgObjects(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Object subject) {
    String result = "Objects(s) not updated";
    String dataString = "";
    try {
      JSONArray objectArray = new JSONArray(JSONString);
      for (int i = 0; i < objectArray.length(); i++) {
        logger.log(Level.INFO, "Updating Object {0} " + Integer.toString(i), controller.getObjectType());
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = objectArray.getJSONObject(i).toString();
        subject = gson.fromJson(jString, subject.getClass());
        subject = controller.updateObjects(subject, currentOrgId, currentUserId);
        if (i > 0)
          dataString = dataString.concat(","); 
        dataString = dataString + (new JSONObject(subject)).toString();
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while updating new Object: {0} {1}", new Object[] { controller.getObjectType(), ex });
      result = "DA0008 Exception while updating " + controller.getObjectType() + ": " + ex.getMessage();
    } 
    return wrapAPI(controller.getObjectType(), dataString, "Object(s) updated", result);
  }
  
  public String deleteOrgObjects(String JSONString, Integer currentOrgId, Integer currentUserId, PersistenceHelper controller, Object subject) {
    String result = "Objects(s) not deleted";
    String dataString = "";
    try {
      JSONArray objectArray = new JSONArray(JSONString);
      for (int i = 0; i < objectArray.length(); i++) {
        logger.log(Level.INFO, "Deleting Object {0} " + Integer.toString(i), controller.getObjectType());
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = objectArray.getJSONObject(i).toString();
        subject = gson.fromJson(jString, subject.getClass());
        controller.deleteObject(subject, currentOrgId, currentUserId);
        if (i > 0)
          dataString = dataString.concat(","); 
        dataString = dataString + (new JSONObject(subject)).toString();
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while deleting Object: {0} {1}", new Object[] { controller.getObjectType(), ex });
      result = "DA0008 Exception while deleting " + controller.getObjectType() + ": " + ex.getMessage();
    } 
    return wrapAPI(controller.getObjectType(), dataString, "Object(s) deleted", result);
  }
  
  public String getOrgObjects(int start, int limit, Integer currentOrgId, PersistenceHelper controller, Object subject) {
    String dataString = "";
    String result = "No records found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    List<Object> subjects = null;
    int recordCounter = 0;
    
    
    
    logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[] { controller.getObjectType(), Integer.toString(start) });
    try {
      if (limit == 0) {
        subjects = controller.findObjectEntities(currentOrgId);
      } else {
        subjects = controller.findObjectEntities(limit, start, currentOrgId);
      } 
      int max = subjects.size();
      for (; recordCounter < max; recordCounter++) {
        dataString = dataString + (new JSONObject(subjects.get(recordCounter))).toString();
        if (recordCounter != max - 1)
          dataString = dataString.concat(","); 
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Error in WS method [admin.getObjects]: ", ex);
      result = "Error in WS method [admin.getObjects]; " + ex.getMessage();
      dataString = "";
    } 
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");
        success.put("totalCount", Integer.valueOf(controller.getObjectCountByOrg(currentOrgId)));
        success.put(controller.getObjectType(), new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getObjects]: " + ex.getMessage();
      return result;
    } 
  }
  
  public String getOrgObjectsExtended(int start, int limit, String filter, String sort, Integer currentOrgId, PersistenceHelper controller, Object subject) {
    String dataString = "";
    String result = "No records found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    List<Object> subjects = null;
    int recordCounter = 0;
    
    
    //System.out.println("This is ar AdminService start"+start);
    //System.out.println("This is ar AdminService limit"+limit);
    //System.out.println("This is ar AdminService filter"+filter);
    //System.out.println("This is ar AdminService sort"+sort);    
    //System.out.println("This is ar AdminService currentOrgId"+currentOrgId);
    
    
    
    
    
    String whereClause = constructWhereClause(filter, sort);
    
    System.out.println("his is ar AdminService whereClause"+whereClause);
    
    logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[] { controller.getObjectType(), Integer.toString(start) });
    try {
      if (limit == 0) {
        subjects = controller.findObjectEntitiesExtendedOrg(currentOrgId, whereClause);
      } else {
        subjects = controller.findObjectEntitiesExtendedOrg(limit, start, currentOrgId, whereClause);
      } 
      int max = subjects.size();
      for (; recordCounter < max; recordCounter++) {
        dataString = dataString + (new JSONObject(subjects.get(recordCounter))).toString();
        if (recordCounter != max - 1)
          dataString = dataString.concat(","); 
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Error in WS method [admin.getObjects]: ", ex);
      result = "Error in WS method [admin.getObjects]; " + ex.getMessage();
      dataString = "";
    } 
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");
        success.put("totalCount", Integer.valueOf(controller.getObjectCountExtendedOrg(currentOrgId, whereClause)));
        success.put(controller.getObjectType(), new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getObjects]: " + ex.getMessage();
      return result;
    } 
  }
  
  public String getOrgObjectsById(int start, int limit, int id, Integer currentOrgId, PersistenceHelper controller, Object subject) {
    String dataString = "";
    String result = "No records found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    List<Object> subjects = null;
    int recordCounter = 0;
    int max = 0;
    logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[] { controller.getObjectType(), Integer.toString(start) });
    try {
      if (limit == 0) {
        subjects = controller.findObjectEntitiesById(limit, start, id, currentOrgId);
      } else {
        subjects = controller.findObjectEntitiesById(limit, start, id, currentOrgId);
      } 
      max = subjects.size();
      for (; recordCounter < max; recordCounter++) {
        dataString = dataString + (new JSONObject(subjects.get(recordCounter))).toString();
        if (recordCounter != max - 1)
          dataString = dataString.concat(","); 
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Error in WS method [admin.getObjectsById]: ", ex);
      result = "Error in WS method [admin.getObjectsById]; " + ex.getMessage();
      dataString = "";
    } 
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");
        success.put("totalCount", Integer.valueOf(max));
        success.put(controller.getObjectType(), new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjectsById]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getObjectsById]: " + ex.getMessage();
      return result;
    } 
  }
  
  public String getOrgObjectsByGroup(int start, int limit, int idGroup, Integer currentOrgId, PersistenceHelper controller, Object subject) {
    String dataString = "";
    String result = "No records found";
    int hitCounter = 0;
    int totalCount = 0;
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    List<Object> subjects = null;
    int recordCounter = 0;
    logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[] { controller.getObjectType(), Integer.toString(start) });
    try {
      if (limit == 0) {
        if (idGroup == 0) {
          subjects = controller.findObjectEntities(currentOrgId);
        } else {
          subjects = controller.findObjectEntitiesByOrgGroup(currentOrgId, Integer.valueOf(idGroup));
        } 
      } else if (idGroup == 0) {
        subjects = controller.findObjectEntities(limit, start, currentOrgId);
      } else {
        subjects = controller.findObjectEntitiesByOrgGroup(limit, start, currentOrgId, Integer.valueOf(idGroup));
      } 
      int max = subjects.size();
      for (; recordCounter < max; recordCounter++) {
        dataString = dataString + (new JSONObject(subjects.get(recordCounter))).toString();
        hitCounter++;
        if (recordCounter != max - 1)
          dataString = dataString.concat(","); 
      } 
      dataString = "[" + dataString + "]";
      if (idGroup == 0) {
        totalCount = controller.getObjectCountByOrg(currentOrgId);
      } else {
        totalCount = controller.getObjectCountByOrgGroup(currentOrgId, Integer.valueOf(idGroup));
      } 
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Error in WS method [admin.getOrgObjectsByGroup]: ", ex);
      result = "Error in WS method [admin.getOrgObjectsByGroup]; " + ex.getMessage();
    } 
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(hitCounter) + " " + controller.getObjectType() + " retrieved");
        success.put("totalCount", Integer.valueOf(totalCount));
        success.put(controller.getObjectType(), new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getOrgObjectsByGroup]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getOrgObjectsByGroup]";
      return result;
    } 
  }
  
  public String getOrgObjectsExtendedGroup(int start, int limit, String filter, String sort, int idGroup, Integer currentOrgId, PersistenceHelper controller, Object subject) {
    String dataString = "";
    String result = "No records found";
    int hitCounter = 0;
    int totalCount = 0;
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    List<Object> subjects = null;
    int recordCounter = 0;
    String whereClause = constructWhereClause(filter, sort);
    logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[] { controller.getObjectType(), Integer.toString(start) });
    try {
      if (limit == 0) {
        if (idGroup == 0) {
          subjects = controller.findObjectEntitiesExtendedOrg(currentOrgId, whereClause);
        } else {
          subjects = controller.findObjectEntitiesExtendedOrgGroup(currentOrgId, Integer.valueOf(idGroup), whereClause);
        } 
      } else if (idGroup == 0) {
        subjects = controller.findObjectEntitiesExtendedOrg(limit, start, currentOrgId, whereClause);
      } else {
        subjects = controller.findObjectEntitiesExtendedOrgGroup(limit, start, currentOrgId, Integer.valueOf(idGroup), whereClause);
      } 
      int max = subjects.size();
      for (; recordCounter < max; recordCounter++) {
        dataString = dataString + (new JSONObject(subjects.get(recordCounter))).toString();
        hitCounter++;
        if (recordCounter != max - 1)
          dataString = dataString.concat(","); 
      } 
      dataString = "[" + dataString + "]";
      if (idGroup == 0) {
        totalCount = controller.getObjectCountExtendedOrg(currentOrgId, whereClause);
      } else {
        totalCount = controller.getObjectCountExtendedOrgGroup(currentOrgId, Integer.valueOf(idGroup), whereClause);
      } 
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Error in WS method [admin.getOrgObjectsExtendedGroup]: ", ex);
      result = "Error in WS method [admin.getOrgObjectsExtendedGroup]; " + ex.getMessage();
    } 
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(hitCounter) + " " + controller.getObjectType() + " retrieved");
        success.put("totalCount", Integer.valueOf(totalCount));
        success.put(controller.getObjectType(), new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getOrgObjectsExtendedGroup]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getOrgObjectsExtendedGroup]";
      return result;
    } 
  }
  
  public List<Object> getOrgObjectsListByGroup(int start, int limit, int idGroup, Integer currentOrgId, PersistenceHelper controller, Object subject) {
    List<Object> subjects = null;
    List<Object> subjectsFound = new ArrayList();
    int recordCounter = 0;
    logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[] { controller.getObjectType(), Integer.toString(start) });
    if (limit == 0) {
      subjects = controller.findObjectEntities(currentOrgId);
    } else {
      subjects = controller.findObjectEntities(limit, start, currentOrgId);
    } 
    int max = subjects.size();
    for (; recordCounter < max; recordCounter++) {
      if (idGroup == 0 || checkGroupHasEntityExists(idGroup, controller.getObjectId(subjects.get(recordCounter)).intValue(), controller.getObjectType()))
        subjectsFound.add(subjects.get(recordCounter)); 
    } 
    return subjectsFound;
  }
  
  public String changePassword(String JSONString, Integer currentUserId) {
    String result = "Password not changed.";
    User user = null;
    String dataString = "";
    if (userController == null)
      userController = new UserJpaController(); 
    user = userController.findUser(currentUserId);
    try {
      JSONObject JSONContent = new JSONObject(JSONString);
      logger.log(Level.INFO, "Changing password for User {0}", user.getAlias());
      if (!Auth.verifyPassword(user.getAlias(), JSONContent.optString("op"))) {
        result = "DA0100 Authentication error.";
      } else {
        user.setPassword(HashEncrypter.SHA1(JSONContent.optString("np")));
        Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
        user.setPassChangedTS(currentTimestamp);
        user = userController.edit(user);
        dataString = "[]";
      } 
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while saving new password for User: {0} {1}", new Object[] { user.getAlias(), ex });
      result = "DA0102 Exception while saving new password for User " + user.getAlias() + ": " + ex.getMessage();
    } 
    return wrapAPI("password", dataString, "Password changed.", result);
  }
  
  public String resetPassword(String alias, Integer currentUserId) {
    String result = "Password not changed.";
    List<User> users = null;
    String dataString = "";
    if (userController == null)
      userController = new UserJpaController(); 
    users = userController.findUserByAlias(alias);
    try {
      logger.log(Level.SEVERE, "Resetting password for user {0} by user with ID {1}", new Object[] { alias, currentUserId.toString() });
      ((User)users.get(0)).setPassword(HashEncrypter.SHA1(Auth.tempPassword));
      Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
      ((User)users.get(0)).setPassChangedTS(currentTimestamp);
      userController.edit(users.get(0));
      dataString = "[]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while restting password for User: {0} {1}", new Object[] { alias, ex });
      result = "DA0102 Exception while resetting password for User " + alias + ": " + ex.getMessage();
    } 
    return wrapAPI("password", dataString, "Password reset successful.", result);
  }
  
  public List<Integer> getUserProjects(int userId) {
    List<Integer> projectList = new ArrayList<>();
    if (userhasroleController == null)
      userhasroleController = new UserHasRoleJpaController(); 
    UserHasRolePK uhrPK = new UserHasRolePK();
    uhrPK.setUseridUser(userId);
    List<UserHasRole> uhr = userhasroleController.findUserHasRoleByUserIdEntities(userId);
    if (uhr.isEmpty()) {
      logger.log(Level.INFO, "UserProjectRole does not exist");
      return null;
    } 
    Integer p = Integer.valueOf(0);
    projectList.add(p);
    for (UserHasRole t : uhr) {
      if (!Objects.equals(t.getUserHasRolePK().getIdGroup(), p)) {
        p = t.getUserHasRolePK().getIdGroup();
        projectList.add(p);
      } 
    } 
    return projectList;
  }
  
  public List<Role> getUserRoles(int userId) {
    List<Role> roles = new ArrayList<>();
    if (userhasroleController == null)
      userhasroleController = new UserHasRoleJpaController(); 
    if (roleController == null)
      roleController = new RoleJpaController(); 
    UserHasRolePK uhrPK = new UserHasRolePK();
    uhrPK.setUseridUser(userId);
    List<UserHasRole> uhr = userhasroleController.findUserHasRoleByUserIdEntities(userId);
    if (uhr.isEmpty()) {
      logger.log(Level.INFO, "UserRole does not exist");
      return null;
    } 
    for (UserHasRole t : uhr) {
      int i = t.getUserHasRolePK().getRoleidRole();
      Role r = roleController.findRole(Integer.valueOf(i));
      if (r != null)
        roles.add(r); 
    } 
    return roles;
  }
  
  public List<Role> getUserRolesByApp(int userId, String app) {
    List<Role> roles = new ArrayList<>();
    if (userhasroleController == null)
      userhasroleController = new UserHasRoleJpaController(); 
    if (roleController == null)
      roleController = new RoleJpaController(); 
    UserHasRolePK uhrPK = new UserHasRolePK();
    uhrPK.setUseridUser(userId);
    List<UserHasRole> uhr = userhasroleController.findUserHasRoleByUserIdEntities(userId);
    if (uhr.isEmpty()) {
      logger.log(Level.INFO, "No UserRole exist for user ID: " + Integer.toString(userId));
      return null;
    } 
    for (UserHasRole t : uhr) {
      int i = t.getUserHasRolePK().getRoleidRole();
      Role r = roleController.findRole(Integer.valueOf(i));
      if (r != null && r.getModules() != null && r.getModules().contains(app))
        roles.add(r); 
    } 
    return roles;
  }
  
  public List<Role> getUserRolesByProject(int userId, int projectId) {
    List<Role> roles = new ArrayList<>();
    if (userhasroleController == null)
      userhasroleController = new UserHasRoleJpaController(); 
    if (roleController == null)
      roleController = new RoleJpaController(); 
    UserHasRolePK uhrPK = new UserHasRolePK();
    uhrPK.setUseridUser(userId);
    List<UserHasRole> uhr = userhasroleController.findUserHasRoleByUserIdEntities(userId);
    if (uhr.isEmpty()) {
      logger.log(Level.INFO, "UserProjectRole does not exist");
      return null;
    } 
    for (UserHasRole t : uhr) {
      if (t.getUserHasRolePK().getIdGroup().intValue() == projectId) {
        int i = t.getUserHasRolePK().getRoleidRole();
        Role r = roleController.findRole(Integer.valueOf(i));
        if (r != null)
          roles.add(r); 
      } 
    } 
    return roles;
  }
  
  public boolean checkUserHasProjectRoleExists(int userId, int roleId, int projectId) {
    if (userhasroleController == null)
      userhasroleController = new UserHasRoleJpaController(); 
    UserHasRolePK uhrPK = new UserHasRolePK(userId, roleId, projectId);
    UserHasRole uhr = userhasroleController.findUserHasRole(uhrPK);
    if (uhr == null) {
      logger.log(Level.INFO, "UserProjectRole does not exist");
      return false;
    } 
    logger.log(Level.INFO, "UserProjectRole does exist");
    return true;
  }
  
  private UserHasRole createUserRoles(User user, int id, int currentUserId, int projectId) throws EntityExistsException, NonexistentEntityException, Exception {
    logger.log(Level.SEVERE, "User {0} creating relationship between role {1} and user {2}", new Object[] { Integer.toString(currentUserId), Integer.toString(id), user.getAlias() });
    if (userhasroleController == null)
      userhasroleController = new UserHasRoleJpaController(); 
    if (checkUserHasProjectRoleExists(user.getIdUser(), id, projectId) == true)
      throw new EntityExistsException("The role for user " + user.getAlias() + " already exists."); 
    UserHasRolePK uhrPK = new UserHasRolePK(user.getIdUser(), id, projectId);
    UserHasRole uhr = new UserHasRole(uhrPK);
    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
    uhr.setCreatedTS(currentTimestamp);
    uhr.setUpdatedTS(currentTimestamp);
    uhr.setCreatedBy(Integer.valueOf(currentUserId));
    uhr.setUpdatedBy(Integer.valueOf(currentUserId));
    uhr = userhasroleController.edit(uhr);
    AuditLogger.log(currentUserId, uhr.getUserHasRolePK().getUseridUser(), "create", uhr);
    return uhr;
  }
  
  
  private UserHasRole createUserProjectRoles(User user, int id, int currentUserId, int projectId) throws EntityExistsException, NonexistentEntityException, Exception {
    logger.log(Level.SEVERE, "User {0} creating relationship between role {1} and user {2}", new Object[] { Integer.toString(currentUserId), Integer.toString(id), user.getAlias() });
    
    
    if (userhasprojectroleController == null)
      userhasprojectroleController = new UserHasProjectRoleJpaController(); 
    
    if (checkUserHasProjectRoleExists(user.getIdUser(), id, projectId) == true)
      throw new EntityExistsException("The role for user " + user.getAlias() + " already exists."); 
    
    UserHasRolePK uhrPK = new UserHasRolePK(user.getIdUser(), id, projectId);
    UserHasRole uhr = new UserHasRole(uhrPK);
    
    System.out.println("projectId at Admin Service impl createUserProjectRoles"+projectId);
    System.out.println("id"+id);
    System.out.println("currentUserId"+currentUserId);     
    System.out.println("projectId"+projectId);     
    
            
    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
    uhr.setCreatedTS(currentTimestamp);
    uhr.setUpdatedTS(currentTimestamp);
    uhr.setCreatedBy(Integer.valueOf(currentUserId));
    uhr.setUpdatedBy(Integer.valueOf(currentUserId));
    
    uhr = userhasprojectroleController.edit(uhr);
    
    
    AuditLogger.log(currentUserId, uhr.getUserHasRolePK().getUseridUser(), "create", uhr);
    return uhr;
 
  
  }
  
  public String createUserRoles(String JSONString, Integer currentUserId) {
    String result = "UserRole(s) not created";
    User user = null;
    String dataString = "";
    try {
      JSONObject JSONContentRoot = new JSONObject(JSONString);
      JSONArray userArray = JSONContentRoot.getJSONArray("users");
      for (int ii = 0; ii < userArray.length(); ii++) {
        logger.log(Level.INFO, "Creating Roles for User {0}", userArray.getJSONObject(ii).getString("alias"));
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = userArray.getJSONObject(ii).toString();
        user = (User)gson.fromJson(jString, User.class);
        JSONArray roleArray = JSONContentRoot.getJSONArray("roles");
        for (int i = 0; i < roleArray.length(); i++) {
          UserHasRole uhr = createUserRoles(user, roleArray.getInt(i), currentUserId.intValue(), 0);
          if (i > 0)
            dataString = dataString.concat(","); 
          dataString = dataString + (new JSONObject(uhr)).toString();
        } 
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while creating new User Role: {0} {1}", new Object[] { user.getAlias(), ex });
      result = "DA0010 Exception while creating new User Role: " + ex.getMessage();
    } 
    return wrapAPI("userRoles", dataString, "User Role(s) created", result);
  }
  
  public String createUserProjectRoles(String JSONString, Integer currentUserId) {
    String result = "UserRole(s) not created";
    User user = null;
    String dataString = "";
    try {
      JSONObject JSONContentRoot = new JSONObject(JSONString);
      JSONArray userArray = JSONContentRoot.getJSONArray("users");
      for (int ii = 0; ii < userArray.length(); ii++) {
        logger.log(Level.INFO, "Creating Roles for User {0}", userArray.getJSONObject(ii).getString("alias"));
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = userArray.getJSONObject(ii).toString();
        user = (User)gson.fromJson(jString, User.class);
        JSONArray entitlementsArray = JSONContentRoot.getJSONArray("entitlements");
        for (int j = 0; j < entitlementsArray.length(); j++) {
          JSONObject entObject = entitlementsArray.getJSONObject(j);
          Integer projectId = Integer.valueOf(entObject.getInt("project"));
          JSONArray roleArray = entObject.getJSONArray("roles");
          
          
          for (int i = 0; i < roleArray.length(); i++) {
            UserHasRole uhr = createUserProjectRoles(user, roleArray.getInt(i), currentUserId.intValue(), projectId.intValue());
            
            if (i > 0)
              dataString = dataString.concat(","); 
            dataString = dataString + (new JSONObject(uhr)).toString();
          } 
        } 
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while creating new User Role: {0} {1}", new Object[] { user.getAlias(), ex });
      result = "DA0012 Exception while creating new Project Role for User: " + ex.getMessage();
    } 
    return wrapAPI("userRoles", dataString, "User Role(s) created", result);
  }
  
  private UserHasRole deleteUserRoles(User user, int id, int currentUserId, int projectId) throws NonexistentEntityException, Exception {
    logger.log(Level.SEVERE, "User {0} deleting relationship between role {1} and user {2}", new Object[] { Integer.toString(currentUserId), Integer.toString(id), user.getAlias() });
    
    System.out.println("This is at deleteUserRoles"+user.toString());
    System.out.println("This is at deleteUserRoles"+id);
    System.out.println("This is at deleteUserRoles"+currentUserId);
    System.out.println("This is at deleteUserRoles"+projectId);
    
    if (userhasroleController == null)
      userhasroleController = new UserHasRoleJpaController(); 
    UserHasRolePK uhrPK = new UserHasRolePK(user.getIdUser(), id, projectId);
    UserHasRole uhr = new UserHasRole(uhrPK);
    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
    uhr.setUpdatedTS(currentTimestamp);
    uhr.setUpdatedBy(Integer.valueOf(currentUserId));
    
    System.out.println("This is at destroy");
    userhasroleController.destroy(uhrPK);
    AuditLogger.log(currentUserId, uhr.getUserHasRolePK().getUseridUser(), "delete", uhr);
    System.out.println("This is at destroy" +uhr);
    return uhr;
  }
  
  public String deleteUserRoles(String JSONString, Integer currentUserId) {
    String result = "UserRole(s) not deleted";
    User user = null;
    String dataString = "";
    try {
      JSONObject JSONContentRoot = new JSONObject(JSONString);
      JSONArray userArray = JSONContentRoot.getJSONArray("users");
      for (int ii = 0; ii < userArray.length(); ii++) {
        logger.log(Level.INFO, "Deleting Roles for User {0}", userArray.getJSONObject(ii).getString("alias"));
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = userArray.getJSONObject(ii).toString();
        user = (User)gson.fromJson(jString, User.class);
        JSONArray roleArray = JSONContentRoot.getJSONArray("roles");
        for (int i = 0; i < roleArray.length(); i++) {
          UserHasRole uhr = deleteUserRoles(user, roleArray.getInt(i), currentUserId.intValue(), 0);
          if (i > 0)
            dataString = dataString.concat(","); 
          dataString = dataString + (new JSONObject(uhr)).toString();
        } 
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while deleting  role releationship for User: {0} {1}", new Object[] { user.getAlias(), ex });
      result = "DA0011 Exception while deleting Role releationship for User: " + ex.getMessage();
    } 
    return wrapAPI("userRoles", dataString, "User Role(s) deleted", result);
  }
  
  public String deleteUserProjectRoles(String JSONString, Integer currentUserId) {
    String result = "UserRole(s) not deleted";
    User user = null;
    String dataString = "";
    try {
      JSONObject JSONContentRoot = new JSONObject(JSONString);
      JSONArray userArray = JSONContentRoot.getJSONArray("users");
      for (int ii = 0; ii < userArray.length(); ii++) {
        logger.log(Level.INFO, "Deleting Project Roles for User {0}", userArray.getJSONObject(ii).getString("alias"));
        
        System.out.println("This is at deleteUserProjectRoles JSONString"+JSONString);
        System.out.println("This is at deleteUserProjectRoles currentUserId"+currentUserId);
        
        
        
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = userArray.getJSONObject(ii).toString();
        user = (User)gson.fromJson(jString, User.class);
        JSONArray entitlementsArray = JSONContentRoot.getJSONArray("entitlements");
       
        
        
        for (int j = 0; j < entitlementsArray.length(); j++) {
          JSONObject entObject = entitlementsArray.getJSONObject(j);
          Integer projectId = Integer.valueOf(entObject.getInt("project"));
          JSONArray roleArray = entObject.getJSONArray("roles");
        
          System.out.println("This is inside entitment projectId"+projectId);
          System.out.println("This is inside entitment roleArray"+roleArray);
          
                 
          for (int i = 0; i < roleArray.length(); i++) 
          {
             System.out.println("This is inside role roleArray befire deleting user"+user.toString());
             System.out.println("This is inside role roleArray befire deleting user"+roleArray.getInt(i));
             System.out.println("This is inside role roleArray befire deleting user"+currentUserId.intValue());
             System.out.println("This is inside role roleArray befire deleting user"+projectId.intValue());
              
             UserHasRole uhr = deleteUserRoles(user, roleArray.getInt(i), currentUserId.intValue(), projectId.intValue());
             System.out.println("deleteUserProjectRoles after returning uhr"+uhr.toString());
              
              
            if (i > 0)
              dataString = dataString.concat(","); 
            dataString = dataString + (new JSONObject(uhr)).toString();
          } 
          
          
          
        } 
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while deleting project role releationship for User: {0} {1}", new Object[] { user.getAlias(), ex });
      result = "DA0013 Exception while deleting Project Role releationship for User: " + ex.getMessage();
    } 
    return wrapAPI("userProjectRoles", dataString, "User Project Role(s) deleted", result);
  }
  
  public boolean checkRoleHasPermissionExists(int permId, int roleId) {
    if (permissionController == null)
      permissionController = new PermissionJpaController(); 
    PermissionPK pPK = new PermissionPK(permId, roleId);
    Permission permission = permissionController.findPermission(pPK);
    if (permission == null) {
      logger.log(Level.INFO, "Role Permisison does not exist");
      return false;
    } 
    logger.log(Level.INFO, "Role Permission does exist");
    return true;
  }
  
  private Permission createRolePermissions(int roleId, int permId, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
    logger.log(Level.SEVERE, "User {0} creating relationship between role {1} and permission template {2}", new Object[] { Integer.toString(currentUserId), Integer.toString(roleId), Integer.toString(permId) });
    if (permissionController == null)
      permissionController = new PermissionJpaController(); 
    if (checkRoleHasPermissionExists(permId, roleId) == true)
      throw new EntityExistsException("The permission for role " + Integer.toString(roleId) + " already exists."); 
    if (permissiontemplateController == null)
      permissiontemplateController = new PermissiontemplateJpaController(); 
    PermissionPK pPK = new PermissionPK(permId, roleId);
    Permission permission = new Permission(pPK);
    Permissiontemplate permTemplate = (Permissiontemplate)permissiontemplateController.findObjectEntityById(Integer.valueOf(permId));
    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
    permission.setModule(permTemplate.getModule());
    permission.setCreatedTS(currentTimestamp);
    permission.setUpdatedTS(currentTimestamp);
    permission.setCreatedBy(Integer.valueOf(currentUserId));
    permission.setUpdatedBy(Integer.valueOf(currentUserId));
    permission = permissionController.edit(permission);
    AuditLogger.log(currentUserId, permission.getPermissionPK().getPermTempidPermTemp(), "create", permission);
    updateRoleModules(Integer.valueOf(roleId));
    return permission;
  }
  
  public String createRolePermissions(String JSONString, Integer currentUserId) {
    String result = "RolePermission(s) not created";
    Role role = null;
    String dataString = "";
    try {
      JSONObject JSONContentRoot = new JSONObject(JSONString);
      JSONArray roleArray = JSONContentRoot.getJSONArray("roles");
      for (int ii = 0; ii < roleArray.length(); ii++) {
        logger.log(Level.INFO, "Creating Permissions for Role {0}", roleArray.getJSONObject(ii).getString("name"));
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = roleArray.getJSONObject(ii).toString();
        role = (Role)gson.fromJson(jString, Role.class);
        JSONArray permissionArray = JSONContentRoot.getJSONArray("permissions");
        for (int i = 0; i < permissionArray.length(); i++) {
          Permission permission = createRolePermissions(role.getIdRole().intValue(), permissionArray.getInt(i), currentUserId.intValue());
          if (i > 0)
            dataString = dataString.concat(","); 
          dataString = dataString + (new JSONObject(permission)).toString();
        } 
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while creating new permission for Role: {0} {1}", new Object[] { role.getName(), ex });
      result = "DA0010 Exception while creating new Permission: " + ex.getMessage();
    } 
    return wrapAPI("rolePermissions", dataString, "Role Permissions(s) created", result);
  }
  
  private Permission deleteRolePermissions(int roleId, int permId, int currentUserId) throws NonexistentEntityException, Exception {
    logger.log(Level.SEVERE, "User {0} deleting relationship between role {1} and permission {2}", new Object[] { Integer.toString(currentUserId), Integer.toString(roleId), Integer.toString(permId) });
    if (permissionController == null)
      permissionController = new PermissionJpaController(); 
    PermissionPK pPK = new PermissionPK(permId, roleId);
    Permission permission = new Permission(pPK);
    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
    permission.setUpdatedTS(currentTimestamp);
    permission.setUpdatedBy(Integer.valueOf(currentUserId));
    permissionController.destroy(pPK);
    AuditLogger.log(currentUserId, permission.getPermissionPK().getPermTempidPermTemp(), "delete", permission);
    updateRoleModules(Integer.valueOf(roleId));
    return permission;
  }
  
  public String deleteRolePermissions(String JSONString, Integer currentUserId) {
    String result = "Role Permission(s) not deleted";
    Role role = null;
    String dataString = "";
    try {
      JSONObject JSONContentRoot = new JSONObject(JSONString);
      JSONArray roleArray = JSONContentRoot.getJSONArray("roles");
      for (int ii = 0; ii < roleArray.length(); ii++) {
        logger.log(Level.INFO, "Deleting Permissions for Role {0}", roleArray.getJSONObject(ii).getString("name"));
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = roleArray.getJSONObject(ii).toString();
        role = (Role)gson.fromJson(jString, Role.class);
        JSONArray permissionArray = JSONContentRoot.getJSONArray("permissions");
        for (int i = 0; i < permissionArray.length(); i++) {
          Permission permission = deleteRolePermissions(role.getIdRole().intValue(), permissionArray.getInt(i), currentUserId.intValue());
          if (i > 0)
            dataString = dataString.concat(","); 
          dataString = dataString + (new JSONObject(permission)).toString();
        } 
      } 
      dataString = "[" + dataString + "]";
      result = "Role Permission(s) deleted";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while deleting permission releationship for Role: {0} {1}", new Object[] { role.getName(), ex });
      result = "DA0014 Exception while deleting Permission releationship for Role: " + ex.getMessage();
    } 
    return wrapAPI("rolePermissions", dataString, "Role Permission(s) deleted", result);
  }
  
  public String getEntityGroupInfo(Integer entityId, String type) {
    String dataString = "";
    String result = "No Group Entities found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    JSONArray JSONArrayGroups = null;
    try {
      dataString = getEntityGroupJSON(entityId.intValue(), type);
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while retrieving Groups for Entity {0}.", Integer.toString(entityId.intValue()));
      result = "DA0210 Exception parsing Group into JSON";
    } 
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        JSONArrayGroups = new JSONArray(dataString);
        success.put("success", true);
        status.put("message", JSONArrayGroups.length() + " group entities retrieved");
        success.put("totalCount", Integer.valueOf(JSONArrayGroups.length()));
        success.put("groups", JSONArrayGroups);
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getEntityGroup]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getEntityGroup]";
      return result;
    } 
  }
  
  public List<Object> getEntityGroups(Integer entityId, String type) {
    List<Object> entities = new ArrayList();
    if (grouphasentityController == null)
      grouphasentityController = new GroupHasEntityJpaController(); 
    List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityEntitiesByIdAndType(entityId, type);
    if (ghe.isEmpty())
      return null; 
    if (groupController == null)
      groupController = new GroupJpaController(); 
    for (GroupHasEntity t : ghe) {
      int i = t.getGroupHasEntityPK().getGroupidGroup();
      Object r = groupController.findObjectEntityById(Integer.valueOf(i));
      if (r != null)
        entities.add(r); 
    } 
    return entities;
  }
  
  public List<Object> getGroupEntities(int groupId, PersistenceHelper entityController, String type) {
    List<Object> entities = new ArrayList();
    GroupHasEntityPK shcPK = new GroupHasEntityPK();
    shcPK.setGroupidGroup(groupId);
    if (grouphasentityController == null)
      grouphasentityController = new GroupHasEntityJpaController(); 
    List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityGroupByIdAndType(Integer.valueOf(groupId), type);
    if (ghe.isEmpty())
      return null; 
    for (GroupHasEntity t : ghe) {
      int i = t.getGroupHasEntityPK().getEntityidEntity();
      Object r = entityController.findObjectEntityById(Integer.valueOf(i));
      if (r != null)
        entities.add(r); 
    } 
    return entities;
  }
  
  public boolean checkGroupHasEntityExists(int groupId, int entityId, String type) {
    if (grouphasentityController == null)
      grouphasentityController = new GroupHasEntityJpaController(); 
    List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityEntitiesByIdAndType(Integer.valueOf(entityId), type);
    if (ghe.isEmpty())
      return false; 
    if (groupController == null)
      groupController = new GroupJpaController(); 
    for (GroupHasEntity t : ghe) {
      int i = t.getGroupHasEntityPK().getGroupidGroup();
      if (i == groupId)
        return true; 
    } 
    return false;
  }
  
  private GroupHasEntity createGroupEntities(GroupTable group, int id, String type, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
    logger.log(Level.SEVERE, "User {0} creating relationship between entity {1} and group {2}", new Object[] { Integer.toString(currentUserId), Integer.toString(id), group.getName() });
    
    if (checkGroupHasEntityExists(group.getIdGroup().intValue(), id, type) == true)
      throw new EntityExistsException("The entity for group " + group.getName() + " already exists."); 
    
    GroupHasEntityPK ghePK = new GroupHasEntityPK(group.getIdGroup().intValue(), id);
    GroupHasEntity ghe = new GroupHasEntity(ghePK);
    
    System.out.println("String type at GroupHasEntity createGroupEntities"+type);
    ghe.setType(type);
    ghe.setActive(Boolean.TRUE);
    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
    ghe.setCreatedTS(currentTimestamp);
    ghe.setUpdatedTS(currentTimestamp);
    ghe.setCreatedBy(Integer.valueOf(currentUserId));
    ghe.setUpdatedBy(Integer.valueOf(currentUserId));
    ghe = grouphasentityController.edit(ghe);
    return ghe;
    
  }
  
  public String createGroupEntities(String JSONString, Integer currentUserId) {
    String result = "Group Entities not created";
    GroupTable group = null;
    String dataString = "";
    try {
      JSONObject JSONContentRoot = new JSONObject(JSONString);
      JSONArray anArray = JSONContentRoot.getJSONArray("groups");
      for (int ii = 0; ii < anArray.length(); ii++) {
        logger.log(Level.INFO, "Creating Entities for Group {0}", anArray.getJSONObject(ii).getString("name"));
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = anArray.getJSONObject(ii).toString();
        group = (GroupTable)gson.fromJson(jString, GroupTable.class);
        JSONArray entityArray = JSONContentRoot.getJSONArray("entities");
        for (int i = 0; i < entityArray.length(); i++) {
          JSONObject entity = entityArray.getJSONObject(i);
          String type = entity.getString("type");
          Integer idEntity = Integer.valueOf(entity.getInt("idEntity"));
          GroupHasEntity ghe = createGroupEntities(group, idEntity.intValue(), type, currentUserId.intValue());
          if (i > 0)
            dataString = dataString.concat(","); 
          dataString = dataString + (new JSONObject(ghe)).toString();
        } 
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while creating new Group Entity: {0} {1}", new Object[] { group.getName(), ex });
      result = "DA0108 Exception while creating new Group Entity: " + ex.getMessage();
    } 
    return wrapAPI("groupEntities", dataString, "Group Entities created", result);
  }
  
  public GroupHasEntity createGroupEntities(int idGroup, int id, String type, int currentUserId) throws EntityExistsException, NonexistentEntityException, Exception {
    GroupTable group = new GroupTable();
    group.setIdGroup(Integer.valueOf(idGroup));
    return createGroupEntities(group, id, type, currentUserId);
  }
  
  public String deleteGroupEntities(String JSONString, Integer currentUserId) {
    String result = "Group Entities not deleted";
    GroupTable group = null;
    String dataString = "";
    try {
      JSONObject JSONContentRoot = new JSONObject(JSONString);
      JSONArray anArray = JSONContentRoot.getJSONArray("groups");
      for (int ii = 0; ii < anArray.length(); ii++) {
        logger.log(Level.INFO, "Deleting Entities for Group {0}", anArray.getJSONObject(ii).getString("name"));
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = anArray.getJSONObject(ii).toString();
        group = (GroupTable)gson.fromJson(jString, GroupTable.class);
        JSONArray entityArray = JSONContentRoot.getJSONArray("entities");
        for (int i = 0; i < entityArray.length(); i++) {
          JSONObject entity = entityArray.getJSONObject(i);
          String type = entity.getString("type");
          Integer idEntity = Integer.valueOf(entity.getInt("idEntity"));
          GroupHasEntity ghe = deleteGroupEntity(group.getIdGroup().intValue(), idEntity.intValue(), type, currentUserId.intValue());
          if (i > 0)
            dataString = dataString.concat(","); 
          dataString = dataString + (new JSONObject(ghe)).toString();
        } 
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while deleting category releationship for Study: {0} {1}", new Object[] { group.getName(), ex });
      result = "DA0111 Exception while deleting entity releationship for group: " + ex.getMessage();
    } 
    return wrapAPI("groupEntities", dataString, "Group Entities deleted", result);
  }
  
  public String deleteAllGroupEntities(String JSONString, Integer currentUserId) {
    String result = "Entity groups not deleted";
    GroupTable group = null;
    String dataString = "";
    try {
      JSONObject JSONContentRoot = new JSONObject(JSONString);
      JSONArray anArray = JSONContentRoot.getJSONArray("groups");
      for (int ii = 0; ii < anArray.length(); ii++) {
        logger.log(Level.INFO, "Deleting Entities for Group {0}", anArray.getJSONObject(ii).getString("name"));
        Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss.S").setDateFormat("yyyy-MM-dd").create();
        String jString = anArray.getJSONObject(ii).toString();
        group = (GroupTable)gson.fromJson(jString, GroupTable.class);
        JSONArray entityArray = JSONContentRoot.getJSONArray("entities");
        for (int i = 0; i < entityArray.length(); i++) {
          JSONObject entity = entityArray.getJSONObject(i);
          String type = entity.getString("type");
          Integer idEntity = Integer.valueOf(entity.getInt("idEntity"));
          List<Object> geList = getEntityGroups(idEntity, type);
          for (Object ge : geList) {
            deleteGroupEntity(((GroupTable)ge).getIdGroup().intValue(), idEntity.intValue(), type, currentUserId.intValue());
            if (i > 0)
              dataString = dataString.concat(","); 
            dataString = dataString + (new JSONObject(ge)).toString();
          } 
        } 
      } 
      dataString = "[" + dataString + "]";
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while deleting category releationship for Study: {0} {1}", new Object[] { group.getName(), ex });
      result = "DA0109 Exception while deleting entity releationship for group: " + ex.getMessage();
    } 
    return wrapAPI("groupEntities", dataString, "Entity groups deleted", result);
  }
  
  public GroupHasEntity deleteGroupEntity(int idGroup, int id, String type, int currentUserId) throws NonexistentEntityException, Exception {
    logger.log(Level.SEVERE, "User {0} deleting relationship between entity {1} and group {2}", new Object[] { Integer.toString(currentUserId), Integer.toString(id), Integer.toString(idGroup) });
    GroupHasEntityPK ghePK = new GroupHasEntityPK(idGroup, id);
    GroupHasEntity ghe = new GroupHasEntity(ghePK);
    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
    ghe.setUpdatedTS(currentTimestamp);
    ghe.setUpdatedBy(Integer.valueOf(currentUserId));
    grouphasentityController.destroyByType(ghePK, type);
    return ghe;
  }
  
  public String getGroupEntityInfo(int idGroup, PersistenceHelper entityController, String type) {
    String dataString = "";
    String result = "No Group Entities found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    JSONArray JSONArrayEntities = null;
    try {
      dataString = getGroupEntityJSON(idGroup, entityController, type);
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "DA0110 Exception while retrieving Entities for Group {0}: {1]", new Object[] { Integer.toString(idGroup), ex.getMessage() });
      result = "DA0210 Exception parsing Group into JSON";
    } 
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        JSONArrayEntities = new JSONArray(dataString);
        success.put("success", true);
        status.put("message", JSONArrayEntities.length() + " group entities retrieved");
        success.put("totalCount", Integer.valueOf(JSONArrayEntities.length()));
        success.put("entities", JSONArrayEntities);
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getGroupEntity]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getGroupEntity]";
      return result;
    } 
  }
  
  public String getGroupsByType(int start, int limit, String type, int id, GroupJpaController controller) {
    String dataString = "";
    String result = "No records found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    List<Object> subjects = null;
    int recordCounter = 0;
    logger.log(Level.INFO, "Getting list of {0} starting at {1}", new Object[] { controller.getObjectType(), Integer.toString(start) });
    subjects = controller.findOrgGroupTableEntitiesByType(limit, start, type, Integer.valueOf(id));
    int max = subjects.size();
    for (; recordCounter < max; recordCounter++) {
      dataString = dataString + (new JSONObject(subjects.get(recordCounter))).toString();
      if (recordCounter != max - 1)
        dataString = dataString.concat(","); 
    } 
    dataString = "[" + dataString + "]";
    try {
      if ("".equals(dataString)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        success.put("success", true);
        status.put("message", Integer.valueOf(recordCounter) + " " + controller.getObjectType() + " retrieved");
        success.put("totalCount", Integer.valueOf(controller.getObjectCount(id)));
        success.put(controller.getObjectType(), new JSONArray(dataString));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjectsById]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getObjectsById]: " + ex.getMessage();
      return result;
    } 
  }
  
  public boolean checkUserExists(String userAlias) {
    if (userController == null)
      userController = new UserJpaController(); 
    List<User> user = userController.findUserByAlias(userAlias);
    if (user == null) {
      logger.log(Level.INFO, "User " + userAlias + " does not exist");
      return false;
    } 
    if (user.size() == 0) {
      logger.log(Level.INFO, "User " + userAlias + " does not exist");
      return false;
    } 
    logger.log(Level.INFO, "User " + userAlias + " exists");
    return true;
  }
  
  public String getUserAuthInfo(String userAlias, String currApp, HttpServletRequest request) {
    String dataStringRoles = "";
    String dataStringMenu = "";
    String dataStringPreferences = "{\"graphPackage\":\"\"}";
    int roleRecordCounter = 0;
    int grandRoleRecordCounter = 0;
    String result = "No users found.";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    JSONObject JSONRoles = new JSONObject();
    if (userController == null)
      userController = new UserJpaController(); 
    if (permissionController == null)
      permissionController = new PermissionJpaController(); 
    List<User> users = userController.findUserByAlias(userAlias);
    if (users != null && !users.isEmpty()) {
      logger.log(Level.INFO, "User {0} exists. Retrieving roles", userAlias);
      List<Role> roles = getUserRolesByApp(((User)users.get(0)).getIdUser(), currApp);
      if (roles == null || roles.isEmpty()) {
        logger.log(Level.INFO, "User {0} has no roles for application {1}", new Object[] { ((User)users.get(0)).getAlias(), currApp });
        result = "No Roles assigned to the user for this application.";
      } else {
        dataStringPreferences = "{\"graphPackage\":\"" + ((User)users.get(0)).getGraphPackage() + "\"}";
        dataStringRoles = "[";
        dataStringMenu = "[";
        List<Integer> projects = getUserProjects(((User)users.get(0)).getIdUser());
        if (projects != null)
          for (int j = 0, z = 0; j < projects.size(); j++) {
            roles = getUserRolesByProject(((User)users.get(0)).getIdUser(), ((Integer)projects.get(j)).intValue());
            if (roles != null && !roles.isEmpty()) {
              if (z > 0) {
                dataStringRoles = dataStringRoles.concat(",");
                dataStringMenu = dataStringMenu.concat(",");
              } 
              z++;
              roleRecordCounter = roles.size();
              grandRoleRecordCounter += roleRecordCounter;
              try {
                List<Permissiontemplate> allPermissionTemplates = new ArrayList<>();
                dataStringRoles = dataStringRoles + "{\"projectId\": " + ((Integer)projects.get(j)).toString() + ",\"roles\":[";
                dataStringMenu = dataStringMenu + "{\"projectId\": " + ((Integer)projects.get(j)).toString() + ",\"menu\":\"[";
                for (int i = 0; i < roleRecordCounter; i++) {
                  logger.log(Level.INFO, "Processing role {0}", ((Role)roles.get(i)).getName());
                  if (i > 0)
                    dataStringRoles = dataStringRoles.concat(","); 
                  dataStringRoles = dataStringRoles + (new JSONObject(roles.get(i))).toString();
                  List<Permission> permissions = permissionController.findPermissionByRoleId(((Role)roles.get(i)).getIdRole());
                  List<Permissiontemplate> permTemplates = getRolePermissions(permissions);
                  allPermissionTemplates = Util.mergeLists(new List[] { allPermissionTemplates, permTemplates });
                  dataStringRoles = Util.removeLastChar(dataStringRoles) + ",\"permissions\":" + getRolePermissionsJSON(permTemplates) + "}";
                } 
                dataStringRoles = dataStringRoles + "]}";
                allPermissionTemplates = (List<Permissiontemplate>)allPermissionTemplates.stream().distinct().collect(Collectors.toList());
                dataStringMenu = dataStringMenu + buildMenuFromPermissions(allPermissionTemplates);
                dataStringMenu = dataStringMenu + "]\"}";
                Auth.addPermissionsToSession(request, dataStringRoles);
              } catch (Exception ex) {
                Logger.getLogger(AdminServiceImpl.class.getName()).log(Level.SEVERE, (String)null, ex);
                result = "DA0027 Exception parsing Role into JSON";
              } 
            } 
          }  
        dataStringRoles = dataStringRoles + "]";
        dataStringMenu = dataStringMenu + "]";
      } 
    } 
    try {
      if ("".equals(dataStringRoles)) {
        success.put("success", false);
        status.put("message", result);
        success.put("totalCount", "0");
        success.put("mainMenu", "");
        JSONRoles.put("projects", new JSONArray("[]"));
        success.put("userAuth", JSONRoles);
        success.put("userPref", new JSONObject(dataStringPreferences));
      } else {
        success.put("success", true);
        status.put("message", grandRoleRecordCounter + " userAuth entries retrieved");
        success.put("totalCount", Integer.valueOf(grandRoleRecordCounter));
        success.put("mainMenu", new JSONArray(dataStringMenu));
        JSONRoles.put("projects", new JSONArray(dataStringRoles));
        success.put("userAuth", JSONRoles);
        success.put("userPref", new JSONObject(dataStringPreferences));
        success.put("userAlias", userAlias);
        success.put("userId", ((User)users.get(0)).getIdUser());
        success.put("orgId", ((User)users.get(0)).getOrganization_idOrganization());
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getObjects]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getObjects]";
      return result;
    } 
  }
  
  private List<Permissiontemplate> getRolePermissions(List<Permission> permissions) {
    List<Permissiontemplate> permissiontemplates = new ArrayList<>();
    if (permissiontemplateController == null)
      permissiontemplateController = new PermissiontemplateJpaController(); 
    for (Permission t : permissions) {
      int i = t.getPermissionPK().getPermTempidPermTemp();
      Permissiontemplate p = permissiontemplateController.findPermissiontemplate(Integer.valueOf(i));
      if (p != null)
        permissiontemplates.add(p); 
    } 
    Collections.sort(permissiontemplates, (Comparator<? super Permissiontemplate>)new PermissiontemplateComp());
    return permissiontemplates;
  }
  
  private String getRolePermissionsJSON(int idRole) {
    String dataStringPermissions = "";
    int permissionCounter = 0;
    logger.log(Level.INFO, "Retrieving Permissions for Role {0}", Integer.toString(idRole));
    if (permissionController == null)
      permissionController = new PermissionJpaController(); 
    List<Permission> permissions = permissionController.findPermissionByRoleId(Integer.valueOf(idRole));
    if (permissions != null && !permissions.isEmpty()) {
      logger.log(Level.INFO, "Role {0} has permissions. Retrieving permission data.", Integer.toString(idRole));
      List<Permissiontemplate> permtemp = getRolePermissions(permissions);
      permissionCounter = permtemp.size();
      for (int i = 0; i < permissionCounter; i++) {
        if (i > 0)
          dataStringPermissions = dataStringPermissions.concat(","); 
        dataStringPermissions = dataStringPermissions + (new JSONObject(permtemp.get(i))).toString();
      } 
      dataStringPermissions = "[" + dataStringPermissions + "]";
      return dataStringPermissions;
    } 
    return dataStringPermissions;
  }
  
  private String getRolePermissionsJSON(List<Permissiontemplate> permtemp) {
    String dataStringPermissions = "";
    int permissionCounter = 0;
    logger.log(Level.INFO, "Retrieving permission data.");
    permissionCounter = permtemp.size();
    for (int i = 0; i < permissionCounter; i++) {
      if (i > 0)
        dataStringPermissions = dataStringPermissions.concat(","); 
      dataStringPermissions = dataStringPermissions + (new JSONObject(permtemp.get(i))).toString();
    } 
    dataStringPermissions = "[" + dataStringPermissions + "]";
    return dataStringPermissions;
  }
  
  public String getRolePermissionInfo(int idRole) {
    String dataStringPermissions = "";
    String result = "No permissions found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    JSONObject JSONPermissions = new JSONObject();
    JSONArray JSONArrayPermissions = null;
    logger.log(Level.INFO, "Retrieving Permission for Role {0}.", Integer.toString(idRole));
    try {
      dataStringPermissions = getRolePermissionsJSON(idRole);
    } catch (Exception ex) {
      Logger.getLogger(AdminServiceImpl.class.getName()).log(Level.SEVERE, (String)null, ex);
      result = "DA0027 Exception parsing Permission into JSON";
    } 
    try {
      if ("".equals(dataStringPermissions)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        JSONArrayPermissions = new JSONArray(dataStringPermissions);
        success.put("success", true);
        status.put("message", Integer.valueOf(JSONArrayPermissions.length()) + " role permission entries retrieved");
        success.put("totalCount", Integer.valueOf(JSONArrayPermissions.length()));
        JSONPermissions.put("permissions", JSONArrayPermissions);
        success.put("rolePermissions", JSONPermissions);
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getRolePermissions]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getRolePermissions]";
      return result;
    } 
  }
  
  private String getEntityGroupJSON(int idEntity, String type) {
    String dataString = "";
    int categoryCounter = 0;
    List<Object> entities = getEntityGroups(Integer.valueOf(idEntity), type);
    if (entities != null && !entities.isEmpty()) {
      categoryCounter = entities.size();
      for (int i = 0; i < categoryCounter; i++) {
        if (i > 0)
          dataString = dataString.concat(","); 
        dataString = dataString + (new JSONObject(entities.get(i))).toString();
      } 
      dataString = "[" + dataString + "]";
      return dataString;
    } 
    return dataString;
  }
  
  private String getGroupEntityJSON(int idGroup, PersistenceHelper entityController, String type) {
    String dataString = "";
    int counter = 0;
    List<Object> entities = getGroupEntities(idGroup, entityController, type);
    if (entities != null && !entities.isEmpty()) {
      counter = entities.size();
      for (int i = 0; i < counter; i++) {
        if (i > 0)
          dataString = dataString.concat(","); 
        dataString = dataString + (new JSONObject(entities.get(i))).toString();
      } 
      dataString = "[" + dataString + "]";
      return dataString;
    } 
    return dataString;
  }
  
  public String getProjectRoles(int idUser) {
    String dataStringPermissions = "";
    String result = "No Project Roles found";
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    logger.log(Level.INFO, "Retrieving Project/Roles for User {0}.", Integer.toString(idUser));
    try {
      dataStringPermissions = getProjectRolesJSON(idUser);
    } catch (Exception ex) {
      Logger.getLogger(AdminServiceImpl.class.getName()).log(Level.SEVERE, (String)null, ex);
      result = "DA0027 Exception parsing Project/Roles into JSON";
    } 
    try {
      if ("".equals(dataStringPermissions)) {
        success.put("success", false);
        status.put("message", result);
      } else {
        JSONArray JSONArrayEntitlements = new JSONArray(dataStringPermissions);
        success.put("success", true);
        status.put("message", JSONArrayEntitlements.length() + " project roles entries retrieved");
        success.put("totalCount", Integer.valueOf(JSONArrayEntitlements.length()));
        success.put("projectRoles", JSONArrayEntitlements);
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin.getProjectRoles]: ", (Throwable)ex);
      result = "JSON error in WS method [admin.getProjectRoles]";
      return result;
    } 
  }
  
  private String getProjectRolesJSON(int idUser) {
    String dataString = "";
    logger.log(Level.INFO, "Retrieving Project/Roles for User {0}", Integer.toString(idUser));
    if (userhasprojectroleController == null)
      userhasprojectroleController = new UserHasProjectRoleJpaController(); 
    List<UserHasRole> uhr = userhasprojectroleController.findUserHasRoleByUserIdEntities(idUser);
    if (uhr != null && !uhr.isEmpty()) {
      logger.log(Level.INFO, "User {0} has projects/roles. Retrieving entitlements data.", Integer.toString(idUser));
      int uhrCounter = uhr.size();
      for (int i = 0; i < uhrCounter; i++) {
        if (i > 0)
          dataString = dataString.concat(","); 
        dataString = dataString + "{\"UseridUser\":" + Integer.toString(((UserHasRole)uhr.get(i)).getUserHasRolePK().getUseridUser()) + ",";
        dataString = dataString + "\"RoleidRole\":" + Integer.toString(((UserHasRole)uhr.get(i)).getUserHasRolePK().getRoleidRole()) + ",";
        dataString = dataString + "\"idGroup\":" + ((UserHasRole)uhr.get(i)).getUserHasRolePK().getIdGroup().toString() + ",";
        dataString = dataString + "\"createdBy\":" + ((UserHasRole)uhr.get(i)).getCreatedBy().toString() + ",";
        dataString = dataString + "\"createdTS\":\"" + ((UserHasRole)uhr.get(i)).getCreatedTS().toString() + "\",";
        dataString = dataString + "\"updatedBy\":" + ((UserHasRole)uhr.get(i)).getUpdatedBy().toString() + ",";
        dataString = dataString + "\"updatedTS\":\"" + ((UserHasRole)uhr.get(i)).getUpdatedTS().toString() + "\"}";
      } 
      dataString = "[" + dataString + "]";
      return dataString;
    } 
    return dataString;
  }
  
  private boolean updateRoleModules(Integer roleId) throws Exception {
    if (roleController == null)
      roleController = new RoleJpaController(); 
    if (permissionController == null)
      permissionController = new PermissionJpaController(); 
    Role role = roleController.findRole(roleId);
    List<Permission> pList = permissionController.findPermissionByRoleId(roleId);
    String modules = "";
    for (int i = 0; i < pList.size(); i++)
      modules = modules + ((Permission)pList.get(i)).getModule().toString() + ","; 
    role.setModules(modules);
    roleController.edit(role);
    return true;
  }
  
  public String login(String userAlias, String password, String app, HttpServletRequest request) {
    logger.log(Level.INFO, "Logging in user {0}", userAlias);
    User user = null;
    if (Auth.verifyPassword(userAlias, password)) {
      logger.log(Level.INFO, "User {0} authenticated", userAlias);
      if (userController == null)
        userController = new UserJpaController(); 
      ArrayList<User> users = new ArrayList();
      users = (ArrayList)userController.findUserByAlias(userAlias);
      user = users.get(0);
      if (user.getPassExpirationTS().before(new Date())) {
        logger.log(Level.INFO, "Password expired on {0}", user.getPassExpirationTS().toString());
        return "Password expired. Contact your System Administrator to extend permissions.";
      } 
      if (!user.getActive().booleanValue()) {
        logger.log(Level.INFO, "User {0} is not active", user.getAlias());
        return "User profile not active";
      } 
      String allApps = "";
      
      if (app != null) {
        List<Role> roles = getUserRoles(user.getIdUser());
        
        if (roles == null || roles.isEmpty()) {
          logger.log(Level.INFO, "User {0} has no Roles", user.getAlias());
          return "No Roles assigned to the user for this application.";
        } 
        
        for (int i = 0; i < roles.size(); i++)
          allApps = allApps + ((Role)roles.get(i)).getModules() + ","; 
        
        
        if (!allApps.contains(app + ",")) {
          logger.log(Level.INFO, "User {0} is not authorized for application {1}", new Object[] { user.getAlias(), app });
          return "User not authorized for this application.";
        } 
      } else {
        app = "0";
      } 
      Auth.createUserSession(user, app, allApps, request);
      return "";
    } 
    return "User not authenticated.";
  }
  
  public boolean logout(HttpServletRequest request) {
    logger.log(Level.INFO, "Terminating session {0}", request.getSession().toString());
    if (Auth.killSession(request))
      return true; 
    return false;
  }
  
  public String authorize(String userAlias, String password, String organization) {
    logger.log(Level.INFO, "Authorizing User {0} for Organization {1}", new Object[] { userAlias, organization });
    User user = null;
    ArrayList<User> users = new ArrayList();
    if (userController == null)
      userController = new UserJpaController(); 
    users = (ArrayList)userController.findUserByAlias(userAlias);
    user = users.get(0);
    if (Auth.verifyPassword(userAlias, password) && user.getActive().booleanValue() == true) {
      logger.log(Level.INFO, "User {0} authenticated.", userAlias);
      if (organizationController == null)
        organizationController = new OrganizationJpaController(); 
      if (user.getPassExpirationTS().before(new Date())) {
        logger.log(Level.INFO, "Password expired.", user.getPassExpirationTS().toString());
        return "DA0001 Password expired.";
      } 
      Organization org = organizationController.findOrganization(user.getOrganization_idOrganization());
      if (org == null) {
        logger.log(Level.INFO, "Organization with ID {0} not authorized. Not in database.", org.getIdOrganization().toString());
        return "DA0002 User not authorized.";
      } 
      if (!org.getIdOrganization().toString().equals(organization)) {
        logger.log(Level.INFO, "Organization with ID {0} not authorized. Not matching request id {1}", new Object[] { org.getIdOrganization().toString(), organization });
        return "DA0003 User not authorized.";
      } 
      logger.log(Level.INFO, "User from Organization with ID {0} authorized", org.getIdOrganization().toString());
      return "OK";
    } 
    return "";
  }
  
  public User goPseudo(String JSONString, Integer currentUserId) {
    User user = new User();
    if (userController == null)
      userController = new UserJpaController(); 
    try {
      JSONObject userAuth = new JSONObject(JSONString);
      JSONArray users = userAuth.getJSONArray("userAuth");
      JSONObject goUser = users.optJSONObject(0);
      user = userController.findUser(Integer.valueOf(goUser.getInt("idUser")));
      AuditLogger.log(currentUserId.intValue(), user.getIdUser(), "pseudo", user);
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "Exception while switching users from superuser {0} to {1}: {2}", new Object[] { currentUserId.toString(), Integer.valueOf(user.getIdUser()), ex });
    } 
    return user;
  }
  
  private String buildMenuFromPermissions(List<Permissiontemplate> permTemplate) {
    String menuString = "";
    int i = 0;
    for (Permissiontemplate pt : permTemplate) {
      if (pt.getActive().booleanValue() == true && "menu".equals(pt.getType().toLowerCase())) {
        if (i++ > 0)
          menuString = menuString + ","; 
        menuString = menuString + "{text:'" + pt.getName() + "',iconCls:'" + pt.getTag().toLowerCase() + "-icon16',controller:'delta3.controller." + pt.getTag() + "'}";
      } 
    } 
    return menuString;
  }
  
  public String changeLoggingLevel(String levelString, Integer currUserId) {
    String result = "Logging level not changed";
    Level newLevel = Level.INFO;
    try {
      newLevel = Level.parse(levelString);
    } catch (Exception e) {
      result = "Couldn't set Logging level to: " + levelString;
    } 
    Logger rootLogger = Logger.getLogger("");
    for (Handler handler : rootLogger.getHandlers())
      handler.setLevel(newLevel); 
    rootLogger.setLevel(newLevel);
    AuditLogger.log(currUserId.intValue(), newLevel.intValue(), "loggingLevel", newLevel);
    return wrapAPI("loggingLevel", "[]", "Logging level changed to " + newLevel.getName(), result);
  }
  
  public String getLoggingLevel() {
    String result = "";
    Logger rootLogger = Logger.getLogger("");
    for (Handler handler : rootLogger.getHandlers()) {
      Level level = handler.getLevel();
      result = result + "\"" + level.getName() + "\",";
    } 
    result = Util.removeLastChar(result);
    return wrapAPI("loggingLevel", "[" + result + "]", "Logging level retrieved", "Logging level not retrieved");
  }
  
  
  
  
  
  
  
  
  
  public String constructWhereClause(String filter, String sort) {
    String whereClause = "";
    try {
      if (filter != null) {
        JSONArray filters = new JSONArray(filter);
        for (int i = 0; i < filters.length(); i++) {
          whereClause = whereClause + " AND ";
          JSONObject filterObject = filters.getJSONObject(i);
          String property = filterObject.getString("property");
          String value = filterObject.getString("value");
          boolean anyMatch = filterObject.getBoolean("anyMatch");
          if (anyMatch) {
            whereClause = whereClause + property + " LIKE '%" + value + "%'";
          } else {
            whereClause = whereClause + property + " = '" + value + "'";
          } 
        } 
      } 
      if (sort != null) {
        JSONArray sorts = new JSONArray(sort);
        for (int i = 0; i < sorts.length(); i++) {
          if (i > 0) {
            whereClause = whereClause + " AND ";
          } else {
            whereClause = whereClause + " ORDER BY ";
          } 
          JSONObject sortObject = sorts.getJSONObject(i);
          String property = sortObject.getString("property");
          String direction = sortObject.getString("direction");
          whereClause = whereClause + property + " " + direction;
        } 
      } 
      return whereClause;
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error while processing " + filter + sort, ex.getMessage());
      return null;
    } 
  }
  
  public String wrapAPI(String objectId, String result, String msgSuccess, String msgFailure) {
    JSONObject success = new JSONObject();
    JSONObject status = new JSONObject();
    try {
      if ("".equals(result)) {
        success.put("success", false);
        status.put("message", msgFailure);
      } else {
        success.put("success", true);
        status.put("message", msgSuccess);
        success.put(objectId, new JSONArray(result));
      } 
      success.put("status", status);
      return success.toString();
    } catch (JSONException ex) {
      logger.log(Level.SEVERE, "JSON error in WS method [admin." + objectId + "] while processing " + msgFailure, (Throwable)ex);
      return msgFailure;
    } 
  }
}
