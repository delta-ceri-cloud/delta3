/*
 * This method generates Flat Table
 * It is initiated by Scheduler
 */

package com.ceri.delta.server.processor;

import com.ceri.delta.entity.model.Model;
import com.ceri.delta.entity.study.Study;
import com.ceri.delta.jpa.model.ModelJpaController;
import com.ceri.delta.jpa.study.StudyJpaController;
import com.ceri.delta.server.ModelServiceImpl;
import com.ceri.delta.server.StudyServiceImpl;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CERI Lahey
 */
public class ProcessModel implements Runnable {
    private Integer modelId, userId;
    private StudyServiceImpl studyService = null;
    private ModelServiceImpl modelService = null;
    private StudyJpaController studyController = null;
    private ModelJpaController modelController = null;
    private static final Logger logger = Logger.getLogger(ProcessVirtual0.class.getName());        
    
    public ProcessModel() {
    }
    
    public void init(int objectId, int idUser) {
        modelId = objectId;
        userId = idUser;
        studyService = new StudyServiceImpl();
        modelService = new ModelServiceImpl();
        studyController = new StudyJpaController();
        modelController = new ModelJpaController();
        System.out.println("This is at Init at model processor modelId"+modelId+" userId"+userId);
        logger.log(Level.SEVERE,"ProcessStudyAll initialized.");        
    }  
    
    public void run() { 
       // Study study = (Study) studyController.findStudy(studyId);      
        Model model= (Model)modelController.findModel(modelId);     
        System.out.println("This is model which is going to run"+model.toString());
        //Model model = modelController.findModel(study.getIdModel());
        //System.out.println("This is model which is wanted to run"+model.toString());
        //System.out.println("This is at void run study found :"+study+" model found"+model);
        if ( model != null ) {
            logger.log(Level.SEVERE,"ProcessModel starting Flat Table processing.");
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
            
            System.out.println("Model is not null and starting flat table generation model found"+model);
            modelService.processFlatTableAll(model, currentTimestamp.toString(), userId,  true);   
            
        } else {
            logger.log(Level.SEVERE,"ProcessModel abandoned. No model found for this study.");
            
        }
    }
}
