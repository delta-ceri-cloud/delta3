/*
 * This method generates Flat Table and executes study.
 * It is initiated by Scheduler
 */

package com.ceri.delta.server.processor;

import com.ceri.delta.entity.model.Model;
import com.ceri.delta.entity.study.Study;
import com.ceri.delta.jpa.model.ModelJpaController;
import com.ceri.delta.jpa.study.StudyJpaController;
import com.ceri.delta.property.Property;
import com.ceri.delta.server.ModelServiceImpl;
import com.ceri.delta.server.StudyServiceImpl;
import com.ceri.delta.util.JSON.JSONException;
import com.ceri.delta.util.JSON.JSONObject;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CERI Lahey
 */
public class ProcessStudyAll implements Runnable {
    private Integer studyId, userId;
    private StudyServiceImpl studyService = null;
    private ModelServiceImpl modelService = null;
    private StudyJpaController studyController = null;
    private ModelJpaController modelController = null;
    private static final Logger logger = Logger.getLogger(ProcessVirtual0.class.getName());        
    
    public ProcessStudyAll() {
    }
    
    public void init(int objectId, int idUser) {
        studyId = objectId;
        userId = idUser;
        studyService = new StudyServiceImpl();
        modelService = new ModelServiceImpl();
        studyController = new StudyJpaController();
        modelController = new ModelJpaController();
        logger.log(Level.SEVERE,"ProcessStudyAll initialized.");        
    }  
    
    public void run() { 
        Study study = (Study) studyController.findStudy(studyId);       
        
        Model model = modelController.findModel(study.getIdModel());
       

        if ( model != null ) {
            logger.log(Level.SEVERE,"ProcessStudyAll starting Flat Table processing.");
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
            
            logger.log(Level.SEVERE,"Model is not null and starting flat table generation :");
            modelService.processFlatTableAll(model, currentTimestamp.toString(), userId,  true);   
            
            logger.log(Level.SEVERE,"ProcessStudyAll starting Study execution.");
            study.setStatus("New");
            studyService.executeStudy(study, userId, userId, false);
            studyService = null; 
            modelService = null; 
            studyId = null; 
            userId = null; 
            modelController = null; 
            studyController = null;     
            logger.log(Level.SEVERE,"ProcessStudyAll finished.");
            
        } else {
            logger.log(Level.SEVERE,"ProcessStudyAll abandoned. No model found for this study.");
            study.setStatus("PR0001 Error: no model found");
        }
    }
    
    
}
