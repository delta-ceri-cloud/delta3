/*
 * This class is used to start asynch processing of Atomic Fields
 */

package com.ceri.delta.server.processor;

import com.ceri.delta.server.ModelServiceImpl;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;

/**
 *
 * @author Boston Advanced Analytics
 */
public class ProcessVirtual1 implements Runnable {
    private AsyncContext asyncContext;
    private String data, timeStampId;
    private Integer userId;
    private ModelServiceImpl modelService = null;
    private static final Logger logger = Logger.getLogger(ProcessVirtual1.class.getName());        

    public ProcessVirtual1(AsyncContext asyncContext, String load, String timeStamp, Integer id) {
        this.asyncContext = asyncContext;
        data = load;
        timeStampId = timeStamp;
        userId = id;
        modelService = new ModelServiceImpl();
        logger.log(Level.SEVERE,"ProcessVirtual1 initialized. Data to be processed: " + data);
    }

    public void run() {
        if ( asyncContext != null ) {
            asyncContext.complete();    
        }     
        modelService.processSecondaryVirtualFields(data, timeStampId, userId);   
        modelService = null; userId = null; data = null; asyncContext = null;
        logger.log(Level.SEVERE,"ProcessVirtual1 finished.");
    }     
}
