/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.server.rest;

import com.ceri.delta.entity.Method;
import com.ceri.delta.entity.events.Eventtemplate;
import com.ceri.delta.entity.process.Process;
import com.ceri.delta.entity.study.Stat;
import com.ceri.delta.jpa.process.ProcessJpaController;
import com.ceri.delta.jpa.study.StatJpaController;
import com.ceri.delta.security.Auth;
import com.ceri.delta.server.AdminServiceImpl;
import com.ceri.delta.server.StatServiceImpl;
import com.ceri.delta.server.EventServiceImpl;
import com.ceri.delta.util.JSON.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Coping Systems Inc.
 */
@Path("process")
@Consumes({"text/plain","text/html","application/html","application/xhtml","application/x-www-form-urlencoded","application/json"})
public class Delta3StatisticsResource {
    private static final Logger logger = Logger.getLogger(Delta3StatisticsResource.class.getName());
    private static StatServiceImpl statService; 
    private static AdminServiceImpl adminService;  
    private static EventServiceImpl eventService;      
    private static ProcessJpaController processController;      
    private static StatJpaController statController;          
    private String _corsHeaders;    


    /**
     * Creates a new instance of Delta3ModelResource
     */
    public Delta3StatisticsResource() {
        if ( adminService == null ) {
            adminService = new AdminServiceImpl();
        }      
        if ( statService == null ) {
            statService = new StatServiceImpl();
        }    
        if ( eventService == null ) {
            eventService = new EventServiceImpl();
        }         
        if ( processController == null ) {
            processController = new ProcessJpaController();
        }  
        if ( statController == null ) {
            statController = new StatJpaController();
        }        
    }

    @Path("/callLocalStatPackage")
    @GET  
    @Produces("application/json")
    public Response callLocalStatPackage(@QueryParam(value = "cmd") final String command, @QueryParam(value = "data") final String data, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.callLocalStatPackage]->" + command;
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSessionLight(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.INFO, logId + " request from user {0}", userId.toString()); 
        result = statService.callLocalStatPackage(command, data); 
        return Response.ok(result).build();   
    }
    
    @Path("/initializeStatPackage")
    @GET  
    @Produces("application/json")
    public Response initializeStatPackage(@QueryParam(value = "processId") final String processId, @QueryParam(value = "guid") final String guid, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.initializeExternalStatPackage]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
        String alias = Auth.getUserAliasFromSession(request);

        result = statService.initializeExternalStatPackage(alias);                             
        return Response.ok(result).build();   
    }
    
    
    
    @Path("/pingStatPackage")
    @GET  
    @Produces("application/json")
    public Response pingStatPackage(@QueryParam(value = "processId") final String processId, @QueryParam(value = "guid") final String guid, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.pingStatPackage]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSessionLight(request);
        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.INFO, logId + " request from user {0}", userId.toString()); 
        result = statService.pingStatPackage();  
        
        return Response.ok(result).build();   
    }

    @Path("/getStatResults")
    @POST  
    @Produces("application/json")
    public Response getStatResults(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getStatResults]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = statService.getStatResults(decodedContent);                             
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/getStatResult")
    @GET  
    @Produces("application/json")
    public Response getStatResult(@QueryParam(value = "processId") final String processId, @QueryParam(value = "guid") final String guid, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getStatResult]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
        String alias = Auth.getUserAliasFromSession(request);

        result = statService.getStatResult(processId, guid, alias);                             
        return Response.ok(result).build();         
    }

    @Path("/getJobStatus")
    @GET  
    @Produces("application/json")
    public Response getJobStatus(@QueryParam(value = "processId") final String processId, @QueryParam(value = "guid") final String guid, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getJobStatus]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
        String alias = Auth.getUserAliasFromSession(request);

        result = statService.getJobStatus(processId, guid, alias);                             
        return Response.ok(result).build();   
    }
 
    @Path("/saveResults")
    @POST
    @Produces("application/json")
    public Response saveResults(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.saveResults]";
        String result = logId + " failed";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = statService.saveResults(decodedContent, organizationId, userId);
            return Response.ok(result).build();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }


    @Path("/saveResultsFile")
    @POST
    @Consumes("multipart/form-data")
    @Produces("application/json")
    public Response saveResultsFile(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.importResultsFile]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        try {
            int beginIndex = payload.indexOf("{");
            int endIndex = payload.lastIndexOf("\r\n------");
            String decodedContent = payload.substring(beginIndex, endIndex);
            String encodedContent = decodedContent.replaceAll("[\\\"]","\\\\\"");
            String content = "{\"processes\":[{\"idProcess\":0,\"idStudy\":0,\"idModel\":0,\"idFilter\":0,\"status\":\"Imported\",\"progress\":100,\"active\":true,\"manualExecution\":false,\"description\":\"" 
                                + "imported\",\"name\":\"External Study\",\"message\":\"\",\"data\":\"" + encodedContent + "\"}]}";           
            result = statService.saveResults(content, organizationId, userId);                  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while importing results from file: ", ex);
            result = "File import error: " + ex.getMessage();
        }        
        return Response.ok(result).build(); 
    } 

    
    @Path("/receiveResults")
    @POST
    @Produces("application/json")
    public Response recieveResults(String payload, @Context HttpServletRequest request) {
        logger.log(Level.INFO,"AT receive result================");
        
        String UNAME = request.getParameter("Username");
        String PASS = request.getParameter("Password");
        System.out.println("AT receive result================"+payload);
        
        //byte[] decodedBytesUNAME = Base64.getDecoder().decode(UNAME);
        //String decodedStringUNAME = new String(decodedBytesUNAME);
        
        //byte[] decodedBytesPASS = Base64.getDecoder().decode(PASS);
        //String decodedStringPASS = new String(decodedBytesPASS);
        
        //String userAlias = decodedStringUNAME;
        //String password =  decodedStringPASS;
        
        String userAlias = UNAME;
        String password =  PASS;
        
        System.out.println("Result of UNAME :"+UNAME);
        System.out.println("Result of PASS :"+PASS);

        
        String logId = "DELTA3 WS method [process.receiveResults]";
        String result = "ST0000 " + logId + " failed. ";     
        
        if (Auth.verifyPassword(userAlias, password)!=true) 
        {    
            
            logger.log(Level.INFO,"Username and Password is NOT verified"); 
            result = "ST0002 " + logId + " User not authorized.";
        }
        else{
            
            logger.log(Level.INFO,"Username and Password is verified"); 
            logger.log(Level.INFO, "AT /receiveResults trying to login User {0} from receiveResult API",userAlias);
            
            String resultLogin = adminService.login(userAlias, password, "1", request);
            logger.log(Level.INFO,"Result of resultLogin {0}",resultLogin); 
            System.out.println("Result of resultLogin :"+resultLogin);
            
             if (!resultLogin.equals("")) 
             {
               //System.out.println("CAN NOT LOGIN Success false");        
               logger.log(Level.SEVERE,"CAN NOT LOGIN Success false");
               result = "ST0002 " + logId + " User not authorized.";
             }
             
             else{
              logger.log(Level.INFO,"LOGIN IS SUCCESSFULL"); 
              //System.out.println("LOGIN IS SUCCESSFULL");              
              //System.out.println("getAttribute userAlias " +request.getSession().getAttribute("userAlias"));
              //System.out.println("getAttribute userID " +request.getSession().getAttribute("userId"));
              //System.out.println("getAttribute orgID "+request.getSession(false).getAttribute("organizationId"));
              String sessionId = request.getSession(false).getId();
              Integer userId = (Integer)request.getSession(false).getAttribute("userId");
              Integer organizationId = (Integer)request.getSession(false).getAttribute("organizationId");
              
              if (userId == null) {    
                    return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
                }
                logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
                if ( organizationId != 1 ) {
                    result = "ST0002 " + logId + " User not authorized.";
                    
                }
              
                try {
                    result = statService.receiveResults(payload);    
                    return Response.ok(result).build();  
                } catch (Exception ex) 
                {
                    logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
                    result += ex.getMessage();
                } 
             }
        }
        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
        
        /*
        //Below code is commented and working without encoding
        System.out.println("AT receive result================");
        System.out.println("This is payload" +payload);
        System.out.println("params"+request.getParameter("Username")); 
        
        System.out.println("This is get attribute" +request.getSession().getAttribute("userAlias"));
        System.out.println("This is get attribute" +request);
        String logId = "DELTA3 WS method [process.receiveResults]";
        String result = "ST0000 " + logId + " failed. ";     
        
        //if user is other than cso admin use this
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        System.out.println("This is userId" +userId);
        System.out.println("This is organizationId" +organizationId);    
        if(request.getParameter("Username").equals("csoadmin")==false)
        {
           
            if (userId == null) {    
                return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
            }
            logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
            if ( organizationId != 1 ) {
                    result = "ST0002 " + logId + " User not authorized.";
            } 

        }
        
        else {        
            try {
                System.out.println("This is csoadmin, directly use function");
                //String decodedContent = URLDecoder.decode(payload, "UTF-8");           
                result = statService.receiveResults(payload);    
                System.out.println("This is aafter stat service receiver result\n");
                System.out.println("this is result" +result);
                System.out.println("this is response string "+Response.ok(result).build());
                return Response.ok(result).build();  
            } catch (Exception ex) {
                logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
                result += ex.getMessage();
            }       
        }
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    
        */
    
    }
    
    @Path("/processResults")
    @POST
    @Produces("application/json")
    public Response processResults(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.processResults]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = statService.processResults(decodedContent);                
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    //-------------------------------------------------------------------------- New process added for latest run
    @GET
    @Path("/getLatestProcess")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getLatestProcess(@QueryParam(value = "studyId") final String studyId, @Context HttpServletRequest request) throws InterruptedException {
        String logId = "DELTA3 WS method [process.getLatestProcess]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());                           
      
        result = statService.getLatestProcess(Integer.parseInt(studyId), organizationId);
        return Response.ok(result).build();  
    }

    
    //-------------------------------------------------------------------------- ProcessesGrid CRUD
        /**
     * Retrieves representation of an instance of com.ceri.delta.server.rest.Delta3AdminResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getProcesses")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getProcesses(@QueryParam(value = "filter") final String filter, @QueryParam(value = "sort") final String sort, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @QueryParam(value = "group") final String group, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getProcesses]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());                           
        result = adminService.getOrgObjectsExtendedGroup(Integer.parseInt(start), Integer.parseInt(limit), filter, sort, Integer.parseInt(group), organizationId, processController, new Process());
        return Response.ok(result).build();  
    }    
  
    @Path("/createProcesses")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createProcesses(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.createProcesses]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, processController, new Process());
            } else {
                result = adminService.createOrgObjects(decodedContent, organizationId, userId, processController, new Process());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updateProcesses")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updateProcesses(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.updateProcesses]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, processController, new Process());
            } else {
                result = adminService.updateOrgObjects(decodedContent, organizationId, userId, processController, new Process());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @POST
    @Path("/removeProcesses")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response removeProcesses(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.removeProcesses]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            //String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( payload.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.deleteOrgObjects("[" + payload + "]", organizationId, userId, processController, new Process());
            } else {
                result = adminService.deleteOrgObjects(payload, organizationId, userId, processController, new Process());                
            }            
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
    
    //-------------------------------------------------------------------------- Get Descriptive Stats
    @POST
    @Path("/getDStatsFlatTable")
    @Produces("application/json")
    public Response getDStatsFlatTable(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getDStatsFlatTable]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());             
     
        result = statService.getDStatsPreprocessor(payload, true, userId);
        return Response.ok(result).build();  
    }    

    //-------------------------------------------------------------------------- Get Descriptive Stats
    @POST
    @Path("/getDStatsStagingTable")
    @Produces("application/json")
    public Response getDStatsStagingTable(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getDStatsStagingTable]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());             
     
        result = statService.getDStatsPreprocessor(payload, false, userId);
        return Response.ok(result).build();  
    }    

    //-------------------------------------------------------------------------- Get Descriptive Stats
    @POST
    @Path("/getCorrelationFlatTable")
    @Produces("application/json")
    public Response getCorrelationFlatTable(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getCorrelationFlatTable]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());             
     
        result = statService.getCorrelationProcessor(payload, true);
        return Response.ok(result).build();  
    }    

    //-------------------------------------------------------------------------- Get Descriptive Stats
    @POST
    @Path("/getCorrelationStagingTable")
    @Produces("application/json")
    public Response getCorrelationStagingTable(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getCorrelationStagingTable]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());             
     
        result = statService.getCorrelationProcessor(payload, false);
        return Response.ok(result).build();  
    }    
    
    //-------------------------------------------------------------------------- Get Match Sets
    @GET
    @Path("/getMatchSets")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getMatchSets(@QueryParam(value = "studyId") final String studyId, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.getMatchSets]";
        String result = "DS0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());                           
      
        result = statService.getMatchSets(Integer.parseInt(studyId), organizationId);
        return Response.ok(result).build();  
    }  

    //-------------------------------------------------------------------------- Stat (Packages)    
    @GET
    @Path("/getStats")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getStats(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.getStats]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, statController, new Method());
        return Response.ok(result).build();  
    }    
   
    @POST
    @Path("/updateStats")
    @Produces("application/json")
    public Response updateStats(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.updateStats]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        // to do: for security reasons need to add code preventing updates to objects not belonging to the org
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, statController, new Stat());                    
            } else {
               result = adminService.updateOrgObjects(decodedContent, organizationId, userId, statController, new Stat());                      
            }               
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_STAT_UPDATE_EVENT, result);          
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @Path("/createStats")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createStats(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [admin.createStats]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, statController, new Stat());                    
            } else {

               result = adminService.createOrgObjects(decodedContent, organizationId, userId, statController, new Stat());                      
            }      
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_STAT_CREATE_EVENT, result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
    @Path("/removeStats")
    @POST
    @Produces("application/json")
    public Response removeStats(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.removeStats]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);           
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.deleteOrgObjects("[" + decodedContent + "]", organizationId, userId, statController, new Stat());
            } else {
                result = adminService.deleteOrgObjects(decodedContent, organizationId, userId, statController, new Stat());                
            }   
            logger.log(Level.SEVERE, logId + " returning: {0}", result);            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @Path("/cloneStats")
    @POST
    @Produces("application/json")
    public Response cloneStatFromRecord1(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [process.cloneStats]";
        String result = "MB0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        //Integer organizationId = Auth.getOrganizationIdFromSession(request);           
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");    
            Integer targetOrgId = Integer.parseInt(decodedContent);
            Stat s = statController.findStat(1);
            s.setIdOrganization(targetOrgId);
            s.setCreatedBy(userId);
            String dataString = new JSONObject(s).toString();                    
            result = adminService.createOrgObjects("[" + dataString + "]", targetOrgId, userId, statController, new Stat());                   
            logger.log(Level.SEVERE, logId + " returning: {0}", result);            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while cloning stat record from record 1", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }   
    
   @OPTIONS
   @Path("process")
   public Response corsMyResourceShare(@HeaderParam("Access-Control-Request-Headers") String requestH) {
      _corsHeaders = requestH;
      return makeCORS(Response.ok(), requestH);
   }
   
   private Response makeCORS(Response.ResponseBuilder req, String returnMethod) {
       Response.ResponseBuilder rb = req.header("Access-Control-Allow-Origin", "*")
          .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

       if (!"".equals(returnMethod)) {
          rb.header("Access-Control-Allow-Headers", returnMethod);
       }

       return rb.build();
    }

   private Response makeCORS(Response.ResponseBuilder req) {
       return makeCORS(req, _corsHeaders);
    }      
}
