/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.server.rest;

import com.ceri.delta.entity.Configuration;
import com.ceri.delta.entity.Method;
import com.ceri.delta.entity.events.Eventtemplate;
import com.ceri.delta.entity.logregr.Logregr;
import com.ceri.delta.entity.study.Filter;
import com.ceri.delta.entity.study.Study;
import com.ceri.delta.jpa.MethodJpaController;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.logregr.LogregrJpaController;
import com.ceri.delta.jpa.model.ModelJpaController;
import com.ceri.delta.jpa.study.FilterJpaController;
import com.ceri.delta.jpa.study.StudyJpaController;
import com.ceri.delta.jpa.ConfigurationJpaController;
import com.ceri.delta.security.Auth;
import com.ceri.delta.server.AdminServiceImpl;
import com.ceri.delta.server.EventServiceImpl;
import com.ceri.delta.server.LogregrServiceImpl;
import com.ceri.delta.server.StudyServiceImpl;
import com.ceri.delta.util.JSON.JSONArray;
import com.ceri.delta.util.JSON.JSONException;
import com.ceri.delta.util.JSON.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 * @author Lahey Clinic.
 */
@Path("study")
@Consumes({"text/plain","text/html","application/html","application/xhtml","application/x-www-form-urlencoded","application/json"})
public class Delta3StudyResource {
    private static final Logger logger = Logger.getLogger(Delta3StudyResource.class.getName());
    private static AdminServiceImpl adminService;   
    private static StudyServiceImpl studyService;     
    private static EventServiceImpl eventService;       
    private static LogregrServiceImpl logregrService;
    private static ModelJpaController modelController;
    private static StudyJpaController studyController;  
    private static FilterJpaController filterController;      
    private static LogregrJpaController logregrController;
    private static MethodJpaController methodController;    
    private static ConfigurationJpaController configurationController;        

    public Delta3StudyResource() {
        if ( adminService == null ) {
            adminService = new AdminServiceImpl();
        }      
        if ( studyService == null ) {
            studyService = new StudyServiceImpl();
        }    
        if ( eventService == null ) {
            eventService = new EventServiceImpl();
        }           
        if ( logregrService == null ) {
            logregrService = new LogregrServiceImpl();
        }           
        if ( studyController == null ) {
            studyController = new StudyJpaController();
        }      
        if ( modelController == null ) {
            modelController = new ModelJpaController();
        }    
        if ( filterController == null ) {
            filterController = new FilterJpaController();
        }         
        if ( logregrController == null ) {
            logregrController = new LogregrJpaController();
        }     
        if ( methodController == null ) {
            methodController = new MethodJpaController();
        }         
        if ( configurationController == null ) {
            configurationController = new ConfigurationJpaController();
        }        
    }
    
    //-------------------------------------------------------------------------- Studies

    @Path("/getStudyDetails")
    @GET  
    @Produces("application/json")
    public Response getStudyDetails(@QueryParam(value = "studyId") final String studyId, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.getStudyDetails]";
        String result = "ST0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request); 
        Integer organizationId = Auth.getOrganizationIdFromSession(request);           
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {  
            result = studyService.getStudyDetails(studyId, organizationId, userId);   
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while getting study details", ex);
            result += " " + ex.getMessage();            
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 

    @GET
    @Path("/getStudies")   
    @Produces("application/json")
    public Response getStudies(@QueryParam(value = "filter") final String filter, @QueryParam(value = "sort") final String sort, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start,  @QueryParam(value = "limit") final String limit, @QueryParam(value = "group") final String group, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.getStudies]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());              
      
        result = studyService.getOrgStudiesExtendedGroup(Integer.parseInt(start), Integer.parseInt(limit), Integer.parseInt(group), filter, sort, organizationId, studyController, new Study());
        //result = adminService.getOrgObjectsByGroup(Integer.parseInt(start), Integer.parseInt(limit), Integer.parseInt(group), organizationId, studyController, new Study());  
        return Response.ok(result).build();  
    }    
  
    @Path("/createStudies")
    @POST
    @Produces("application/json")
    public Response createStudies(String payload, @Context HttpServletRequest request) throws Exception {
        JSONObject responseJSON = null;
        String logId = "DELTA3 WS method [study.createStudies]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, studyController, new Study());
            } else {
                result = adminService.createOrgObjects(decodedContent, organizationId, userId, studyController, new Study());                
            }  
            responseJSON = new JSONObject(result);
            if ( responseJSON.getBoolean("success") == true ) {
                JSONArray studiesArray = responseJSON.getJSONArray("studies");
                JSONObject study = (JSONObject)studiesArray.get(0);
                Integer idStudy = study.getInt("idStudy");    
                Integer idGroup = study.getInt("idGroup");
                adminService.createGroupEntities(idGroup, idStudy, "studies", userId);
            }            
            return Response.ok(result).build();  
        } catch (NonexistentEntityException | JSONException | UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception. ", ex.getMessage());           
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updateStudies") 
    @Produces("application/json")
    public Response updateStudies(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.updateStudies]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            System.out.println("This is at update studies for param"+payload);
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, studyController, new Study());
            } else {
                result = adminService.updateOrgObjects(decodedContent, organizationId, userId, studyController, new Study());                
            }            
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Delta 3 WS method [model.updateStudies] exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.ok(adminService.wrapAPI("", "", "", result)).build();  
    }    

   
    @Path("/removeStudies")
    @POST
    @Produces("application/json")
    public Response removeStudies(String payload, @Context HttpServletRequest request) throws Exception {
        //JSONObject responseJSON = null;        
        String logId = "DELTA3 WS method [study.removeStudies]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);      
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");  
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                 result = studyService.deleteStudies("[" + decodedContent + "]", userId);
            } else {
                 result = studyService.deleteStudies(decodedContent, userId);    
            }
            /*responseJSON = new JSONObject(result);
            if ( responseJSON.getBoolean("success") == true ) {
                JSONArray studiesArray = responseJSON.getJSONArray("studies");
                JSONObject study = (JSONObject)studiesArray.get(0);
                Integer idStudy = study.getInt("idStudy");    
                Integer idGroup = study.getInt("idGroup");
                adminService.deleteGroupEntity(idGroup, idStudy, "studies", userId);
            }   */           
            return Response.ok(result.toString()).build();     
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build(); 
    }    
 
    @Path("/cloneStudy")
    @POST
    @Produces("application/json")
    public Response cloneStudy(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.cloneStudy]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());       
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = studyService.cloneStudy("[" + decodedContent + "]", organizationId, userId, studyController, new Study());
            } else {
                result = studyService.cloneStudy(decodedContent, organizationId, userId, studyController, new Study());                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/splitStudy")
    @POST
    @Produces("application/json")
    public Response splitStudy(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.splitStudy]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
     
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = studyService.splitStudy("[" + decodedContent + "]", organizationId, userId, studyController, new Study());
            } else {
                result = studyService.splitStudy(decodedContent, organizationId, userId, studyController, new Study());                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/initStudyMethods")
    @POST
    @Produces("application/json")
    public Response initStudyMethods(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.initStudyMethods]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());    
        try {   
            String decodedContent = URLDecoder.decode(payload, "UTF-8");
            result = studyService.initStudyMethods(decodedContent, organizationId, userId);  
            return Response.ok(result).build();             
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while initializing stat methods: ", ex.getMessage());
        }
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @Path("/executeStudy")
    @POST
    @Produces("application/json")
    public Response executeStudy(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.executeStudy]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = studyService.executeStudy("[" + decodedContent + "]", organizationId, userId, new Study(), true);
            } else {
                result = studyService.executeStudy(decodedContent, organizationId, userId, new Study(), true);                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/resetAndExecuteStudy")
    @POST
    @Produces("application/json")
    public Response resetAndExecuteStudy(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.executeStudy]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());      
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = studyService.resetAndExecuteStudy("[" + decodedContent + "]", organizationId, userId, new Study(), true);
            } else {
                result = studyService.resetAndExecuteStudy(decodedContent, organizationId, userId, new Study(), true);                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @Path("/advanceStudy")
    @POST
    @Produces("application/json")
    public Response advanceStudy(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.advanceStudy]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);    
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
   
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = studyService.advanceStudyStatus("[" + decodedContent + "]", userId);
            } else {
                result = studyService.advanceStudyStatus(decodedContent, userId);                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
    
    @Path("/rollbackStudy")
    @POST
    @Produces("application/json")
    public Response rollbackStudy(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.rollbackStudy]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);     

        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());       
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = studyService.rollbackStudyStatus("[" + decodedContent + "]", userId);
            } else {
                result = studyService.rollbackStudyStatus(decodedContent, userId);                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    /*@Path("/getStudyNextStepParams")
    @POST
    @Produces("application/json")
    public Response getStudyNextStepParams(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.getStudyNextStepParams]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
    
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = studyService.getStudyNextStepParams("[" + decodedContent + "]", organizationId, userId, new Study());
            } else {
                result = studyService.getStudyNextStepParams(decodedContent, organizationId, userId, new Study());                
            }   
            return Response.ok(result).build();  
        } catch (Exception ex) {
            result = logId + " error: " +  ex.getMessage();
            logger.log(Level.SEVERE, result, ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }*/
     
    @GET
    @Path("/getModelFilters")   
    @Produces("application/json")
    public Response getModelFilters(@QueryParam(value = "model") final String model, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        JSONObject requestContent;
        String logId = "DELTA3 WS method [study.getModelFilters]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         

        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());    
        String decodedContent = model;   
        try {
            // modelId is used to narrow down what this request brings back
            // todo: add security check user->org->model
            decodedContent = URLDecoder.decode(model, "UTF-8");
            requestContent = new JSONObject(decodedContent);
            JSONArray userArray = requestContent.getJSONArray("models");
            JSONObject models = (JSONObject)userArray.get(0);
            Integer modelId = models.getInt("idModel");
            if ( modelId == 0 ) {   
                result = studyService.getFilters(Integer.parseInt(start), Integer.parseInt(limit), userId, organizationId);        
            } else {
                result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), modelId, filterController, new Filter());
            }
        logger.log(Level.SEVERE, logId + " returning: {0}", result);        
        return Response.ok(result).build();         
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex.getMessage());    
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, logId + "Exception: ", ex.getMessage()); 
    }   
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }   
  
    @GET
    @Path("/getFilters")   
    @Produces("application/json")    
    public Response getFilters(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.getFilters]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());                 
        result = studyService.getFilters(Integer.parseInt(start), Integer.parseInt(limit), userId, organizationId);
        logger.log(Level.SEVERE, logId + " returning: {0}", result);        
        return Response.ok(result).build();  
    } 

    @GET
    @Path("/getFilterUsage")   
    @Produces("application/json")
    public Response getFilterUsage(@QueryParam(value = "filter") final String filter, @Context HttpServletRequest request) {
        JSONObject requestContent;
        String logId = "DELTA3 WS method [study.getFilterUsage]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
  
        String decodedContent = filter;   
        try {
            decodedContent = URLDecoder.decode(filter, "UTF-8");
            requestContent = new JSONObject(decodedContent);
            Integer filterId = requestContent.getInt("idFilter");    
            result = studyService.getFilterUsage(filterId, userId, organizationId);        
            logger.log(Level.SEVERE, logId + " returning: {0}", result);        
            return Response.ok(result).build();         
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex.getMessage());    
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, logId + " exception: ", ex.getMessage()); 
    }   
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }   
 
   @Path("/cloneFilter")
    @POST
    @Produces("application/json")
    public Response cloneFilter(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.cloneFilter]";
        String result = "ST0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);       
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
      
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = studyService.cloneFilter(decodedContent, userId);                 
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }  
    
    @Path("/createModelFilters")
    @POST
    @Produces("application/json")
    public Response createModelFilters(String payload, @Context HttpServletRequest request) {
        JSONObject requestContent;
        String logId = "DELTA3 WS method [study.createModelFilters]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");   
            requestContent = new JSONObject(decodedContent);
            // for Filter modelId is used as data access mechanism, instead of usual organizationId ?         
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, filterController, new Filter());
            } else {
                result = adminService.createOrgObjects(decodedContent, organizationId, userId, filterController, new Filter());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex.getMessage());    
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, logId + " exception: ", ex.getMessage()); 
        }             
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/updateModelFilters")
    @POST
    @Produces("application/json")
    public Response updateModelFilters(String payload, @Context HttpServletRequest request) {
        JSONObject requestContent;
        String logId = "DELTA3 WS method [study.updateModelFilters]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");   
            requestContent = new JSONObject(decodedContent);
            // for Filter modelId is used as data access mechanism, instead of usual organizationId
            // todo: security check user->org->model
            //Integer modelId = requestContent.getInt("modelidModel");            
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, filterController, new Filter());
            } else {
                result = adminService.updateOrgObjects(decodedContent, organizationId, userId, filterController, new Filter());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex.getMessage());    
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, logId + " exception: ", ex.getMessage()); 
        }             
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @POST
    @Path("/updateFilters") 
    @Produces("application/json")
    public Response updateFilters(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.updateFilters]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, filterController, new Filter());
            } else {
                result = adminService.updateOrgObjects(decodedContent, organizationId, userId, filterController, new Filter());                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }      

    @Path("/removeModelFilters")
    @POST
    @Produces("application/json")
    public Response removeModelFilters(String payload, @Context HttpServletRequest request) {
        JSONObject requestContent;
        String logId = "DELTA3 WS method [study.removeModelFilters]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");   
            requestContent = new JSONObject(decodedContent);
            // for Filter modelId is used as data access mechanism, instead of usual organizationId
            // todo: security check user->org->model
            Integer modelId = requestContent.getInt("modelidModel");            
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                studyService.deleteFilterFormulas("[" + decodedContent + "]", userId);
                result = adminService.deleteOrgObjects("[" + decodedContent + "]", modelId, userId, filterController, new Filter());
            } else {
                studyService.deleteFilterFormulas(decodedContent, userId);
                result = adminService.deleteOrgObjects(decodedContent, organizationId, userId, filterController, new Filter());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex.getMessage());    
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, logId + " exception: ", ex.getMessage()); 
        }             
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @Path("/removeFiltersAndFormulas")
    @POST
    @Produces("application/json")
    // currently not used by DELTA
    public Response removeFilterAndFormulas(String payload, @Context HttpServletRequest request) {
        JSONObject requestContent;
        String logId = "DELTA3 WS method [study.removeModelFilters]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);   
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");   
            requestContent = new JSONObject(decodedContent);
            // todo: security check user->org->model
            Integer modelId = requestContent.getInt("modelidModel");            
            studyService.deleteFilterAndFormulas(decodedContent, userId);             
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex.getMessage());    
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, logId + " exception: ", ex.getMessage()); 
        }             
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @Path("/validateFilter")
    @POST  
    @Produces("application/json")
    public Response validateFilter(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.validateFilter]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = studyService.validateFilter(decodedContent, userId);      
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
    
    @Path("/getStudyCategory")
    @GET
    @Produces("application/json")
    public Response getStudyCategory(@QueryParam(value = "study") final String studyString, @Context HttpServletRequest request) {
        JSONObject requestContent;    
        int idStudy;
        String logId = "DELTA3 WS method [study.getStudyCategory]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);     
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
         try {
            requestContent = new JSONObject(studyString);
            JSONArray studyArray = requestContent.getJSONArray("studies");
            JSONObject study = (JSONObject)studyArray.get(0);
            idStudy = study.getInt("idStudy");
            result = studyService.getStudyCategoryInfo(idStudy);            
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, null, ex);
            logger.log(Level.SEVERE, logId + " exception: ", ex.toString()); 
        }       
        return Response.ok(result).build(); 
    }    
    
    @Path("/addStudyCategory")
    @POST
    @Produces("application/json")
    public Response addStudyCategory(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.addStudyCategory]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);    
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = studyService.createStudyCategories(decodedContent, userId);
            return Response.ok(result).build();        
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.ok(result).build();  
    }
   
    @Path("/removeStudyCategory")
    @POST
    @Produces("application/json")
    public Response removeStudyCategory(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.removeStudyCategory]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);      
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = studyService.deleteStudyCategories(decodedContent, userId);
            return Response.ok(result).build();     
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.ok(result).build();  
    }

    @Path("/getFilterFormula")
    @GET
    @Produces("application/json")
    public Response getFilterFormula(@QueryParam(value = "filter") final String filterString, @Context HttpServletRequest request) {
        JSONObject requestContent;    
        int idFilter;
        String logId = "DELTA3 WS method [study.getFilterFormula]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);      
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
         try {
            requestContent = new JSONObject(filterString);
            JSONArray filterArray = requestContent.getJSONArray("filters");
            JSONObject filter = (JSONObject)filterArray.get(0);
            idFilter = filter.getInt("idFilter");  
            result = studyService.getFilterFormulaInfo(idFilter);            
        } catch (JSONException ex) {
            logger.log(Level.SEVERE, logId + " exception: ", ex.toString()); 
        }       
        return Response.ok(result).build(); 
    }    
    
    @Path("/getFilterWhereClause")
    @GET
    @Produces("application/json")
    public Response getFilterWhereClause(@QueryParam(value = "filter") final String filterString, @Context HttpServletRequest request) {
        JSONObject requestContent;    
        Integer idFilter;
        String logId = "DELTA3 WS method [study.getFilterWhereClause]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);      
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 
        try {
            idFilter = Integer.parseInt(filterString);  
            result = studyService.getFilterWhereClause(idFilter);            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception: ", ex.toString()); 
        }       
        return Response.ok(result).build(); 
    }    
    
    @Path("/createFilterFormula")
    @POST
    @Produces("application/json")
    public Response createFilterFormula(@QueryParam(value = "filter") final String filterString, String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.addFilterFormula]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);   
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = studyService.createFilterFormulas(decodedContent, userId);
            return Response.ok(result).build();        
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.ok(result).build();  
    }
   
    @Path("/removeFilterFormula")
    @POST
    @Produces("application/json")
    public Response removeFilterFormula(@QueryParam(value = "filter") final String filterString, String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.removeFilterFormula]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");  
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = studyService.deleteFilterFormula("[" + decodedContent + "]", userId);         
            } else {           
                result = studyService.deleteFilterFormula(decodedContent, userId);
            }
            return Response.ok(result).build();     
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.ok(result).build();  
    }

    @Path("/updateFilterFormula")
    @POST
    @Produces("application/json")
    public Response updateFilterFormula(@QueryParam(value = "filter") final String filterString, String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.updateFilterFormula]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);     
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");            
            result = studyService.updateFilterFormula(decodedContent, userId);
            return Response.ok(result).build();     
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.ok(result).build();  
    }
 
    //-------------------------------------------------------------------------- LogregrGrid CRUD
        /**
     * @param filter
     * @param sort
     * @param page
     * @param start
     * @param limit
     * @param request
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getLogregr")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getLogregr(@QueryParam(value = "filter") final String filter, @QueryParam(value = "sort") final String sort, @QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @QueryParam(value = "group") final String group, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.getLogregr]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        result = adminService.getOrgObjectsExtendedGroup(Integer.parseInt(start), Integer.parseInt(limit), filter, sort, Integer.parseInt(group), organizationId, logregrController, new com.ceri.delta.entity.process.Process());        
        //result = adminService.getOrgObjectsExtended(Integer.parseInt(start), Integer.parseInt(limit), filter, sort, organizationId, logregrController, new com.ceri.delta.entity.process.Process());     
        //result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, logregrController, new Logregr());
        return Response.ok(result).build();  
    }    
  
    @Path("/createLogregr")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createLogregr(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.createLogregr]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = logregrService.createLogregr("[" + decodedContent + "]", organizationId, userId);
            } else {
                result = logregrService.createLogregr(decodedContent, organizationId, userId);                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL: ", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

  
    @Path("/createLogregrEmpty")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createLogregrEmpty(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.createLogregr]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, logregrController, new Logregr());
            } else {
                result = adminService.createOrgObjects(decodedContent, organizationId, userId, logregrController, new Logregr());                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL: ", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @POST
    @Path("/updateLogregr")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updateLogregr(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.updateLogregr]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");  
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, logregrController, new Logregr());
            } else {
                result = adminService.updateOrgObjects(decodedContent, organizationId, userId, logregrController, new Logregr());                
            }            
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();        
        }        
        return Response.ok(adminService.wrapAPI("", "", "", result)).build();  
    }    

    
    @POST
    @Path("/removeLogregr")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response removeLogregr(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.removeLogregr]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);     
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = logregrService.deleteLogregr("[" + decodedContent + "]", userId);
            } else {
                result = logregrService.deleteLogregr(decodedContent, userId);                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }      

    @GET
    @Path("/getLRFIntervals")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getLRFIntervals(@QueryParam(value = "studyId") final String studyId, @QueryParam(value = "type") final String type, @QueryParam(value = "chunking") final String chunking, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.getLRFIntervals]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);     
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());               
 
        result = logregrService.getLRFIntervals(studyId, type, chunking, userId);
        return Response.ok(result).build();  
    }    
      
    //-------------------------------------------------------------------------- Get LogregrField
        /**
     * Retrieves representation of an instance of com.ceri.delta.server.rest.Delta3LogregrResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getLogregrFields")
    @Produces("application/json")
    public Response getLogregrFields(@QueryParam(value = "logregr") final String logregrString, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.getLogregrFields]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());               
   
        result = logregrService.getLogregrFields(logregrString);
        return Response.ok(result).build();  
    }    
  
    @Path("/createLogregrFields")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createLogregrFields(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.createLogregrFields]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);           
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = logregrService.createLogregrFields("[" + decodedContent + "]", userId);
            } else {
                result = logregrService.createLogregrFields(decodedContent, userId);                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updateLogregrFields")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updateLogregrFields(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.updateLogregrFields]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);     
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = logregrService.updateLogregrFields("[" + decodedContent + "]", userId);
            } else {
                result = logregrService.updateLogregrFields(decodedContent, userId);                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
    
    @POST
    @Path("/removeLogregrFields")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response removeLogregrFields(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.removeLogregrFields]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);         
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = logregrService.deleteLogregrFields("[" + decodedContent + "]", userId);
            } else {
                result = logregrService.deleteLogregrFields(decodedContent, userId);                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
    
    //-------------------------------------------------------------------------- Get LogregrDates
    /**
     * Retrieves representation of an instance of com.ceri.delta.server.rest.Delta3LogregrResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/getLogregrDates")
    @Produces("application/json")
    public Response getLogregrDates(@QueryParam(value = "logregr") final String logregrString, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.getLogregrDates]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);     
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());                
   
        result = logregrService.getLogregrDates(logregrString);
        return Response.ok(result).build();  
    }    
  
    @Path("/createLogregrDates")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createLogregrDates(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.createLogregrDates]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);    
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = logregrService.createLogregrDates("[" + decodedContent + "]", userId);
            } else {
                result = logregrService.createLogregrDates(decodedContent, userId);                
            }              
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }

    @POST
    @Path("/updateLogregrDates")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response updateLogregrDates(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.updateLogregrDates]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = logregrService.updateLogregrDates("[" + decodedContent + "]", userId);
            } else {
                result = logregrService.updateLogregrDates(decodedContent, userId);                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
    
    @POST
    @Path("/removeLogregrDates")
    @Consumes("application/json")    
    @Produces("application/json")
    public Response removeLogregrDates(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.removeLogregrDates]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);        
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = logregrService.deleteLogregrDates("[" + decodedContent + "]", userId);
            } else {
                result = logregrService.deleteLogregrDates(decodedContent, userId);                
            }            
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }  
 
    @Path("/getLRF")
    @GET  
    @Produces("application/json")
    public Response getLRF(@QueryParam(value = "logregr") final String logregr, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.getLRF]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);           
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(logregr, "UTF-8");              
            result = logregrService.getLRFString(decodedContent, userId);      
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while verifying formula statement", ex);
            result += " " + ex.getMessage();            
        }        
        return Response.status(Response.Status.SEE_OTHER).entity(result).build();
    } 

    @Path("/getLRFforModel")
    @GET  
    @Produces("application/json")
    public Response getLRFforModel(@QueryParam(value = "modelId") final String modelId, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.getLRFforModel]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);          
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {  
            result = logregrService.getLRFforModel(modelId);   
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while getting formula statement", ex);
            result += " " + ex.getMessage();            
        }        
        return Response.status(Response.Status.SEE_OTHER).entity(result).build();
    } 
    
    @Path("/getLRFforStudy")
    @GET  
    @Produces("application/json")
    public Response getLRFforStudy(@QueryParam(value = "studyId") final String studyId, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.getLRFforStudy]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);          
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {  
            result = logregrService.getLRFforStudy(studyId);   
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while getting formula statement", ex);
            result += " " + ex.getMessage();            
        }        
        return Response.status(Response.Status.SEE_OTHER).entity(result).build();
    } 

    @Path("/getLRFforProcess")
    @GET  
    @Produces("application/json")
    public Response getLRFforProcess(@QueryParam(value = "processId") final String processId, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.getLRFforProcess]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);          
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {  
            result = logregrService.getLRFforProcess(processId, userId);   
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while getting formula statement", ex);
            result += " " + ex.getMessage();            
        }        
        return Response.status(Response.Status.SEE_OTHER).entity(result).build();
    } 
  
    @Path("/calculateStartDate")
    @POST
    @Produces("application/json")
    public Response calculateStartDate(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.calculateStartDate]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);    
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = logregrService.calculateDate(decodedContent);                           
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception: ", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    @Path("/getResultsforStudy")
    @GET  
    @Produces("application/json")
    public Response getResultsforStudy(@QueryParam(value = "studyId") final String studyId, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.getResultsforStudy]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);          
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  

        try {  
            result = studyService.getResultsforStudy(studyId, userId);   
            logger.log(Level.SEVERE, logId + " returning: {0}", result);
            return Response.ok(result).build();  
        } catch (Exception ex) {
            logger.log(Level.SEVERE, logId + " exception while getting results for study", ex);
            result += " " + ex.getMessage();            
        }        
        return Response.status(Response.Status.SEE_OTHER).entity(result).build();
    } 
    
    @Path("/cloneLRF")
    @POST
    @Produces("application/json")
    public Response cloneLRF(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [logregr.cloneLRF]";
        String result = "LR0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);       
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
      
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = logregrService.cloneLRF(decodedContent, userId);                 
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }      
    
    @Path("/exportDoubleArray")
    @POST
    @Produces("application/json")
    public Response exportDoubleArray(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.exportDoubleArray]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
     
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            result = studyService.exportDoubleArrayCSV(decodedContent, organizationId, userId);                 
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
    
    @POST
    @Path("/exportStudy") 
    @Produces("application/json")
    public Response exportStudy(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.exportStudy]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
     
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = studyService.exportStudy("[" + decodedContent + "]", organizationId, userId, new Study());
            } else {
                result = studyService.exportStudy(decodedContent, organizationId, userId, new Study());                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }   
    
    @POST
    @Path("/exportStudyInfo") 
    @Produces("application/json")
    public Response exportStudyInfo(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [model.exportStudyInfo]";
        String result = "ST0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());
     
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");   
            ServletContext context = request.getServletContext();
            String webRootPath = context.getRealPath("/");
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = studyService.exportStudyInfo("[" + decodedContent + "]", organizationId, userId, webRootPath);
            } else {
                result = studyService.exportStudyInfo(decodedContent, organizationId, userId, webRootPath);                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Delta3AdminResource.class.getName()).log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }       
    //-------------------------------------------------------------------------- Methods    
    @GET
    @Path("/getMethods")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getMethods(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.getMethods]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, methodController, new Method());
        return Response.ok(result).build();  
    }    
   
    @POST
    @Path("/updateMethods")
    @Produces("application/json")
    public Response updateMethods(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.updateMethods]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        // to do: for security reasons need to add code preventing updates to objects not belonging to the org
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, methodController, new Method());                    
            } else {
               result = adminService.updateOrgObjects(decodedContent, organizationId, userId, methodController, new Method());                      
            }               
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_METHOD_UPDATE_EVENT, result);          
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @Path("/createMethods")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createMethods(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.createMethods]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, methodController, new Method());                    
            } else {

               result = adminService.createOrgObjects(decodedContent, organizationId, userId, methodController, new Method());                      
            }      
            int start = result.indexOf("idConfiguration")-1;
            int end = result.indexOf("}", start);
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_METHOD_CREATE_EVENT, result.substring(start,end));
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }   
    
    //-------------------------------------------------------------------------- Configurations    
    @GET
    @Path("/getConfigurations")
    @Consumes("application/xml")    
    @Produces("application/json")
    public Response getConfigurations(@QueryParam(value = "page") final String page, @QueryParam(value = "start") final String start, @QueryParam(value = "limit") final String limit, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.getConfigurations]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());        
        result = adminService.getOrgObjects(Integer.parseInt(start), Integer.parseInt(limit), organizationId, configurationController, new Configuration());
        return Response.ok(result).build();  
    }    
   
    @POST
    @Path("/updateConfigurations")
    @Produces("application/json")
    public Response updateConfigurations(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.updateConfigurations]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        // to do: for security reasons need to add code preventing updates to objects not belonging to the org
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.updateOrgObjects("[" + decodedContent + "]", organizationId, userId, configurationController, new Configuration());                    
            } else {
               result = adminService.updateOrgObjects(decodedContent, organizationId, userId, configurationController, new Configuration());                      
            }       
            int start = result.indexOf("idConfiguration")-1;
            int end = result.indexOf("}", start);  
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_DB_UPDATE_EVENT, result.substring(start,end));
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }    
 
    @Path("/createConfigurations")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response createConfigurations(String payload, @Context HttpServletRequest request) {
        String logId = "DELTA3 WS method [study.createConfigurations]";
        String result = "WA0000 " + logId + " failed.";
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);            
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString());  
        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
               result = adminService.createOrgObjects("[" + decodedContent + "]", organizationId, userId, configurationController, new Configuration());                    
            } else {
               result = adminService.createOrgObjects(decodedContent, organizationId, userId, configurationController, new Configuration());                      
            }
            int start = result.indexOf("idConfiguration")-1;
            int end = result.indexOf("}", start);  
            eventService.logEvent(organizationId, userId, Eventtemplate.CONFIG_DB_CREATE_EVENT, result.substring(start,end));
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    } 
    
    
     //v3.64 added code for delete connection https://ceri-lahey.atlassian.net/browse/DELQA-593
    @Path("/deleteConfigurations")
    @POST
    @Consumes("application/json")    
    @Produces("application/json")
    public Response deleteConfigurations(String payload, @Context HttpServletRequest request) {
        System.out.println("This is at delete configuration");
        String logId = "DELTA3 WS method [study.deleteConfigurations]";
        String result = "WA0000 " + logId + " failed.";        
        Integer userId = Auth.getUserIdFromSession(request);
        Integer organizationId = Auth.getOrganizationIdFromSession(request);
        if (userId == null) {
            return Response.ok(adminService.wrapAPI("", "", "", "Session lost.\nPlease login again.")).build();
        }
        logger.log(Level.SEVERE, logId + " request from user {0}", userId.toString()); 

        try {
            String decodedContent = URLDecoder.decode(payload, "UTF-8");           
            if ( decodedContent.charAt(0) == '{' ) { // need to add square brackets
                result = adminService.deleteOrgObjects("[" + decodedContent + "]", organizationId, userId, configurationController, new Configuration());
            } else {
                result = adminService.deleteOrgObjects(decodedContent, organizationId, userId, configurationController, new Configuration());                
            }   
            return Response.ok(result).build();  
        } catch (UnsupportedEncodingException ex) {
            logger.log(Level.SEVERE, logId + " exception while decoding URL", ex);
            result += " " + ex.getMessage();
        }        
        return Response.status(Response.Status.NOT_FOUND).entity(result).build();
    }
    
    
}
