package com.ceri.delta.jpa.event;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.events.Task;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author CERI Lahey
 */
public class TaskJpaController implements Serializable {

    public TaskJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("CeriPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 
    
    public void create(Task task) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(task);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Task edit(Task task) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            task = em.merge(task);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = task.getIdTask();
                if (findTask(id) == null) {
                    throw new NonexistentEntityException("The task with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return task;
    }

    public com.ceri.delta.entity.events.Task destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        com.ceri.delta.entity.events.Task task;        
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            try {
                task = em.getReference(com.ceri.delta.entity.events.Task.class, id);
                task.getIdTask();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The task with id " + id + " no longer exists.", enfe);
            }
            em.remove(task);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return task;
    }

    public List<Task> findTaskEntities() {
        return findTaskEntities(true, -1, -1);
    }

    public List<Task> findTaskEntities(int maxResults, int firstResult) {
        return findTaskEntities(false, maxResults, firstResult);
    }

    private List<Task> findTaskEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Task as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Task> findOrgTaskEntities(Integer currentOrgId) {
        return findOrgTaskEntities(true, -1, -1, currentOrgId);
    }

    public List<Task> findOrgTaskEntities(int maxResults, int firstResult, Integer currentOrgId) {
        return findOrgTaskEntities(false, maxResults, firstResult, currentOrgId);
    }

    private List<Task> findOrgTaskEntities(boolean all, int maxResults, int firstResult, Integer currentOrgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Task as o where Organization_idOrganization = " + currentOrgId.toString());
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Task findTask(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Task.class, id);
        } finally {
            em.close();
        }
    }

    public int getTaskCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Task as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
