/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.jpa;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.Configuration;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.exceptions.PreexistingEntityException;
import com.ceri.delta.security.exceptions.DeltaOperationNotAllowedException;
import com.ceri.delta.security.AuditLogger;
import com.ceri.delta.security.DesEncrypter;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author NVK00.
 */
public class ConfigurationJpaController implements PersistenceHelper, Serializable {

    public static String dbPhrase = "dbPhrase"; public ConfigurationJpaController() {
    this.emf = null;
    this.emf = PersistenceManager.getInstance().getEntityManagerFactory("CeriPU");
  }
    
    
    private EntityManagerFactory emf;
    public EntityManager getEntityManager() {
        return this.emf.createEntityManager();
    }

    
    public void create(Configuration configuration) throws PreexistingEntityException, Exception{
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(configuration);
            em.getTransaction().commit();
        }catch (Exception ex) {
            if (findConfiguration(configuration.getIdConfiguration()) != null) {
            throw new PreexistingEntityException("Configuration " + configuration + " already exists.", ex);
        } 
        throw ex;  
        }  
        
        finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    
    
    public Configuration edit(Configuration configuration) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            configuration = em.merge(configuration);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = configuration.getIdConfiguration();
                if (findConfiguration(id) == null) {
                    throw new NonexistentEntityException("The configuration with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return configuration;
    }
    
    
    

    public void destroy(Integer id) throws NonexistentEntityException {
       EntityManager em = null; 
      try {
      Configuration configuration;
      em = getEntityManager();
      em.getTransaction().begin();
      
      try {
        configuration = (Configuration)em.getReference(Configuration.class, id);
        configuration.getIdConfiguration();
      } catch (EntityNotFoundException enfe) {
        throw new NonexistentEntityException("The configuration with id " + id + " no longer exists.", enfe);
      } 
      em.remove(configuration);
      em.getTransaction().commit();
    } finally {
      if (em != null) {
        em.close();
      }
    } 
  }
    
    
    public List<Object> findConfigurationEntities() { return findConfigurationEntities(true, -1, -1); }


  
  public List<Object> findConfigurationEntities(int maxResults, int firstResult) { return findConfigurationEntities(false, maxResults, firstResult); }

  
  private List<Object> findConfigurationEntities(boolean all, int maxResults, int firstResult) {
     EntityManager em = getEntityManager();
    try {
      Query q = em.createQuery("select object(o) from Configuration as o");
      if (!all) {
        q.setMaxResults(maxResults);
        q.setFirstResult(firstResult);
      } 
      return q.getResultList();
    } finally {
      em.close();
    } 
  }
  
  public Configuration findConfiguration(Integer id) {
     EntityManager em = getEntityManager();
    try {
      return (Configuration)em.find(Configuration.class, id);
    } finally {
      em.close();
    } 
  }

  
  public List<Object> findOrgConfigurationEntities(Integer orgId) { return findOrgConfigurationEntities(true, -1, -1, orgId); }


  
  public List<Object> findOrgConfigurationEntities(int maxResults, int firstResult, Integer orgId) { return findOrgConfigurationEntities(false, maxResults, firstResult, orgId); }

  
  private List<Object> findOrgConfigurationEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
     EntityManager em = getEntityManager();
    try {
      Query q = em.createQuery("select object(o) from Configuration as o where Organization_idOrganization = " + orgId.toString());
      if (!all) {
        q.setMaxResults(maxResults);
        q.setFirstResult(firstResult);
      } 
      return q.getResultList();
    } finally {
      em.close();
    } 
  }
  
  public int getConfigurationCount() {
     EntityManager em = getEntityManager();
    try {
      Query q = em.createQuery("select count(o) from Configuration as o");
      return ((Long)q.getSingleResult()).intValue();
    } finally {
      em.close();
    } 
  }
  
  public int getOrgConfigurationCount(Integer orgId) {
    EntityManager em = getEntityManager();
    try {
      return ((Long)em.createQuery("select count(o) from Configuration as o where Organization_idOrganization = " + orgId.toString()).getSingleResult()).intValue();
    } finally {
      em.close();
    } 
  }


  
  public Object createObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception { throw new UnsupportedOperationException("Not supported yet."); }



  
  public Object updateObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception { throw new UnsupportedOperationException("Not supported yet."); }



  
  public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
    ((Configuration)object).setActive(Boolean.TRUE);
    ((Configuration)object).setOrganization(currentOrgId.intValue());
    DesEncrypter des = new DesEncrypter(dbPhrase);
    if (((Configuration)object).getDbPassword() != null && !"".equals(((Configuration)object).getDbPassword())) {
      ((Configuration)object).setDbPassword(des.encrypt(((Configuration)object).getDbPassword()));
    }
    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
    ((Configuration)object).setCreatedTS(currentTimestamp);
    ((Configuration)object).setCreatedBy(currentUserId);
    ((Configuration)object).setUpdatedTS(currentTimestamp);
    ((Configuration)object).setUpdatedBy(currentUserId);
    AuditLogger.log(currentUserId.intValue(), ((Configuration)object).getIdConfiguration().intValue(), "create", object);
    return edit((Configuration)object);
  }

  
  public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
    ((Configuration)object).setOrganization(currentOrgId.intValue());
    DesEncrypter des = new DesEncrypter(dbPhrase);
    if (((Configuration)object).getDbPassword() != null && !"".equals(((Configuration)object).getDbPassword())) {
      ((Configuration)object).setDbPassword(des.encrypt(((Configuration)object).getDbPassword()));
    }
    Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
    ((Configuration)object).setUpdatedTS(currentTimestamp);
    ((Configuration)object).setUpdatedBy(currentUserId);
    AuditLogger.log(currentUserId.intValue(), ((Configuration)object).getIdConfiguration().intValue(), "update", object);
    return edit((Configuration)object);
  }


  
  public String getObjectType() { return "configurations"; }



  
  public List<Object> findObjectEntities(int maxResults, int firstResult) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public List<Object> findObjectEntities() { return findConfigurationEntities(); }



  
  public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) { return findOrgConfigurationEntities(maxResults, firstResult, currentOrgId); }



  
  public List<Object> findObjectEntities(Integer currentOrgId) { return findOrgConfigurationEntities(currentOrgId); }




  
  public int getObjectCount() { return getConfigurationCount(); }



  
  public int getObjectCountByOrg(Integer currentOrgId) { return getOrgConfigurationCount(currentOrgId); }



  
  public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException 
  {   
      throw new UnsupportedOperationException("Not supported yet."); 
  }

  
//v3.64 added code for delete connection https://ceri-lahey.atlassian.net/browse/DELQA-593
@Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.security.exceptions.DeltaOperationNotAllowedException, NonexistentEntityException {
        System.out.println("At delete Object");
        System.out.println("User ID:"+currentUserId );
        
        EntityManager em = getEntityManager();
        Query q = em.createQuery("select u from UserHasRole u where u.userHasRolePK.roleidRole=11 and u.userHasRolePK.useridUser = "+currentUserId.toString());
        List results = q.getResultList();
        System.out.println("Query "+q);
        System.out.println("Results "+results);
        
        EntityManager em1 = getEntityManager();
        Query q1 = em1.createQuery("select count(u) from UserHasRole u where u.userHasRolePK.roleidRole=11 and u.userHasRolePK.useridUser = "+currentUserId.toString());
        int result1=q.getResultList().size();
        
        System.out.println("Query1 "+q1);
        System.out.println("Results1 "+result1);
        
        if ( !Objects.equals(((Configuration)subject).getOrganization(), currentOrgId) ) {
            throw new DeltaOperationNotAllowedException("Operation not authorized.");             
        }
        
        
        if (result1>=1) {
            System.out.println("Deleted configuration");
            AuditLogger.log(currentUserId, ((Configuration)subject).getIdConfiguration(), "delete", subject);   
            destroy(((Configuration)subject).getIdConfiguration());
         } 
        else{
            throw new UnsupportedOperationException("User is not authorised to delete database configuration. Please contact organization admin.");
        }

    }


  
  public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public int getObjectCount(int currentUserId) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public Object findObjectEntityById(Integer objectId) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public Integer getObjectId(Object subject) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public List<Object> findObjectEntitiesByOrgGroup(int maxResults, int firstResult, Integer currentOrgId, Integer idGroup) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public int getObjectCountByOrgGroup(Integer currentOrgId, Integer groupId) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public List<Object> findObjectEntitiesByOrgGroup(Integer currentOrgId, Integer idGroup) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public List<Object> findObjectEntitiesExtendedOrg(int maxResults, int firstResult, Integer currentOrgId, String whereClause) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public List<Object> findObjectEntitiesExtendedOrg(Integer currentOrgId, String whereClause) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public List<Object> findObjectEntitiesExtendedOrgGroup(Integer currentOrgId, Integer idGroup, String whereClause) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public List<Object> findObjectEntitiesExtendedOrgGroup(int maxResults, int firstResult, Integer currentOrgId, Integer idGroup, String whereClause) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public int getObjectCountExtendedOrg(Integer currentOrgId, String whereClause) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public int getObjectCountExtendedOrgGroup(Integer currentOrgId, Integer groupId, String whereClause) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public List<Object> findObjectEntitiesExtended(int maxResults, int firstResult, String whereClause) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public List<Object> findObjectEntitiesExtended(String whereClause) { throw new UnsupportedOperationException("Not supported yet."); }



  
  public int getObjectCountExtended(String whereClause) { throw new UnsupportedOperationException("Not supported yet."); }
   
    
}