/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.jpa.study;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.GroupHasEntity;
import com.ceri.delta.entity.study.Study;
import com.ceri.delta.jpa.GroupHasEntityJpaController;
import com.ceri.delta.jpa.PersistenceHelper;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class StudyJpaController implements PersistenceHelper, Serializable {
     private static GroupHasEntityJpaController grouphasentityController;
     
    @SuppressWarnings("empty-statement")
    public StudyJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 

    public void create(Study study) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(study);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Study edit(Study study) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            study = em.merge(study);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = study.getIdStudy();
                if (findStudy(id) == null) {
                    throw new NonexistentEntityException("The study with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return study;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Study study;
            try {
                study = em.getReference(Study.class, id);
                study.getIdStudy();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The study with id " + id + " no longer exists.", enfe);
            }
            em.remove(study);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Study> findStudyByName(String name) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Study.findByName");
            q.setParameter("name", name);
            return q.getResultList();            
        } finally {
            em.close();
        }
    }

    
    public List<Object> findStudyEntities() {
        return findStudyEntities(true, -1, -1);
    }

    public List<Object> findStudyEntities(int maxResults, int firstResult) {
        return findStudyEntities(false, maxResults, firstResult);
    }

    private List<Object> findStudyEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Study as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    //-----------------------------------------------------------------------------------------------------------------
    public List<Object> findOrgStudyEntities(Integer orgId) {
        return findOrgStudyEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgStudyEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgStudyEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgStudyEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Study as o where idOrganization = " + orgId.toString());            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    //-----------------------------------------------------------------------------------------------------------------
    public List<Object> findOrgStudyEntitiesExtended(Integer orgId, String whereClause) {
        return findOrgStudyEntitiesExtended(true, -1, -1, orgId, whereClause);
    }

    public List<Object> findOrgStudyEntitiesExtended(int maxResults, int firstResult, Integer orgId, String whereClause) {
        return findOrgStudyEntitiesExtended(false, maxResults, firstResult, orgId, whereClause);
    }

    private List<Object> findOrgStudyEntitiesExtended(boolean all, int maxResults, int firstResult, Integer orgId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Study as o where idOrganization = " + orgId.toString() + " " + whereClause);            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    private List<Object> findStudyEntitiesByOrgGroup(boolean all, int maxResults, int firstResult, Integer orgId, Integer groupId) {
        EntityManager em = getEntityManager();
        try {
           Query q = em.createQuery("select object(o) from Study as o where idOrganization = " + orgId.toString() + " and o.idStudy in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'studies' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")"); 
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
 
    //-------------------------------------------------------------------------------------------------------------------------------
    public List<Object> findStudyEntitiesExtendedGroup(Integer orgId, Integer groupId, String whereClause) {
        return findStudyEntitiesExtendedGroup(true, -1, -1, orgId, groupId, whereClause);
    }

    public List<Object> findStudyEntitiesExtendedGroup(int maxResults, int firstResult, Integer orgId, Integer groupId, String whereClause) {
        return findStudyEntitiesExtendedGroup(false, maxResults, firstResult, orgId, groupId, whereClause);
    }

    private List<Object> findStudyEntitiesExtendedGroup(boolean all, int maxResults, int firstResult, Integer orgId, Integer groupId, String whereClause) {
        EntityManager em = getEntityManager();
        Query q;
        try {
            
            
            /* 
            Query q = em.createQuery("select object(o) from Study as o where idOrganization = " + orgId.toString() + " and o.idStudy in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'studies' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ") " + whereClause); 
            */
            
           System.out.println("-----------------findStudyEntitiesExtendedGroup--------------");
           System.out.println("all"+all);
           System.out.println("maxResults"+maxResults);
           System.out.println("orgId"+orgId);
           System.out.println("groupId"+groupId);
           System.out.println("firstResult"+firstResult);
           System.out.println("whereClause"+whereClause);
          
           
          
          if (grouphasentityController == null) {
            grouphasentityController = new GroupHasEntityJpaController();
            }
          String mystudies="studies";
          List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityGroupByIdAndType(groupId, mystudies);
          if (ghe.isEmpty()) {
            System.out.println("This is at returning null");
            return new ArrayList<Object>();
            //return null;
          }
          for(int i=0;i<ghe.size();i++){
              System.out.println("ghe"+ghe.get(i));
          }
           
           List<Integer> mylist = new ArrayList<Integer>();
          for(GroupHasEntity t : ghe) {
            int i = t.getGroupHasEntityPK().getEntityidEntity();
            mylist.add(i);            
          }
           System.out.println("mylist"+mylist);
           System.out.println("mylist string"+mylist.toString());
           String mylist1= mylist.toString().replace("[", "(");
           String mylist2= mylist1.replace("]", ")");
           System.out.println("mylist after replace"+mylist2); 
         
         
           
           
           
           if(!mylist.isEmpty()){
               q = em.createQuery
               ("select object(o) from Study as o where o.idOrganization = " + orgId.toString() + 
                " and o.idStudy in " 
                + mylist2 + whereClause); 
         
           }
           else{
               
               q = em.createQuery
               ("select object(o) from Study as o where o.idOrganization = " + orgId.toString() + 
                " and o.idStudy in " 
                + mylist2);
           }
           
           
            /*Query q = em.createQuery("select object(o) from Study as o where organization = " 
                   + orgId.toString() + " and o.idStudy in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'studies' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")"); */
           
            /*
             Query q = em.createQuery
            ("select object(o) from Study as o where o.idOrganization = " + orgId.toString() + 
                " and o.idStudy in " 
                + mylist2 + whereClause); 
         
           
             
            q = em.createQuery("select object(o) from Study as o where idOrganization = " + orgId.toString() 
                    + " and o.idGroup = "+ groupId.toString() + " "+ whereClause); 

            */
            
            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Object findStudy(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Study.class, id);
        } finally {
            em.close();
        }
    }

    public int getStudyCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Study as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getStudyCountByOrg(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Study as o where idOrganization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    

    public int getStudyCountByOrgGroup(Integer orgId, Integer groupId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Study as o where idOrganization = " + orgId.toString() + " and o.idStudy in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'studies' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")"); 
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    

    
    public int getStudyCountExtendedOrg(Integer orgId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Study as o where idOrganization = " + orgId.toString() + " " + whereClause);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    

    public int getStudyCountExtendedGroup(Integer orgId, Integer groupId, String whereClause) {
        EntityManager em = getEntityManager();
        Query q;
        try {
            
            /*
            Query q = em.createQuery("select count(o) from Study as o where idOrganization = " + orgId.toString() + " and o.idStudy in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'studies' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ") " + whereClause); */
            
            
            System.out.println("==============This is at getStudyCountExtendedGroup=========");
            if (grouphasentityController == null) {
            grouphasentityController = new GroupHasEntityJpaController();
            }
            
            String mystudies="studies";
            List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityGroupByIdAndType(groupId, mystudies);
            for(int i=0;i<ghe.size();i++){
                System.out.println("ghe"+ghe.get(i));
            }
            
            
            List<Integer> mylist = new ArrayList<Integer>();
            for (GroupHasEntity t : ghe) {
              int i = t.getGroupHasEntityPK().getEntityidEntity();
              mylist.add(i);            
            }

            System.out.println("mylist"+mylist);
            System.out.println("mylist string"+mylist.toString());
            String mylist1= mylist.toString().replace("[", "(");
            String mylist2= mylist1.replace("]", ")");
            System.out.println("mylist after replace"+mylist2);

            
            if(!mylist.isEmpty()){
                q = em.createQuery
                 ("select count(o) from Study as o where o.idOrganization = " + orgId.toString() + 
                     " and o.idStudy in " 
                     + mylist2 + whereClause); 
                }
            else{
                return 0;
            }
            
            
            /*
            q = em.createQuery
             ("select count(o) from Study as o where o.idOrganization = " + orgId.toString() + 
                 " and o.idGroup = "+ groupId.toString() +" "+ whereClause); 
            
            */
            
            
            
            
            
            
           
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    
    
    public List<Study> findStudyEntitiesByFilterId(Integer filterId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Study as o where Filter_idFilter = " + filterId.toString());            
            return q.getResultList();
        } finally {
            em.close();
        }
    }
 
    public List<Study> findStudyEntitiesByModelId(Integer modelId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Study as o where idModel = " + modelId.toString());            
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
   @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {  
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((com.ceri.delta.entity.study.Study)object).setCreatedTS(currentTimestamp);
        ((com.ceri.delta.entity.study.Study)object).setCreatedBy(currentUserId);        
        ((com.ceri.delta.entity.study.Study)object).setUpdatedTS(currentTimestamp);
        ((com.ceri.delta.entity.study.Study)object).setUpdatedBy(currentUserId);      
        com.ceri.delta.entity.study.Study obj = edit(((com.ceri.delta.entity.study.Study)object));
        AuditLogger.log(currentUserId, obj.getIdStudy(), "create", obj);
        return obj;   
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        ((com.ceri.delta.entity.study.Study)object).setUpdatedTS(currentTimestamp);
        ((com.ceri.delta.entity.study.Study)object).setUpdatedBy(currentUserId);      
        com.ceri.delta.entity.study.Study obj = edit(((com.ceri.delta.entity.study.Study)object));
        AuditLogger.log(currentUserId, obj.getIdStudy(), "update", obj);
        return obj;   
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        ((com.ceri.delta.entity.study.Study)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((com.ceri.delta.entity.study.Study)object).setCreatedTS(currentTimestamp);
        ((com.ceri.delta.entity.study.Study)object).setCreatedBy(currentUserId);        
        ((com.ceri.delta.entity.study.Study)object).setUpdatedTS(currentTimestamp);
        ((com.ceri.delta.entity.study.Study)object).setUpdatedBy(currentUserId);      
        com.ceri.delta.entity.study.Study obj = edit(((com.ceri.delta.entity.study.Study)object));
        AuditLogger.log(currentUserId, obj.getIdStudy(), "create", obj);
        return obj;          
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        ((com.ceri.delta.entity.study.Study)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());        
        ((com.ceri.delta.entity.study.Study)object).setUpdatedTS(currentTimestamp);
        ((com.ceri.delta.entity.study.Study)object).setUpdatedBy(currentUserId);      
        com.ceri.delta.entity.study.Study obj = edit(((com.ceri.delta.entity.study.Study)object));
        AuditLogger.log(currentUserId, obj.getIdStudy(), "update", obj);
        return obj;  
    }

    @Override
    public String getObjectType() {
        return "studies";
    }

    @Override
    public Integer getObjectId(Object subject) {
        return ((Study)subject).getIdStudy();
    }
    
    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findStudyEntities(maxResults, firstResult);   
    }

    @Override
    public List<Object> findObjectEntities() {
        return findStudyEntities();   
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
         return findOrgStudyEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    public Object findObjectEntityById(Integer objectId) {
         return findStudy(objectId);
    }    
    
    @Override
    public int getObjectCount() {
        return getStudyCount();
    }

    @Override
    public int getObjectCountByOrg(Integer currentOrgId) {
        return getStudyCountByOrg(currentOrgId);
    }

    @Override
    public int getObjectCountByOrgGroup(Integer currentOrgId, Integer groupId) {
        return getStudyCountByOrgGroup(currentOrgId, groupId);
    }
    
    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((com.ceri.delta.entity.study.Study)subject).getIdStudy(), "delete", subject);
        destroy(((com.ceri.delta.entity.study.Study)subject).getIdStudy());
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((com.ceri.delta.entity.study.Study)subject).getIdStudy(), "delete", subject);
        destroy(((com.ceri.delta.entity.study.Study)subject).getIdStudy());
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrg(int maxResults, int firstResult, Integer currentOrgId, String whereClause) {
        return findOrgStudyEntitiesExtended(maxResults, firstResult, currentOrgId, whereClause);
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrg(Integer currentOrgId, String whereClause) {
        return findOrgStudyEntitiesExtended(currentOrgId, whereClause);
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtendedOrg(Integer currentOrgId, String whereClause) {
        return getStudyCountExtendedOrg(currentOrgId, whereClause);
    }

    @Override
    public int getObjectCountExtendedOrgGroup(Integer currentOrgId, Integer groupId, String whereClause) {
        return getStudyCountExtendedGroup(currentOrgId, groupId, whereClause);
    }
    
    @Override
    public List<Object> findObjectEntitiesByOrgGroup(Integer orgId, Integer groupId) {
        return findStudyEntitiesByOrgGroup(true, -1, -1, orgId, groupId);
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(int maxResults, int firstResult, Integer orgId, Integer groupId) {
        return findStudyEntitiesByOrgGroup(false, maxResults, firstResult, orgId, groupId);
    }
    
    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(Integer currentOrgId, Integer idGroup, String whereClause) {
        return findStudyEntitiesExtendedGroup(currentOrgId, idGroup, whereClause);
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(int maxResults, int firstResult, Integer currentOrgId, Integer idGroup, String whereClause) {
        return findStudyEntitiesExtendedGroup(maxResults, firstResult, currentOrgId, idGroup, whereClause);
    }

    @Override
    public List<Object> findObjectEntitiesExtended(int maxResults, int firstResult, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
}
