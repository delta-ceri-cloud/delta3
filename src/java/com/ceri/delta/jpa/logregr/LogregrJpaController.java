/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.jpa.logregr;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.GroupHasEntity;
import com.ceri.delta.entity.logregr.Logregr;
import com.ceri.delta.jpa.GroupHasEntityJpaController;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.PersistenceHelper;
import com.ceri.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author Coping Systems Inc.
 */
public class LogregrJpaController implements PersistenceHelper, Serializable {
    private static GroupHasEntityJpaController grouphasentityController;
    
    @SuppressWarnings("empty-statement")
    public LogregrJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    } 

    public void create(Logregr logregr) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(logregr);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public Logregr edit(Logregr logregr) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            logregr = em.merge(logregr);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = logregr.getIdLogregr();
                if (findLogregr(id) == null) {
                    throw new NonexistentEntityException("The logregr with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return logregr;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Logregr logregr;
            try {
                logregr = em.getReference(Logregr.class, id);
                logregr.getIdLogregr();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The logregr with id " + id + " no longer exists.", enfe);
            }
            em.remove(logregr);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Object> findLogregrEntities() {
        return findLogregrEntities(true, -1, -1);
    }

    public List<Object> findLogregrEntities(int maxResults, int firstResult) {
        return findLogregrEntities(false, maxResults, firstResult);
    }

    private List<Object> findLogregrEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Logregr as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }


    public List<Object> findOrgLogregrEntities(Integer orgId) {
        return findOrgLogregrEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgLogregrEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgLogregrEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgLogregrEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Logregr as o where idOrganization = " + orgId.toString());
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Logregr findLogregr(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Logregr.class, id);
        } finally {
            em.close();
        }
    }

    public List<Logregr> findLogregrByProcessId(Integer processId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Logregr as o where idProcess = " + processId.toString());            
            return q.getResultList();
        } finally {
            em.close();
        }
    } 
    
    public List<Logregr> findLogregrByNameAndProcessId(String name, Integer processId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Logregr as o where name = '" + name + "'"
            + " and idProcess = " + processId.toString() + " order by idProcess");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }     
    
    public List<Logregr> findLogregrByStudyId(Integer studyId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Logregr as o where idStudy = " + studyId.toString() + " order by idProcess");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }    
    
    public List<Logregr> findLogregrByModelId(Integer modelId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Logregr as o where idModel = " + modelId.toString() + " order by idProcess");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }    
    
    public int getLogregrCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Logregr as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public int getLogregrCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Logregr as o where idOrganization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    } 
    
    @Override    
    public List<Object> findObjectEntitiesExtendedOrg(Integer orgId, String whereClause) {
        return findObjectEntitiesExtendedOrg(true, -1, -1, orgId, whereClause);
    }
    
    @Override
    public List<Object> findObjectEntitiesExtendedOrg(int maxResults, int firstResult, Integer orgId, String whereClause) {
        return findObjectEntitiesExtendedOrg(false, maxResults, firstResult, orgId, whereClause);
    }
    
    private List<Object> findObjectEntitiesExtendedOrg(boolean all, int maxResults, int firstResult, Integer orgId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Logregr as o where idOrganization = " 
                    + orgId.toString() + whereClause);            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public int getLogregrCountExtendedOrg(Integer orgId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Logregr as o where idOrganization = " 
                    + orgId.toString() + whereClause);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
   
    public int getLogregrCountExtendedOrgGroup(Integer orgId, Integer groupId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
            Query q ;
            /*Query q = em.createQuery("select count(o) from Logregr as o where idOrganization = " + orgId.toString() + " and o.idModel in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'models' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")" + whereClause); */
            
            System.out.println("==============This is at getModelCountExtendedOrgGroup=========");
            if (grouphasentityController == null) {
            grouphasentityController = new GroupHasEntityJpaController();
            }
            
            String mymodel="models";
            List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityGroupByIdAndType(groupId, mymodel);
            for(int i=0;i<ghe.size();i++){
                System.out.println("ghe"+ghe.get(i));
            }
            
            
            
            List<Integer> mylist = new ArrayList<Integer>();
            for (GroupHasEntity t : ghe) {
              int i = t.getGroupHasEntityPK().getEntityidEntity();
              mylist.add(i);            
            }

            System.out.println("mylist"+mylist);
            System.out.println("mylist string"+mylist.toString());
            String mylist1= mylist.toString().replace("[", "(");
            String mylist2= mylist1.replace("]", ")");
            System.out.println("mylist after replace"+mylist2);

            if(!mylist.isEmpty()){
            
            q = em.createQuery
             ("select count(o) from Logregr as o where idOrganization = " + orgId.toString() + 
                 " and o.idModel in " 
                 + mylist2 + whereClause); 
            }
            
            else{
                return 0;
            }
            
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }   
    
    @Override
    public Object createObjects(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Logregr)object).setCreatedTS(currentTimestamp);
        ((Logregr)object).setCreatedBy(currentUserId);        
        ((Logregr)object).setUpdatedTS(currentTimestamp);
        ((Logregr)object).setUpdatedBy(currentUserId);        
        Logregr obj = edit(((Logregr)object));
        AuditLogger.log(currentUserId, ((Logregr)obj).getIdModel(), "create", obj);   
        return obj;
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Logregr)object).setUpdatedTS(currentTimestamp);
        ((Logregr)object).setUpdatedBy(currentUserId);   
        Logregr obj = edit(((Logregr)object));        
        AuditLogger.log(currentUserId, ((Logregr)obj).getIdModel(), "update", obj);      
        return obj;
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Logregr)object).setIdOrganization(currentOrgId);
        ((Logregr)object).setCreatedTS(currentTimestamp);
        ((Logregr)object).setCreatedBy(currentUserId);        
        ((Logregr)object).setUpdatedTS(currentTimestamp);
        ((Logregr)object).setUpdatedBy(currentUserId);        
        Logregr obj = edit(((Logregr)object));
        AuditLogger.log(currentUserId, ((Logregr)obj).getIdModel(), "create", obj);   
        return obj;
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
        ((Logregr)object).setIdOrganization(currentOrgId);    
        ((Logregr)object).setUpdatedTS(currentTimestamp);
        ((Logregr)object).setUpdatedBy(currentUserId);   
        Logregr obj = edit(((Logregr)object));        
        AuditLogger.log(currentUserId, ((Logregr)obj).getIdModel(), "update", obj);      
        return obj;
    }

    @Override
    public String getObjectType() {
        return "logregrs";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
        return findOrgLogregrEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        return getLogregrCount();
    }

    @Override
    public int getObjectCountByOrg(Integer currentOrgId) {
        return getLogregrCount(currentOrgId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((Logregr)subject).getIdLogregr(), "delete", subject);
        destroy(((Logregr)subject).getIdLogregr());
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws com.ceri.delta.jpa.exceptions.NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object findObjectEntityById(Integer objectId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getObjectId(Object subject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(int maxResults, int firstResult, Integer currentOrgId, Integer idGroup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountByOrgGroup(Integer currentOrgId, Integer groupId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(Integer currentOrgId, Integer idGroup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(Integer orgId, Integer groupId, String whereClause) {
        return findObjectEntitiesExtendedOrgGroup(true, -1, -1, orgId, groupId, whereClause);
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(int maxResults, int firstResult, Integer orgId, Integer groupId, String whereClause) {
        return findObjectEntitiesExtendedOrgGroup(false, maxResults, firstResult, orgId, groupId, whereClause);
    }

    private List<Object> findObjectEntitiesExtendedOrgGroup(boolean all, int maxResults, int firstResult, Integer orgId, Integer groupId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
            
            Query q;
            /*
            Query q = em.createQuery("select object(o) from Logregr as o where idOrganization = " + orgId.toString() + " and o.idModel in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'models' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")" + whereClause);*/  
           
           if (grouphasentityController == null) {
            grouphasentityController = new GroupHasEntityJpaController();
            }
          String mymodel="models";
          List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityGroupByIdAndType(groupId, mymodel);
          
           if (ghe.isEmpty()) {
            System.out.println("This is at returning null");
            return new ArrayList<Object>();
            //return null;
          }
          
          
          for(int i=0;i<ghe.size();i++){
              System.out.println("ghe"+ghe.get(i));
          }
          
          List<Integer> mylist = new ArrayList<Integer>();
          for(GroupHasEntity t : ghe) {
            int i = t.getGroupHasEntityPK().getEntityidEntity();
            mylist.add(i);            
          }
           System.out.println("mylist"+mylist);
           System.out.println("mylist string"+mylist.toString());
          
           String mylist1= mylist.toString().replace("[", "(");
           String mylist2= mylist1.replace("]", ")");
           System.out.println("mylist after replace"+mylist2); 
          
            
           
           if(!mylist.isEmpty()){
            q = em.createQuery
            ("select object(o) from Model as o where organization = " + orgId.toString() + 
                " and o.idModel in " 
                + mylist2 + whereClause);   
            }
           else{
               q = em.createQuery("select object(o) from Model as o where organization = " + orgId.toString() + 
                " and o.idModel in " 
                + mylist2);
           }
          
           
           
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }   

    @Override
    public int getObjectCountExtendedOrg(Integer currentOrgId, String whereClause) {
        return getLogregrCountExtendedOrg(currentOrgId, whereClause);
    }

    @Override
    public int getObjectCountExtendedOrgGroup(Integer currentOrgId, Integer groupId, String whereClause) {
        return getLogregrCountExtendedOrgGroup(currentOrgId, groupId, whereClause);
    }

    @Override
    public List<Object> findObjectEntitiesExtended(int maxResults, int firstResult, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
