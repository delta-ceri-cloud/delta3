package com.ceri.delta.jpa;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.GroupHasEntity;
import com.ceri.delta.entity.GroupHasEntityPK;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

public class GroupHasEntityJpaController implements Serializable {
  private EntityManagerFactory emf = null;
  
  public EntityManager getEntityManager() {
    return this.emf.createEntityManager();
  }
  
  public GroupHasEntityJpaController() {
    this.emf = PersistenceManager.getInstance().getEntityManagerFactory("CeriPU");
  }
  
  public void create(GroupHasEntity groupHasEntity) throws PreexistingEntityException, Exception {
    if (groupHasEntity.getGroupHasEntityPK() == null)
      groupHasEntity.setGroupHasEntityPK(new GroupHasEntityPK()); 
    EntityManager em = null;
    try {
      em = getEntityManager();
      em.getTransaction().begin();
      em.persist(groupHasEntity);
      em.getTransaction().commit();
    } catch (Exception ex) {
      if (findGroupHasEntity(groupHasEntity.getGroupHasEntityPK()) != null)
        throw new PreexistingEntityException("GroupHasEntity " + groupHasEntity + " already exists.", ex); 
      throw ex;
    } finally {
      if (em != null)
        em.close(); 
    } 
  }
  
  public GroupHasEntity edit(GroupHasEntity groupHasEntity) throws NonexistentEntityException, Exception {
    EntityManager em = null;
    try {
      em = getEntityManager();
      em.getTransaction().begin();
      groupHasEntity = (GroupHasEntity)em.merge(groupHasEntity);
      em.getTransaction().commit();
    } catch (Exception ex) {
      String msg = ex.getLocalizedMessage();
      if (msg == null || msg.length() == 0) {
        GroupHasEntityPK id = groupHasEntity.getGroupHasEntityPK();
        if (findGroupHasEntity(id) == null)
          throw new NonexistentEntityException("The groupHasEntity with id " + id + " no longer exists."); 
      } 
      throw ex;
    } finally {
      if (em != null)
        em.close(); 
    } 
    return groupHasEntity;
  }
  
  public void destroy(GroupHasEntityPK id) throws NonexistentEntityException {
    EntityManager em = null;
    try {
      GroupHasEntity groupHasEntity;
      em = getEntityManager();
      em.getTransaction().begin();
      try {
        groupHasEntity = (GroupHasEntity)em.getReference(GroupHasEntity.class, id);
        groupHasEntity.getGroupHasEntityPK();
      } catch (EntityNotFoundException enfe) {
        throw new NonexistentEntityException("The groupHasEntity with id " + id + " no longer exists.", enfe);
      } 
      em.remove(groupHasEntity);
      em.getTransaction().commit();
    } finally {
      if (em != null)
        em.close(); 
    } 
  }
  
  public void destroyByType(GroupHasEntityPK id, String type) throws NonexistentEntityException {
    EntityManager em = null;
    try {
      GroupHasEntity groupHasEntity;
      em = getEntityManager();
      em.getTransaction().begin();
      try {
        groupHasEntity = (GroupHasEntity)em.getReference(GroupHasEntity.class, id);
        groupHasEntity.getGroupHasEntityPK();
        if ((groupHasEntity.getType() == null) ? (type != null) : !groupHasEntity.getType().equals(type))
          throw new EntityNotFoundException(); 
      } catch (EntityNotFoundException enfe) {
        throw new NonexistentEntityException("The groupHasEntity with id " + id + " no longer exists.", enfe);
      } 
      em.remove(groupHasEntity);
      em.getTransaction().commit();
    } finally {
      if (em != null)
        em.close(); 
    } 
  }
  
  public List<GroupHasEntity> findGroupHasEntityEntities() {
    return findGroupHasEntityEntities(true, -1, -1);
  }
  
  public List<GroupHasEntity> findGroupHasEntityEntities(int maxResults, int firstResult) {
    return findGroupHasEntityEntities(false, maxResults, firstResult);
  }
  
  private List<GroupHasEntity> findGroupHasEntityEntities(boolean all, int maxResults, int firstResult) {
    EntityManager em = getEntityManager();
    try {
      Query q = em.createQuery("select object(o) from GroupHasEntity as o");
      if (!all) {
        q.setMaxResults(maxResults);
        q.setFirstResult(firstResult);
      } 
      return q.getResultList();
    } finally {
      em.close();
    } 
  }
  
  public List<GroupHasEntity> findGroupHasEntityEntitiesByIdAndType(Integer entityId, String type) {
    EntityManager em = getEntityManager();
    try {
      Query q = em.createNamedQuery("GroupHasEntity.findByEntityidEntityAndType");
      q.setParameter("entityidEntity", entityId);
      q.setParameter("type", type);
      return q.getResultList();
    } finally {
      em.close();
    } 
  }
  
  public List<GroupHasEntity> findGroupHasEntityGroupByIdAndType(Integer groupId, String type) {
    EntityManager em = getEntityManager();
    try {
      Query q = em.createNamedQuery("GroupHasEntity.findByGroupidGroupAndType");
      q.setParameter("groupidGroup", groupId);
      q.setParameter("type", type);
      return q.getResultList();
    } finally {
      em.close();
    } 
  }
  
  public GroupHasEntity findGroupHasEntity(GroupHasEntityPK id) {
    EntityManager em = getEntityManager();
    try {
      return (GroupHasEntity)em.find(GroupHasEntity.class, id);
    } finally {
      em.close();
    } 
  }
  
  public int getGroupHasEntityCount() {
    EntityManager em = getEntityManager();
    try {
      Query q = em.createQuery("select count(o) from GroupHasEntity as o");
      return ((Long)q.getSingleResult()).intValue();
    } finally {
      em.close();
    } 
  }
  
  public int getGroupHasEntityCountbyModel() {
    EntityManager em = getEntityManager();
    try {
      Query q = em.createQuery("select count(o.groupHasEntityPK) from GroupHasEntity as o");
      System.out.println("This is getGroupHasEntityCountbyModel count" +q.getSingleResult());
      //System.out.println(q.getResultList());
      System.out.println(q.getSingleResult());                      
      return ((Long)q.getSingleResult()).intValue();
    } finally {
      em.close();
    } 
  }
  
}

