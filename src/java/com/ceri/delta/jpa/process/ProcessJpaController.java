/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.delta.jpa.process;

import com.ceri.delta.db.PersistenceManager;
import com.ceri.delta.entity.GroupHasEntity;
import com.ceri.delta.entity.process.Process;
import com.ceri.delta.jpa.GroupHasEntityJpaController;
import com.ceri.delta.jpa.PersistenceHelper;
import com.ceri.delta.jpa.exceptions.NonexistentEntityException;
import com.ceri.delta.jpa.exceptions.PreexistingEntityException;
import com.ceri.delta.security.AuditLogger;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import java.net.URL;
import java.net.InetAddress;
import java.util.ArrayList;
/**
 *
 * @author Coping Systems Inc.
 */
public class ProcessJpaController implements PersistenceHelper, Serializable {
     private static GroupHasEntityJpaController grouphasentityController;
    @SuppressWarnings("empty-statement")
    public ProcessJpaController() {
        this.emf = PersistenceManager.getInstance().getEntityManagerFactory("DeltaPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }    

    public Process create(Process process) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(process);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProcess(process.getIdProcess()) != null) {
                throw new PreexistingEntityException("Process " + process + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return process;
    }

    public Process edit(Process process) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            process = em.merge(process);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = process.getIdProcess();
                if (findProcess(id) == null) {
                    throw new NonexistentEntityException("The process with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return process;
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Process process;
            try {
                process = em.getReference(Process.class, id);
                process.getIdProcess();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The process with id " + id + " no longer exists.", enfe);
            }
            em.remove(process);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
            
    public List<Object> findProcessEntities() {
        return findProcessEntities(true, -1, -1);
    }

    public List<Object> findProcessEntities(int maxResults, int firstResult) {
        return findProcessEntities(false, maxResults, firstResult);
    }

    private List<Object> findProcessEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    //--------------------------------------------------------------------------       
    public List<Object> findOrgProcessEntities(Integer orgId) {
        return findOrgProcessEntities(true, -1, -1, orgId);
    }

    public List<Object> findOrgProcessEntities(int maxResults, int firstResult, Integer orgId) {
        return findOrgProcessEntities(false, maxResults, firstResult, orgId);
    }

    private List<Object> findOrgProcessEntities(boolean all, int maxResults, int firstResult, Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o where idOrganization = " + orgId.toString());            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public Process findProcess(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Process.class, id);
        } finally {
            em.close();
        }
    }

    public List<Process> findProcessEntitiesByStudyId(Integer studyId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o where idStudy = " + studyId.toString() + " order by idProcess");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Object> findActiveProcessEntities() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o where progress > 0 and progress < 100 and status = 'InProgress'");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Object> findActiveProcessEntities(String timestamp) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o where progress > 0 and progress < 100 and status = 'InProgress' and updatedTS > '" + timestamp + "'");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Object> findProcessByStudyId(Integer studyId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o where idStudy = " + studyId.toString() + " order by idProcess");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }     
    
    public List<Object> findProcessByNameAndStudyId(String name, Integer studyId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o where name = '" + name + "'"
                    + " and idStudy = " + studyId.toString() + " order by idProcess");            
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public int getProcessCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Process as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public int getProcessCount(Integer orgId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Process as o where idOrganization = " + orgId.toString());
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
 
    public int getProcessCountExtendedOrg(Integer orgId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Process as o where idOrganization = " 
                    + orgId.toString() + whereClause);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int getProcessCountByOrgGroup(Integer orgId, Integer groupId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Process as o where idOrganization = " + orgId.toString() + " and o.idStudy in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'studies' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")"); 
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }       
    
    public int getProcessCountExtendedOrgGroup(Integer orgId, Integer groupId, String whereClause) {
        EntityManager em = getEntityManager();
         Query q;
        try {
            
            System.out.println("orgId "+orgId);
            System.out.println("groupId "+groupId);
            /*
            Query q = em.createQuery("select count(o) from Process as o where idOrganization = " + orgId.toString() + " and o.idStudy in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'studies' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")" + whereClause); 
            
            */
            
            System.out.println("==============This is at ProcessCount=========");
            if (grouphasentityController == null) {
            grouphasentityController = new GroupHasEntityJpaController();
            }
            
            String mystudies="studies";
            List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityGroupByIdAndType(groupId, mystudies);
            for(int i=0;i<ghe.size();i++){
                System.out.println("ghe"+ghe.get(i));
            }
            
            
            List<Integer> mylist = new ArrayList<Integer>();
            for (GroupHasEntity t : ghe) {
              int i = t.getGroupHasEntityPK().getEntityidEntity();
              mylist.add(i);            
            }

            System.out.println("mylist"+mylist);
            System.out.println("mylist string"+mylist.toString());
            String mylist1= mylist.toString().replace("[", "(");
            String mylist2= mylist1.replace("]", ")");
            System.out.println("mylist after replace"+mylist2);

            
            if(!mylist.isEmpty()){
                q = em.createQuery
                 ("select count(o) from Process as o where o.idOrganization = " + orgId.toString() + 
                     " and o.idStudy in " 
                     + mylist2 + whereClause); 
                }
            else{
                return 0;
            }
            
            
            
            
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }    
    
    @Override
    public Object createObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        ((Process)object).setGuid(UUID.randomUUID().toString());      
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Process)object).setCreatedTS(currentTimestamp);
        ((Process)object).setCreatedBy(currentUserId);        
        ((Process)object).setUpdatedTS(currentTimestamp);
        ((Process)object).setUpdatedBy(currentUserId);
        String IdFile = UUID.randomUUID().toString();
        ((Process)object).setIdFile(IdFile);
        //((Process)object).setIdFile(((Process)object).getGuid());
        String fileurlstring="http://"+InetAddress.getLocalHost().getHostAddress() +":8080/Delta3/resources/"+IdFile+".csv";
        
        //String fileurlstring="http://"+InetAddress.getLocalHost().getHostAddress() +":8080/Delta3/resources/files/"+IdFile+".csv";
        //String fileurlstring="http://"+InetAddress.getLocalHost().getHostAddress() +":8080/Delta3/resources/files/"+UUID.randomUUID().toString()+".csv";
        System.out.println("AT file urlstring 1 jpa "+fileurlstring);
        ((Process)object).setfileUrl(fileurlstring);
        
        Process obj = edit(((Process)object));
        AuditLogger.log(currentUserId, obj.getIdProcess(), "create", obj);
        return obj;   
    }

    @Override
    public Object updateObjects(Object object, Integer currentUserId) throws NonexistentEntityException, Exception {
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());          
        ((Process)object).setUpdatedTS(currentTimestamp);
        ((Process)object).setUpdatedBy(currentUserId);      
        Process obj = edit(((Process)object));
        AuditLogger.log(currentUserId, obj.getIdProcess(), "create", obj);
        return obj;   
    }

    @Override
    public Object createObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        ((Process)object).setGuid(UUID.randomUUID().toString());     
        ((Process)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());  
        ((Process)object).setCreatedTS(currentTimestamp);
        ((Process)object).setCreatedBy(currentUserId);        
        ((Process)object).setUpdatedTS(currentTimestamp);
        ((Process)object).setUpdatedBy(currentUserId); 
        String IdFile = UUID.randomUUID().toString();
        ((Process)object).setIdFile(IdFile);
        //((Process)object).setIdFile(((Process)object).getGuid());
        String fileurlstring="http://"+InetAddress.getLocalHost().getHostAddress() +":8080/Delta3/resources/"+IdFile+".csv";
        
        //String fileurlstring="http://"+InetAddress.getLocalHost().getHostAddress() +":8080/Delta3/resources/files/"+IdFile+".csv";
        //String fileurlstring="http://"+InetAddress.getLocalHost().getHostAddress() +":8080/Delta3/resources/files/"+UUID.randomUUID().toString()+".csv";
        System.out.println("AT file urlstring 1 jpa "+fileurlstring);
        ((Process)object).setfileUrl(fileurlstring);
        
        Process obj = edit(((Process)object));
        AuditLogger.log(currentUserId, obj.getIdProcess(), "create", obj);
        return obj;          
    }

    @Override
    public Object updateObjects(Object object, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException, Exception {
        ((Process)object).setIdOrganization(currentOrgId);       
        Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());        
        ((Process)object).setUpdatedTS(currentTimestamp);
        ((Process)object).setUpdatedBy(currentUserId);      
        Process obj = edit(((Process)object));
        AuditLogger.log(currentUserId, obj.getIdProcess(), "create", obj);
        return obj;  
    }

    @Override
    public String getObjectType() {
        return "processes";
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult) {
        return findProcessEntities(maxResults, firstResult);   
    }

    @Override
    public List<Object> findObjectEntities() {
        return findProcessEntities();   
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntities(int maxResults, int firstResult, Integer currentOrgId) {
         return findOrgProcessEntities(maxResults, firstResult, currentOrgId);
    }

    @Override
    public List<Object> findObjectEntities(Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesById(int maxResults, int firstResult, int id, Integer currentOrgId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCount() {
        return getProcessCount();
    }

    @Override
    public int getObjectCountByOrg(Integer currentOrgId) {
        return getProcessCount(currentOrgId);
    }

    @Override
    public void deleteObject(Object subject, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((Process)subject).getIdProcess(), "delete", subject);
        destroy(((Process)subject).getIdProcess());
    }

    @Override
    public void deleteObject(Object subject, Integer currentOrgId, Integer currentUserId) throws NonexistentEntityException {
        AuditLogger.log(currentUserId, ((Process)subject).getIdProcess(), "delete", subject);
        destroy(((Process)subject).getIdProcess());
    }

    @Override
    public int getObjectCount(int currentUserId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object findObjectEntityById(Integer objectId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getObjectId(Object subject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(Integer orgId, Integer groupId) {
        return findObjectEntitiesByOrgGroup(true, -1, -1, orgId, groupId);
    }

    @Override
    public List<Object> findObjectEntitiesByOrgGroup(int maxResults, int firstResult, Integer orgId, Integer groupId) {
        return findObjectEntitiesByOrgGroup(false, maxResults, firstResult, orgId, groupId);
    }

    private List<Object> findObjectEntitiesByOrgGroup(boolean all, int maxResults, int firstResult, Integer orgId, Integer groupId) {
        EntityManager em = getEntityManager();
        try {
           Query q = em.createQuery("select object(o) from Process as o where idOrganization = " + orgId.toString() + " and o.idStudy in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'studies' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")"); 
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public int getObjectCountByOrgGroup(Integer currentOrgId, Integer groupId) {
        return getProcessCountByOrgGroup(currentOrgId, groupId);
    }

    @Override
    public int getObjectCountExtendedOrg(Integer currentOrgId, String whereClause) {
        return getProcessCountExtendedOrg(currentOrgId, whereClause);
    }

    @Override
    public int getObjectCountExtendedOrgGroup(Integer currentOrgId, Integer groupId, String whereClause) {
        return getProcessCountExtendedOrgGroup(currentOrgId, groupId, whereClause);
    }

    @Override
    public List<Object> findObjectEntitiesExtended(int maxResults, int firstResult, String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> findObjectEntitiesExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getObjectCountExtended(String whereClause) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override    
    public List<Object> findObjectEntitiesExtendedOrg(Integer orgId, String whereClause) {
        return findObjectEntitiesExtendedOrg(true, -1, -1, orgId, whereClause);
    }
    
    @Override
    public List<Object> findObjectEntitiesExtendedOrg(int maxResults, int firstResult, Integer orgId, String whereClause) {
        return findObjectEntitiesExtendedOrg(false, maxResults, firstResult, orgId, whereClause);
    }
    
    private List<Object> findObjectEntitiesExtendedOrg(boolean all, int maxResults, int firstResult, Integer orgId, String whereClause) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Process as o where idOrganization = " 
                    + orgId.toString() + whereClause);            
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    //add new code
     public String findLatestProcessByStudyId(Integer studyId) throws InterruptedException {
        EntityManager em = getEntityManager();
       
        try {
            
            System.out.println("This is at the fin latestbyiD method");
            System.out.println("This is studyId"+studyId);
            
            //select input from Process as p where idStudy = "+studyId+" order by createdTS DESC LIMIT 1
            //Query q = em.createQuery("select object(o) from Process as o where idStudy = " 
            //        + studyId.toString() + " order by idProcess"); 
            
            //Query q1 = em.createQuery("select input from Process as p where idStudy = "+studyId.toString()+" order by createdTS DESC");
            
            Query q1 = em.createQuery("select input from Process as p where idStudy = 127 order by createdTS DESC");
            
            
            if(q1.getResultList().get(0)==null)
            {
                
                    Thread.sleep(5000);
                    System.out.println("q1 get 0---------------"+q1.getResultList());
                    System.out.println("q1 get 1---------------"+q1.getResultList().get(0).toString());
                    System.out.println("q1 get 2---------------"+q1.toString());
                    String str=(String) q1.getResultList().get(0);
                    //q1.setFirstResult(0);
                    return str;
                    
            }
            else{
                String str=(String) q1.getResultList().get(0);
                //q1.setFirstResult(0);
                return str;
            }
        } finally {
            em.close();
        }
    }
    
    
    
    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(Integer orgId, Integer groupId, String whereClause) {
        return findObjectEntitiesExtendedOrgGroup(true, -1, -1, orgId, groupId, whereClause);
    }

    @Override
    public List<Object> findObjectEntitiesExtendedOrgGroup(int maxResults, int firstResult, Integer orgId, Integer groupId, String whereClause) {
        return findObjectEntitiesExtendedOrgGroup(false, maxResults, firstResult, orgId, groupId, whereClause);
    }

    private List<Object> findObjectEntitiesExtendedOrgGroup(boolean all, int maxResults, int firstResult, Integer orgId, Integer groupId, String whereClause) {
        EntityManager em = getEntityManager();
        Query q;
        try {
            
            /*
            Query q = em.createQuery("select object(o) from Process as o where idOrganization = " + orgId.toString() + " and o.idStudy in " 
                + "(select g.groupHasEntityPK.entityidEntity from GroupHasEntity as g where g.type = 'studies' and g.groupHasEntityPK.groupidGroup = " 
                + groupId.toString() + ")" + whereClause); */
           
            System.out.println("-----------------findObjectEntitiesExtendedOrgGroup--------------");
           System.out.println("all"+all);
           System.out.println("maxResults"+maxResults);
           System.out.println("orgId"+orgId);
           System.out.println("groupId"+groupId);
           System.out.println("firstResult"+firstResult);
           System.out.println("whereClause"+whereClause);
          
          if (grouphasentityController == null) {
            grouphasentityController = new GroupHasEntityJpaController();
            }
          String mystudies="studies";
          List<GroupHasEntity> ghe = grouphasentityController.findGroupHasEntityGroupByIdAndType(groupId, mystudies);
          if (ghe.isEmpty()) {
            System.out.println("This is at returning null");
            return new ArrayList<Object>();
            //return null;
          }
          for(int i=0;i<ghe.size();i++){
              System.out.println("ghe"+ghe.get(i));
          }
           
           List<Integer> mylist = new ArrayList<Integer>();
          for(GroupHasEntity t : ghe) {
            int i = t.getGroupHasEntityPK().getEntityidEntity();
            mylist.add(i);            
          }
           System.out.println("mylist"+mylist);
           System.out.println("mylist string"+mylist.toString());
           String mylist1= mylist.toString().replace("[", "(");
           String mylist2= mylist1.replace("]", ")");
           System.out.println("mylist after replace"+mylist2); 
         
         
           if(!mylist.isEmpty()){
               q = em.createQuery
               ("select object(o) from Process as o where o.idOrganization = " + orgId.toString() + 
                " and o.idStudy in " 
                + mylist2 + whereClause); 
         
           }
           else{
               
               q = em.createQuery
               ("select object(o) from Process as o where o.idOrganization = " + orgId.toString() + 
                " and o.idStudy in " 
                + mylist2);
           }
           
           
          
           
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }    
}
