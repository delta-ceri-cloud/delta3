/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceri.proxy.service;

import com.copsys.statproxy.Proxy;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Coping Systems Inc.
 */
public class ProxyBridge {
    private static final Logger logger = Logger.getLogger(ProxyBridge.class.getName());        
    private static StatisticsServiceImpl statService = null;
    private static Proxy p = null;
    
    public ProxyBridge() {
        statService = new StatisticsServiceImpl();  
    } 

    
    public Proxy getProxy(int multicast, String partnerIp, String interfaceIp) {
        if ( p == null ) {
            p = Proxy.getInstance();
            p.intialize(Proxy.DELTA_MODE, multicast, partnerIp, interfaceIp);
            p.addDeltaObserver(statService);
        } else {
            logger.log(Level.INFO, "Proxy already initialized. Passing existing instance.");
        }
        return p;
    }
    
    public Proxy stopProxy() {
        if ( p != null ) {

        } else {
            logger.log(Level.INFO, "Proxy not initialized. Nothing to stop.");
        }
        return p;
    }    
}
