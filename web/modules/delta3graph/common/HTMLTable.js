/**
 * (HTML) Table generator
 */
   
var HTMLTable = function() {    
    var id = '',
    tag = '',
    width = '100%', 
    tableTitle = ''
};


HTMLTable.prototype.init = function (tblConfig, id, width) {
    this.tag = tblConfig.tag;
    this.id = id;
    this.tableTitle = tblConfig.description;
    this.everyTab = tblConfig.everyTab;
    if ( typeof width === 'undefined' ) {
        this.width = "100%";
    } else {
        this.width = width;
    }
}

HTMLTable.prototype.getMarkup = function (resultsArray, index) {
    var retHtml = '<table class="delta3table" id=' + this.id + ' width=' + this.width + ' >';
    retHtml += '<caption class="delta3table">' + this.tableTitle + '</caption>';
    retHtml += this.buildHeader(resultsArray);
    retHtml += this.buildRows(resultsArray, this.everyTab, index);
    retHtml += '</table>';
    return retHtml;
}

HTMLTable.prototype.buildHeader = function(resultsArray) {
    var j, i;
    var topColumnName = [];
    var middleColumnName = [];
    var lastTopColumnName = '';
    var lastMiddleColumnName = '';    
    var bottomColumnName = [];
    var topHeaderRowPresent = false;
    var middleHeaderRowPresent = false;
    var bottomHeaderRowPresent = false;   
    var topColSpan = 0;
    var middleColSpan = 0; 

    for (j = 0; j < resultsArray.results.length; j++) {
        if (resultsArray.results[j].name === this.tag) {
            var size = resultsArray.results[j].columns.length;
             //console.log("resultsArray.results", resultsArray.results[j]);
             //console.log("columns names", resultsArray.results[j].name);
             
            // pre-processor loop
            for (i = 0; i < size; i++) {
                if ((typeof resultsArray.results[j].columns[i].parentcolumn !== 'undefined') && (resultsArray.results[j].columns[i].parentcolumn.name !== "")) {
                    if ((typeof resultsArray.results[j].columns[i].parentcolumn.parentcolumn !== 'undefined') && (resultsArray.results[j].columns[i].parentcolumn.parentcolumn.name !== "")) {
                        topColumnName[i] = resultsArray.results[j].columns[i].parentcolumn.parentcolumn.name;
                        middleColumnName[i] = resultsArray.results[j].columns[i].parentcolumn.name;
                        bottomColumnName[i] = resultsArray.results[j].columns[i].name;
                        topHeaderRowPresent = true;
                    } else {
                        middleColumnName[i] = resultsArray.results[j].columns[i].parentcolumn.name;
                        bottomColumnName[i] = resultsArray.results[j].columns[i].name;
                        middleHeaderRowPresent=true;
                    }
                } else {
                    if ((typeof resultsArray.results[j].columns[i].parentcolumn !== 'undefined') && (typeof resultsArray.results[j].columns[i].parentcolumn.parentcolumn !== 'undefined')) {
                        // the case of missing middle column name
                        topColumnName[i] = resultsArray.results[j].columns[i].parentcolumn.parentcolumn.name;
                        middleColumnName[i] = resultsArray.results[j].columns[i].parentcolumn.name;
                        bottomColumnName[i] = resultsArray.results[j].columns[i].name;
                        middleHeaderRowPresent=true;
                    } else {
                        // regular column with no middle or top grouping
                        
                        //v3.64 Added formatting for  https://ceri-lahey.atlassian.net/browse/DELQA-397
                        bottomColumnName[i] = resultsArray.results[j].columns[i].name;
                        
                        // console.log("bottomColumnName[i]", bottomColumnName[i]);
                        if(bottomColumnName[i].trim()==="RowId"){
                            bottomColumnName[i]="RowID";
                            //console.log("replace row id");
                        }
                        else if(bottomColumnName[i].trim()==="stdError"){
                            bottomColumnName[i]="Standard Error";
                            //console.log("replace stdError");
                        }
                        else if(bottomColumnName[i].trim()==="testStat"){
                            bottomColumnName[i]="TestStat";
                            //console.log("replace TestStat");
                        }
                        else if(bottomColumnName[i].trim()==="dof"){
                            bottomColumnName[i]="DOF";
                            //console.log("replace dof");
                        }
                        
                        else if(bottomColumnName[i].trim()==="pValue"){
                            bottomColumnName[i]="P-Value";
                            //console.log("replace pValue");
                        }
                       
                        else if(resultsArray.results[j].name.trim()==="LogisticRegressionFormulaOutput" && bottomColumnName[i].trim()==="name"){
                            bottomColumnName[i]="Variable Name";
                            //console.log("replace Variable Name");
                        }
                        
                        else if(resultsArray.results[j].name.trim()==="LogisticRegressionFormulaOutput" && bottomColumnName[i].trim()==="value"){
                            bottomColumnName[i]="Value";
                            //console.log("replace Value");
                        }
                        
                        bottomHeaderRowPresent=true;
                    }
                }
            }

            // loop creating HTML
            var hStringTop = '<tr class="delta3table">';
            var hStringMiddle = '<tr class="delta3table">';
            var hStringBottom = '<tr class="delta3table">';
            
            for (i=0; i<size; i++) {
                hStringBottom += '<th class="delta3table">' + bottomColumnName[i] + '</th>';
                if (typeof topColumnName[i] !== 'undefined') {
                    if ( lastTopColumnName !== topColumnName[i] ) {                
                        if ( topColSpan > 1 ) {
                            hStringTop += '<th class="delta3table" colspan="' + topColSpan + '">' + lastTopColumnName + '</th>';
                        } else {
                            hStringTop += '<th class="delta3table">' + lastTopColumnName + '</th>';
                        }
                        lastTopColumnName = topColumnName[i];
                        topColSpan=1;
                    } else {                       
                        topColSpan++;
                    }
                } else {
                    topColSpan++;
                }
                if (typeof middleColumnName[i] !== 'undefined') {
                    if ( lastMiddleColumnName !== middleColumnName[i] ) {
                        if ( middleColSpan > 1 ) {
                            hStringMiddle += '<th class="delta3table" colspan="' + middleColSpan + '">' + lastMiddleColumnName + '</th>';
                        } else {
                            hStringMiddle += '<th class="delta3table">' + lastMiddleColumnName + '</th>';
                        }
                        lastMiddleColumnName = middleColumnName[i];
                        middleColSpan=1;
                    } else {
                        middleColSpan++;
                    }
                } else {
                    middleColSpan++;
                }          
            }
            // process trailing headers
            if ( topColSpan > 1 ) {
                hStringTop += '<th class="delta3table" colspan="' + topColSpan + '">' + lastTopColumnName + '</th>';
            } else {
                hStringTop += '<th class="delta3table">' + lastTopColumnName + '</th>';
            }         
            if ( middleColSpan > 1 ) {
                hStringMiddle += '<th class="delta3table" colspan="' + middleColSpan + '">' + lastMiddleColumnName + '</th>';
            } else {
                hStringMiddle += '<th class="delta3table">' + lastMiddleColumnName + '</th>';
            }       
            // close tags
            hStringTop += '</tr>';
            hStringMiddle += '</tr>';
            hStringBottom += '</tr>';
            var hStringAll = '';
            // add non empty header rows
            if ( topHeaderRowPresent === true ) hStringAll += hStringTop;
            if ( middleHeaderRowPresent === true ) hStringAll += hStringMiddle;
            if ( bottomHeaderRowPresent === true ) hStringAll += hStringBottom;            
            return hStringAll;
        }
    }
    return '';  
};

// this one is not used as buildHeader() can serve o create plain headers as well
HTMLTable.prototype.buildHeader_plain = function(resultsArray) {
    var j, i;
    var hString = '';
    for (j = 0; j < resultsArray.results.length; j++) {
        if (resultsArray.results[j].name === this.tag) {
            var hString = '<tr class="delta3table">';
            for (i = 0; i < resultsArray.results[j].columns.length; i++) {
                hString += '<th class="delta3table">' + resultsArray.results[j].columns[i].name + '</th>';
            }
            hString += '</tr>';
        }
    }
    return hString;
};

HTMLTable.prototype.buildRows = function(resultsArray, everyTab, index) {
    var dataRow = '<tbody class="delta3table">'; 
    var tagClass = '';
    var runIndex=0;
    for (var i = 0; i < resultsArray.results.length; i++) {
        if (resultsArray.results[i].name === this.tag) {
            if ( typeof index === 'undefined' || index === runIndex || everyTab === true ) {   // skip records leading to index record         
                for (var j = 0; j < resultsArray.results[i].data.length; j++) {
                    dataRow += '<tr class="delta3table">';
                    for (var z = 0; z < resultsArray.results[i].data[j].length; z++) {
                        // set identifiers for custom css
                        tagClass = "class=\"delta3table\"";
                        try {
                            switch (resultsArray.results[i].columns[z].parentcolumn.name.toUpperCase()) {
                                case "UNMATCHED":    
                                    tagClass = "class=\"delta3table d3g_custom_prior-to-match\"";
                                    //console.log("Prior to Match: " + resultsArray.results[i].columns[z].parentcolumn.parentcolumn.name);
                                    break;
                                case "MATCHED CONTROL":    
                                    tagClass = "class=\"delta3table d3g_custom_after-match\"";
                                    //console.log("After Match: " + resultsArray.results[i].columns[z].parentcolumn.parentcolumn.name);
                                    break;
                                case "MATCHED TREATED":    
                                    tagClass = "class=\"delta3table d3g_custom_unmatched\"";
                                    //console.log("Unmatch: " + resultsArray.results[i].columns[z].parentcolumn.parentcolumn.name);
                                    break;
                                case "undefined":
                                case "UNDEFINED":
                                default:                           
                                    break;
                            }                             
                            switch (resultsArray.results[i].columns[z].parentcolumn.parentcolumn.name.toUpperCase()) {
                                case "PRIOR TO MATCH": 
                                    tagClass = "class=\"delta3table d3g_custom_prior-to-match\"";
                                    //console.log("Prior to Match: " + resultsArray.results[i].columns[z].parentcolumn.parentcolumn.name);
                                    break;
                                case "AFTER MATCH":
                                    tagClass = "class=\"delta3table d3g_custom_after-match\"";
                                    //console.log("After Match: " + resultsArray.results[i].columns[z].parentcolumn.parentcolumn.name);
                                    break;
                                case "UNMATCHED EXPOSURES": 
                                    tagClass = "class=\"delta3table d3g_custom_unmatched\"";
                                    //console.log("Unmatch: " + resultsArray.results[i].columns[z].parentcolumn.parentcolumn.name);
                                    break;
                                case "undefined":
                                case "UNDEFINED":
                                default:                           
                                    break;
                            }                               
                        } catch (err) {
                        }
                        if ( resultsArray.results[i].data[j][z] === 'null' || resultsArray.results[i].data[j][z] === null) {
                            resultsArray.results[i].data[j][z] = '';
                        }
                        dataRow += '<td ' + tagClass + '>' + padWithZeros(formatString(resultsArray.results[i].data[j][z],4)) + '</td>';
                    }
                    dataRow += '</tr>';
                } 
                break;
            } else {
                runIndex++;
            }
        }
    }  
    return dataRow + '</tbody>';
};

function padWithZeros(string, num) {
    if ( string === 'null' || string === null ) {
        string = '';
    }    
    var dotIndex = string.indexOf('.');
    if ( dotIndex === 0 ) {
        string = '0' + string;
        dotIndex = string.indexOf('.');    
    }
    if ( dotIndex !== -1 ) {
        // this would format all 0 to 0.0000, even those that are integer in nature
        // instead the burden of formatting was shiffted to DETLAlytics Result processing
//        if ( string === '0' ) {
//            string += '.';
//            for (var i=0; i<num; i++) {
//                string += '0';
//            }
//        }
//    } else {
        var numberOfZeros = num - (string.length - dotIndex - 1);   
        for (var i=0; i<numberOfZeros; i++) {
            string += '0';
        }
    }
    return string;
}

function formatString(item) {
    if (typeof item === 'string' || item instanceof String) {
        return item;
    } else {
        item = Math.round(10000 * item)/10000;
        return padWithZeros(item.toString(),4);
    }
}

module.exports.HTMLTable = HTMLTable;
module.exports.getMarkup = HTMLTable.prototype.getMarkup;
module.exports.init = HTMLTable.prototype.init;


