/**
 * D3 Observed/Expected Multi Chart local
 */

var ObservedExpectedMultiChart = function() {    
        var id,
        blockId,
        combinedSVGId,
        combinedDIVId,
        chartHeight,
        chartWidth, 
        graphTitle,
        graphSubTitle,
        yAxisTitle;   
    };

ObservedExpectedMultiChart.prototype.init = function(graphTitle, graphSubTitle, yAxisTitle, id, chartParams, idProcess) {
    this.graphTitle = graphTitle;  
    this.graphSubTitle = graphSubTitle;            
    this.yAxisTitle = yAxisTitle;
    this.param = chartParams;
    this.id = id;
    this.blockId = '_oe';
    this.combinedSVGId = id + this.blockId + "Chart";    
    this.combinedDIVId = id + this.blockId + "Chart";    
    this.idProcess = idProcess;
};  

ObservedExpectedMultiChart.prototype.getMarkup = function (index, store, cell, d3, callback) {
    console.log('d3 ObservedExpectedMultiChart generation started.');
    var me = this;
    var backgroundColor = "white";   
    var outcomeVariables = this.yAxisTitle.split(",");       
    var div = document.getElementById(cell);
    var svg = d3.select(div).append("svg")
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")           
            .attr("width", this.param.width)
            .attr("height", this.param.height) 
            .attr("class", "delta3chart")
            .style("background-color", backgroundColor)      
            .style("font-family",this.param.fontFamily)
            .style("shape-rendering","crispEdges")    
            .style("font-size",this.param.fontSize)    
            .attr("id", this.combinedSVGId + index);  

    // Create Margins and Axis and hook our zoom function
    var margin = {top: this.param.marginTop, right: this.param.marginRight, bottom: this.param.marginBottom, left: this.param.marginLeft},         
        width = this.param.width - margin.left - margin.right,
        height = this.param.height - margin.top - margin.bottom;

    var timeLabels = [];
    var drawPrimaryCI = false;
    var drawSecondaryCI = false;
    timeLabels[0] = '';
    for (var i=0; i<store[0].data.length; i++) {
        timeLabels[i+1] = store[0].data[i].time;
        if ( store[0].data[i].expLow !== 0.0 && store[0].data[i].expHigh !== 0.0 ) drawPrimaryCI = true;
        if ( store[0].data[i].exp2Low !== 0.0 && store[0].data[i].exp2High !== 0.0 ) drawSecondaryCI = true;        
    }
    timeLabels[i+2] = '';

    var x = d3.scale.ordinal()
        .domain(timeLabels)
        .rangeRoundPoints([0, width]);

    var maxY = this.calcStoreMaxYValue(store, drawPrimaryCI, drawSecondaryCI);
    var minY = this.calcStoreMinYValue(store, drawPrimaryCI, drawSecondaryCI);
    var y = d3.scale.linear()
        .domain([minY-Math.abs(maxY/10), maxY+Math.abs(maxY/10)])
        .range([height, 0]); 

    var yCount = d3.scale.linear()
        .domain([this.calcStoreMinYRightValue(store), this.calcStoreMaxYRightValue(store)])
        .range([height, 0]);

    var chart = svg.append("svg:g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");      

    var xAxis = d3.svg.axis()
            .scale(x)
            .tickSize(-height)     
            .tickPadding(10)
            .tickSubdivide(true)
            .orient("bottom");         

    var yAxis = d3.svg.axis()
            .scale(y)
            .tickPadding(0.1)
            .tickSize(-width)
            .tickSubdivide(true) 
            .tickPadding(6)
            .orient("left");     

    var yAxisRight = d3.svg.axis()
            .scale(yCount)                               
            .orient("right");
 
    chart.append("svg:g")
        .append("rect")
        .attr("fill","#f2f2f2")
        .attr("width", width)
        .attr("height", height)
        .on("click", function () {
            callback(me.param.idStudy, me.idProcess);      
            });

   chart.append("svg:g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .selectAll("text")  
                .style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", ".15em")
                .attr("transform", "rotate(-45)" );  

    chart.append("svg:g")         
        .attr("class", "y axis")    
        .call(yAxis)           
        .append("text")
            .attr("class", "Cumulative")
            .attr("transform", "rotate(-90)")  
            .attr("y", (-margin.left) + 12)
            .attr("x", -2*height/3);         	

    chart.selectAll('.axis line')
         .style({"fill": "none","stroke": "white","shape-rendering": "crispEdges"});
    chart.selectAll('.axis path')
         .style({"fill": "none","stroke": "black","shape-rendering": "crispEdges"});

    chart.append("clipPath")
        .attr("id", "clip" + cell)
        .append("rect")
            .attr("width", width)
            .attr("height", height);    
    
    legend(chart, store,  width, outcomeVariables, me, changeResultOpacity);
    
    for (var ii=0; ii<store.length; ii++) {                     
        drawSingleResult(ii, store, this.param.lowerOpacity);
        if ( ii === delta3.utils.GlobalVars.maxOutcomesPerChart - 1 ) break;        
    }
    
    // display graph title
    svg.append("svg:g")
        .append("text")
            .attr("x", width/3)
            .attr("y", 2*margin.top/3)
            .text( this.graphTitle )
            .attr("font-size", this.param.fontSize);                
    
    console.log('d3 ObservedExpectedMultiChart generation finished.');
    return svg;    
 
    
    function drawSingleResult(ii, store, opa) {
        var color = delta3.utils.GlobalVars.colorLowArray[ii];       
        
        var lineExpMedium = d3.svg.line()          
            .x(function(d) {return x(d.time); })
            .y(function(d) {return y(d.expMedium); })
            .interpolate("basis");
        var lineExpLow = d3.svg.line()          
            .x(function(d) {return x(d.time); })
            .y(function(d) {return y(d.expLow); })
            .interpolate("basis");       
        var lineExpHigh = d3.svg.line()          
            .x(function(d) {return x(d.time); })
            .y(function(d) {return y(d.expHigh); })
            .interpolate("basis");   
        if ( drawSecondaryCI === true ) {
            var lineExp2Low = d3.svg.line()          
                .x(function(d) {return x(d.time); })
                .y(function(d) {return y(d.exp2Low); })
                .interpolate("basis");       
            var lineExp2High = d3.svg.line()          
                .x(function(d) {return x(d.time); })
                .y(function(d) {return y(d.exp2High); })
                .interpolate("basis");   
        }

        var lineExpCount = d3.svg.line()          
            .x(function(d) {return x(d.time); })
            .y(function(d) {return yCount(d.expCount); })        
            .interpolate("basis");       
    
        var groupGraphs = chart.append("svg:g")            
                                .attr("class","OE Graphs")
                                .attr('id','OE Graphs' + ii)
                                .attr("stroke","white")
                                .style('fill-opacity', opa)    
                                .style('stroke-opacity', opa)
                                .attr("fill","none")
                                .attr("clip-path", "url(#clip" + cell + ")");   

        groupGraphs.append("svg:path")
                .style("shape-rendering","auto")         
                .attr("class", "COUNT_1")
                .attr('id','COUNT_1' + ii)
                .attr("clip-path", "url(#clip" + cell + ")")
                .attr("stroke", "green") 
                .attr("stroke-width",1)
                .attr("fill","none")
                .attr("d", lineExpCount(store[ii].data));        

        if ( drawSecondaryCI === true ) {
            // An area generator, for the fill.
            var area = d3.svg.area()
                .interpolate("basis")
                .x(function(d) { return x(d.time); })
                .y0(function(d) { return y(d.exp2Low); })
                .y1(function(d) { return y(d.exp2High); });      
        }

        if ( drawSecondaryCI === true ) {
            // Add the area path.
            groupGraphs.append("svg:path")
                    .attr("class", "area")
                    .attr('id','area' + ii)
                    .attr("clip-path", "url(#clip" + cell + ")")
                    .attr("fill",color)
                    .style("fill-opacity", opa/2)
                    .attr("d", area(store[ii].data));        
        }

        groupGraphs.append("svg:path")
                .style("shape-rendering","auto")    
                .style('fill-opacity', opa)    
                .style('stroke-opacity', opa)        
                .attr("class", "EE_1")
                .attr('id','EE_1' + ii)
                .attr("clip-path", "url(#clip" + cell + ")")
                .attr("stroke", color) 
                .attr("stroke-width",1)
                .attr("fill","none")
                .attr("d", lineExpLow(store[ii].data));            

        groupGraphs.append("svg:path")
                .style("shape-rendering","auto")   
                .style('fill-opacity', opa)    
                .style('stroke-opacity', opa)        
                .attr("class", "EE_2")
                .attr('id','EE_2' + ii)
                .attr("clip-path", "url(#clip" + cell + ")")
                .attr("stroke", color) 
                .attr("stroke-width",2)
                .attr("fill","none")       
                .attr("d", lineExpMedium(store[ii].data));   

        groupGraphs.append("svg:path")
                .style("shape-rendering","auto")     
                .style('fill-opacity', opa)    
                .style('stroke-opacity', opa)       
                .attr("class", "EE_3")
                .attr('id','EE_3' + ii)
                .attr("clip-path", "url(#clip" + cell + ")")
                .attr("stroke", color) 
                .attr("stroke-width",1)
                .attr("fill","none")
                .attr("d", lineExpHigh(store[ii].data));              
        if ( drawSecondaryCI === true ) {
            groupGraphs.append("svg:path")
                    .style("shape-rendering","auto")         
                    .style('fill-opacity', opa)    
                    .style('stroke-opacity', opa)            
                    .attr("class", "EE_1")
                    .attr('id','EE_1' + ii)
                    .attr("clip-path", "url(#clip" + cell + ")")
                    .attr("stroke", color) 
                    .attr("stroke-width",1)
                    .attr("fill","none")
                    .attr("d", lineExp2Low(store[ii].data));            

            groupGraphs.append("svg:path")
                    .style("shape-rendering","auto")      
                    .style('fill-opacity', opa)    
                    .style('stroke-opacity', opa)            
                    .attr("class", "EE_3")
                    .attr('id','EE_3' + ii)
                    .attr("clip-path", "url(#clip" + cell + ")")
                    .attr("stroke", color) 
                    .attr("stroke-width",1)
                    .attr("fill","none")
                    .attr("d", lineExp2High(store[ii].data));  
        }                

        var rectangles = chart.selectAll("obsMarker")
            .data(store[ii].data)
            .enter() 
            .append("rect")
                .attr("class","obsMarker")
                .attr('id','obsMarker' + ii)
                .style('fill-opacity', opa)    
                .style('stroke-opacity', opa)           
                .attr("fill", color)
                .attr("x", function (d) {
                    var ret = x(d.time)-2;
                    return ret;
                })
                .attr("y", function (d) {
                    var ret = y(d.obsHigh);
                    return ret;
                })
                .attr("height", function (d) {
                    var ret = (y(d.obsLow) - y(d.obsHigh));
                    return ret;
                })
                .attr("width", 3);

        var circles = chart.selectAll("obsMarkerCircle")
            .data(store[ii].data)
            .enter()
            .append("circle")
                .attr("class","obsMarkerCirle")
                .attr('id','obsMarkerCircle' + ii)
                .style('fill-opacity', opa)    
                .style('stroke-opacity', opa)           
                .attr("fill", function (d) {
                    return color;
                })
                .attr("cx", function (d) {
                    var ret = x(d.time)-1;
                    return ret;
                })
                .attr("cy", function (d) {
                    var ret = y(d.obsMedium);
                    return ret;
                })
                .attr("r", function (d) {
                    return 5;
                })
                .append("svg:title")
                    .text(function(d) { 
                        if ( typeof d.alert.description === 'undefined') {    
                            return outcomeVariables[ii] + '\n' + d.time + '\n  Exp: ' 
                                   + d.expLow + ', '   
                                   + d.expMedium + ', '     
                                   + d.expHigh + ' '
                                   + '\n  Obs: ' 
                                   + d.obsLow + ', '   
                                   + d.obsMedium + ', '     
                                   + d.obsHigh + ' ';
                        } else {
                            return outcomeVariables[ii] + '\n' + d.alert.description + '\n'
                                   + d.time + '\n  Exp: ' 
                                   + d.expLow + ', '   
                                   + d.expMedium + ', '     
                                   + d.expHigh + ' '
                                   + '\n  Obs: ' 
                                   + d.obsLow + ', '   
                                   + d.obsMedium + ', '     
                                   + d.obsHigh + ' ';       
                        }
                    });
    }
    
    function changeResultOpacity(index, opa) {
    
        chart.selectAll("#COUNT_1" + index)  
            .style('fill-opacity', opa)    
            .style('stroke-opacity', opa);
        chart.selectAll("#EE_1" + index)  
            .style('fill-opacity', opa)    
            .style('stroke-opacity', opa);
        chart.selectAll("#EE_2" + index)  
            .style('fill-opacity', opa)    
            .style('stroke-opacity', opa);
        chart.selectAll("#EE_3" + index)  
            .style('fill-opacity', opa)    
            .style('stroke-opacity', opa);
    
        chart.selectAll("#area" + index)  
            .style('fill-opacity', opa/2)    
            .style('stroke-opacity', opa/2);         
            
        chart.selectAll("#expMarker" + index)
            .style('fill-opacity', opa)
            .style('stroke-opacity', opa);      
        chart.selectAll("#obsMarkerCircle" + index)
            .style('fill-opacity', opa)      
            .style('stroke-opacity', opa);
    }        
    
};                  
 
ObservedExpectedMultiChart.prototype.calcStoreMinYValue = function (store, primaryCI, secondaryCI) {
    var tmp;
    if ( secondaryCI === true ) {
        tmp = store[0].data[0].exp2Low;
    } else {
        tmp = store[0].data[0].expLow;
    }    
    for (var ii=0; ii<store.length; ii++) {       
        for (var i=0; i< store[ii].data.length; i++) {
            if (primaryCI === true && store[ii].data[i].obsLow < tmp) {
                tmp = store[ii].data[i].obsLow;
            }
            if (secondaryCI === true && store[ii].data[i].exp2Low < tmp) {
                tmp = store[ii].data[i].exp2Low;
            }            
        }
    }
    return tmp;
};
    
ObservedExpectedMultiChart.prototype.calcStoreMaxYValue = function (store, primaryCI, secondaryCI) {
    var tmp;
    if ( secondaryCI === true ) {
        tmp = store[0].data[0].exp2High;
    } else {
        tmp = store[0].data[0].expHigh;
    }
    for (var ii=0; ii<store.length; ii++) {       
        for (var i=0; i< store[ii].data.length; i++) {
            if (primaryCI === true && store[ii].data[i].obsHigh > tmp) {
                tmp = store[ii].data[i].obsHigh;
            }
            if (secondaryCI === true && store[ii].data[i].exp2High > tmp) {
                tmp = store[ii].data[i].exp2High;
            }            
        }
    }
    return tmp;
};        

ObservedExpectedMultiChart.prototype.calcStoreMinYRightValue = function (store) {
    var bottomMargin = 1;
    var tmp = store[0].data[0].expCount;
    for (var ii=0; ii<store.length; ii++) {       
        for (var i=0; i< store[ii].data.length; i++) {
            if (store[ii].data[i].expCount < tmp) {
                tmp = store[ii].data[i].expCount;
            }            
        }
    }
    return (tmp - bottomMargin);
};

ObservedExpectedMultiChart.prototype.calcStoreMaxYRightValue = function (store) {
    var topMargin = 1;
    var tmp = store[0].data[0].expCount;
    for (var ii=0; ii<store.length; ii++) {       
        for (var i=0; i< store[ii].data.length; i++) {
            if (store[ii].data[i].expCount > tmp) {
                tmp = store[ii].data[i].expCount;
            }          
        }
    }
    return (tmp + topMargin);
};
    
ObservedExpectedMultiChart.prototype.download = function(params, index) {
    var div = document.getElementById(this.combinedSVGId + index);
    //div.style.backgroundColor = "white";
    var html = div.outerHTML;
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();
    image.onload = function() {
        canvas.width = image.width;
        canvas.height = image.height;                                
        context.drawImage(image, 0, 0);
        var canvasdata = canvas.toDataURL("image/" +  + params.format +"\"");        
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.download = params.filename + "." + params.format;
        a.href = canvasdata;
        a.click();       
    };  
    image.src = imgsrc;
};
  
ObservedExpectedMultiChart.prototype.download = function(params, index) {
    var div = document.getElementById(this.combinedSVGId + index);
    //div.style.backgroundColor = "white";
    var html = div.outerHTML;
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();
    image.onload = function() {
        canvas.width = image.width;
        canvas.height = image.height;                                
        context.drawImage(image, 0, 0);
        var canvasdata = canvas.toDataURL("image/" +  + params.format +"\"");        
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.download = params.filename + "." + params.format;
        a.href = canvasdata;
        a.click();       
    };  
    image.src = imgsrc;
};

module.exports.ObservedExpectedMultiChart = ObservedExpectedMultiChart;
module.exports.getMarkup = ObservedExpectedMultiChart.prototype.getMarkup;
module.exports.init = ObservedExpectedMultiChart.prototype.init;
module.exports.download = ObservedExpectedMultiChart.prototype.download;
