/**
 * D3 Prop Diff Chart local
 */
    
var PropDiffChart = function() {    
        var id,
        blockId,       
        combinedSVGId,
        combinedDIVId,
        chartHeight,
        chartWidth, 
        graphTitle,
        graphSubTitle,
        yAxisTitle;
    };

PropDiffChart.prototype.init = function(graphTitle, graphSubTitle, yAxisTitle, id, chartParams) {
    this.graphTitle = graphTitle;
    this.graphSubTitle = graphSubTitle;
    this.yAxisTitle = yAxisTitle;
    this.param = chartParams;
    this.id = id;
    this.blockId = '_pd';
    this.combinedSVGId = id + this.blockId + "Chart";    
    this.combinedDIVId = id + this.blockId + "Chart";    
};                    
 
PropDiffChart.prototype.getMarkup = function (index, store, cell, d3) {

    console.log("d3 ProdDiff generation started.");
    // Create Margins and Axis and hook our zoom function
    var margin = {top: this.param.marginTop, right: this.param.marginRight, bottom: this.param.marginBottom, left: this.param.marginLeft},      
        width = this.param.width - margin.left - margin.right,
        height = this.param.height - margin.top - margin.bottom;

    var timeLabels = [];
    timeLabels[0] = '';
    for (var i=0; i<store.data.length; i++)
        timeLabels[i+1] = store.data[i].time;
    timeLabels[i+2] = '';

    var x = d3.scale.ordinal()
        .domain(timeLabels)
        .rangeRoundPoints([0, width]);

    var maxY = this.calcStoreMaxYValue(store);
    var minY = this.calcStoreMinYValue(store);
    var y = d3.scale.linear()
        .domain([minY-Math.abs(maxY/10), maxY+Math.abs(maxY/10)])
        .range([height, 0]); 
    var div = document.getElementById(cell);

    var svg = d3.select(div).append("svg")
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")       
            .attr("class", "delta3chart")
            //.attr("viewbox", "0 0 " + this.chartWidth + " " + this.chartHeight)
            .attr("width", this.param.width)
            .attr("height", this.param.height)   
            //.attr("preserveAspectRatio", "xMidYMid meet")    
            .style("background-color", "white")      
            .style("font-family",this.param.fontFamily)
            .style("shape-rendering","crispEdges")    
            .style("font-size",this.param.fontSize)    
            .attr("id", this.combinedSVGId + index);   

    var chart = svg.append("svg:g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var xAxis = d3.svg.axis()
        .scale(x)
        .tickSize(-height)     
        .tickPadding(10)
        .tickSubdivide(true)
        .orient("bottom");         

    var yAxis = d3.svg.axis()
        .scale(y)
        .tickPadding(6)
        .tickSize(-width)
        .tickSubdivide(true)            
        .orient("left");

    chart.append("svg:g")
        .append("rect")
        .attr("fill","#f2f2f2")
        .attr("width", width)
        .attr("height", height);

    chart.append("g")                
        .attr("class", "x axis")   
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)    
        .append("text")
            .attr("class", "Time Period")
            .attr("y", margin.bottom-2)	  
            .attr("x", width/2)
            .text('Time Period');
    
    chart.append("g")         
        .attr("class", "y axis")             
        .call(yAxis)      
        .append("text")
            .attr("class", "Cumulative Proportional")
            .attr("transform", "rotate(-90)")   
            .attr("y", (-margin.left) + 12)
            .attr("x", -2*height/3)         
            .text(this.yAxisTitle);		

    chart.selectAll('.axis line')
         .style({"fill": "none","stroke": "white","shape-rendering": "crispEdges"});
    chart.selectAll('.axis path')
         .style({"fill": "none","stroke": "black","shape-rendering": "crispEdges"});
 
    var zeroLine = d3.svg.line()          
        .x(function(d) {return x(d.time); })
        .y(function(d) {return y(0); })        
        .interpolate("basis");    

    chart.append("svg:path")
            .attr("class", "ZERO_LINE")
            //.attr("clip-path", "url(#clip" + cell + ")")
            .attr("stroke", "red") 
            .attr("stroke-width",1)
            .attr("fill","none")
            .attr("d", zeroLine(store.data));             

    var rectangles = chart.selectAll("obsMarker")
        .data(store.data)
        .enter()
        .append("rect")
            .attr("class","obsMarker")
            //.attr("fill", "#2d578b")
            .attr("fill", "steelblue")
            //.attr("stroke", "black")    
            .attr("x", function (d) {
                var ret = x(d.time)-2;
                return ret;
            })
            .attr("y", function (d) {
                var ret = y(d.high);
                return ret;
            })
            .attr("height", function (d) {
                var ret = (y(d.low) - y(d.high));
                return ret;
            })
            .attr("width", 3);

    var circles = chart.selectAll("obsMarkerCircle")
        .data(store.data)
        .enter()
        .append("circle")
            .attr("class","obsMarkerCirle") 
            .attr("fill", function (d) {
//                alertColor( d.alert.type );                
                switch ( d.alert.type ) {
                    case 2: 
                        return "yellow";
                    case 4:
                        return "gray";
                    case 3:
                        return "red";
                    case 5:
                        return "blue";
                    default:
                        return "lightgreen";                     
                }
            })
            .attr("cx", function (d) {
                var ret = x(d.time)-1;
                return ret;
            })
            .attr("cy", function (d) {
                var ret = y(d.medium);
                return ret;
            })
            .attr("r", function (d) {
                return 5;
            })
            .append("title")
                .text(function(d) { 
                    if ( typeof d.alert.decription !== 'undefined') {
                        return d.alert.description + '\n'
                                + 'Upper CI: ' + d.high + '\n'
                                + 'Mean: ' + d.medium + '\n'
                                + 'Lower CI: ' + d.low;
                    } else {      
                        return 'Upper CI: ' + d.high + '\n'
                                + 'Mean: ' + d.medium + '\n'
                                + 'Lower CI: ' + d.low;
                    }                    
                });

    var labels = chart.selectAll("labels")
            .data(store.data)
            .enter()
            .append("text")
                .attr("class","labels")    
                .attr("font-family", this.param.fontFamily)
                .attr("font-size", this.param.fontSize)
                .attr("fill", "black")
                .text(function(d) { return d.medium + " %"; })
                .attr("x", function(d) { return x(d.time)+4; })
                .attr("y", function(d) { return y(d.medium)-4; });
        
    svg.append("svg:g")
        .append("text")
            .attr("x", width/2)
            .attr("y", 2*margin.top/3)
            .text( this.graphSubTitle )
            .attr("text-anchor", "middle")        
            .attr("font-size", this.param.fontSize);           
    svg.append("svg:g")
        .append("text")
            .attr("x", width/2)
            .attr("y", margin.top/3)
            .text( this.graphTitle )
            .attr("text-anchor", "middle")       
            .attr("font-size", this.param.fontSize);    
    
    console.log("d3 ProdDiff generation finished.");
    return svg;    
};
        
PropDiffChart.prototype.calcStoreMinYValue = function(store) {
    return 0;
};
    
PropDiffChart.prototype.calcStoreMaxYValue = function(store) {
    var tmp = store.data[0].high;
    for (var i=0; i< store.data.length; i++) {
        if (store.data[i].high > tmp) {
            tmp = store.data[i].high;
        }        
    }
    return tmp;
};  
    
PropDiffChart.prototype.download = function(params, index) {
    var div = document.getElementById(this.combinedSVGId + index);
    var html = div.outerHTML;     
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();
    image.onload = function() {
        canvas.width = image.width;
        canvas.height = image.height;                              
        context.drawImage(image, 0, 0);
        var canvasdata = canvas.toDataURL("image/" +  + params.format +"\"");
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.download = params.filename + "." + params.format;
        a.href = canvasdata;
        a.click();
    };     
    image.src = imgsrc;    

};      

module.exports.PropDiffChart = PropDiffChart;
module.exports.getMarkup = PropDiffChart.prototype.getMarkup;
module.exports.init = PropDiffChart.prototype.init;
module.exports.download = PropDiffChart.prototype.download;