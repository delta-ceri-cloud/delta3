/**
 * D3 Risk Adjusted Observed Expected Chart local
 */
    
var RiskAdjustedOEMultiChart = function() {    
        var id,
        blockId,       
        combinedSVGId,
        combinedDIVId,
        graphTitle,
        graphSubTitle,
        yAxisTitle;
    };

RiskAdjustedOEMultiChart.prototype.init = function(graphTitle, graphSubTitle, yAxisTitle, id, chartParams, idProcess) {
    this.graphTitle = graphTitle;
    this.graphSubTitle = graphSubTitle;        
    this.yAxisTitle = yAxisTitle;
    this.param = chartParams;
    this.id = id;
    this.blockId = '_raoe';
    this.combinedSVGId = id + this.blockId + "Chart";    
    this.combinedDIVId = id + this.blockId + "Chart";    
    this.idProcess = idProcess;
};                    
 
RiskAdjustedOEMultiChart.prototype.getMarkup = function (index, store, cell, d3, callback) {
    console.log('d3 RiskAdjustedOEChart generation started.');     
    var me = this;    
    var outcomeVariables = this.yAxisTitle.split(",");
    this.yAxisTitle = this.graphTitle;
    var div = document.getElementById(cell);
    var svg = d3.select(div).append("svg")
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")           
            .attr("width", this.param.width)
            .attr("height", this.param.height) 
            .attr("class", "delta3chart")
            .style("background-color", this.param.backgroundColor)      
            .style("font-family",this.param.fontFamily)
            .style("shape-rendering","crispEdges")    
            .style("font-size",this.param.fontSize)    
            .attr("id", this.combinedSVGId + index);  

    // Create Margins and Axis and hook our zoom function
    var margin = {top: this.param.marginTop, right: this.param.marginRight, bottom: this.param.marginBottom, left: this.param.marginLeft},
        width = this.param.width - margin.left - margin.right,
        height = this.param.height - margin.top - margin.bottom;

    var timeLabels = [];
    var drawPrimaryCI = [];
    var drawSecondaryCI = [];
    timeLabels[0] = '';
    for (var ii=0; ii<store.length; ii++) {
        drawPrimaryCI.push(false);
        drawSecondaryCI.push(false);
        for (var i=0; i<store[0].data.length; i++) {
            timeLabels[i+1] = store[0].data[i].time;
            if ( store[ii].data[i].expLow !== 0.0 && store[ii].data[i].expHigh !== 0.0 ) drawPrimaryCI[ii] = true;
            if ( store[ii].data[i].exp2Low !== 0.0 && store[ii].data[i].exp2High !== 0.0 ) drawSecondaryCI[ii] = true;        
        }
        timeLabels[i+2] = '';
    }

    var x = d3.scale.ordinal()
        .domain(timeLabels)
        .rangeRoundPoints([0, width]);

    var maxY = this.calcStoreMaxYValue(store, drawPrimaryCI, drawSecondaryCI);
    var minY = this.calcStoreMinYValue(store, drawPrimaryCI, drawSecondaryCI);
    var y = d3.scale.linear()
        .domain([0, maxY+Math.abs(maxY/10)])
        .range([height, 0]); 

    var yCount = d3.scale.linear()
        .domain([this.calcStoreMinYRightValue(store, drawPrimaryCI, drawSecondaryCI), 
                this.calcStoreMaxYRightValue(store, drawPrimaryCI, drawSecondaryCI)])
        .range([height, 0]);

    var chart = svg.append("svg:g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");      

    var xAxis = d3.svg.axis()
            .scale(x)
            .tickSize(-height)     
            .tickPadding(10)
            .tickSubdivide(true)
            .orient("bottom");         
        
    var yAxis = d3.svg.axis()
            .scale(y)
            .tickPadding(0.1)
            .tickSize(-width)
            .tickSubdivide(true) 
            .tickPadding(6)
            .orient("left");    

    var yAxisRight = d3.svg.axis()
            .scale(yCount)                               
            .orient("right");
 
    chart.append("svg:g")
        .append("rect")
        .attr("fill","#f2f2f2")
        .attr("width", width)
        .attr("height", height)
        .on("click", function () {
            callback(me.param.idStudy, me.idProcess);      
            });

    chart.append("svg:g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .selectAll("text")  
                .style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", ".15em")
                .attr("transform", "rotate(-45)" );    

    chart.append("svg:g")         
        .attr("class", "y axis")    
        .call(yAxis)           
        .append("text")
            .attr("class", "Cumulative")
            .attr("transform", "rotate(-90)")
            .attr("y", (-margin.left) + 12)
            .attr("x", -2*height/3);      
            //.text(this.yAxisTitle);	

    chart.selectAll('.axis line')
         .style({"fill": "none","stroke": "white","shape-rendering": "crispEdges"});
    chart.selectAll('.axis path')
         .style({"fill": "none","stroke": "black","shape-rendering": "crispEdges"});


    chart.append("clipPath")
        .attr("id", "clip" + cell)
        .append("rect")
            .attr("width", width)
            .attr("height", height);

    var lineExpCount = d3.svg.line()          
        .x(function(d) {return x(d.time); })
        .y(function(d) {return yCount(d.expCount); })        
        .interpolate("basis");              

    var groupGraphs = chart.append("svg:g")            
                            .attr("class","OE Graphs")
                            .attr("stroke","white")
                            .attr("stroke-width",1)
                            .style("fill-opacity", 1)
                            .attr("fill","none")
                            .attr("clip-path", "url(#clip" + cell + ")");    
    groupGraphs.append("svg:path")
        .style("shape-rendering","auto")         
        .attr("class", "COUNT_1")
        .attr("clip-path", "url(#clip" + cell + ")")
        .attr("stroke", "green") 
        .attr("stroke-width",1)
        .attr("fill","none")
        .attr("d", lineExpCount(store[0].data));     

    legend(chart, store,  width, outcomeVariables, me, changeResultOpacity);
    
    for (var ii=0; ii<store.length; ii++) {                 
        drawSingleResult(ii, store, this.param.lowerOpacity);
        if ( ii === delta3.utils.GlobalVars.maxOutcomesPerChart - 1 ) break;
    }

    // display graph title
    svg.append("svg:g")
        .append("text")
            .attr("x", width/3)
            .attr("y", 2*margin.top/3)
            .text( this.graphTitle )
            .attr("font-size", this.param.fontSize);    
    
    console.log('d3 RiskAdjustedOEChart generation finished.');
    return svg;       
   
    function drawSingleResult(index, store, opa) {
        var expRectangles = chart.selectAll("expMarker")
            .data(store[index].data)
            .enter()
            .append("rect")
                .attr('id','expMarker' + index)
                .attr("class","expMarker")
                .style('fill-opacity', opa)
                .style('stroke-opacity', opa)        
                .style("fill",function(d) { return delta3.utils.GlobalVars.colorLowArray[index]; } )  
                .style("stroke-width","1")
                .style("stroke",delta3.utils.GlobalVars.colorLowArray[index])
                .attr("width", 3 )
                .attr("height", 3 )
                .attr("x", function (d) {
                    var ret = x(d.time)-1;
                    return ret;
                })
                .attr("y", function (d) {
                    var ret = y(d.expMedium)-1;
                    return ret;
                });           
        //Unadjusted Vertical Confidence Interval of Expectation
        var expLine = chart.selectAll("expLine")
                .data(store[index].data)
                .enter()
                .append("line")
                    .attr('id','expLine' + index)
                    .attr("class", "expLine")
                    .style("fill","none" )  
                    .style('fill-opacity', opa)    
                    .style('stroke-opacity', opa)
                    .style("stroke-width","2")
                    .style("stroke",delta3.utils.GlobalVars.colorLowArray[index])
                    .attr("x1", function(d) {  return x(d.time); })
                            .attr("y1", function(d) { return y(d.expHigh); })			
                    .attr("x2", function(d) {  return x(d.time); })
                            .attr("y2", function(d) { return y(d.expLow); });

        if ( drawSecondaryCI === true ) {
            //Alpha Adjusted Upper Vertical Confidence Interval of Expectation        
            var expDashLineUpper = chart.selectAll("expDashLineUpper")
                    .data(store[index].data)
                    .enter()
                    .append("line")
                        .attr('id','expDashLineUpper' + index)
                        .attr("class", "expDashLineUpper")
                        .style("fill","none" )  
                        .style('fill-opacity', opa)    
                        .style('stroke-opacity', opa)
                        .style("stroke-dasharray",("2,2"))
                        .style("stroke-width","2")
                        .style("stroke",delta3.utils.GlobalVars.colorLowArray[index])
                        .attr("x1", function(d) {  return x(d.time); })
                                .attr("y1", function(d) { return y(d.exp2High); })			
                        .attr("x2", function(d) {  return x(d.time); })
                                .attr("y2", function(d) { return y(d.expHigh); });

                //Alpha Adjusted Lower Vertical Confidence Interval of Expectation
            var expDashLineLower = chart.selectAll("expDashLineLower")
                    .data(store[index].data)
                    .enter()
                    .append("line")
                        .attr('id','expDashLineLower' + index)
                        .attr("class", "expDashLineLower")
                        .style("fill","none" )  
                        .style('fill-opacity', opa)          
                        .style('stroke-opacity', opa)
                        .style("stroke-dasharray",("2,2"))
                        .style("stroke-width","2")
                        .style("stroke",delta3.utils.GlobalVars.colorLowArray[index])
                        .attr("x1", function(d) {  return x(d.time); })
                                .attr("y1", function(d) { return y(d.exp2Low); })			
                        .attr("x2", function(d) {  return x(d.time); })
                                .attr("y2", function(d) { return y(d.expLow); });
            }            

        var obsCircles = chart.selectAll("obsMarkerCircle")
            .data(store[index].data)
            .enter()
            .append("circle")
                .style("stroke-width","1")
                .style("stroke",delta3.utils.GlobalVars.colorLowArray[index])
                .attr('id','obsMarkerCircle' + index)
                .attr("class","obsMarkerCircle")
                .attr('fill-opacity', opa)      
                .style('stroke-opacity', opa)
                .attr("fill", function (d) {
                    return delta3.utils.GlobalVars.colorLowArray[index];
                })
                .attr("cx", function (d) {
                    var ret = x(d.time);
                    return ret;
                })
                .attr("cy", function (d) {
                    var ret = y(d.obsMedium);
                    return ret;
                })
                .attr("r", function (d) {
                    return 3;
                })
                .append("svg:title")
                    .text(function(d) { 
                        if ( typeof d.alert.description === 'undefined') {    
                            return outcomeVariables[index] + '\n' + d.time                            
                                + '\n  Observed Rate: ' 
                                + d.obsMedium + ', '  
                                + '\n  Expected Rate: ' 
                                + d.exp2Low + ', '  
                                + d.expLow + ', '   
                                + d.expMedium + ', '     
                                + d.expHigh + ', '  
                                + d.exp2High + ' ';
                        } else {
                            return outcomeVariables[index] + '\n' + d.alert.description + '\n'
                                + d.time                           
                                + '\n  Observed Rate: ' 
                                + d.obsMedium + ', '  
                                + '\n  Expected Rate: ' 
                                + d.exp2Low + ', '  
                                + d.expLow + ', '   
                                + d.expMedium + ', '     
                                + d.expHigh + ', '  
                                + d.exp2High + ' ';      
                        }
                    });
    }    

    function changeResultOpacity(index, opa) {
        chart.selectAll("#expMarker" + index)
            .style('fill-opacity', opa)
            .style('stroke-opacity', opa);       
        
        //Unadjusted Vertical Confidence Interval of Expectation
        chart.selectAll("#expLine" + index)  
            .style('fill-opacity', opa)    
            .style('stroke-opacity', opa);

        if ( drawSecondaryCI === true ) {
            //Alpha Adjusted Upper Vertical Confidence Interval of Expectation        
            chart.selectAll("#expDashLineUpper" + index)
                .style('fill-opacity', opa)    
                .style('stroke-opacity', opa);

            //Alpha Adjusted Lower Vertical Confidence Interval of Expectation
            chart.selectAll("#expDashLineLower" + index) 
                .style('fill-opacity', opa)          
                .style('stroke-opacity', opa);
            }            

        chart.selectAll("#obsMarkerCircle" + index)
            .style('fill-opacity', opa)      
            .style('stroke-opacity', opa);
    }           
};                  

RiskAdjustedOEMultiChart.prototype.calcStoreMinYValue = function (store, primaryCI, secondaryCI) {
    var tmp;
    for (var ii=0; ii<store.length; ii++) {    
        if ( secondaryCI[ii] === true ) {
            tmp = store[ii].data[0].exp2Low;
        } else {
            tmp = store[ii].data[0].expLow;
        }    
        for (var i=0; i< store[ii].data.length; i++) {
            if (primaryCI[ii] === true && store[ii].data[i].obsLow < tmp) {
                tmp = store[ii].data[i].obsLow;
            }
            if (secondaryCI[ii] === true && store[ii].data[i].exp2Low < tmp) {
                tmp = store[ii].data[i].exp2Low;
            }            
        }
    }
    return tmp;
};
    
RiskAdjustedOEMultiChart.prototype.calcStoreMaxYValue = function (store, primaryCI, secondaryCI) {
    var tmp;
    for (var ii=0; ii<store.length; ii++) {    
        if ( secondaryCI[ii] === true ) {
            tmp = store[ii].data[0].exp2High;
        } else {
            tmp = store[ii].data[0].expHigh;
        }
        for (var i=0; i< store[ii].data.length; i++) {
            if (primaryCI[ii] === true && store[ii].data[i].obsHigh > tmp) {
                tmp = store[ii].data[i].obsHigh;
            }
            if (secondaryCI[ii] === true && store[ii].data[i].exp2High > tmp) {
                tmp = store[ii].data[i].exp2High;
            }            
        }
    }
    return tmp;
};        

RiskAdjustedOEMultiChart.prototype.calcStoreMinYRightValue = function (store) {
    return 0;
};

RiskAdjustedOEMultiChart.prototype.calcStoreMaxYRightValue = function (store) {
    var topMargin = 1;
    for (var ii=0; ii<store.length; ii++) {    
        var tmp = store[ii].data[0].expCount;
        for (var i=0; i< store[ii].data.length; i++) {
            if (store[ii].data[i].expCount > tmp) {
                tmp = store[ii].data[i].expCount;
            }          
        }
    }
    return (tmp + topMargin);
};
    
RiskAdjustedOEMultiChart.prototype.download = function(params, index) {
    var div = document.getElementById(this.combinedSVGId + index);
    var html = div.outerHTML;     
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();
    image.onload = function() {
        canvas.width = image.width;
        canvas.height = image.height;                              
        context.drawImage(image, 0, 0);
        var canvasdata = canvas.toDataURL("image/" +  + params.format +"\"");
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.download = params.filename + "." + params.format;
        a.href = canvasdata;
        a.click();
    };     
    image.src = imgsrc;    

};      

module.exports.RiskAdjustedOEChart = RiskAdjustedOEMultiChart;
module.exports.getMarkup = RiskAdjustedOEMultiChart.prototype.getMarkup;
module.exports.init = RiskAdjustedOEMultiChart.prototype.init;
module.exports.download = RiskAdjustedOEMultiChart.prototype.download;