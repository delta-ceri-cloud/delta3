/**
 * D3 Survival Matched Chart
 */
    
var SurvivalMatchedChart = function() {    
        var id,
        blockId,       
        combinedSVGId,
        combinedDIVId,
        chartHeight,
        chartWidth, 
        browser,
        graphTitle,
        graphSubTitle,
        yAxisTitle;
    };

SurvivalMatchedChart.prototype.init = function(graphTitle, graphSubTitle, yAxisTitle, xAxisTitle, id, chartParams, executeFromBrowser) {
    this.graphTitle = graphTitle;
    this.graphSubTitle = graphSubTitle;    
    this.yAxisTitle = yAxisTitle;
    this.xAxisTitle = xAxisTitle;    
    this.param = chartParams;
    this.browser = executeFromBrowser;
    this.id = id;
    this.blockId = '_srvm';
    this.combinedSVGId = id + this.blockId + "Chart";    
    this.combinedDIVId = id + this.blockId + "Chart";    
};                    
 
SurvivalMatchedChart.prototype.getMarkup = function (index, store, cell, d3) {
    
    console.log("d3 SurvivalMatchedChart generation started.");    
try {      
    //var colors = d3.scale.category20();
    // Create Margins and Axis and hook our zoom function
    var me = this;
    var lineItems = ["Matched Control","Matched Treated"];
    var margin = {top: this.param.marginTop, right: this.param.marginRight, bottom: this.param.marginBottom, left: this.param.marginLeft},      
        width = this.param.width - margin.left - margin.right,
        height = this.param.height - margin.top - margin.bottom;

    var x = d3.scale.linear()
        .domain([0, this.calcStoreMaxXValue(store)+1]) // plus 1 to show last record of data
        .range([0, width]);

    var y = d3.scale.linear()
        .domain([this.calcStoreMinYValue(store), this.calcStoreMaxYValue(store)])
        .range([height, 0]);

    var xAxis = d3.svg.axis()
            .scale(x)
            .tickSize(-height)
            .tickPadding(10)           
            .tickSubdivide(true)
            .orient("bottom");

    var yAxis = d3.svg.axis()
            .scale(y)
            .tickPadding(10)
            .tickSize(-width)
            .tickSubdivide(true)            
            .orient("left"); 
    // Create D3 line object and draw data on our SVG object
    store = this.removeTrailingValues(store);
    var svg, zoom;
    if ( this.browser === true ) {
        console.log('enabling zoom for the Survival graph');
        zoom = d3.behavior.zoom()
            .x(x)
            .y(y)
            .scaleExtent([1, 10])
            .on("zoom", zoomedSurv);
    
        var div = document.getElementById(cell);       
        svg = d3.select(div).append("svg")
            .call(zoom)
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")       
            .attr("class", "delta3chart")    
            .attr("width", this.param.width)
            .attr("height", this.param.height)    
            .style("background-color", "white")      
            .style("font-family",this.param.fontFamily)
            .style("shape-rendering","crispEdges")    
            .style("font-size",this.param.fontSize)    
            .style("text-align","center")        
            .attr("id", this.combinedSVGId + index);       
    } else {
        var div = document.getElementById(cell);  
        svg = d3.select(div).append("svg")
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")    
            .attr("class", "delta3chart")    
            .attr("width", this.param.width)
            .attr("height", this.param.height)  
            .style("background-color", "white")      
            .style("font-family",this.param.fontFamily)
            .style("shape-rendering","crispEdges")    
            .style("font-size",this.param.fontSize)    
            .attr("id", this.combinedSVGId + index); 
    }
    
    var rect = svg.append("g")
        .append("rect")
        .attr("fill","#f2f2f2")
        .attr("x", margin.left)
        .attr("y", margin.top)
        .attr("width", width)
        .attr("height", height);

    var chart = svg.append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var gX = chart.append("svg:g")                
        .attr("class", "x axis")  
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis) 
        .append("text")
            .attr("class", "Time")
            .attr("y", margin.bottom-2)	  
            .attr("x", width/3)
        .text(this.xAxisTitle);

    var gY = chart.append("svg:g")         
        .attr("class", "y axis")    
        .call(yAxis)              
        .append("text")
            .attr("class", "Survival")
            .attr("transform", "rotate(-90)")
            .attr("y", (-margin.left) + 14)
            .attr("x", -2*height/3);	

    chart.selectAll('.axis line')
         .style({"fill": "none","stroke": "white","shape-rendering": "crispEdges"});
    chart.selectAll('.axis path')
         .style({"fill": "none","stroke": "black","shape-rendering": "crispEdges"});

    chart.append("clipPath")
        .attr("id", "clip" + cell)
        .append("rect")
            .attr("width", width)
            .attr("height", height);

    var line = d3.svg.line()          
        .x(function(d) {
            return x(d.duration); 
            })  
        .y(function(d) {
            if ( d.probability !== "" )
                return y(d.probability); 
            else 
                return 0;
            })          
        .interpolate("step-after");
   
    var areaCI = d3.svg.area()
        .x(function(d) { 
            return x(d.duration);})
        .y0(function(d) { 
            if ( d.lower_confidence_interval === "null" )
                return 0;       
            else
                return y(d.lower_confidence_interval);})
        .y1(function(d) { 
            if ( d.upper_confidence_interval === "null" )
                return 0;
            else 
                return y(d.upper_confidence_interval);})
        .interpolate("step-after");   

    var groupSurvGraphs = chart.append("svg:g")            
                            .attr("class","Survival Graphs")
                            .attr("stroke-width",1)
                            .style("fill-opacity", 1)
                            .attr("clip-path", "url(#clip" + cell + ")");
    
    var mcStore = this.getMatchedControlStore(store);
    //console.log("This is mcstore store "+JSON.stringify(mcStore.data));
    //console.log("This is param"+JSON.stringify(this.param));
    //console.log("This is param inputparams"+JSON.stringify(this.param.inputParams));
    
    groupSurvGraphs.append("svg:path")
            .attr("class", "SRV_MControl")
            .attr('id','SRV_LINE0')    
            .attr("clip-path", "url(#clip" + cell + ")")  
            .attr("stroke", colorLowArray[0]) 
            .attr("stroke-opacity", this.param.lowerOpacity)    
            .attr("stroke-width",2)
            .attr("fill","none")
            .attr("d", line(mcStore.data));    
    
    var mtStore = this.getMatchedTreatedStore(store); 
    //console.log("This is mtstore store -------------------------------"+JSON.stringify(mtStore.data));
    //console.log("This is param"+JSON.stringify(this.param));
    //console.log("This is param inputparams"+JSON.stringify(this.param.inputParams));
    
    
    groupSurvGraphs.append("svg:path")
            .attr("class", "SRV_MTreated")
            .attr('id','SRV_LINE1')
            .attr("clip-path", "url(#clip" + cell + ")")  
            .attr("stroke", colorLowArray[1]) 
            .attr("stroke-opacity", this.param.lowerOpacity)    
            .attr("stroke-width",2)
            .attr("fill","none")
            .attr("d", line(mtStore.data));    
        
    groupSurvGraphs.append("svg:path")
            .attr("class", "SRV_AREA_CI_MControl")
            .attr('id','SRV_AREA0')    
            .attr("clip-path", "url(#clip" + cell + ")")
            .attr("fill",colorLowArray[0])
            .style("fill-opacity", this.param.lowerOpacity/2)
            .attr("d", areaCI(mcStore.data));       
    
    groupSurvGraphs.append("svg:path")
            .attr("class", "SRV_AREA_CI_MTreated")
            .attr('id','SRV_AREA1')    
            .attr("clip-path", "url(#clip" + cell + ")")
            .attr("fill",colorLowArray[1])
            .style("fill-opacity", this.param.lowerOpacity/2)
            .attr("d", areaCI(mtStore.data));         
    
    if ( mcStore.data.length === 0 && mtStore.data.length === 0 ) {
        var uStore = this.getUnmatchedStore(store);
        
        
        groupSurvGraphs.append("svg:path")
                .attr("class", "SRV_U")
                .attr("clip-path", "url(#clip" + cell + ")")  
                .attr("stroke", "steelblue") 
                .attr("stroke-opacity", this.param.higherOpacity)
                .attr("stroke-width",2)
                .attr("fill","none")
                .attr("d", line(uStore.data));    
        groupSurvGraphs.append("svg:path")
                .attr("class", "SRV_AREA_CI_U")
                .attr("clip-path", "url(#clip" + cell + ")")
                .attr("fill","#99CCFF")
                .style("fill-opacity", this.param.higherOpacity/2)
                .attr("d", areaCI(uStore.data));          
        } else {
            this.legend(chart,width,lineItems,me,changeResultOpacity);
        }

    svg.append("svg:g")
        .append("text")
            .attr("x", width/2)
            .attr("y", 2*margin.top/3) 
            .text( this.graphSubTitle )
            .attr("text-anchor", "middle")
            .attr("font-size", this.param.fontSize);      
    svg.append("svg:g")
        .append("text")
            .attr("x", width/2)
            .attr("y", margin.top/3)
            .text( this.graphTitle )
            .attr("text-anchor", "middle")    
            .attr("font-size", this.param.fontSize);         
    
    console.log("d3 SurvivalMatchedChart generation finished.");
    return svg;   
    
    function zoomedSurv() {
        var t = zoom.translate();
        var tx = t[0];
        var ty = t[1];
        tx = Math.min(tx,0);
        zoom.translate([tx,ty]);
        chart.select(".x.axis").call(xAxis);
        chart.select(".y.axis").call(yAxis);
//        chart.select(".SRV_U").attr('d', line(uStore.data));
        chart.select(".SRV_MControl").attr('d', line(mcStore.data));
        chart.select(".SRV_MTreated").attr('d', line(mtStore.data));        
        chart.select(".SRV_AREA_CI_MControl").attr('d', areaCI(mcStore.data));        
        chart.select(".SRV_AREA_CI_MTreated").attr('d', areaCI(mtStore.data)); 
        if ( mcStore.data.length === 0 && mtStore.data.length === 0 ) {
            chart.select(".SRV_U").attr('d', line(uStore.data));        
            chart.select(".SRV_AREA_CI_U").attr('d', areaCI(uStore.data));  
        }        
    } 
    
    function changeResultOpacity(index, opa) {
        chart.selectAll("#SRV_LINE" + index)  
            .style('fill-opacity', opa)    
            .style('stroke-opacity', opa);
        chart.selectAll("#SRV_AREA" + index)  
            .style('fill-opacity', opa/2)    
            .style('stroke-opacity', opa/2);    
    }
} catch (err) {
    console.log("error: " + err.message);
}
};

SurvivalMatchedChart.prototype.calcStoreMinYValue = function(store) {
    var tmp = parseFloat(store.data[0].probability_unmatched);      
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].probability_matched_control) < tmp) {
            tmp = parseFloat(store.data[i].probability_matched_control);
        }
    }   
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].lower_cb_alpha_spending_matched_control) < tmp) {
            tmp = parseFloat(store.data[i].lower_cb_alpha_spending_matched_control);
        }
    }     
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].lower_cb_matched_control) < tmp) {
            tmp = parseFloat(store.data[i].lower_cb_matched_control);
        }
    }        
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].probability_matched_treated) < tmp) {
            tmp = parseFloat(store.data[i].probability_matched_treated);
        }
    }        
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].lower_cb_alpha_spending_matched_treated) < tmp) {
            tmp = parseFloat(store.data[i].lower_cb_alpha_spending_matched_treated);
        }
    }     
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].lower_cb_matched_treated) < tmp) {
            tmp = parseFloat(store.data[i].lower_cb_matched_treated);
        }
    }   
    var marg = tmp - tmp/100;
    if ( marg < 0 ) {
        return (0);
    } else {
        return marg;
    }
};
    
SurvivalMatchedChart.prototype.calcStoreMaxYValue = function(store) {
    var tmp = parseFloat(store.data[0].probability_unmatched);      
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].probability_matched_control) > tmp) {
            tmp = parseFloat(store.data[i].probability_matched_control);
        }
    }   
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].upper_cb_alpha_spending_matched_control) > tmp) {
            tmp = parseFloat(store.data[i].upper_cb_alpha_spending_matched_control);
        }
    }     
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].upper_cb_matched_control) > tmp) {
            tmp = parseFloat(store.data[i].upper_cb_matched_control);
        }
    }        
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].probability_matched_treated) > tmp) {
            tmp = parseFloat(store.data[i].probability_matched_treated);
        }
    }        
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].upper_cb_alpha_spending_matched_treated) > tmp) {
            tmp = parseFloat(store.data[i].upper_cb_alpha_spending_matched_treated);
        }
    }     
    for (var i=0; i< store.data.length; i++) {
        if (parseFloat(store.data[i].upper_cb_matched_treated) > tmp) {
            tmp = parseFloat(store.data[i].upper_cb_matched_treated);
        }
    }    
    return (tmp + tmp/100);
};  

SurvivalMatchedChart.prototype.calcStoreMaxXValue = function(store) {
    var tmp = parseInt(store.data[0].duration);
    for (var i=0; i< store.data.length; i++) {
        if (parseInt(store.data[i].duration) > tmp) {
            tmp = parseInt(store.data[i].duration);
        }
    }
    return (tmp); 
}

SurvivalMatchedChart.prototype.removeTrailingValues = function(store) {
    for (var i=0; i< store.data.length; i++) {
        if ( store.data[i].probability === "" ) {
            store.data.splice(i,1);
            i--;
        }
    }
    return store; 
}

//--new code v3.65 removed alpha spending
SurvivalMatchedChart.prototype.getUnmatchedStore = function(store) {
    var dataArray = [];
    var i = 0;
    for (; i< store.data.length; i++) { 
            dataArray.push({
                duration: store.data[i].duration, 
                probability: store.data[i].probability_unmatched, 
                lower_confidence_interval: store.data[i].lower_cb_unmatched,
                upper_confidence_interval: store.data[i].upper_cb_unmatched
            });            
        
    }
    // duplicate last record to draw it fully
    i--;
        dataArray.push({
            duration: store.data[i].duration+1, 
            probability: store.data[i].probability_unmatched, 
            lower_confidence_interval: store.data[i].lower_cb_unmatched,
            upper_confidence_interval: store.data[i].upper_cb_unmatched
        });                
    return ({data: dataArray}); 
}

/*--old code
SurvivalMatchedChart.prototype.getUnmatchedStore = function(store, alphaSpending) {
    var dataArray = [];
    var i = 0;
    for (; i< store.data.length; i++) {
        if ( alphaSpending ) {
            dataArray.push({
                duration: store.data[i].duration, 
                probability: store.data[i].probability_unmatched, 
                lower_confidence_interval: store.data[i].lower_cb_aplha_spending_unmatched,
                upper_confidence_interval: store.data[i].upper_cb_alpha_spending_unmatched
            });
        } else {
            dataArray.push({
                duration: store.data[i].duration, 
                probability: store.data[i].probability_unmatched, 
                lower_confidence_interval: store.data[i].lower_cb_unmatched,
                upper_confidence_interval: store.data[i].upper_cb_unmatched
            });            
        }
    }
    // duplicate last record to draw it fully
    i--;
    if ( alphaSpending ) {
        dataArray.push({
            duration: store.data[i].duration+1, 
            probability: store.data[i].probability_unmatched, 
            lower_confidence_interval: store.data[i].lower_cb_aplha_spending_unmatched,
            upper_confidence_interval: store.data[i].upper_cb_alpha_spending_unmatched
        });
    } else {
        dataArray.push({
            duration: store.data[i].duration+1, 
            probability: store.data[i].probability_unmatched, 
            lower_confidence_interval: store.data[i].lower_cb_unmatched,
            upper_confidence_interval: store.data[i].upper_cb_unmatched
        });            
    }    
    return ({data: dataArray}); 
}*/



//---new code v3.65 removed alpha spending
SurvivalMatchedChart.prototype.getMatchedControlStore = function(store) {
    console.log("This is at mcstore");
    var dataArray = [];
    var i = 0;
    for (; i< store.data.length; i++) {
            dataArray.push({
                duration: store.data[i].duration, 
                probability: store.data[i].probability_matched_control, 
                lower_confidence_interval: store.data[i].lower_cb_matched_control,
                upper_confidence_interval: store.data[i].upper_cb_matched_control
            });
    }
    // duplicate last record to draw it fully
    i--;
        dataArray.push({
            duration: store.data[i].duration+1, 
            probability: store.data[i].probability_matched_control, 
            lower_confidence_interval: store.data[i].lower_cb_matched_control,
            upper_confidence_interval: store.data[i].upper_cb_matched_control
        });
    return ({data: dataArray}); 
}


//new code v3.65 removed alpha spending
SurvivalMatchedChart.prototype.getMatchedTreatedStore = function(store) {
    console.log("at treated store");
    var dataArray = [];
    var i = 0;
    for (; i< store.data.length; i++) {
         
            dataArray.push({
                duration: store.data[i].duration, 
                probability: store.data[i].probability_matched_treated, 
                lower_confidence_interval: store.data[i].lower_cb_matched_treated,
                upper_confidence_interval: store.data[i].upper_cb_matched_treated
            });            
        
    }
    // duplicate last record to draw it fully
    i--;
     
        dataArray.push({
            duration: store.data[i].duration+1, 
            probability: store.data[i].probability_matched_treated, 
            lower_confidence_interval: store.data[i].lower_cb_matched_treated,
            upper_confidence_interval: store.data[i].upper_cb_matched_treated
        });                
    return ({data: dataArray}); 
}



/*---old code
SurvivalMatchedChart.prototype.getMatchedControlStore = function(store, alphaSpending) {
    var dataArray = [];
    var i = 0;
    for (; i< store.data.length; i++) {
        if ( alphaSpending ) {
            dataArray.push({
                duration: store.data[i].duration, 
                probability: store.data[i].probability_matched_control, 
                lower_confidence_interval: store.data[i].lower_cb_alpha_spending_matched_control,
                upper_confidence_interval: store.data[i].upper_cb_alpha_spending_matched_control
            });            
        } else {
            dataArray.push({
                duration: store.data[i].duration, 
                probability: store.data[i].probability_matched_control, 
                lower_confidence_interval: store.data[i].lower_cb_matched_control,
                upper_confidence_interval: store.data[i].upper_cb_matched_control
            });
        }
    }
    // duplicate last record to draw it fully
    i--;
    if ( alphaSpending ) {
        dataArray.push({
            duration: store.data[i].duration+1, 
            probability: store.data[i].probability_matched_control, 
            lower_confidence_interval: store.data[i].lower_cb_alpha_spending_matched_control,
            upper_confidence_interval: store.data[i].upper_cb_alpha_spending_matched_control
        });            
    } else {
        dataArray.push({
            duration: store.data[i].duration+1, 
            probability: store.data[i].probability_matched_control, 
            lower_confidence_interval: store.data[i].lower_cb_matched_control,
            upper_confidence_interval: store.data[i].upper_cb_matched_control
        });
    }    
    return ({data: dataArray}); 
}*/

/*old code
SurvivalMatchedChart.prototype.getMatchedTreatedStore = function(store, alphaSpending) {
    var dataArray = [];
    var i = 0;
    for (; i< store.data.length; i++) {
        if ( alphaSpending ) {
            dataArray.push({
                duration: store.data[i].duration, 
                probability: store.data[i].probability_matched_treated, 
                lower_confidence_interval: store.data[i].lower_cb_alpha_spending_matched_treated,
                upper_confidence_interval: store.data[i].upper_cb_alpha_spending_matched_treated
            });
        } else {
            dataArray.push({
                duration: store.data[i].duration, 
                probability: store.data[i].probability_matched_treated, 
                lower_confidence_interval: store.data[i].lower_cb_matched_treated,
                upper_confidence_interval: store.data[i].upper_cb_matched_treated
            });            
        }
    }
    // duplicate last record to draw it fully
    i--;
    if ( alphaSpending ) {
        dataArray.push({
            duration: store.data[i].duration+1, 
            probability: store.data[i].probability_matched_treated, 
            lower_confidence_interval: store.data[i].lower_cb_alpha_spending_matched_treated,
            upper_confidence_interval: store.data[i].upper_cb_alpha_spending_matched_treated
        });
    } else {
        dataArray.push({
            duration: store.data[i].duration+1, 
            probability: store.data[i].probability_matched_treated, 
            lower_confidence_interval: store.data[i].lower_cb_matched_treated,
            upper_confidence_interval: store.data[i].upper_cb_matched_treated
        });            
    }    
    return ({data: dataArray}); 
}
       
 */

SurvivalMatchedChart.prototype.legend = function(chart, width, lineItems, that, changeResultOpacity) {
    chart.append("svg:g")
        .append("rect")
            .style('fill', 'white')
            .style('stroke', 'white')  
            .style('stroke-opacity', that.param.lowerOpacity)
            .attr('class', 'legend')            
            .attr('width', that.param.legendWidth)
            .attr('height', lineItems.length * that.param.legendSpacing + that.param.legendSpacingMargin)
            .attr("x", width - that.param.legendWidth - that.param.legendSpacingMargin - 1)
            .attr("y", that.param.legendSpacingMargin + 1);   
    
    for (var ii=0; ii<lineItems.length; ii++) {
       chart.append("svg:g")
            .append("rect")
                .style('fill', colorLowArray[ii])
                .style('stroke', colorLowArray[ii])  
                .style('stroke-opacity', that.param.lowerOpacity)
                .attr('class', 'rectMap')            
                .attr('id', 'rectMap' + ii)
                .attr('width', that.param.legendRectSize)
                .attr('height', that.param.legendRectSize)
                .attr("x", width - that.param.legendWidth + 2*that.param.legendSpacingMargin)
                .attr("y", ii * that.param.legendSpacing + 2*that.param.legendSpacingMargin)
                .on("mouseover", function () {
                        var index = parseInt(this.id.substring(7));
                        changeResultOpacity(index, that.param.higherOpacity);
                    })
                .on("mouseout", function () {
                        var index = parseInt(this.id.substring(7));                    
                        changeResultOpacity(index, that.param.lowerOpacity);                    
                    });        
  
        chart.append("svg:g")
            .append('text') 
                .attr('id', 'textMap' + ii)
                .style("font-family",that.param.fontFamily)
                .style("font-size",that.param.legendFontSize)   
                .style('fill', colorLowArray[ii])
                .style('stroke', colorLowArray[ii])   
                .style('stroke-opacity', that.param.lowerOpacity)
                .style("stroke-width","1")
                //.style("shape-rendering","crispEdges")     
                .attr("x", width - that.param.legendWidth + that.param.legendSpacing + 2*that.param.legendSpacingMargin)
                .attr("y", ii * that.param.legendSpacing + that.param.legendSpacing)          
                .text(lineItems[ii])
                .on("mouseover", function () {
                        var index = parseInt(this.id.substring(7));
                        changeResultOpacity(index, that.param.higherOpacity);
                    })
                .on("mouseout", function () {
                        var index = parseInt(this.id.substring(7));                    
                        changeResultOpacity(index, that.param.lowerOpacity);                    
                    });          
        if ( ii === maxOutcomesPerChart - 1 ) break;        
    }    
}
    
SurvivalMatchedChart.prototype.download = function(params, index) {
    var div = document.getElementById(this.combinedSVGId + index);
    //div.style.backgroundColor = "white";
    var html = div.outerHTML;     
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();
    image.onload = function() {
        canvas.width = image.width;
        canvas.height = image.height;                              
        context.drawImage(image, 0, 0);
        var canvasdata = canvas.toDataURL("image/" +  + params.format +"\"");
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.download = params.filename + "." + params.format;
        a.href = canvasdata;
        a.click();
    };     
    image.src = imgsrc;    
};      

module.exports.SurvivalMatchedChart = SurvivalMatchedChart;
module.exports.getMarkup = SurvivalMatchedChart.prototype.getMarkup;
module.exports.init = SurvivalMatchedChart.prototype.init;
module.exports.download = SurvivalMatchedChart.prototype.download;