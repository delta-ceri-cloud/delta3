/**
 * D3 Prop Diff Chart local
 */
    
var PropDiffMultiChart = function() {    
        var id,
        blockId,       
        combinedSVGId,
        combinedDIVId,
        chartHeight,
        chartWidth, 
        graphTitle,
        graphSubTitle,
        yAxisTitle;
    };

PropDiffMultiChart.prototype.init = function(graphTitle, graphSubTitle, yAxisTitle, id, chartParams, idProcess) {
    this.graphTitle = graphTitle;
    this.graphSubTitle = graphSubTitle;        
    this.yAxisTitle = yAxisTitle;
    this.param = chartParams;
    this.id = id;
    this.blockId = '_pd';
    this.combinedSVGId = id + this.blockId + "Chart";    
    this.combinedDIVId = id + this.blockId + "Chart";    
    this.idProcess = idProcess;
};                    
 
PropDiffMultiChart.prototype.getMarkup = function (index, store, cell, d3, callback) {

    console.log("d3 ProdDiff generation started.");
    var me = this;
    var backgroundColor = "white";
    var outcomeVariables = this.yAxisTitle.split(",");    
    // Create Margins and Axis and hook our zoom function

    var margin = {top: this.param.marginTop, right: this.param.marginRight, bottom: this.param.marginBottom, left: this.param.marginLeft},         
        width = this.param.width - margin.left - margin.right,
        height = this.param.height - margin.top - margin.bottom;

    var timeLabels = [];
    timeLabels[0] = '';
    for (var i=0; i<store[0].data.length; i++)
        timeLabels[i+1] = store[0].data[i].time;
    timeLabels[i+2] = '';

    var x = d3.scale.ordinal()
        .domain(timeLabels)
        .rangeRoundPoints([0, width]);

    var maxY = this.calcStoreMaxYValue(store);
    var minY = this.calcStoreMinYValue(store);
    var y = d3.scale.linear()
        .domain([minY-Math.abs(maxY/10), maxY+Math.abs(maxY/10)])
        .range([height, 0]); 
    var div = document.getElementById(cell);

    var svg = d3.select(div).append("svg")
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")       
            .attr("class", "delta3chart")
            //.attr("viewbox", "0 0 " + this.chartWidth + " " + this.chartHeight)
            .attr("width", this.param.width)
            .attr("height", this.param.height)   
            //.attr("preserveAspectRatio", "xMidYMid meet")    
            .style("background-color", backgroundColor)      
            .style("font-family",this.param.fontFamily)
            .style("shape-rendering","crispEdges")    
            .style("font-size",this.param.fontSize)    
            .attr("id", this.combinedSVGId + index);   

    var chart = svg.append("svg:g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var xAxis = d3.svg.axis()
        .scale(x)
        .tickSize(-height)     
        .tickPadding(10)
        .tickSubdivide(true)
        .orient("bottom");         

    var yAxis = d3.svg.axis()
            .scale(y)
            .tickPadding(0.1)
            .tickSize(-width)
            .tickSubdivide(true) 
            .tickPadding(6)
            .orient("left");    

    chart.append("svg:g")
        .append("rect")
        .attr("fill","#f2f2f2")
        .attr("width", width)
        .attr("height", height)
        .on("click", function () {
            callback(me.param.idStudy, me.idProcess);      
            });
            
   chart.append("svg:g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .selectAll("text")  
                .style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", ".15em")
                .attr("transform", "rotate(-45)" );  
    
    chart.append("g")         
        .attr("class", "y axis")             
        .call(yAxis)      
        .append("text")
            .attr("class", "Cumulative Proportional")
            .attr("transform", "rotate(-90)")   
            .attr("y", (-margin.left) + 12)
            .attr("x", -2*height/3);         
            //.text(this.yAxisTitle);	

    chart.selectAll('.axis line')
         .style({"fill": "none","stroke": "white","shape-rendering": "crispEdges"});
    chart.selectAll('.axis path')
         .style({"fill": "none","stroke": "black","shape-rendering": "crispEdges"});
 
    var zeroLine = d3.svg.line()          
        .x(function(d) {return x(d.time); })
        .y(function(d) {return y(0); })        
        .interpolate("basis");    

    chart.append("svg:path")
            .attr("class", "ZERO_LINE")
            //.attr("clip-path", "url(#clip" + cell + ")")
            .attr("stroke", "red") 
            .attr("stroke-width",1)
            .attr("fill","none")
            .attr("d", zeroLine(store[0].data));             
    
    legend(chart, store,  width, outcomeVariables, me, changeResultOpacity);
    
    for (var ii=0; ii<store.length; ii++) {   
        drawSingleResult(ii, store, this.param.lowerOpacity);
        if ( ii === delta3.utils.GlobalVars.maxOutcomesPerChart - 1 ) break;        
    }
        
    svg.append("svg:g")
        .append("text")
            .attr("x", width/3)
            .attr("y", 2*margin.top/3)
            .text( this.graphTitle )
            .attr("font-size", this.param.fontSize);    
    
    console.log("d3 ProdDiff generation finished.");
    return svg;    
    
    function drawSingleResult(ii, store, opa) {
        var color = delta3.utils.GlobalVars.colorLowArray[ii];       
        
        var expLine = chart.selectAll("expLine")
                .data(store[ii].data)
                .enter()
                .append("line")
                    .attr("class", "expLine")
                    .attr('id','expLine' + ii)
                    .style("fill","none" )  
                    .style('stroke-opacity', opa)
                    .style("stroke-dasharray",("3,3"))
                    .style("stroke-width","3")
                    .style("stroke",color)
                    .attr("x1", function(d) {  return x(d.time); })
                            .attr("y1", function(d) { return y(d.high); })			
                    .attr("x2", function(d) {  return x(d.time); })
                            .attr("y2", function(d) { return y(d.low); });

        var circles = chart.selectAll("obsMarkerCircle")
            .data(store[ii].data)
            .enter()
            .append("circle")
                .attr("class","obsMarkerCircle") 
                .attr('id','obsMarkerCircle' + ii)
                .style('stroke-opacity', opa)
                .attr("fill", function (d) {
                    return color;          
                })
                .attr("cx", function (d) {
                    var ret = x(d.time)-1;
                    return ret;
                })
                .attr("cy", function (d) {
                    var ret = y(d.medium);
                    return ret;
                })
                .attr("r", function (d) {
                    return 5;
                })
                .append("title")
                    .text(function(d) { 
                        if ( typeof d.alert.decription !== 'undefined') {
                            return outcomeVariables[ii] + '\n' + d.alert.description + '\n'
                                    + 'Upper CI: ' + d.high + '\n'
                                    + 'Mean: ' + d.medium + '\n'
                                    + 'Lower CI: ' + d.low;
                        } else {      
                            return outcomeVariables[ii] + '\n' + 'Upper CI: ' + d.high + '\n'
                                    + 'Mean: ' + d.medium + '\n'
                                    + 'Lower CI: ' + d.low;
                        }                    
                    });
    }
    
    function changeResultOpacity(index, opa) {
        chart.selectAll("#expLine" + index)
            .style('fill-opacity', opa)
            .style('stroke-opacity', opa);       
        
        chart.selectAll("#obsMarkerCircle" + index)  
            .style('fill-opacity', opa)    
            .style('stroke-opacity', opa);
    }     
};
        
PropDiffMultiChart.prototype.calcStoreMinYValue = function(store) {
    return 0;
};
    
PropDiffMultiChart.prototype.calcStoreMaxYValue = function(store) {
    var tmp = store[0].data[0].high;
    for (var ii=0; ii<store.length; ii++) {        
        for (var i=0; i< store[ii].data.length; i++) {
            if (store[ii].data[i].high > tmp) {
                tmp = store[ii].data[i].high;
            }        
        }
    }
    return tmp;
};  
    
PropDiffMultiChart.prototype.download = function(params, index) {
    var div = document.getElementById(this.combinedSVGId + index);
    //div.style.backgroundColor = "white";
    var html = div.outerHTML;     
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var canvas = document.createElement('canvas');
    var context = canvas.getContext("2d");
    var image = new Image();
    image.onload = function() {
        canvas.width = image.width;
        canvas.height = image.height;                              
        context.drawImage(image, 0, 0);
        var canvasdata = canvas.toDataURL("image/" +  + params.format +"\"");
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.download = params.filename + "." + params.format;
        a.href = canvasdata;
        a.click();
    };     
    image.src = imgsrc;    

};      

module.exports.PropDiffChart = PropDiffMultiChart;
module.exports.getMarkup = PropDiffMultiChart.prototype.getMarkup;
module.exports.init = PropDiffMultiChart.prototype.init;
module.exports.download = PropDiffMultiChart.prototype.download;