/* 
 * Configuration Model
 */

Ext.define('delta3.model.ConfigurationModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idConfiguration', type: 'int'},
        'name',
        'description',
        'type',
        {name: 'active', type: 'boolean'}, 
        'connectionInfo',
        'dbUser',
        'dbPassword',
        {name: 'idOrganization', type: 'int'},
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});


