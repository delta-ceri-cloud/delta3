/* 
 * Method Model
 */

Ext.define('delta3.model.MethodModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idMethod', type: 'int'},
        'shortName',
        'name',
        'description',
        {name: 'sequenceNumber', type: 'int'},        
        {name: 'active', type: 'boolean'}, 
        'methodParams',
        {name: 'idOrganization', type: 'int'},
        {name: 'idStat', type: 'int'},        
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});


