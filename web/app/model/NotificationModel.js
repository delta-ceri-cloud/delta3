/* 
 * Notification Model
 */

Ext.define('delta3.model.NotificationModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idNotification', type: 'int'},
        'name',
        'description',
        'eventData',     
        {name: 'eventObjectId', type: 'int'},
        {name: 'eventObjectType'},
        {name: 'retryCount', type: 'int'},        
        {name: 'timeOut', type: 'int'},
        {name: 'ackRequired', type: 'boolean'},      
        {name: 'acknowledged', type: 'boolean'},                  
        {name: 'idUser', type: 'int'},    
        {name: 'idGroup', type: 'int'},    
        {name: 'sendEmail', type: 'boolean'},           
        {name: 'active', type: 'boolean'},   
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS',
        {name: 'idNotificationTemplate', type: 'int'},        
        {name: 'idEvent', type: 'int'}
    ]
});



