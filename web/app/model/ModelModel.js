/* 
 * Model(as in DELTA) Model
 */

Ext.define('delta3.model.ModelModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idModel', type: 'int'},
        {name: 'name', type: 'string'},
        {name: 'description', type: 'string'},
        'type',
        {name: 'locked', type: 'boolean'},         
        {name: 'dump', type: 'boolean'},                
        {name: 'active', type: 'boolean'}, 
        {name: 'isAtomicFinished', type: 'boolean'}, 
        {name: 'isMissingFinished', type: 'boolean'}, 
        {name: 'isVirtualFinished', type: 'boolean'},        
        {name: 'isSecondaryVirtualFinished', type: 'boolean'},         
        {name: 'idGroup', type: 'int'},
        'idConfiguration',
        'connectionInfo',
        'dbUser',
        'dbPassword',
        'outputName',     
        'status',
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS',
        /*v3.64 Added processing id DELQA-612*/
        'processingId',
        /*v3.64 Added Original model ID DELQA-557*/
        {name: 'originalModelId', type: 'int' },
        {name: 'projectAssigned', type: 'int' }
    ]
});


