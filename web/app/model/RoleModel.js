/* 
 * Role Model
 */

Ext.define('delta3.model.RoleModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idRole', type: 'int'},
        'name',
        'description',
        'type',
        {name: 'active', type: 'boolean'}, 
        {name: 'parentId', type: 'int'},        
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});



