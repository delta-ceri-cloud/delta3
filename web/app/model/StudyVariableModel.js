/* 
 * Study Variable Model
 */

Ext.define('delta3.model.StudyVariableModel', {
    extend: 'Ext.data.Model',
    fields: [
        'name',
        'description',
        'fieldKind',
        'fieldClass'
    ]
});



