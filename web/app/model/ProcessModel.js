/* 
 * Process Model
 */

Ext.define('delta3.model.ProcessModel', {
    extend: 'Ext.data.Model',
    fields: [
                {name: 'idProcess', type: 'int', sortType: 'asInt' },
                {name: 'idOrganization', type: 'int' },
                {name: 'idGroup', type: 'int' },
                {name: 'idModel', type: 'int' },    
                {name: 'idStudy', type: 'int' },  
                {name: 'name', type: 'string'},
                {name: 'description', type: 'string'},
                'input',
                'data',
                'status',
                {name: 'message', type: 'string'},
                'filterClause',
                {name: 'idFilter', type: 'int'},
                'guid',
                {name: 'progress', type: 'int' },                
                {name: 'active', type: 'boolean'},   
                {name: 'localStatPackage', type: 'boolean'},    
                {name: 'remoteStatPackage', type: 'boolean'},                    
                {name: 'manualExecution', type: 'boolean'},    
                'submittedTS',
                {name: 'createdBy', type: 'int'},
                'createdTS',
                {name: 'updatedBy', type: 'int'},
                'updatedTS',
                {name: 'idFile', type: 'string' },
                {name: 'fileUrl', type: 'string' }
            ]
});
