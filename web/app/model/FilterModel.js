/* 
 * Filter Model
 */

Ext.define('delta3.model.FilterModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idFilter', type: 'int'},
        'name',
        'description',
        {name: 'resultCount', type: 'int'},
        {name: 'active', type: 'boolean'}, 
        'type',
        {name: 'modelidModel', type: 'int'}, 
        {name: 'organizationidOrganization', type: 'int'},
        {name: 'groupidGroup', type: 'int'},        
        {name: 'createdBy', type: 'int'},
        'createdTS',
        {name: 'updatedBy', type: 'int'},
        'updatedTS'
    ]
});


