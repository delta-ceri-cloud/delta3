/* 
 * Logistic Regression Model
 */

Ext.define('delta3.model.LogregrModel', {
    extend: 'Ext.data.Model',
    fields: [
                {name: 'idLogregr', type: 'int' },
                'name',
                'description',
                'status',
                'formula',
                'intervalData',
                'intervalQuery',
                {name: 'idOrganization', type: 'int' },
                {name: 'idModel', type: 'int' },       
                {name: 'modelColumnidSequencer', type: 'int' },                  
                {name: 'idStudy', type: 'int' },
                {name: 'idProcess', type: 'int' },                
                {name: 'originalLogregrId', type: 'int' },                               
                {name: 'active', type: 'boolean'},                 
                {name: 'createdBy', type: 'int'},
                'createdTS',
                {name: 'updatedBy', type: 'int'},
                'updatedTS'
            ]
});
