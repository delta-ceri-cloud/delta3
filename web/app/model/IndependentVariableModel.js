/* 
 * Independent Variable Model
 */

Ext.define('delta3.model.IndependentVariableModel', {
    extend: 'Ext.data.Model',
    fields: [
        'name',
        'description',
        'fieldKind',
        'fieldClass'
    ]
});



