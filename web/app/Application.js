/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('delta3.Application', {
    extend: 'Ext.app.Application',
    
    name: 'delta3',
    controllers: ['Main'],
    views: [
        'delta3.view.login.Login'
    ],
    requires: [
        'delta3.controller.LoginController'
    ],
    init: function () {
        // https://docs.sencha.com/extjs/6.0/wh...de.html#Button
        Ext.enableAriaButtons = false;
        Ext.enableAriaPanels = false;
        },    
    launch: function () {
        //var loggedIn = false;
        Ext.create('delta3.utils.GlobalVars');
        Ext.create('delta3.utils.GlobalFunc');
        Ext.create('delta3.utils.Tooltips');         
        delta3.utils.GlobalFunc.getCurrentUserAuthInfo(stillLoggedin, notLoggedin);        
        function stillLoggedin() {
            var loginController = Ext.create('delta3.controller.LoginController'); 
            // this call includes creation of 'view-main'
            loginController.initializeGlobalVariables();           
        }
        function notLoggedin() {
            Ext.create({
                xtype: 'login'
            });            
        }
    },
    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});



