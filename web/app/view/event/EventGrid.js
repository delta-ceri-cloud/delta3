/* 
 * Event Grid
 */

Ext.define('delta3.view.event.EventGrid', {
    extend: 'Ext.grid.Panel',
    columnLines: true,
    enableLocking: true,
    alias: 'widget.grid.event',
    itemId: 'eventGrid',
    autoScroll: false,
    renderTo: document.body,
    minHeight: 150,    
    height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.column.Check',
        'delta3.store.EventStore',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator'   
    ],
    border: false,
    dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',            
            items: [{
                itemId: 'EventDelete',
                text: 'Delete',
                iconCls: 'delete-icon16',
                handler: function() {
                    Ext.Msg.show({
                        title:'DELTA',
                        message: 'You are about to delete Event. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                var thisGrid = Ext.ComponentQuery.query('#eventGrid')[0];
                                var sm = thisGrid.getSelectionModel();
                                thisGrid.plugins[0].cancelEdit();
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                                thisGrid.store.reload();
                            }
                        }
                    });                    
                }
            }]
        },{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            itemId: 'eventPaging',
            emptyMsg: 'No events found',
            displayInfo: true
        }],
    modelSelected: {
        idModel: 0,
        name: ""
    },
    fieldStore: {},
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.dockedItems[1].store = me.store;
        me.store.load();        
        me.plugins = me.buildPlugins();     
        me.columns = me.buildColumns(me.fieldStore);
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];  
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me})); 
    },
    buildColumns: function(fieldStore) {
        var userStore = delta3.store.UserStore.create();
        userStore.load();
        return [
            {text: 'ID', dataIndex: 'idEvent', width: 60},
            {text: 'Description', dataIndex: 'description', width: 100, editor: 'textfield'},
            {text: 'Template', dataIndex: 'idEventTemplate', width: 60},    
            {text: 'Object ID', dataIndex: 'objectId', width: 50},
            {text: 'Object Type', dataIndex: 'objectType', width: 60},              
            {text: "To be executed TS", width: 120, sortable: true, dataIndex: 'executeTS', type: 'date', dateFormat: 'Y-m-d H:i:s', editor: 'datefield'},
            {text: 'Scheduled By', dataIndex: 'executeBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', userStore);
                }},            
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', userStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', userStore);
                }},
            {text: 'Organization', dataIndex: 'idOrganization', width: 60}
        ];
    },
    buildStore: function() {
        return Ext.create('delta3.store.EventStore',{groupId: Ext.ComponentQuery.query('#mainViewPort')[0].project});
    },
    buildPlugins: function() {
        return [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            itemId: 'eventGridEditor',
            listeners: {
                edit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#eventGrid')[0];
                    thisGrid.store.save();
                }
            }
            })
        ];
    }    
});

