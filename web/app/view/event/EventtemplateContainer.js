/* 
 * Eventtemplate Container
 */

Ext.define('delta3.view.event.EventtemplateContainer', {
    extend: 'Ext.container.Container',
    itemId: 'eventtemplateContainer',
    alias:  'widget.container.eventtemplates',
    //layout: 'fit',
    initComponent:  function(){ 
        var me = this;  
        me.items = {
                xtype:  'grid.eventtemplate',
                region: 'center'
        },   
        me.callParent();
    }
});

