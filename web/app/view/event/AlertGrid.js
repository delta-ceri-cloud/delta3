/* 
 * Alert Grid
 */

Ext.define('delta3.view.event.AlertGrid', {
    extend: 'Ext.grid.Panel',
    columnLines: true,
    enableLocking: true,
    alias: 'widget.grid.alert',
    itemId: 'alertGrid',
    autoScroll: false,
    renderTo: document.body,
    minHeight: 150,
    height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.column.Check',
        'delta3.store.AlertStore',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator'           
    ],
    border: false,
    listeners: 
            
        {    
        
            beforerender: function() {
                //delta3.utils.GlobalFunc.showDeltaMessage('Please refresh grid for updated view');           
            console.log("before afterrender");
            var thisGrid = Ext.ComponentQuery.query('#alertGrid')[0];           
            thisGrid.store.load();
            Ext.ComponentQuery.query('#alertGrid')[0].getView().refresh(); 
            var userStore = delta3.store.UserStore.create();
            userStore.load();
            var eventtemplateStore = delta3.store.EventtemplateStore.create();
            eventtemplateStore.load();       

                /*
                Ext.Msg.show({
                    closable: false,
                    msg: 'All entitiles associated with selected project have retrived successfully. Please click "ok" to refresh view.',
                    title: 'Project Change',
                    buttons: Ext.Msg.OK, 
                    width: 300,
                    fn: function(buttonId) {
                      if (buttonId === 'ok') {
                       console.log(btn);
                        var thisGrid = Ext.ComponentQuery.query('#alertGrid')[0];
                        //thisGrid.store.clearFilter(true);
                        //thisGrid.store.filter('active', true);              
                        thisGrid.store.reload();
                        Ext.ComponentQuery.query('#alertGrid')[0].getView().refresh();
                      }
                    }
                  });*/
        },
        afterrender: function() {
                       setTimeout(function() 
                       { 
                       console.log("at set time out");
                        var thisGrid = Ext.ComponentQuery.query('#alertGrid')[0];
                        Ext.ComponentQuery.query('#alertGrid')[0].getView().refresh();
                        console.log("store load count :"+thisGrid.store.getCount());                        
                        thisGrid.store.clearFilter(true);
                        //thisGrid.store.filter('active', true);              
                        thisGrid.store.reload();
                        thisGrid.store.loadPage(1);  
                       
                       }, 400);
                        
        }
             
    },
    
    
    dockedItems: [
        
        {
            xtype: 'toolbar',
            dock: 'top',            
            items: [
                
                {
                itemId: 'addAlert',
                text: 'Add Alert',
                iconCls: 'add_new-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#alertGrid')[0];
                    thisGrid.plugins[0].cancelEdit();
                    // Create a new record instance
                    
                    
                    var r = delta3.model.AlertModel.create({
                        active: true,
                        priority: 1,
                        createdTS: '0000-00-00 00:00:00.0',
                        updatedTS: '0000-00-00 00:00:00.0'});
                    thisGrid.store.insert(0, r);
                    thisGrid.plugins[0].startEdit(0, 0);
                }
            }, 
            {
                itemId: 'AlertDelete',
                text: 'Delete',
                iconCls: 'delete-icon16',
                handler: function() {
                    Ext.Msg.show({
                        title:'DELTA',
                        message: 'You are about to delete Alert. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                var thisGrid = Ext.ComponentQuery.query('#alertGrid')[0];
                                var sm = thisGrid.getSelectionModel();
                                thisGrid.plugins[0].cancelEdit();
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                                //thisGrid.store.reload();                                
                            }
                        }
                    });                       
                }
        },
        
        {
            itemId: 'refreshAlert',
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            tooltip: delta3.utils.Tooltips.modelBtnRefresh,
            listeners: {
                'click': function(cmb, rec, idx) {
                var thisGrid = Ext.ComponentQuery.query('#alertGrid')[0];
                thisGrid.store.load();
                    }
                }
        }
            
        ]
        },{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No events found',
            displayInfo: true
        }],
    modelSelected: {
        idModel: 0,
        name: ""
    },
    fieldStore: {},
    
    
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
       
        me.dockedItems[1].store = me.store;
        me.store.load();        
        
        
        me.plugins = me.buildPlugins();     
        me.columns = me.buildColumns();
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0]; 
        
        
        if (delta3.utils.GlobalFunc.isPermitted("AlertDelete") === true) {
            tbr.add({
                itemId: 'AlertDelete',
                text: 'Delete',
                iconCls: 'delete-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#alertGrid')[0];
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    thisGrid.store.remove(sm.getSelection());
                    thisGrid.store.sync();
                }
            });
        }
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me})); 
    },
    
    
    buildColumns: function() {
        var userStore = delta3.store.UserStore.create();
        userStore.load();
        var eventtemplateStore = delta3.store.EventtemplateStore.create();
        eventtemplateStore.load();        
        return [
            //{text: 'ID', dataIndex: 'idAlert', width: 30},
            {text: 'Event', dataIndex: 'idEventTemplate', width: 200,
                editor: new Ext.form.ComboBox({
                    store: eventtemplateStore,
                    itemId: 'eventComboBox',
                    displayField: 'name',
                    valueField: 'idEventTemplate',
                    queryMode: 'local',
                    multiSelect: false,
                    forceSelection: true,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            var h = cmb.getValue();
                            var thisGrid = Ext.ComponentQuery.query('#alertGrid')[0];
                            thisGrid.modelSelected = cmb.displayTplData[0];
                        }
                    }
                    
                }),
                
                
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idEventTemplate'];
                    var eventtemplate = eventtemplateStore.findRecord('idEventTemplate', tableIndex, undefined, undefined, undefined, true);
                    if (eventtemplate === null)
                        return null;
                    else {
                        rec.data['name'] = eventtemplate.get('name');
                        rec.data['description'] = eventtemplate.get('description');
                        rec.data['type'] = eventtemplate.get('type');
                        return eventtemplate.get('name');
                    }
                },
            
                afterrender:function(){
                    
                        var thisGrid = Ext.ComponentQuery.query('#alertGrid')[0];           
                         thisGrid.store.load();
                        Ext.ComponentQuery.query('#alertGrid')[0].getView().refresh(); 
                }
            
            },
            
            
            
            //{text: 'Name', dataIndex: 'name', width: 100, editor: 'textfield'},
            {text: 'Description', dataIndex: 'description', width: 200},       
            /*{text: 'Priority', dataIndex: 'priority', width: 80,  editor:
                    new Ext.form.ComboBox({
                        store: delta3.utils.GlobalVars.notificationPriorityComboBoxStore,
                        displayField: 'priority',
                        valueField: 'value',
                        queryMode: 'local',
                        itemId: 'alertPriorityComboBox',
                        forceSelection: true
                    }),
                    renderer: function(val, cell, rec, r_idx, c_idx, store) {
                        var typeString = delta3.utils.GlobalVars.notificationPriorityComboBoxStore.findRecord('value', rec.data['priority']);
                        if (typeString === null)
                            return 'error';
                        else
                            return typeString.get('priority');
                    }
                }, */
            //{text: 'Ack', disabled: true, xtype: 'checkcolumn', dataIndex: 'ackRequired', width: 50, editor: 'checkboxfield'},     
            //{text: 'Time Out', dataIndex: 'timeOut', width: 60, editor: 'textfield'},  
            {text: 'Send Email', disabled: true, xtype: 'checkcolumn', dataIndex: 'sendEmail', width: 80, editor: 'checkboxfield'},               
            
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', width: 50, 
                editor: 'checkboxfield'},
            
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 80,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', userStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 80,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', userStore);
                }}
        ];
    },
    buildStore: function() {
        return Ext.create('delta3.store.AlertStore');
    },
    
    
    buildPlugins: function() {
        return [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: true,
            itemId: 'alertGridEditor',
            listeners: {
                
                edit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#alertGrid')[0];
                    thisGrid.store.save();
                    Ext.ComponentQuery.query('#alertGrid')[0].getView().refresh();
                    
                },
                
                afterEdit : function(grid, row, field, rowIndex, columnIndex){
                     var thisGrid = Ext.ComponentQuery.query('#alertGrid')[0];
                    //thisGrid.store.load();
                    Ext.ComponentQuery.query('#alertGrid')[0].getView().refresh();
                }
            }
            })
        ];
    }    
});

