/* 
 * Logistic Regression Container
 */

Ext.define('delta3.view.logregr.LogregrContainer', {
    extend: 'Ext.container.Container',
    itemId: 'logregrContainer',
    alias:  'widget.container.logregrs',
    initComponent:  function(){ 
        var me = this;     
        me.items = {
                xtype:  'grid.logregr',
                region: 'center'
        },   
        me.callParent();
    }
});

