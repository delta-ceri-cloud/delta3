/* 
 * Logistic Regresson Formula Field Grid
 */

Ext.define('delta3.view.logregr.LogregrFieldGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.logregrFieldGrid',
    itemId: 'logregrFieldGrid',
    autoScroll: false,
    renderTo: document.body,
    width: delta3.utils.GlobalVars.initWidth,
    height: delta3.utils.GlobalVars.initHeight,
    requires: [
        'Ext.data.Store',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.layout.container.Table',
        'Ext.grid.column.Column',
        'delta3.store.LogregrFieldStore'
    ],
    border: false,
    viewConfig: {
        enableTextSelection: true
    },
    logregrId: {},
    logregrName: {},
    fieldStore: {},
    initComponent: function() {
        var me = this;
        me.store = me.buildStore(me.logregrId, me.logregrName);
        me.columns = me.buildColumns();
        me.plugins = me.buildPlugins();
        me.callParent();
        me.store.load();
    },
    listeners: {
        afterrender: function(grid) {
            var currentDataIndex, columnText;
            var menu = grid.headerCt.getMenu();
            menu.add([{
                    text: 'Delete',
                    iconCls: 'delete-icon16',
                    handler: function() {
                        
                        var dateStore = Ext.StoreMgr.lookup("logregrDateStore");
                        for (var j = 0; j < dateStore.data.length; j++) {
                            var rec = dateStore.getAt(j);
                            if (rec.get('name') === columnText) {
                                dateStore.removeAt(j);
                            }
                        }
                        dateStore.sync();
                        grid.headerCt.remove(currentDataIndex);
                        grid.width -= delta3.utils.GlobalVars.dateLength;
                        grid.getView().refresh();
                        grid.updateLayout();
                    }
                }]);
                menu.add([{
                    text: 'Change',
                    iconCls: 'change-icon16',
                    handler: function() {
                        
                        //var dateStore = Ext.StoreMgr.lookup("logregrDateStore");
                        var win = new Ext.Window(
                                {
                                    layout: 'fit',
                                    width: 400,
                                    height: 200,
                                    itemId: 'newDatePopup',
                                    modal: true,
                                    closeAction: 'hide',
                                    items: new Ext.Panel(
                                            {
                                                frame: true,
                                                labelWidth: 90,
                                                labelAlign: 'right',
                                                title: 'Specify new date for Logistic Regression Formula',
                                                bodyStyle: 'padding:5px 5px 0',
                                                width: 400,
                                                height: 200,
                                                defaultType: 'displayfield',
                                                buttons: [
                                                    {text: 'Save',
                                                        handler: function() {
                                                            var nd = Ext.ComponentQuery.query('#newDate')[0].value
                                                            if (nd !== null) {
                                                                var patt = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$/;
                                                                if ( patt.test(nd) === true ) {
                                                                    var dateStore = Ext.StoreMgr.lookup("logregrDateStore");
                                                                    for (var j = 0; j < dateStore.data.length; j++) {
                                                                        var rec = dateStore.getAt(j);
                                                                        if (rec.get('name') === columnText) {
                                                                            rec.set('name', nd)
                                                                        }
                                                                    }
                                                                    dateStore.sync();
                                                                    Ext.ComponentQuery.query('#newDatePopup')[0].destroy();
                                                                    grid.getView().refresh();
                                                                    grid.updateLayout();
                                                                } else {
                                                                    delta3.utils.GlobalFunc.showDeltaMessage('Enter date using mm/dd/yyyy format');
                                                                }
                                                            }
                                                        }
                                                    },
                                                    {text: 'Cancel',
                                                        handler: function() {
                                                            Ext.ComponentQuery.query('#newDatePopup')[0].destroy();
                                                        }
                                                    }
                                                ],
                                                items: [{
                                                        xtype: 'textfield',
                                                        fieldLabel: 'New Date',
                                                        itemId: 'newDate',
                                                        labelWidth: 100,
                                                        labelAlign: 'left',                   
                                                        allowBlank: true
                                                    }
                                                ]
                                            })
                                });
                        win.show();
                    }
                }]);            
            menu.on('beforeshow', function() {
                // get data index of column for which menu will be displayed
                currentDataIndex = menu.activeHeader.fullColumnIndex;
                columnText = menu.activeHeader.text;
            });
        }
    },
    buildColumns: function() {
        return [
            {text: 'ID', dataIndex: 'idLogregrField', width: delta3.utils.GlobalVars.idLength},
            {xtype: 'actioncolumn',
                width: 26,
                items: [{
                        iconCls: 'delete-icon16',
                        tooltip: 'Delete row',
                        handler: function(grid, rowIndex, colIndex) {
                            var logregrFieldGrid = Ext.ComponentQuery.query('#logregrFieldGrid')[0];
                            var rec = logregrFieldGrid.getStore().getAt(rowIndex);
                            if (rec.data.value !== 'C') {
                                logregrFieldGrid.getStore().removeAt(rowIndex);
                                logregrFieldGrid.store.sync();
                                delta3.utils.GlobalFunc.clearLRFormula(rec.data.idLogregr);
                            } else {
                                Ext.Msg.alert("DELTA", '"intercept" record cannot be removed.');
                            }
                        }
                    }]
            },
            {text: 'Risk Factor', dataIndex: 'name', width: delta3.utils.GlobalVars.uiControlLength}
        ];
    },
    buildPlugins: function() {
        return [Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToEdit: 2,
                autoCancel: true,
                itemId: 'logregrFieldGridEditor',
                listeners: {
                    edit: function(rowEditor, changes, r) {
                        var fRec = changes.store.getAt(changes.rowIdx);
                        var thisGrid = Ext.ComponentQuery.query('#logregrFieldGrid')[0];
                        var dateStore = Ext.StoreMgr.lookup("logregrDateStore");
                        var newField;
                        var i = 0;
                        for (var j = delta3.utils.GlobalVars.dateColumnZero; j < rowEditor.editor.items.length; j++) {
                            var x = rowEditor.editor.items.getAt(j).value;
                            for (; i < dateStore.data.length; i++) {
                                var rec = dateStore.getAt(i);
                                if (rec.get('idLogregrField') === fRec.get('idLogregrField')) {
                                    rec.set('value', x);
                                    newField = new Ext.data.field.Field({name: 'value' + i, type: 'string', persist: false});
                                    thisGrid.store.model.addFields(newField);  
                                    fRec.set('value' + i, x);                                       
                                    i++;
                                    break;
                                }
                            }
                        }
                        thisGrid.getView().refresh();
                        thisGrid.updateLayout();
                        var regrFieldPopup = Ext.ComponentQuery.query('#logregrFieldPopup')[0]; 
                        regrFieldPopup.updateLayout();        
                        dateStore.sync(); 
                        delta3.utils.GlobalFunc.clearLRFormula(rec.data.idLogregr);
                    }
                }
            })];
    },
    addField: function(fieldName, idRisk) {
        var thisGrid = Ext.ComponentQuery.query('#logregrFieldGrid')[0];
        if (thisGrid.store.findRecord('modelColumnidRisk', idRisk)) {
            return;
        }
        thisGrid.height += 10;
        thisGrid.updateLayout();
        var r = delta3.model.LogregrFieldModel.create({
            active: true,
            name: fieldName,
            value: '0.0',
            idLogregr: thisGrid.logregrId,
            modelColumnidRisk: idRisk,
            createdTS: '0000-00-00 00:00:00.0',
            updatedTS: '0000-00-00 00:00:00.0'});
        thisGrid.store.insert(0, r);
        // the caller is responsible fir the sync with backend
    },
    addColumn: function(popupWin) {
        var df = Ext.getCmp("fDate");
        var regrFieldGrid = Ext.ComponentQuery.query('#logregrFieldGrid')[0];
        if (regrFieldGrid.store.data.length > 0) {
            // do this only if at least one field is defined                                        
            var dateStore = Ext.StoreMgr.lookup("logregrDateStore");
            var val = df.getValue().toLocaleDateString();
            for (var i = 0; i < regrFieldGrid.store.data.length; i++) {
                var rec = regrFieldGrid.getStore().getAt(i);
                var v = rec.get("value");
                if ( v === 'C' ) v = '0.0';
                var r = delta3.model.LogregrDateModel.create({
                    active: true,
                    name: val,
                    value: v,
                    idLogregr: popupWin.logregrId,
                    modelColumnidSequencer: popupWin.sequencerId,
                    idLogregrField: rec.get("idLogregrField"),
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});
                dateStore.insert(dateStore.getCount(), r);
            }
            dateStore.sync();
            //var len = regrFieldGrid.columns.length;
            var len = regrFieldGrid.headerCt.gridDataColumns.length;
            var column = new Ext.grid.column.Column({text: val, dataIndex: 'value' + len, width: delta3.utils.GlobalVars.dateLength, editor: 'textfield'});
            regrFieldGrid.headerCt.insert(len, column);
            regrFieldGrid.width += delta3.utils.GlobalVars.dateLength;
            regrFieldGrid.getView().refresh();
            popupWin.updateLayout();
        }        
    },
    buildStore: function(logregrId, logregrName) {
        return new delta3.store.LogregrFieldStore({logregrId: logregrId, logregrName: logregrName});
    }
});

