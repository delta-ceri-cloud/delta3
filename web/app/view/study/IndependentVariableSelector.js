/**
 * CERI-Lahey (c) 2016
 */
Ext.define('delta3.view.study.IndependentVariableSelector', {
    extend: 'Ext.container.Container',
    
    requires: [
        'Ext.grid.*',
        'Ext.layout.container.HBox',
        'delta3.model.IndependentVariableModel'
    ],    
    xtype: 'dd-grid-to-grid',
    
    
    width: 470,
    height: 160,
    layout: {
        type: 'hbox',
        align: 'stretch',
        padding: 5
    },
    
    riskFactorStore: {},
    indepVariableStore: {},
    indepVariablesList: {},
    
    initComponent: function(){
        var group1 = this.id + 'group1',
            group2 = this.id + 'group2',
            columns = [{
                text: 'Variable Name', 
                flex: 1, 
                sortable: true, 
                dataIndex: 'name'
            }, {
                text: 'Kind', 
                width: 70, 
                sortable: true, 
                dataIndex: 'fieldKind'
            }];
        this.indepVariableStore = new Ext.data.Store({model: delta3.model.IndependentVariableModel}); 
        var arrayOfIndepVariables = this.indepVariablesList.split(",");
        for (var i=0; i< arrayOfIndepVariables.length; i++) {
            if ( this.riskFactorStore.getTotalCount() > 0 ) {
                var index = this.riskFactorStore.find('name', arrayOfIndepVariables[i]);
                var record = this.riskFactorStore.getAt(index);
                this.riskFactorStore.removeAt(index);
                this.indepVariableStore.add(record);
            }
        }
        
        this.items = [{
            itemId: 'riskFactorGrid',
            flex: 1,
            xtype: 'grid',
            multiSelect: true,
                viewConfig: {
                plugins: {
                    ptype: 'gridviewdragdrop',
                    containerScroll: true,
                    dragGroup: group1,
                    dropGroup: group2
                },
                listeners: {
                    drop: function(node, data, dropRec, dropPosition) {
                        var dropOn = dropRec ? ' ' + dropPosition + ' ' + dropRec.get('name') : ' on empty view';
                    }
                }
            },
            store: this.riskFactorStore,
            columns: columns,
            title: 'Data Model Risk Factors',
//            tools: [{
//                type: 'refresh',
//                tooltip: 'Reset both grids',
//                scope: this,
//                handler: this.onResetClick
//            }],
            stripeRows: true,        
            margin: '0 5 0 0'
        }, {
            itemId: 'indepVariable',
            flex: 1,
            xtype: 'grid',
            viewConfig: {
                plugins: {
                    ptype: 'gridviewdragdrop',
                    containerScroll: true,
                    dragGroup: group2,
                    dropGroup: group1
                },
                listeners: {
                    drop: function(node, data, dropRec, dropPosition) {
                        var dropOn = dropRec ? ' ' + dropPosition + ' ' + dropRec.get('name') : ' on empty view';
                    }
                }
            },
            store: this.indepVariableStore,
            columns: columns,
//            tools: [{
//                type: 'search',
//                tooltip: 'Retrieve variable list',
//                scope: this,
//                handler: this.onRetrieveClick
//            }],            
            stripeRows: true,
            title: 'Selected Independent Variables',
            getRawValue: function() {
                var valueString = '';
                for ( var i=0; i<this.store.data.length; i++) {
                    if ( i > 0 ) {
                        valueString += ',';
                    }
                    valueString += this.store.getAt(i).get('name');
                }
                return valueString;
            }            
        }];

        this.callParent();
    },
    
    onResetClick: function(){
        Ext.Msg.show({
            title: 'DELTA',
            message: 'You are about to clear variable list. Would you like to proceed?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    //refresh source grid
                    this.down('#riskFactorGrid').getStore().loadData(this.variableData);
                    //purge destination grid
                    this.down('#indepVariable').getStore().removeAll();
                }
            }
        });        
    },
    
    onRetrieveClick: function(){
        Ext.Msg.show({
            title: 'DELTA',
            message: 'You are about to retrieve variable list. Would you like to proceed?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {

                }
            }
        });        
    }    
}
);
