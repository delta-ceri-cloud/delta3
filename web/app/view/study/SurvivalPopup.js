/* 
 * Survival Method Parameters Popup Window
 */

Ext.define('delta3.view.study.SurvivalPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.survival',
    layout: 'fit',
    width: 500,
    height: 450,
    itemId: 'methodSurvivalPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA',
    listeners: {
        beforedestroy: function(rowEditor, context, eOpt) {
            this.fieldStore.clearFilter(true);
        }
    },    
    labelWidth: 160,
    labelAlign: 'left',   
    startDate: {},
    endDate: {},
    step: 0,
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    depVariable: {},
    expVariable: {},
    params: {},
    fieldId: 0,
    newFieldId: {},
    modelId: {},
    studyId: {},
    items: [],
    buttons: [
        {
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodSurvivalPopup')[0];        
                //var genericIdCheckbox = Ext.ComponentQuery.query('#genericIdCheckbox')[0];          
                var index = thisWin.selectedStudyRecord.get('idKey');
                thisWin.fieldStore.clearFilter(true);
                var uniqueField = thisWin.fieldStore.findRecord('idModelColumn', index);
//                if ( genericIdCheckbox.value === true ) {
//                    thisWin.params[thisWin.step].inputs.studyUniqueId.values[0] = 'id';
//                } else {
                    thisWin.params[thisWin.step].inputs.studyUniqueId.values[0] = uniqueField.get("name");
//                }
//                thisWin.selectedStudyRecord.set('isGenericId', genericIdCheckbox.value);  
                
                var theIndepItems = Ext.ComponentQuery.query('#indepVariable')[0];   
                if ( typeof theIndepItems !== 'undefined' ) {
                    var listOfIndepVariables = theIndepItems.getRawValue();
                    var arrayOfIndepVariables = listOfIndepVariables.split(',');                
                    thisWin.params[thisWin.step].inputs.independentVariableSelection.values = [];              
                    for (var i = 0; i < arrayOfIndepVariables.length; i++) {
                        thisWin.params[thisWin.step].inputs.independentVariableSelection.values[i] = arrayOfIndepVariables[i];
                    }              
                }        

                thisWin.params[thisWin.step].inputs.sequencingVariableSelection.values[0] = Ext.ComponentQuery.query('#seqVariable')[0].value;          
                if ( typeof Ext.ComponentQuery.query('#avs')[0] !== 'undefined' ) {
                    thisWin.params[thisWin.step].inputs.automaticVariableSelection.values[0] = Ext.ComponentQuery.query('#avs')[0].value;  
                }                     
                var startVariable = Ext.ComponentQuery.query('#startVariable')[0];  
                thisWin.params[thisWin.step].inputs.start.values[0] = startVariable.value;
                var stopVariable = Ext.ComponentQuery.query('#stopVariable')[0]; 
                thisWin.params[thisWin.step].inputs.stop.values[0] = stopVariable.value;        
                thisWin.params[thisWin.step].inputs.dependentVariableSelection.values[0] = thisWin.depVariable;                    
                thisWin.selectedStudyRecord.set('idOutcome', delta3.utils.GlobalFunc.lookupFieldId(thisWin.depVariable));    
                thisWin.params[thisWin.step].inputs.exposureVariableSelection.values[0] = thisWin.expVariable;                    
                thisWin.selectedStudyRecord.set('idTreatment', delta3.utils.GlobalFunc.lookupFieldId(thisWin.expVariable));                  
                var newParams = JSON.stringify(thisWin.params);
                thisWin.selectedStudyRecord.set('methodParams', newParams);            
                var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                var rec = studyGrid.getStore().findRecord('idStudy', thisWin.studyId);      
                rec.set('status', 'New');                 
                studyGrid.getStore().sync();               
                studyGrid.setLoading();
                // Clear the loading mask after 1 second
                setTimeout(function (target) {
                    target.setLoading(false);
                }, 1000, studyGrid);                
            }
        }, {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodSurvivalPopup')[0];
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.fieldStore.clearFilter(true);   
        var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
        var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));
        if ( methodRec === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign a Method to the study first.");
            me.fieldStore.clearFilter(true);
            me.destroy();
        }                
        me.selectedFieldRecord = me.fieldStore.findRecord('idModelColumn', me.fieldId);
        var index = me.selectedStudyRecord.get('idKey');
        var uniqueField = me.fieldStore.findRecord('idModelColumn', index); 
        if ( uniqueField === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
            me.destroy();
        } 
           
        index = me.selectedStudyRecord.get('idDate');
        if ( me.fieldStore.findRecord('idModelColumn', index) === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Sequence Field to the study first.");
            me.destroy();            
        }
        
        var title = 'Survival parameters for study "' + me.selectedStudyRecord.get('name') + '"';
        var studyParams = me.selectedStudyRecord.get('methodParams');
        
        if ( (typeof studyParams !== 'undefined') && (studyParams !== null) && (studyParams !== "") ) {
            // params previously saved for the study
            me.params = JSON.parse(studyParams);        
        } else {
            // empty params string from template
            me.params = JSON.parse(methodRec.data.methodParams);
        }
        if ( typeof me.params[me.step].inputs.independentVariableSelection !== 'undefined') {
            var indepVariables = '';
            var cflag = 0;
            for (var i = 0; i < me.params[me.step].inputs.independentVariableSelection.values.length; i++) {
                if ( me.params[me.step].inputs.independentVariableSelection.values[i] !== 'value0' ) {
                    if ( cflag > 0 ) {
                        indepVariables += ',';
                    }
                    cflag++;
                    indepVariables += me.params[me.step].inputs.independentVariableSelection.values[i];
                }
            }            
        }

        me.items[0] = new Ext.Panel(
                {
                    frame: true,               
                    itemId: 'methodSurvivalPanel',
                    bodyStyle: 'padding:5px 5px 0',
                    title: title,
                    autoScroll: true,
                    defaultType: 'displayfield'
                }); 

          if ( typeof me.params[me.step].inputs.studyUniqueId !== 'undefined' ) {
            me.items[0].add({
                allowBlank: !me.params[me.step].inputs.studyUniqueId.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                fieldLabel: me.params[me.step].inputs.studyUniqueId.inputText,
                itemId: 'studyId',
                value: uniqueField.get("name")
            });
//            me.items[0].add({
//                labelWidth: me.labelWidth,
//                labelAlign: me.labelAlign,                 
//                xtype: 'checkbox',
//                fieldLabel: 'Use generic Id',
//                inputValue: true,
//                value: me.selectedStudyRecord.get('isGenericId'),
//                itemId: 'genericIdCheckbox'                           
//            });
        }         
        if ( typeof me.params[me.step].inputs.dependentVariableSelection !== 'undefined' ) {
            if ( JSON.stringify(me.params[me.step].inputs.dependentVariableSelection.values[0]) === "{}" || me.params[me.step].inputs.dependentVariableSelection.values[0] === 'value0' ) {
                me.depVariable = '';
            } else {
                me.depVariable = me.params[me.step].inputs.dependentVariableSelection.values[0];              
            }                      
            me.fieldStore.filter('fieldClass', 'Outcome');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);         
            var outcomeDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');        
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.dependentVariableSelection.inputText,
                store: outcomeDropdownValues,
                itemId: 'depVariable',
                allowBlank: !me.params[me.step].inputs.dependentVariableSelection.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'value',
                valueField: 'value',
                queryMode: 'local',
                value: me.depVariable,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.depVariable = cmb.getValue();
                    }
                }
            });            
        } 
        if ( typeof me.params[me.step].inputs.exposureVariableSelection !== 'undefined' ) {
            if ( JSON.stringify(me.params[me.step].inputs.exposureVariableSelection.values[0]) === "{}" || me.params[me.step].inputs.exposureVariableSelection.values[0] === 'value0' ) {
                me.expVariable = '';
            } else {
                me.expVariable = me.params[me.step].inputs.exposureVariableSelection.values[0];              
            }                  
            //currValue = me.params[me.step].inputs.exposureVariableSelection.values[0];     
            me.fieldStore.clearFilter(true);        
            me.fieldStore.filter('fieldClass', 'Treatment');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);         
            var outcomeDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');        
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.exposureVariableSelection.inputText,
                store: outcomeDropdownValues,
                itemId: 'expVariable',
                allowBlank: !me.params[me.step].inputs.exposureVariableSelection.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'value',
                valueField: 'value',
                queryMode: 'local',
                value: me.expVariable,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.expVariable = cmb.getValue();
                    }
                }
            });           
        }             

        me.fieldStore.clearFilter(true);        
        me.fieldStore.filter('fieldClass', 'Sequencer');
        me.fieldStore.filter('idModel', me.modelId);
        me.fieldStore.filter('insertable', true);          
        if ( typeof me.params[me.step].inputs.sequencingVariableSelection !== 'undefined' ) {     
            var index = me.selectedStudyRecord.get('idDate');
            var sequenceField = me.fieldStore.findRecord('idModelColumn', index);      
            if ( sequenceField === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Sequence Field is not a Sequencer class or does not exit.");
                me.destroy();    
            }
            var currValue = sequenceField.get("name");                       
            me.items[0].add( {
                    fieldLabel: me.params[me.step].inputs.sequencingVariableSelection.inputText,
                    name: 'seqVariable',
                    itemId: 'seqVariable',
                    allowBlank: !me.params[me.step].inputs.sequencingVariableSelection.required,                            
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,                       
                    value: currValue
                });   
        }
    
        var sequencerStore = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore,'name');
        var dropDownSequencerStart = new Ext.form.field.ComboBox({
            store: sequencerStore,
            displayField: 'value',
            valueField: 'value',
            queryMode: 'local',
            itemId: 'startVariable',
            multiSelect: false,
            forceSelection: false
        });          
        if ( typeof me.params[me.step].inputs.start.values[0] !== 'undefined' ) {
            //var startIndex = sequencerStore.find('value',me.params[me.step].inputs.start.values[0]);
            dropDownSequencerStart.setValue(me.params[me.step].inputs.start.values[0]);
        }        
        var dropDownSequencerStop = new Ext.form.field.ComboBox({
            store: sequencerStore,
            displayField: 'value',
            valueField: 'value',
            queryMode: 'local',
            itemId: 'stopVariable',            
            multiSelect: false,
            forceSelection: false
        });           
        if ( typeof me.params[me.step].inputs.stop.values[0] !== 'undefined' ) {
            //var stopIndex = sequencerStore.find('value',me.params[me.step].inputs.stop.values[0]);
            dropDownSequencerStop.setValue(me.params[me.step].inputs.stop.values[0]);
        }      
        if ( typeof me.params[me.step].inputs.start !== 'undefined' ) {        
            me.items[0].add({
                xtype: 'fieldcontainer',
                margin: '5 10 10 0',                
                fieldLabel: me.params[me.step].inputs.start.inputText,
                allowBlank: !me.params[me.step].inputs.start.required,                
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                                
                layout: 'hbox',
                items: [
                    dropDownSequencerStart
                ]
            });           
        }
        if ( typeof me.params[me.step].inputs.stop !== 'undefined' ) {        
            me.items[0].add({
                xtype: 'fieldcontainer',
                margin: '5 10 10 0',
                fieldLabel: me.params[me.step].inputs.stop.inputText,
                allowBlank: !me.params[me.step].inputs.stop.required,                    
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                                
                layout: 'hbox',
                items: [
                    dropDownSequencerStop
                ]
            });           
        }        
        if ( typeof me.params[me.step].inputs.independentVariableSelection !== 'undefined' ) {
            me.fieldStore.filter('fieldClass', 'Risk Factor');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);                 
            var riskFactorStore = delta3.utils.GlobalFunc.createLocalGridStore(me.fieldStore, delta3.model.IndependentVariableModel.getFields());
            me.items[0].add( 
               Ext.create('delta3.view.study.IndependentVariableSelector', {riskFactorStore: riskFactorStore, indepVariablesList: indepVariables})
            );        
        }     
        if ( typeof me.params[me.step].inputs.automaticVariableSelection !== 'undefined' ) {   
            var currValue = '';            
            if ( typeof me.params[me.step].inputs.automaticVariableSelection.values[0] !== 'undefined' ) {     
                currValue = me.params[me.step].inputs.automaticVariableSelection.values[0];
            }             
            me.items[0].add(delta3.view.study.UtilFunc.checkBoxUI(me.params[me.step].inputs.automaticVariableSelection, me, 'avs'));              
        }                      
        me.items[0].updateLayout();
        me.callParent();    
    }
}
);
 
