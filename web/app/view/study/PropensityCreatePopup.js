/* 
 * Propensity Create Method Popup Window
 */

Ext.define('delta3.view.study.PropensityCreatePopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.methodPropensityCreate',
    layout: 'fit',
    width: 520,
    height: 460,
    itemId: 'methodPropensityCreatePopup',
    requires: [
        'delta3.model.IndependentVariableModel',
        'delta3.view.study.IndependentVariableSelector'
    ],
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA',
    listeners: {
        beforedestroy: function(rowEditor, context, eOpt) {
            this.fieldStore.clearFilter(true);
        }
    },    
    labelWidth: 200,
    labelAlign: 'left',   
    step: 0,
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    depVariable: {},
    expVariable: {},
    params: {},
    fieldId: 0,
    newFieldId: {},
    modelId: {},
    studyId: {},
    items: [],
    buttons: [
        {
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodPropensityCreatePopup')[0];                
                var theIndepItems = Ext.ComponentQuery.query('#indepVariable')[0];        
                var listOfIndepVariables = theIndepItems.getRawValue();
                var arrayOfIndepVariables = listOfIndepVariables.split(",");              
                thisWin.params[thisWin.step].inputs.independentVariableSelection.values = [];              
                for (var i = 0; i < arrayOfIndepVariables.length; i++) {
                    thisWin.params[thisWin.step].inputs.independentVariableSelection.values[i] = arrayOfIndepVariables[i];
                }
                thisWin.fieldStore.clearFilter(true);    
                thisWin.params[thisWin.step].inputs.dependentVariableSelection.values[0] = thisWin.depVariable;                    
                thisWin.selectedStudyRecord.set('idOutcome', delta3.utils.GlobalFunc.lookupFieldId(thisWin.depVariable));               
                var newParams = JSON.stringify(thisWin.params);
                thisWin.selectedStudyRecord.set('methodParams', newParams);            
                var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                var rec = studyGrid.getStore().findRecord('idStudy', thisWin.studyId);      
                rec.set('status', 'New');                 
                studyGrid.getStore().sync();           
                studyGrid.setLoading();
                // Clear the loading mask after 1.5 seconds
                setTimeout(function (target) {
                    target.setLoading(false);
                }, 1500, studyGrid);                
            }
        }, {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodPropensityCreatePopup')[0];
                //thisWin.fieldStore.clearFilter(true);
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.fieldStore.clearFilter(true);   
        var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
        if ( me.modelId === 0 ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Data Model to the study first.");
            me.destroy();
        }           
        var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));
        if ( methodRec === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign a Method to the study first.");
            me.fieldStore.clearFilter(true);
            me.destroy();
        }              
        me.selectedFieldRecord = me.fieldStore.findRecord('idModelColumn', me.fieldId);
        var index = me.selectedStudyRecord.get('idKey');
        var uniqueField = me.fieldStore.findRecord('idModelColumn', index); 
        if ( uniqueField === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
            me.destroy();
        } 
           
        index = me.selectedStudyRecord.get('idDate');
        if ( me.fieldStore.findRecord('idModelColumn', index) === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Sequence Field to the study first.");
            me.destroy();            
        }

        var title = 'PA Formula Create parameters for study "' + me.selectedStudyRecord.get('name') + '"';
        var studyParams = me.selectedStudyRecord.get('methodParams');
        var indepVariables = '';
        
        if ( (typeof studyParams !== 'undefined') && (studyParams !== null) && (studyParams !== "") ) {
            // params previously saved for the study
            me.params = JSON.parse(studyParams); 
            for (var i = 0; i < me.params[me.step].inputs.independentVariableSelection.values.length; i++) {
                if ( i > 0 ) {
                    indepVariables += ',';
                }
                indepVariables += me.params[me.step].inputs.independentVariableSelection.values[i];
            }           
        } else {
            // empty params string from template
            me.params = JSON.parse(methodRec.data.methodParams);
        }

        me.items[0] = new Ext.Panel(
                {
                    frame: true,
                    labelWidth: 400,
                    labelAlign: 'right',                    
                    itemId: 'methodPropCreatePanel',
                    bodyStyle: 'padding:5px 5px 0',
                    title: title,
                    autoScroll: true,
                    defaultType: 'displayfield'
                }); 

        if ( typeof me.params[me.step].inputs.studyUniqueId !== 'undefined' ) {
            me.items[0].add({
                allowBlank: !me.params[me.step].inputs.studyUniqueId.required,                 
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                fieldLabel: me.params[me.step].inputs.studyUniqueId.inputText,
                itemId: 'studyId',
                value: uniqueField.get("name")
            });
        }        
        if ( typeof me.params[me.step].inputs.dependentVariableSelection !== 'undefined' ) {
            if ( JSON.stringify(me.params[me.step].inputs.dependentVariableSelection.values[0]) === "{}" || me.params[me.step].inputs.dependentVariableSelection.values[0] === 'value0' ) {
                me.depVariable = '';
            } else {
                me.depVariable = me.params[me.step].inputs.dependentVariableSelection.values[0]; 
            }   
            me.fieldStore.clearFilter(true);        
            me.fieldStore.filter('fieldClass', 'Treatment');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);         
            var outcomeDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');        
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.dependentVariableSelection.inputText,
                store: outcomeDropdownValues,
                itemId: 'depVariable',
                allowBlank: !me.params[me.step].inputs.dependentVariableSelection.required,                 
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'value',
                valueField: 'value',
                queryMode: 'local',
                value: me.depVariable,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.depVariable = cmb.getValue();
                    }
                }
            });            
        }         
        me.fieldStore.clearFilter(true);        
        me.fieldStore.filter('fieldClass', 'Sequencer');
        me.fieldStore.filter('idModel', me.modelId);
        me.fieldStore.filter('insertable', true);          
        if ( typeof me.params[me.step].inputs.sequencingVariableSelection !== 'undefined' ) {     
            var index = me.selectedStudyRecord.get('idDate');
            var sequenceField = me.fieldStore.findRecord('idModelColumn', index);      
            if ( sequenceField === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Sequence Field is not a Sequencer class or does not exit.");
                me.destroy();    
            }
            var currValue = sequenceField.get("name");                       
            me.items[0].add( {
                    fieldLabel: me.params[me.step].inputs.sequencingVariableSelection.inputText,
                    name: 'seqVariable',
                    itemId: 'seqVariable',
                    allowBlank: !me.params[me.step].inputs.sequencingVariableSelection.required,                     
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,                       
                    value: currValue
                });   
        }       
        if ( typeof me.params[me.step].inputs.independentVariableSelection !== 'undefined' ) {
            me.fieldStore.filter('fieldClass', 'Risk Factor');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);                 
            var riskFactorStore = delta3.utils.GlobalFunc.createLocalGridStore(me.fieldStore, delta3.model.IndependentVariableModel.getFields());
            me.items[0].add( 
               Ext.create('delta3.view.study.IndependentVariableSelector', {riskFactorStore: riskFactorStore, indepVariablesList: indepVariables})
            );        
        }        
        me.items[0].updateLayout();
        me.callParent();    
    }
}
);

