/* 
 * Study Container
 */

Ext.define('delta3.view.study.StudyContainer', {
    extend: 'Ext.container.Container',
    itemId: 'studyContainer',
    alias:  'widget.container.studies',
    layout: 'fit',
//--------v3.64 added below code for resolution of JIRA DELQA-735
//height: 800,
    height: delta3.utils.GlobalVars.gridMarginHeight+delta3.utils.GlobalVars.gridRowHeight*delta3.utils.GlobalVars.pageSize,     
//    layout: {    
//        scrollable: true,
//        align: 'stretch',
//        type: 'hbox'
//    },
    renderTo: Ext.getBody(),   

    listeners: {
        activate: function(panel, eOpt) {
            delta3.utils.GlobalVars.FieldStore.load();    
        }
    },     
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.study',
                region: 'center'
        },   
        me.callParent();
    }
});

