/* 
 * Propensity Apply Method Popup Window - this has not been tested
 */

Ext.define('delta3.view.study.PropensityApplyPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.methodPropensityApply',
    layout: 'fit',
    width: 520,
    height: 560,
    itemId: 'methodPropensityApplyPopup',
    requires: [
        'delta3.model.IndependentVariableModel',
        'delta3.view.study.IndependentVariableSelector'
    ],
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA',
    listeners: {
        beforedestroy: function(rowEditor, context, eOpt) {
            this.fieldStore.clearFilter(true);
        }
    },    
    labelWidth: 200,
    labelAlign: 'left',   
    step: 0,
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    depVariable: {},
    expVariable: {},
    idLogregr: '',
    nameLogregr: '',   
    logregrStore: {},    
    params: {},
    fieldId: 0,
    newFieldId: {},
    modelId: {},
    studyId: {},
    items: [],
    buttons: [
        {
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodPropensityApplyPopup')[0];             
                thisWin.params[thisWin.step].inputs.exposureVariableSelection.values = [];              
                var index = thisWin.selectedStudyRecord.get('idKey');
                thisWin.fieldStore.clearFilter(true);
                thisWin.params[thisWin.step].inputs.sequencingVariableSelection.values[0] = Ext.ComponentQuery.query('#seqVariable')[0].value;
                thisWin.params[thisWin.step].inputs.reportingPeriod.values[0] = Ext.ComponentQuery.query('#reportingPeriod')[0].value;             

                thisWin.params[thisWin.step].inputs.dataSplitOption.values[0] = Ext.ComponentQuery.query('#dataSplitOption')[0].value;  
                
                thisWin.params[thisWin.step].inputs.calipers.values[0] = Ext.ComponentQuery.query('#calipers')[0].value;
                thisWin.params[thisWin.step].inputs.caliperType.values[0] = Ext.ComponentQuery.query('#caliperType')[0].value;                                

                thisWin.params[thisWin.step].inputs.matchNumber.values[0] = Ext.ComponentQuery.query('#matchNumber')[0].value;
                thisWin.params[thisWin.step].inputs.matchType.values[0] = Ext.ComponentQuery.query('#matchType')[0].value;  
                thisWin.params[thisWin.step].inputs.matchOrder.values[0] = Ext.ComponentQuery.query('#matchOrder')[0].value;  

                thisWin.params[thisWin.step].inputs.alphaError.values[0] = Ext.ComponentQuery.query('#alphaError')[0].value;
                thisWin.params[thisWin.step].inputs.alphaSpending.values[0] = Ext.ComponentQuery.query('#alphaSpending')[0].value;
               
                thisWin.params[thisWin.step].inputs.dependentVariableSelection.values[0] = thisWin.depVariable;                    
                thisWin.selectedStudyRecord.set('idOutcome', delta3.utils.GlobalFunc.lookupFieldId(thisWin.depVariable));      
                 thisWin.params[thisWin.step].inputs.exposureVariableSelection.values[0] = thisWin.expVariable;                    
                thisWin.selectedStudyRecord.set('idTreatment', delta3.utils.GlobalFunc.lookupFieldId(thisWin.expVariable));         
                for (var i=0; i<thisWin.logregrStore.length; i++) {
                    if ( thisWin.logregrStore[i].model.id === thisWin.idLogregr ) {
                        thisWin.params[thisWin.step].inputs.model = thisWin.logregrStore[i].model;      
                        break;
                    }
                }                
                var newParams = JSON.stringify(thisWin.params);
                thisWin.selectedStudyRecord.set('methodParams', newParams);            
                var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                var rec = studyGrid.getStore().findRecord('idStudy', thisWin.studyId);      
                rec.set('status', 'New');                 
                studyGrid.getStore().sync();              
                studyGrid.setLoading();
                // Clear the loading mask after 1.5 seconds
                setTimeout(function (target) {
                    target.setLoading(false);
                }, 1500, studyGrid);                
            }
        }, {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodPropensityApplyPopup')[0];
                //thisWin.fieldStore.clearFilter(true);
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.fieldStore.clearFilter(true);   
        var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
        if ( me.modelId === 0 ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Data Model to the study first.");
            me.destroy();
        }           
        var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));
        if ( methodRec === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign a Method to the study first.");
            me.fieldStore.clearFilter(true);
            me.destroy();
        }              
        me.selectedFieldRecord = me.fieldStore.findRecord('idModelColumn', me.fieldId);
        var index = me.selectedStudyRecord.get('idKey');
        var uniqueField = me.fieldStore.findRecord('idModelColumn', index); 
        if ( uniqueField === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
            me.destroy();
        } 
           
        index = me.selectedStudyRecord.get('idDate');
        if ( me.fieldStore.findRecord('idModelColumn', index) === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Sequence Field to the study first.");
            me.destroy();            
        }

        var title = 'PA Apply parameters for study "' + me.selectedStudyRecord.get('name') + '"';
        var studyParams = me.selectedStudyRecord.get('methodParams');
        var indepVariables = '';
        
        if ( (typeof studyParams !== 'undefined') && (studyParams !== null) && (studyParams !== "") ) {
            // params previously saved for the study
            me.params = JSON.parse(studyParams); 
            me.nameLogregr = me.params[me.step].inputs.model.name;
            me.idLogregr = me.params[me.step].inputs.model.id;            
        } else {
            // empty params string from template
            me.params = JSON.parse(methodRec.data.methodParams);
        }

        me.items[0] = new Ext.Panel(
                {
                    frame: true,
                    labelWidth: 400,
                    labelAlign: 'right',                    
                    itemId: 'methodPropStep2Panel',
                    bodyStyle: 'padding:5px 5px 0',
                    title: title,
                    autoScroll: true,
                    defaultType: 'displayfield'
                }); 

        if ( typeof me.params[me.step].inputs.studyUniqueId !== 'undefined' ) {
            me.items[0].add({
                allowBlank: !me.params[me.step].inputs.studyUniqueId.required,                 
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                fieldLabel: me.params[me.step].inputs.studyUniqueId.inputText,
                itemId: 'studyId',
                allowBlank: true,
                value: uniqueField.get("name")
            });
        }        
        if ( typeof me.params[me.step].inputs.dependentVariableSelection !== 'undefined' ) {
            if ( JSON.stringify(me.params[me.step].inputs.dependentVariableSelection.values[0]) === "{}" || me.params[me.step].inputs.dependentVariableSelection.values[0] === 'value0' ) {
                me.depVariable = '';
            } else {
                me.depVariable = me.params[me.step].inputs.dependentVariableSelection.values[0]; 
            }   
            me.fieldStore.clearFilter(true);        
            me.fieldStore.filter('fieldClass', 'Outcome');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);         
            var outcomeDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');        
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.dependentVariableSelection.inputText,
                store: outcomeDropdownValues,
                itemId: 'depVariable',
                allowBlank: !me.params[me.step].inputs.dependentVariableSelection.required,                 
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'value',
                valueField: 'value',
                queryMode: 'local',
                value: me.depVariable,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.depVariable = cmb.getValue();
                    }
                }
            });            
        } 
        if ( typeof me.params[me.step].inputs.exposureVariableSelection !== 'undefined' ) {      
            if ( JSON.stringify(me.params[me.step].inputs.exposureVariableSelection.values[0]) === "{}" || me.params[me.step].inputs.exposureVariableSelection.values[0] === 'value0' ) {
                me.expVariable = '';
            } else {
                me.expVariable = me.params[me.step].inputs.exposureVariableSelection.values[0]; 
            }       
            me.fieldStore.clearFilter(true);        
            me.fieldStore.filter('fieldClass', 'Treatment');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);         
            var outcomeDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');        
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.exposureVariableSelection.inputText,
                store: outcomeDropdownValues,
                itemId: 'expVariable',
                allowBlank: !me.params[me.step].inputs.exposureVariableSelection.required,                      
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'value',
                valueField: 'value',
                queryMode: 'local',
                value: me.expVariable,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.expVariable = cmb.getValue();
                    }
                }
            });                  
        }         
        
        me.fieldStore.clearFilter(true);        
        me.fieldStore.filter('fieldClass', 'Sequencer');
        me.fieldStore.filter('idModel', me.modelId);
        me.fieldStore.filter('insertable', true);          
        if ( typeof me.params[me.step].inputs.sequencingVariableSelection !== 'undefined' ) {     
            var index = me.selectedStudyRecord.get('idDate');
            var sequenceField = me.fieldStore.findRecord('idModelColumn', index);      
            if ( sequenceField === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Sequence Field is not a Sequencer class or does not exit.");
                me.destroy();    
            }
            var currValue = sequenceField.get("name");                       
            me.items[0].add( {
                    fieldLabel: me.params[me.step].inputs.sequencingVariableSelection.inputText,
                    name: 'seqVariable',
                    itemId: 'seqVariable',
                    allowBlank: !me.params[me.step].inputs.sequencingVariableSelection.required,                            
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,                       
                    value: currValue
                });   
        }       
        if ( typeof me.params[me.step].inputs.alphaError !== 'undefined' ) { 
            me.items[0].add(delta3.view.study.UtilFunc.textUI(me.params[me.step].inputs.alphaError, me, 'alphaError'));                    
        }
        if ( typeof me.params[me.step].inputs.alphaSpending !== 'undefined' ) {    
            me.items[0].add(delta3.view.study.UtilFunc.checkBoxUI(me.params[me.step].inputs.alphaSpending, me, 'alphaSpending'));                    
        }          
        if ( typeof me.params[me.step].inputs.reportingPeriod !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params[me.step].inputs.reportingPeriod.values[0] !== 'undefined') && (me.params[me.step].inputs.reportingPeriod.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.reportingPeriod.values[0];
            }        
            if ( (typeof me.params[me.step].inputs.reportingPeriod.type !== 'undefined') && (me.params[me.step].inputs.reportingPeriod.type === "MultipleUnique") ) {
                var values = me.params[me.step].inputs.reportingPeriod.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {period: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['period'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.reportingPeriod.inputText,
                store: dropdownValues,
                itemId: 'reportingPeriod',
                allowBlank: !me.params[me.step].inputs.reportingPeriod.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'period',
                valueField: 'period',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        }   
        if ( typeof me.params[me.step].inputs.dataSplitOption !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params[me.step].inputs.dataSplitOption.values[0] !== 'undefined') && (me.params[me.step].inputs.dataSplitOption.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.dataSplitOption.values[0];
            }        
            if ( (typeof me.params[me.step].inputs.dataSplitOption.type !== 'undefined') && (me.params[me.step].inputs.dataSplitOption.type === "MultipleUnique") ) {
                var values = me.params[me.step].inputs.dataSplitOption.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {period: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['period'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.dataSplitOption.inputText,
                store: dropdownValues,
                itemId: 'dataSplitOption',
                allowBlank: !me.params[me.step].inputs.dataSplitOption.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'period',
                valueField: 'period',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        }         
        if ( typeof me.params[me.step].inputs.calipers !== 'undefined' ) {                
            var currValue = '';            
            if ( (typeof me.params[me.step].inputs.calipers.values[0] !== 'undefined') && (me.params[me.step].inputs.calipers.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.calipers.values[0];
            }             
            me.items[0].add({
                xtype: 'textfield',
                fieldLabel: me.params[me.step].inputs.calipers.inputText,
                itemId: 'calipers',
                allowBlank: !me.params[me.step].inputs.calipers.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: currValue
            });
        }        
        if ( typeof me.params[me.step].inputs.caliperType !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params[me.step].inputs.caliperType.values[0] !== 'undefined') && (me.params[me.step].inputs.caliperType.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.caliperType.values[0];
            }        
            if ( (typeof me.params[me.step].inputs.caliperType.type !== 'undefined') && (me.params[me.step].inputs.caliperType.type === "MultipleUnique") ) {
                var values = me.params[me.step].inputs.caliperType.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {availableValues: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['availableValues'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.caliperType.inputText,
                store: dropdownValues,
                itemId: 'caliperType',
                allowBlank: !me.params[me.step].inputs.caliperType.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'availableValues',
                valueField: 'availableValues',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        } 
        if ( typeof me.params[me.step].inputs.matchNumber !== 'undefined' ) {
            var currValue = '';            
            if ( (typeof me.params[me.step].inputs.matchNumber.values[0] !== 'undefined') && (me.params[me.step].inputs.matchNumber.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.matchNumber.values[0];
            }               
            me.items[0].add({
                xtype: 'textfield',
                fieldLabel: me.params[me.step].inputs.matchNumber.inputText,
                itemId: 'matchNumber',
                  allowBlank: !me.params[me.step].inputs.matchNumber.required,                      
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: currValue,
                allowBlank: true
            });
        }         
        if ( typeof me.params[me.step].inputs.matchType !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params[me.step].inputs.matchType.values[0] !== 'undefined') && (me.params[me.step].inputs.matchType.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.matchType.values[0];
            }        
            if ( (typeof me.params[me.step].inputs.matchType.type !== 'undefined') && (me.params[me.step].inputs.matchType.type === "MultipleUnique") ) {
                var values = me.params[me.step].inputs.matchType.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {availableValues: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['availableValues'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.matchType.inputText,
                store: dropdownValues,
                itemId: 'matchType',
                allowBlank: !me.params[me.step].inputs.matchType.required,                      
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'availableValues',
                valueField: 'availableValues',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        }        
        if ( typeof me.params[me.step].inputs.matchOrder !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params[me.step].inputs.matchOrder.values[0] !== 'undefined') && (me.params[me.step].inputs.matchOrder.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.matchOrder.values[0];
            }        
            if ( (typeof me.params[me.step].inputs.matchOrder.type !== 'undefined') && (me.params[me.step].inputs.matchOrder.type === "MultipleUnique") ) {
                var values = me.params[me.step].inputs.matchOrder.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {availableValues: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['availableValues'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.matchOrder.inputText,
                store: dropdownValues,
                itemId: 'matchOrder',
                allowBlank: !me.params[me.step].inputs.matchOrder.required,                      
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'availableValues',
                valueField: 'availableValues',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        }   
        me.items[0].add({
            xtype: 'combobox',
            fieldLabel: 'LR Formula',
            store: {},
            itemId: 'lrFormula',
            allowBlank: false,            
            labelWidth: me.labelWidth,
            labelAlign: me.labelAlign,    
            width: me.componentWidth,                
            displayField: 'name',
            valueField: 'idLogregr',
            queryMode: 'local',
            value: {},
            multiSelect: false,
            forceSelection: false,
            listeners: {
                'select': function(cmb, rec, idx) {
                    me.idLogregr = cmb.getValue();
                }
            }
        });         
        delta3.utils.GlobalFunc.getLRFforModel(me.modelId, finalDisplay);
        function finalDisplay(resp, options) {
            var values = [];
            for ( var i=0; i<resp.length; i++) {
                values[i] = {idLogregr: resp[i].model.id, name: resp[i].model.name};
            }
            var cbStore =  Ext.create('Ext.data.Store', {
                fields: ['idLogregr', 'name'], 
                data: values 
            });
            me.logregrStore = resp;
            var lrComboBox = Ext.ComponentQuery.query('#lrFormula')[0];
            lrComboBox.setStore(cbStore);
            lrComboBox.setValue(me.nameLogregr);                      
            me.items.items[0].updateLayout();            
        }        
        me.items[0].updateLayout();
        me.callParent();    
    }
}
);

