/* 
 * Logistic Regression two steps Combined Method Popup Window
 */

Ext.define('delta3.view.study.LRApplyPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.LRApply',  
    layout: 'fit',
    width: 540,
    height: 450,
    itemId: 'LRApplyPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA',
    listeners: {
        beforedestroy: function(rowEditor, context, eOpt) {
            this.fieldStore.clearFilter(true);
        }
    },    
    labelWidth: 160,
    componentWidth: 476,
    labelAlign: 'left',   
    startDate: {},
    endDate: {},
    step: 0,
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    params: {},
    fieldId: 0,
    depVariable: {},
    idLogregr: '',
    nameLogregr: '',
    logregrStore: {},
    expVariable: {},
    modelId: {},
    studyId: {},
    items: [],
    buttons: [
        {
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#LRApplyPopup')[0];                       
//                var genericIdCheckbox = Ext.ComponentQuery.query('#genericIdCheckbox')[0];       
                         
                var index = thisWin.selectedStudyRecord.get('idKey');
                thisWin.fieldStore.clearFilter(true);                                
                var uniqueField = thisWin.fieldStore.findRecord('idModelColumn', index);
//                if ( genericIdCheckbox.value === true ) {
//                    thisWin.params[thisWin.step].inputs.studyUniqueId.values[0] = 'id';
//                } else {
                    thisWin.params[thisWin.step].inputs.studyUniqueId.values[0] = uniqueField.get("name");
//                }
//                thisWin.selectedStudyRecord.set('isGenericId', genericIdCheckbox.value);  
                                 
                thisWin.params[thisWin.step].inputs.sequencingVariableSelection.values[0] = Ext.ComponentQuery.query('#seqVariable')[0].value;
                thisWin.params[thisWin.step].inputs.reportingPeriod.values[0] = Ext.ComponentQuery.query('#reportingPeriod')[0].value;             
                thisWin.params[thisWin.step].inputs.studyStartDate.values[0] = Ext.ComponentQuery.query('#studyStartDate')[0].value;
                thisWin.params[thisWin.step].inputs.studyEndDate.values[0] = Ext.ComponentQuery.query('#studyEndDate')[0].value;  
                thisWin.params[thisWin.step].inputs.alphaError.values[0] = Ext.ComponentQuery.query('#alphaError')[0].value;
                thisWin.params[thisWin.step].inputs.alphaSpending.values[0] = Ext.ComponentQuery.query('#alphaSpending')[0].value;            
                if ( typeof Ext.ComponentQuery.query('#avs')[0] !== 'undefined' ) {
                    thisWin.params[thisWin.step].inputs.automaticVariableSelection.values[0] = Ext.ComponentQuery.query('#avs')[0].value;  
                }                                                      
                thisWin.params[thisWin.step].inputs.dependentVariableSelection.values[0] = thisWin.depVariable;                                  
                thisWin.selectedStudyRecord.set('idOutcome', delta3.utils.GlobalFunc.lookupFieldId(thisWin.depVariable));  
                thisWin.params[thisWin.step].inputs.exposureVariableSelection.values[0] = thisWin.expVariable;                    
                thisWin.selectedStudyRecord.set('idTreatment', delta3.utils.GlobalFunc.lookupFieldId(thisWin.expVariable));   
                for (var i=0; i<thisWin.logregrStore.length; i++) {
                    if ( thisWin.logregrStore[i].model.id === thisWin.idLogregr ) {
                        thisWin.params[thisWin.step].inputs.model = thisWin.logregrStore[i].model;      
                        break;
                    }
                }
                var newParams = JSON.stringify(thisWin.params);
                thisWin.selectedStudyRecord.set('methodParams', newParams);            
                var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                var rec = studyGrid.getStore().findRecord('idStudy', thisWin.studyId);      
                rec.set('status', 'New');               
                studyGrid.getStore().sync();              
                studyGrid.setLoading();
                // Clear the loading mask after 1.5 seconds
                setTimeout(function (target) {
                    target.setLoading(false);
                }, 1500, studyGrid);                
            }
        }, {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#LRApplyPopup')[0];
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.fieldStore.clearFilter(true);   
        var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
        if ( me.modelId === 0 ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Data Model to the study first.");
            me.destroy();
        }           
        var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));
        if ( methodRec === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign a Method to the study first.");
            me.fieldStore.clearFilter(true);
            me.destroy();
        }        
      
        me.selectedFieldRecord = me.fieldStore.findRecord('idModelColumn', me.fieldId);
        var index = me.selectedStudyRecord.get('idKey');
        var uniqueField = me.fieldStore.findRecord('idModelColumn', index); 
        if ( uniqueField === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
            me.destroy();
        } 
           
        index = me.selectedStudyRecord.get('idDate');
        if ( me.fieldStore.findRecord('idModelColumn', index) === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Sequence Field to the study first.");
            me.destroy();            
        }
        
        var title = 'LR parameters for study "' + me.selectedStudyRecord.get('name') + '"';
        var studyParams = me.selectedStudyRecord.get('methodParams');
        
        if ( (typeof studyParams !== 'undefined') && (studyParams !== null) && (studyParams !== "") ) {
            // params previously saved for the study
            me.params = JSON.parse(studyParams); 
            me.nameLogregr = me.params[me.step].inputs.model.name;
            me.idLogregr = me.params[me.step].inputs.model.id;            
         
        } else {
            // empty params string from template
            me.params = JSON.parse(methodRec.data.methodParams);
        }

        me.items[0] = new Ext.Panel(
                {
                    frame: true,               
                    itemId: 'methodPropStep2Panel',
                    bodyStyle: 'padding:5px 5px 0',
                    title: title,
                    autoScroll: true,
                    defaultType: 'displayfield'
                }); 

        if ( typeof me.params[me.step].inputs.studyUniqueId !== 'undefined' ) {
            me.items[0].add({
                allowBlank: !me.params[me.step].inputs.studyUniqueId.required,                     
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,      
                width: me.componentWidth,                
                fieldLabel: me.params[me.step].inputs.studyUniqueId.inputText,
                itemId: 'studyId',
                value: uniqueField.get("name")
            });
//            me.items[0].add({
//                labelWidth: me.labelWidth,
//                labelAlign: me.labelAlign,     
//                width: me.componentWidth,                
//                xtype: 'checkbox',
//                fieldLabel: 'Use generic Id',
//                inputValue: true,
//                value: me.selectedStudyRecord.get('isGenericId'),
//                itemId: 'genericIdCheckbox'                           
//            });
        }        
        if ( typeof me.params[me.step].inputs.dependentVariableSelection !== 'undefined' ) {
            if ( JSON.stringify(me.params[me.step].inputs.dependentVariableSelection.values[0]) === "{}" || me.params[me.step].inputs.dependentVariableSelection.values[0] === 'value0' ) {
                me.depVariable = '';
            } else {
                me.depVariable = me.params[me.step].inputs.dependentVariableSelection.values[0]; 
            }               
            me.fieldStore.filter('fieldClass', 'Outcome');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);         
            var outcomeDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');        
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.dependentVariableSelection.inputText,
                store: outcomeDropdownValues,
                itemId: 'depVariable',
                allowBlank: !me.params[me.step].inputs.dependentVariableSelection.required,                 
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,      
                width: me.componentWidth,
                displayField: 'value',
                valueField: 'value',
                queryMode: 'local',
                value: me.depVariable,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.depVariable = cmb.getValue();
                    }
                }
            });            
        } 
        if ( typeof me.params[me.step].inputs.exposureVariableSelection !== 'undefined' ) {     
            if ( JSON.stringify(me.params[me.step].inputs.exposureVariableSelection.values[0]) === "{}" || me.params[me.step].inputs.exposureVariableSelection.values[0] === 'value0' ) {
                me.expVariable = '';
            } else {
                me.expVariable = me.params[me.step].inputs.exposureVariableSelection.values[0]; 
            }       
            me.fieldStore.clearFilter(true);        
            me.fieldStore.filter('fieldClass', 'Treatment');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);         
            var outcomeDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');        
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.exposureVariableSelection.inputText,
                store: outcomeDropdownValues,
                itemId: 'expVariable',
                allowBlank: !me.params[me.step].inputs.exposureVariableSelection.required,                 
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,      
                width: me.componentWidth,                
                displayField: 'value',
                valueField: 'value',
                queryMode: 'local',
                value: me.expVariable,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.expVariable = cmb.getValue();
                    }
                }
            });                  
        }              
        if ( typeof me.params[me.step].inputs.studyStartDate !== 'undefined' ) {     
            if ( (typeof me.params[me.step].inputs.studyStartDate.values[0] !== 'undefined') 
                    && (me.params[me.step].inputs.studyStartDate.values[0] !== 'Y-m-d H:i:s.u') ) {     
                me.startDate = new Date(me.params[me.step].inputs.studyStartDate.values[0]);
            } else {
                me.startDate = me.selectedStudyRecord.get('startTS');
                if ( me.startDate === null ) {
                    delta3.utils.GlobalFunc.showDeltaMessage("Study Start Date is not defined.");
                    me.destroy();    
                }                  
            }             
            me.items[0].add(delta3.view.study.UtilFunc.dateUI(me.params[me.step].inputs.studyStartDate, me, 'studyStartDate', me.selectedStudyRecord.get('startTS'), true));        
        } else {
            delta3.utils.GlobalFunc.showDeltaMessage("Please clear Study parameters first.");
            me.destroy();   
        }       
        if ( typeof me.params[me.step].inputs.studyEndDate !== 'undefined' ) {
            if ( (typeof me.params[me.step].inputs.studyEndDate.values[0] !== 'undefined') 
                    && (me.params[me.step].inputs.studyEndDate.values[0] !== 'Y-m-d H:i:s.u') ) {     
                me.endDate = new Date(me.params[me.step].inputs.studyEndDate.values[0]);
            } else {
                me.endDate = me.selectedStudyRecord.get('endTS');
                if ( me.endDate === null ) {
                    delta3.utils.GlobalFunc.showDeltaMessage("Study End Date is not defined.");
                    me.destroy();    
                }                   
            }     
            me.items[0].add(delta3.view.study.UtilFunc.dateUI(me.params[me.step].inputs.studyEndDate, me, 'studyEndDate', me.selectedStudyRecord.get('endTS'), true));
        } else {
            delta3.utils.GlobalFunc.showDeltaMessage("Please clear Study parameters first.");
            me.destroy();   
        }             
        me.fieldStore.clearFilter(true);        
        me.fieldStore.filter('fieldClass', 'Sequencer');
        me.fieldStore.filter('idModel', me.modelId);
        me.fieldStore.filter('insertable', true);          
        if ( typeof me.params[me.step].inputs.sequencingVariableSelection !== 'undefined' ) {     
            var index = me.selectedStudyRecord.get('idDate');
            var sequenceField = me.fieldStore.findRecord('idModelColumn', index);      
            if ( sequenceField === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Sequence Field is not a Sequencer class or does not exit.");
                me.destroy();    
            }
            var currValue = sequenceField.get("name");                       
            me.items[0].add( {
                    fieldLabel: me.params[me.step].inputs.sequencingVariableSelection.inputText,
                    name: 'seqVariable',
                    itemId: 'seqVariable',
                    allowBlank: !me.params[me.step].inputs.sequencingVariableSelection.required,                     
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,        
                    width: me.componentWidth,                    
                    value: currValue
                });   
        }
        if ( typeof me.params[me.step].inputs.alphaError !== 'undefined' ) { 
            me.items[0].add(delta3.view.study.UtilFunc.textUI(me.params[me.step].inputs.alphaError, me, 'alphaError'));                    
        }
        if ( typeof me.params[me.step].inputs.alphaSpending !== 'undefined' ) {    
            me.items[0].add(delta3.view.study.UtilFunc.checkBoxUI(me.params[me.step].inputs.alphaSpending, me, 'alphaSpending'));                    
        }  
        if ( typeof me.params[me.step].inputs.reportingPeriod !== 'undefined' ) {    
            me.items[0].add(delta3.view.study.UtilFunc.multipleChoiceUI(me.params[me.step].inputs.reportingPeriod, 'reportingPeriod', me));
        }    
        me.items[0].add({
            xtype: 'combobox',
            fieldLabel: 'LR Formula',
            store: {},
            itemId: 'lrFormula',
            allowBlank: false,
            labelWidth: me.labelWidth,
            labelAlign: me.labelAlign,    
            width: me.componentWidth,                
            displayField: 'name',
            valueField: 'idLogregr',
            queryMode: 'local',
            value: {},
            multiSelect: false,
            forceSelection: false,
            listeners: {
                'select': function(cmb, rec, idx) {
                    me.idLogregr = cmb.getValue();
                }
            }
        });         
        delta3.utils.GlobalFunc.getLRFforModel(me.modelId, finalDisplay);
        function finalDisplay(resp, options) {
            var values = [];
            for ( var i=0; i<resp.length; i++) {
                values[i] = {idLogregr: resp[i].model.id, name: resp[i].model.name};
            }
            var cbStore =  Ext.create('Ext.data.Store', {
                fields: ['idLogregr', 'name'], 
                data: values 
            });
            me.logregrStore = resp;
            var lrComboBox = Ext.ComponentQuery.query('#lrFormula')[0];
            lrComboBox.setStore(cbStore);
            lrComboBox.setValue(me.nameLogregr);                      
            me.items.items[0].updateLayout();            
        }
        me.items[0].updateLayout();        
        me.callParent();    
    }
}
);
 
