/* 
 * Stat Method Parameter Popup Window
 */

Ext.define('delta3.view.study.popup.MethodParamPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.methodParam',
    layout: 'fit',
    width: 555,
    controlWidth: 464,
    height: 670,
    heightDefault: 230, // used in field container 1 to calculated top margin
    itemId: 'methodParamPopup',
    panelTitle: 'Statistical Parameters',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA Study Statistical Configuration',
    listeners: {
        beforedestroy: function(rowEditor, context, eOpt) {
            this.fieldStore.clearFilter(true);
        }
    },    
    labelWidth: 224,
    labelAlign: 'left',   
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    params: {},
    items: [{
                xtype: 'tabpanel',
                itemId: 'paramPopupTab',
                collapsible: false,
                region: 'center',
                layout: 'fit',              
                defaults: {autoScroll: true},
                viewModel: {
                    data: {
                        depVariable: ''
                    }
                },                
                items: []
            }],
    buttons: [
        {
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodParamPopup')[0];
                
                
                
                for (var i=0; i<thisWin.params.length; i++) {
                    var message = delta3.view.study.UtilFunc.retrieveParams(thisWin, thisWin.params[i]);
                    if ( message !== '') {
                        delta3.utils.GlobalFunc.showDeltaMessage(message);
                        return;
                    }        
                }
                
                
                var newParams = JSON.stringify(thisWin.params);
                thisWin.selectedStudyRecord.set('methodParams', newParams);   
                
                var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                var rec = studyGrid.getStore().findRecord('idStudy', thisWin.studyId);
                rec.set('status', 'New');                 
                studyGrid.getStore().sync();   
                
                
                studyGrid.setLoading();
                // Clear the loading mask after 1.5 seconds
                setTimeout(function (target) {
                    target.setLoading(false);
                }, 1500, studyGrid);                
            }
        }, {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodParamPopup')[0];
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.fieldStore.clearFilter(true);   
        
        
        
        var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
        var methodRec = delta3.view.study.UtilFunc.checkIfBaseParamsPresent(me, methodComboBox);

        if ( typeof methodRec.data.description !== 'undefined' ) {
            me.panelTitle = methodRec.data.description;
        } 
        me.panelTitle += ' for "' + me.selectedStudyRecord.get('name') + '"';
        
        var studyParams = me.selectedStudyRecord.get('methodParams');
        console.log("This is studyparam method"+studyParams);
        
        
        if ( (typeof studyParams !== 'undefined') && (studyParams !== null) && (studyParams !== "") ) {
            // params previously saved for the study
            me.params = JSON.parse(studyParams);    
            console.log("This is studyparam me.params"+me.params);
            
        } else {
            // empty params string from template
            me.params = JSON.parse(methodRec.data.methodParams);
            console.log("This is studyparam me.params"+me.params);
        }
        
        
        
        
        
        me.callParent();
        var theTabs = Ext.ComponentQuery.query('#paramPopupTab')[0];
        for (var i=0; i<me.params.length; i++) {
            var panelTitle = '';
            if ( typeof me.params[i].shortName !== 'undefined' ) {
                panelTitle = me.params[i].shortName;
            } else {
                panelTitle = me.panelTitle;
            }
            var panel = new Ext.Panel(
                {
                    frame: true,
                    layout: 'hbox',
                    labelWidth: 400,
                    labelAlign: 'right',                    
                    itemId: 'methodPropStepPanel' + i,
                    bodyStyle: 'padding:5px 5px 0',
                    title: panelTitle,
                    autoScroll: true,
                    defaultType: 'displayfield'
                });                
            var fieldCont1 = Ext.create({
                xtype: 'fieldcontainer',        
                margin: "10 40 10 10",       // top, right, bottom, left                   
                layout: 'vbox'});
            
            
            var fieldCont2 = Ext.create({
                xtype: 'fieldcontainer',         
                margin: "32 40 10 10",       // top, right, bottom, left                   
                layout: 'vbox'});            
            
            panel.add(fieldCont1);
            panel.add(fieldCont2);
            theTabs.add(panel);       
            
            var height = delta3.view.study.UtilFunc.drawStepUI(me.params[i], me, fieldCont1, fieldCont2);
            if ( height > me.height ) {
                me.height = height;
            }
            panel.updateLayout();
        }      
        theTabs.setActiveTab(0);           
        me.updateLayout();
        me.callParent();    
    }
}
);

