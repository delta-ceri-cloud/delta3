/* 
 * Logistic Regression Param Container
 * Coping Sysytems, Inc
 */

Ext.define('delta3.view.study.LogisticRegrContainer', {
    extend: 'Ext.Window',
    itemId: 'logisticRegrContainer',
    alias: 'widget.container.logisticRegr',
    requires: [
        'Ext.app.ViewModel',
        'delta3.view.study.LogisticRegrPopup1',  
        'delta3.view.study.LogisticRegrPopup2'
    ],
    layout: 'fit',
    width: 500,
    height: 510,
    modal: true,    
    closeAction: 'destroy',
    title: 'DELTA',  
    tab1Saved: false,
    tab2Saved: false,
    listeners: {
        beforedestroy: function(rowEditor, context, eOpt) {
            this.fieldStore.clearFilter(true);
        }
    },    
    items: [{
                xtype: 'tabpanel',
                itemId: 'regrPopupTab',
                collapsible: false,
                region: 'center',
                layout: 'fit',              
                defaults: {autoScroll: true},
                viewModel: {
                    data: {
                        depVariable: ''
                    }
                },                
                items: []
            }],
    buttons: [{
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#logisticRegrContainer')[0];      
                Ext.ComponentQuery.query('#methodLRStep1Panel')[0].save();
                Ext.ComponentQuery.query('#methodLRStep2Panel')[0].save();
                thisWin.saveParams();
            }
        },{
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#logisticRegrContainer')[0];      
//                if ( thisWin.tab1Saved === false || thisWin.tab2Saved === false ) {
//                    Ext.Msg.show({
//                        title:'DELTA',
//                        message: 'You are about to close the window. Would you like to save the parameters?',
//                        buttons: Ext.Msg.YESNO,
//                        icon: Ext.Msg.QUESTION,
//                        fn: function(btn) {
//                            if (btn === 'yes') {
//                                if ( thisWin.tab1Saved === false ) {
//                                    Ext.ComponentQuery.query('#methodLRStep1Panel')[0].save();
//                                }
//                                if ( thisWin.tab2Saved === false ) {
//                                    Ext.ComponentQuery.query('#methodLRStep2Panel')[0].save();
//                                }                                
//                            } 
//                            thisWin.cleanup();
//                        }
//                    });  
//                    return;
//                }
                thisWin.cleanup();
            }
        }],        
    fieldStore: {},
    selectedStudyRecord: {},
    params: [],
    initComponent: function() {
        var me = this;
        me.title = 'DELTA Logistic Regression parameters for study "' + me.selectedStudyRecord.get('name') + '"'; 
        
        // check if all necessary study parameters are present
        var startDate = me.selectedStudyRecord.get('startTS');
        if ( startDate === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Study Start Date is not defined.");
            me.destroy();  
        }       
        var endDate = me.selectedStudyRecord.get('endTS');
        if ( endDate === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Study End Date is not defined.");
            me.destroy(); 
        }          
        var index = me.selectedStudyRecord.get('idKey');
        if ( me.fieldStore.findRecord('idModelColumn', index) === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
            me.destroy();          
        }      
        var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
        var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));
        if ( methodRec === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign a Method to the study first.");
            me.destroy(); 
        }      
        index = me.selectedStudyRecord.get('idDate');
        if ( me.fieldStore.findRecord('idModelColumn', index) === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Sequence Field to the study first.");
            me.destroy();
        }          
        
        var studyParams = me.selectedStudyRecord.get('methodParams');
        if ( (typeof studyParams !== 'undefined') && (studyParams !== null)  && (studyParams !== "") ) {
            // params previously saved for the study
            me.params = JSON.parse(studyParams); 
        }  else {
            var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
            var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));            
            // read empty params string from template in tab panel
            me.params = JSON.parse(methodRec.data.methodParams);
        }
        me.callParent();
        var theTabs = Ext.ComponentQuery.query('#regrPopupTab')[0];
        if ( JSON.stringify(me.params[0].inputs.dependentVariableSelection.values[0]) === "{}" || me.params[0].inputs.dependentVariableSelection.values[0] === 'value0' ) {
            theTabs.config.viewModel.data.depVariable = '';
        } else {
            theTabs.config.viewModel.data.depVariable = me.params[0].inputs.dependentVariableSelection.values[0]; 
        }
        theTabs.add(new delta3.view.study.LogisticRegrPopup1({
                        step: 0,
                        params: me.params[0],
                        methodParams: me.methodParams,
                        fieldStore: me.fieldStore, 
                        selectedStudyRecord: me.selectedStudyRecord, 
                        modelId: me.selectedStudyRecord.data.idModel,
                        studyId: me.selectedStudyRecord.data.idStudy})  );
        theTabs.add(new delta3.view.study.LogisticRegrPopup2({
                        step: 1,
                        params: me.params[1],         
                        methodParams: me.methodParams,
                        fieldStore: me.fieldStore, 
                        selectedStudyRecord: me.selectedStudyRecord, 
                        modelId: me.selectedStudyRecord.data.idModel,
                        studyId: me.selectedStudyRecord.data.idStudy}) );      
        theTabs.setActiveTab(0);     
        me.callParent();       
    },
    saveParams: function() {
        var me = this;
        var newParams = JSON.stringify(me.params);
        me.selectedStudyRecord.data.methodParams = newParams;
        me.selectedStudyRecord.data.status = 'New';
        me.selectedStudyRecord.dirty = true;        
        var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
        var theWin = Ext.ComponentQuery.query('#methodLRStep1Panel')[0];
        var rec = studyGrid.getStore().findRecord('idStudy', theWin.studyId);      
        rec.set('status', 'New');         
        studyGrid.getStore().sync();  
        studyGrid.setLoading();
        // Clear the loading mask after 1 second
        setTimeout(function (target) {
            target.setLoading(false);
        }, 1000, studyGrid);
    },
    cleanup: function() {
        var theTabs = Ext.ComponentQuery.query('#regrPopupTab')[0];    
        var tab = Ext.ComponentQuery.query('#methodLRStep1Panel')[0];
        if ( !Ext.isEmpty(tab) ) {
            tab.removeAll();
            theTabs.remove(tab);
            tab = null;
        } 
        tab = Ext.ComponentQuery.query('#methodLRStep2Panel')[0];
        if ( !Ext.isEmpty(tab) ) {
            // clean up necessary after multiple sub panel allocations while switching tabs
            while ( Ext.ComponentQuery.query('#popupMethodLRStep2SubPanel').length > 0 ) {
                var thePanel = Ext.ComponentQuery.query('#popupMethodLRStep2SubPanel')[0];
                thePanel.close();
                thePanel = null;     
            }
            tab.removeAll();
            theTabs.remove(tab);
            tab = null;
        }           
        theTabs = null;
        this.destroy();        
    }
});
