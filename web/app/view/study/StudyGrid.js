/* 
 * Study Grid
 */

Ext.define('delta3.view.study.StudyGrid', {
    extend: 'Ext.grid.Panel',
    columnLines: true,
    enableLocking: true,
    alias: 'widget.grid.study',
    minHeight: 170,
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.column.Check',
        'Ext.grid.column.Action',        
        'Ext.form.DateField',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator',   
        'delta3.store.FieldStore',
        'delta3.store.StudyStore',
        'delta3.store.FilterStore',
        'delta3.store.GroupEntityStore',
        'delta3.store.TaskStore',        
        'delta3.view.study.OutcomePopup',
        'delta3.view.study.TreatmentPopup',
        'delta3.view.study.CategoryPopup',
        'delta3.view.study.DatePopup',
        'delta3.view.study.KeyPopup',        
        'delta3.view.study.LogisticRegrContainer',      
        'delta3.view.study.popup.MethodParamPopup',  
        'delta3.view.study.PropensityPopup',          
        'delta3.view.study.PropensityApplyPopup',     
        'delta3.view.study.PropensityCreatePopup',          
        'delta3.view.study.SurvivalPopup',  
        'delta3.view.study.SPRTPopup',  
        'delta3.view.study.LRCombinedPopup',
        'delta3.view.study.LRApplyPopup',        
        'delta3.view.study.LRCreatePopup',          
        'delta3.view.filter.FilterPopup',
        'delta3.view.scheduler.ReccurencePopup',
        'delta3.view.study.UtilFunc'
    ],    
    itemId: 'studyGrid',
    selModel: {mode: "SINGLE", allowDeselect: true},
    filterStore: {},
    modelStore: {},
    modelComboBox: {},
    statStore: {},
    methodStore: {},
    taskStore: {},
    modelProjectStore: {},
    idEntity: 0,
    disallowProjectMove: false,    // reversed, see below comment for DELQA-355
    border: false,
    listeners: {
        
        
        beforedestroy: function(panel, eOpt) {
            var mButton = panel.down('#monitorStudyButton');
            clearInterval(mButton.periodicModelStatusCheck);
        },
        afterrender: function() {
            //delta3.utils.GlobalFunc.showDeltaMessage('Please refresh grid for updated view');           
            Ext.Msg.show({
                    closable: false,
                    msg: 'All entitiles associated with selected project have retrived successfully. Please click "ok" to refresh view.',
                    title: 'Project Change',
                    buttons: Ext.Msg.OK, 
                    width: 300,
                    fn: function(buttonId) {
                      if (buttonId === 'ok') {
 //                       console.log(btn);
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        thisGrid.methodStore.clearFilter(true);
                        thisGrid.methodStore.filter('active', true);              
                        thisGrid.store.load();
                        Ext.ComponentQuery.query('#studyGrid')[0].getView().refresh();
                      }
                    }
                  });
        }
        
        
    },     
    dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',            
            items: []
        },{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No studies found',
            pageSize: delta3.utils.GlobalVars.pageSize,   
            displayInfo: true
        }],
    
    modelSelected: {
        idModel: 0,
        name: ""
    },
    
    
    initComponent: function() {
        var me = this;
        
        
        me.projectStore = Ext.ComponentQuery.query('#mainViewPort')[0].projectStore;
        me.dockedItems[0].store = me.store;
        me.store = me.buildStore();    
        me.dockedItems[1].store = me.store;
        me.store.load(); 
        
        
        me.statStore = delta3.store.StatStore.create();       
        me.statStore.load();
        
        
        me.filterStore = delta3.store.FilterStore.create({idModel: 0});
        me.filterStore.load();  
        
        me.modelStore = delta3.utils.GlobalVars.ModelStore;     
        me.modelProjectStore = delta3.store.ModelStore.create({groupId: Ext.ComponentQuery.query('#mainViewPort')[0].project});
        me.modelProjectStore.load();
        

        me.methodStore = delta3.store.MethodStore.create();
        me.methodStore.load();   
        me.methodStore.clearFilter(true);
        me.methodStore.filter('active', true);
        me.methodStore.getSorters().add('sequenceNumber');
        
        me.taskStore = delta3.store.TaskStore.create({groupId: Ext.ComponentQuery.query('#mainViewPort')[0].project, groupType: "studies"});
        me.taskStore.load();       
        
        me.modelComboBox = new Ext.form.ComboBox({
            store: me.modelProjectStore,
            itemId: 'modelComboBox',
            displayField: 'name',
            valueField: 'idModel',
            queryMode: 'local',
            multiSelect: false,
            forceSelection: true,
            listeners: {
                'select': function(cmb, rec, eOpt) {
                    var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];                    
                    thisGrid.modelSelected = cmb.displayTplData[0];                                                      
                }
            }
        });
        
        me.statComboBox = new Ext.form.ComboBox({
                store: me.statStore,
                    //itemId: 'statComboBox',
                    displayField: 'shortName',
                    valueField: 'idStat',
                    queryMode: 'local',
                    multiSelect: false,
                    forceSelection: true,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            var h = cmb.getValue();
                            var r = me.store.getNewRecords();
                            if (r.length > 0) { // process new record
                                r[0].data.idStat = h;
                                r[0].data.statPackageVer = rec.data.configVersion;
                            } else {            // process existing record
                                var sm = me.getSelectionModel();
                                if (typeof sm.getSelection()[0] === 'undefined' ) {
                                    delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                                    return;
                                }   
                                
                                if ( sm.getSelection()[0].data.idStat !== h ) {
                                    Ext.Msg.show({
                                       title:'DELTA',
                                       message: 'You are about to change Statistical Package. Would you like to proceed?',
                                       buttons: Ext.Msg.YESNO,
                                       icon: Ext.Msg.QUESTION,
                                       fn: function(btn) {
                                           if (btn === 'yes') {
                                               var selection = sm.getSelection()[0];
                                               rec.data['idMethod'] = 0;
                                               //selection.data.idMethod = 0;
                                               selection.data.idStat = h;                                                 
                                               selection.data.methodParams = ""; 
                                               selection.data.idTreatment = 0;
                                               selection.data.idOutcome = 0;
                                               selection.data.status = 'New';
                                               selection.dirty = true; 
                                               me.store.sync();                                              
                                           }
                                       }
                                    });                                
                                }
                            }
                        }
                    }
                }),
        
        
        
        // following feature disabled as per Jira DELQA-355
        //if (delta3.utils.GlobalFunc.isPermitted("MoveStudy") === true) {
        //    me.disallowProjectMove = false;
        //}           
        me.columns = me.buildColumns(me);     
        me.plugins = me.buildPlugins();    
        
        
        var deleteStudy;
        if (delta3.utils.GlobalFunc.isPermitted("StudyDelete",Ext.ComponentQuery.query('#mainViewPort')[0].project) === true) {
            deleteStudy = {
                itemId: 'StudyDelete',
                text: 'Delete',
                iconCls: 'delete-icon16',
                tooltip: delta3.utils.Tooltips.studyBtnDelete,               
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    var sm = thisGrid.getSelectionModel();
                    thisGrid.plugins[0].cancelEdit();
                    if (typeof sm.getSelection()[0] === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                        return;
                    }                     
                    Ext.Msg.show({
                        title:'DELTA',
                        message: 'You are about to delete Study. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                /*----below code is from 3.62----------
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                                thisGrid.store.reload();      
                                */
                                
                                //--------v3.64 added below code for resolution of bug DELQA-382 for delete -----------------
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                                //thisGrid.store.reload();    
                            }
                        }
                    });                    
                }
            };
        }                   
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];  
        
        
        tbr.add({
            itemId: 'addStudy',
            text: 'Step 1: Add Study',
            iconCls: 'add_new-icon16',
            tooltip: delta3.utils.Tooltips.studyBtnAdd,               
            handler: function() 
            
            {
                var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];                
                thisGrid.plugins[0].cancelEdit();
                // Create a new record instance
                var r = delta3.model.StudyModel.create({
                    name: '',
                    description: '',
                    idGroup: Ext.ComponentQuery.query('#mainViewPort')[0].project,
                    isMissingDataUsed: true,
                    confidenceInterval: '95',
                    active: true,
                    status: 'New',
                    outcomePositive: false,
                    overwriteResults: false,
                    autoExecute: true,
                    localStatPackage: true,
                    remoteStatPackage: false,
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});   
                
                //thisGrid.store.insert(0, r);   
                var suc = thisGrid.store.insert(0, r);   
                thisGrid.plugins[0].startEdit(0, 0);
            }
           
        });
        
        tbr.add({
            text: 'Step 2: Edit Study',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                //width: 100,
                margin: '0 0 10 0',
                items: [{
                    text: 'Edit Study',
                    iconCls: 'edit-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnEdit,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];                           
                        var sel = thisGrid.getSelectionModel().getSelection()[0];
                        if (typeof sel === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }                            
                        thisGrid.plugins[0].startEdit(sel, 0);
                    }
                }, {
                    text: 'Edit Parameters',
                    iconCls: 'edit_params-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnEditParam,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];                           
                        var sel = thisGrid.getSelectionModel().getSelection()[0];
                        if (typeof sel === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }                            
                        thisGrid.paramPopupDispatcher(sel, me);   
                    }
                }, {
                    text: 'Clear Parameters',
                    iconCls: 'clear_params-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnClearParam,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];                           
                        var sel = thisGrid.getSelectionModel().getSelection()[0];
                        if (typeof sel === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }          
                        Ext.Msg.show({
                            title:'DELTA',
                            message: 'You are about to clear Study Parameters. Would you like to proceed?',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.Msg.QUESTION,
                            fn: function(btn) {
                                if (btn === 'yes') {
                                    sel.data.methodParams = ""; 
                                    sel.data.idTreatment = 0;
                                    sel.data.idOutcome = 0;
                                    sel.data.status = 'New';
                                    sel.dirty = true; 
                                    thisGrid.store.sync();
                                }
                            }
                        });                        
                    }
                }, {
                    text: 'Study Filters',
                    iconCls: 'filter-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnEditFilter,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];                           
                        var sel = thisGrid.getSelectionModel().getSelection()[0];
                        if (typeof sel === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }                            
                        var win = new delta3.view.filter.FilterPopup({
                            fieldStore: delta3.utils.GlobalVars.FieldStore,
                            modelId: sel.data.idModel,
                            subTitle: 'Filter selection for study "' + sel.get('name') + '"',
                            isStudyMode: true,
                            studyId: sel.data.idStudy});
                        win.show();                        
                    }
                },{
                    itemId: 'cloneStudy',
                    text: 'Clone Study',
                    iconCls: 'cloneStudy-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnClone,                       
                    handler: function() {            
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }  
                        delta3.utils.GlobalFunc.doCloneStudy(sm.getSelection()[0].data, thisGrid);
                    }
                }, {
                    itemId: 'splitStudy',
                    text: 'Split Study',
                    iconCls: 'splitStudy-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnSplit,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }  
                        delta3.utils.GlobalFunc.doSplitStudy(sm.getSelection()[0].data, 'testNewStudy');          
                    }
                }, deleteStudy, 
                {
                    itemId: 'recordInfo',
                    text: 'View Properties',
                    iconCls: 'recordInfo-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnProperties,                       
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0]
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        }                            
                        var win = Ext.create('delta3.view.popup.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "Study"});
                        win.show();
                    }
                }]
            })
        });        
        tbr.add({
            text: 'Step 3: Run Study',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                margin: '0 0 10 0',
                items: [
                
                    {
                    itemId: 'executeStudyNow',
                    text: 'Run Study',
                    iconCls: 'study-executeStepNow-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnRun,                   
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        } 
                        //delta3.utils.GlobalFunc.doExecuteStudy(sm.getSelection()[0].data, thisGrid); // older code that showed popup windows    
                        delta3.utils.GlobalFunc.doProcessStudy("ProcessStudyBatch", sm.getSelection()[0].data.idStudy);                       
                    }
                }, {
                    itemId: 'showStudyResultsPopup',
                    text: 'Result Dashboard',
                    iconCls: 'show_results2-icon16',
                    tooltip: delta3.utils.Tooltips.studyBtnShowResults,                   
                    handler: function() {
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        var sm = thisGrid.getSelectionModel();
                        thisGrid.plugins[0].cancelEdit();
                        if (typeof sm.getSelection()[0] === 'undefined') {
                            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                            return;
                        } 
                        var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                        var filterRecord = thisGrid.filterStore.findRecord('idFilter', sm.getSelection()[0].data.idFilter);  
                        var filterName = '';
                        if ( filterRecord !== null ) {
                            filterName = filterRecord.data.name;
                        }        
                        var field = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', sm.getSelection()[0].data.idOutcome);
                        var outcomeName;
                        if (field === null)
                            outcomeName = '';
                        else {
                            outcomeName = field.get("name");    
                        }
                        delta3.utils.GlobalFunc.showLastResultsTab(sm.getSelection()[0].data.idStudy);
                    }
                }]
            })
        });          
        tbr.add({
            text: 'Start Monitor',
            iconCls: 'monitor-icon16',
            itemId: 'monitorStudyButton',
            tooltip: delta3.utils.Tooltips.studyBtnMonitor,               
            periodicModelStatusCheck: {},
            handler: function() {
                var me = this;
                var counter = 20; // Monitor will stop after x calls
                if ( this.text === 'Start Monitor' ) { 
                    me.executeMonitorPulse(me);
                    this.periodicModelStatusCheck = setInterval(function () 
                        {
                            counter--;
                            if ( counter > 0 ) {
                                me.executeMonitorPulse(me);
                            } else {
                                clearInterval(me.periodicModelStatusCheck);
                                me.setText('Start Monitor'); 
                            }
                        }, 
                        9000); // every 9 sec (plus 1 sec for red-eye pulse)
                    this.setText('Stop Monitor');
                } else {
                    clearInterval(me.periodicModelStatusCheck);
                    this.setText('Start Monitor');
                }
            },
            executeMonitorPulse: function(button) {
                button.setIconCls('monitor_on-icon16');
                setTimeout(function(){button.callToGetModelStatus(button)},1000);
            },
            callToGetModelStatus: function(button) {
                var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                thisGrid.store.load();
                button.setIconCls('monitor-icon16');    
            }            
        });
        tbr.add({
            text: 'Import/Export',
            iconCls: 'spec-icon16',
            menu: Ext.create('Ext.menu.Menu', {
                margin: '0 0 10 0',
                items: [{
                        itemId: 'exportStudy',
                        text: 'Export',
                        iconCls: 'exportObject-icon16',
                        tooltip: delta3.utils.Tooltips.studyBtnExport,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                                return;
                            }
                            delta3.utils.GlobalFunc.doExportStudy(sm.getSelection()[0].data);
                        }
                    }, {
                        itemId: 'importStudyParams',
                        text: 'Import',
                        iconCls: 'importObject-icon16',
                        tooltip: delta3.utils.Tooltips.studyBtnImport,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                            thisGrid.plugins[0].cancelEdit();
                            var win = new delta3.view.popup.ImportPopup();
                            win.show();
                        }
                    }, {
                        itemId: 'exportStudyInfo',
                        text: 'Study Info',
                        iconCls: 'information-icon16',
                        tooltip: delta3.utils.Tooltips.studyBtnInfo,
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                                return;
                            }
                            delta3.utils.GlobalFunc.doExportStudyInfo(sm.getSelection()[0].data);
                        }
                    },
                    
                    
                    {
                        text: 'Export Grid to CSV',
                        iconCls: 'exportCSV-icon16',
                        tooltip: delta3.utils.Tooltips.mbBtnExcel,
                        handler: function(b, e) {
                           // console.log("This is at export CSV");
                            b.up('grid').exportGrid('DELTA Studies');
                        }
                    }]
            })
        });        
        tbr.add({
            itemId: 'refreshStudy',
            text: 'Refresh',
            iconCls: 'refresh-icon16',
            tooltip: delta3.utils.Tooltips.studyBtnRefresh,               
            handler: function() {
                
                var projects = Ext.ComponentQuery.query('#studyGrid')[0].modelProjectStore;                
                var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                thisGrid.methodStore.clearFilter(true);
                thisGrid.methodStore.filter('active', true);              
                thisGrid.store.load();
            }
        });        
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me}));   
    },
    
    
    
    buildColumns: function(thisGrid) {
        return [               
            //below code is working code for all studies without project filter
            {text: 'ID', dataIndex: 'idStudy', width: 46, locked: true, tooltip: delta3.utils.Tooltips.studyGridId},                
            {text: 'Name', dataIndex: 'name', width: 100, locked: true, tooltip: delta3.utils.Tooltips.studyGridName, editor: {xtype: 'textfield',maskRe: /[^<>%]/}},
            
            //Below code is working code for all studies without project filter
            {text: 'Description', dataIndex: 'description', width: 130, tooltip: delta3.utils.Tooltips.studyGridDescription, editor: {xtype: 'textfield',maskRe: /[^<>%]/}},
            {text: 'Project', dataIndex: 'idGroup', 
                hidden: thisGrid.disallowProjectMove, hideable: false, width: 100, 
                sortable: false, tooltip: delta3.utils.Tooltips.studyGridProject,
                editor: new Ext.form.ComboBox({
                    store: Ext.ComponentQuery.query('#mainViewPort')[0].projectStore,
                    itemId: 'projectComboBox',
                    displayField: 'name',
                    valueField: 'idGroup',
                    queryMode: 'local',
                    multiSelect: false,
                    forceSelection: true,
                    listeners: {
                        'select': function(cmb, rec, eOpt) {
                            var h = cmb.getValue();
                            var combobox_value= Ext.ComponentQuery.query('#projectComboBox')[0].getValue();                              
                            if(h!==combobox_value){
                                delta3.utils.GlobalFunc.showDeltaMessage('Caution : You are creating/assigning study to diffrent project. If you proceed, it will be automatically moved to assigned project. Other models of this project will not be available to another project.');
                            }
                            
                            var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                            delta3.utils.GlobalFunc.removeAndAddRelationships(rec.get('name)'), thisGrid.idEntity, 'studies', h);               
                        
                        }
                    }
                }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idGroup'];
                    var project = Ext.ComponentQuery.query('#mainViewPort')[0].projectStore.findRecord('idGroup', tableIndex);
                    if (project === null)
                        return null;
                    else {
                        return project.get("name");
                    }
                }}, 
            
            {text: 'Data Model', 
                dataIndex: 'idModel', 
                width: 100, sortable: false, 
                tooltip: delta3.utils.Tooltips.studyGridModel,
                editor: thisGrid.modelComboBox,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idModel'];
                    //var model = delta3.utils.GlobalVars.ModelStore.findRecord('idModel', tableIndex);
                    //var model = Ext.ComponentQuery.query('#mainViewPort')[0].projectStore.findRecord('idModel', tableIndex);
                    //var model = thisGrid.modelStore.findRecord('idModel', tableIndex, undefined, undefined, undefined, true); 
                    var model = delta3.utils.GlobalVars.ModelStore.findRecord('idModel', tableIndex, undefined, undefined, undefined, true);                    
                    if (model === null)
                        return null;
                    else {
                        return model.get("name");
                    }
                    
                }},
            
            
            
            {text: "Start", width: 90, sortable: true, dataIndex: 'startTS', type: 'date',
                dateFormat: 'Y-m-d H:i:s',
                tooltip: delta3.utils.Tooltips.studyGridStart,
                editor: 'datefield',
                renderer: Ext.util.Format.dateRenderer('m/d/Y')},
            
            {text: "End", width: 90, sortable: true, dataIndex: 'endTS', type: 'date',
                dateFormat: 'Y-m-d H:i:s',
                tooltip: delta3.utils.Tooltips.studyGridEnd,
                editor: 'datefield',
                renderer: Ext.util.Format.dateRenderer('m/d/Y')},       
            {text: "Last Processed on", sortable: false, width: 90, dataIndex: 'processedTS', type: 'date', 
                dateFormat: 'Y-m-d H:i:s',
                tooltip: delta3.utils.Tooltips.studyGridLastProcessOn,
               // editor: 'datefield'
                //editor: {xtype: 'datefield',maskRe: /[^<>%]/}
                renderer: Ext.util.Format.dateRenderer('m/d/y g:i:s A')
                //renderer: Ext.util.Format.dateRenderer('m/d/Y')
            },
            
            {xtype: 'actioncolumn',
                width: 60,
                text: 'Schedule',
                renderer: function (value, metadata, record) {
                        var tasks = Ext.ComponentQuery.query('#studyGrid')[0].taskStore;
                        var currTask = tasks.findRecord('objectId',record.get('idStudy'));
                        if ( currTask === null ) {
                            this.items[0].iconCls = 'clock_edit-icon16';
                        } else {
                            this.items[0].iconCls = 'clock_go-icon16';
                        }
                    },
                items: [{
                        iconCls: 'clock_edit-icon16',
                        tooltip: delta3.utils.Tooltips.studyGridScheduleButton,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            if ( rec.data.idModel === 0 ) {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please assign Model to Study first.');
                                return;                                
                            }                            
                            var win = new delta3.view.scheduler.ReccurencePopup 
                                ({selectedRecord: rec, 
                                    entityId: rec.data.idStudy, 
                                    entityType: "studies",
                                    refreshFunction: Ext.ComponentQuery.query('#studyGrid')[0].refreshTasks,
                                    taskStore: Ext.ComponentQuery.query('#studyGrid')[0].taskStore,
                                    checkBoxOnAction: 'generateFTrunStudy',
                                    checkBoxOffAction: 'runStudy'
                                });
                            win.show();
                        }
                    }]
            },            
            {text: 'Stage Status', dataIndex: 'status', width: 130, tooltip: delta3.utils.Tooltips.studyGridStatus, renderer: function(val, cell, rec, r_idx, c_idx, store) {
                        cell.tdAttr = 'data-qtip="' + val + '"';
                        return val;
                    }},
                
                
                
            {text: 'Statistical Package', 
                dataIndex: 'idStat',
                loading: true,
                width: 96, 
                sortable: false, tooltip: delta3.utils.Tooltips.studyGridMethod,
                editor: thisGrid.statComboBox,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idStat'];   
                    var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    var statPackage = thisGrid.statStore.findRecord('idStat', tableIndex, 0, false, true, true);
                    if (statPackage === null || statPackage.get("active") === false ){
                         
                        return null;
                    }
                    else {
                        return statPackage.get("shortName");       
                        
                    }
                   Ext.ComponentQuery.query('#studyGrid')[0].getView().refresh();
                }},
            
            
            
            {text: 'Analysis Method',
                dataIndex: 'idMethod', width: 120, 
                sortable: false, tooltip: delta3.utils.Tooltips.studyGridMethod,
                
                editor: new Ext.form.ComboBox({
                    store: thisGrid.methodStore,
                    itemId: 'methodComboBox',
                    displayField: 'shortName',
                    valueField: 'idMethod',
                    queryMode: 'local',
                    multiSelect: false,
                    forceSelection: true,
                    listeners: {
                        'focusenter': function(cmb, options) {
                            var plug = thisGrid.plugins[0];
                            var rec = thisGrid.store.findRecord('idStudy', plug.cmp.idEntity);      
                            var statId = rec.get('idStat');
                            cmb.store.filter('idStat', statId);
                        },                               
                        'select': function(cmb, rec, idx) {
                            var h = cmb.getValue();
                            var r = thisGrid.store.getNewRecords();
                            if (r.length > 0) { // process new record
                                r[0].data.idMethod = h;
                                r[0].data.methodParams = "";
                            } else {            // process existing record
                                var sm = thisGrid.getSelectionModel();
                                if (typeof sm.getSelection()[0] === 'undefined' ) {
                                    delta3.utils.GlobalFunc.showDeltaMessage('Please select Study first.');
                                    return;
                                }   
                                if ( sm.getSelection()[0].data.idStat !== h ) {
                                    var selection = sm.getSelection()[0];
                                    rec.data['idMethod'] = h;                                              
                                    selection.data.methodParams = ""; 
                                    //selection.data.idTreatment = 0;
                                    //selection.data.idOutcome = 0;
                                    selection.data.status = 'New';
                                    selection.dirty = true;                               
                                }
                            }
                        },
                        
                        'focusleave': function(cmb, options) {
                            cmb.getStore().clearFilter(true);
                            cmb.getStore().filter('active', true);           
                        }
                    }
                }),
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idMethod'];
                    //var method = thisGrid.methodStore.findRecord('idMethod', tableIndex);
                    var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    var method = thisGrid.methodStore.findRecord('idMethod', tableIndex, 0, false, true, true);
                    if (method === null || method.get("active") === false ){
                        //thisGrid.store.load();
                        //Ext.getCmp('combo01').setValue('DEE'); 
                        return null;
                    }
                    
                    else {
                        return method.get("shortName");
                    }
                     Ext.ComponentQuery.query('#studyGrid')[0].getView().refresh();
                }},   
            
            
            
            {text: 'Stat Version', dataIndex: 'statPackageVer', width: 80, tooltip: delta3.utils.Tooltips.studyStatPackageVersion},                                      
            
            
            
            {xtype: 'actioncolumn',
                width: 26,
                items: [{
                        iconCls: 'page_link-icon16',
                        tooltip: delta3.utils.Tooltips.studyGridKeyButton,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            var win = new delta3.view.study.KeyPopup({fieldStore: delta3.utils.GlobalVars.FieldStore, selectedStudyRecord: rec, studyId: rec.data.idStudy});
                            win.show();
                        }
                    }]
            },
            
            
            
            {text: 'Key Field', dataIndex: 'idKey', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.studyGridKey,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idKey'];
                    var field = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', tableIndex);
                    if (field === null)
                        return null;
                    else {
                        return field.get("name");
                    }
                }},  
            
            
            {xtype: 'actioncolumn',
                width: 26,
                items: [{
                        iconCls: 'page_link-icon16',
                        tooltip: delta3.utils.Tooltips.studyGridSequenceButton,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            var win = new delta3.view.study.DatePopup({fieldStore: delta3.utils.GlobalVars.FieldStore, selectedStudyRecord: rec, studyId: rec.data.idStudy});
                            win.show();
                        }
                    }]
            },
            {text: 'Sequence Field', dataIndex: 'idDate', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.studyGridSequence,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['idDate'];
                    var field = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', tableIndex);
                    if (field === null)
                        return null;
                    else {
                        return field.get("name");
                    }
                }},        
            {text: 'Filter', dataIndex: 'idFilter', width: 100, sortable: false, tooltip: delta3.utils.Tooltips.studyGridFilter,           
                renderer: function(val, cell, rec, r_idx, c_idx, store) {          
                    var tableIndex = rec.data['idFilter'];                  
                    var filter = thisGrid.filterStore.findRecord('idFilter', tableIndex);                
                    if (filter === null)
                        return null;
                    else {
                        var tooltip = '';
                        var fields = filter.data['fields'];
                        var tooltip = '';
                        for (var i = 0; i < fields.length; i++) {
                            if (i > 0) {
                                tooltip += ', ';
                            }
                            tooltip = delta3.utils.GlobalFunc.getFilterWhereClause(fields, delta3.utils.GlobalVars.FieldStore);
                        }
                        cell.tdAttr = 'data-qtip="' + tooltip + '"';
                        return filter.get("name");
                    }
                }},
            {text: 'Positive Outcome', dataIndex: 'outcomePositive', width: 100, xtype: 'checkcolumn', tooltip: delta3.utils.Tooltips.studyGridPositiveOutcome,  disabled: true, editor: 'checkboxfield'},              
            {text: 'Overwrite Results', dataIndex: 'overwriteResults', width: 75, xtype: 'checkcolumn', tooltip: delta3.utils.Tooltips.studyGridOverwriteResults,  disabled: true, editor: 'checkboxfield'},   
            //{text: 'DELTAlytics', dataIndex: 'localStatPackage', width: 75, xtype: 'checkcolumn', tooltip: delta3.utils.Tooltips.studyLocalStatPackage,  disabled: true, editor: 'checkboxfield'},  
            //{text: 'External Stat', dataIndex: 'remoteStatPackage', width: 75, xtype: 'checkcolumn', tooltip: delta3.utils.Tooltips.studyRemoteStatPackage,  disabled: true, editor: 'checkboxfield'},  
            //{text: 'Dump Data', dataIndex: 'dumpData', width: 75, xtype: 'checkcolumn', tooltip: delta3.utils.Tooltips.studyGridDumpData,  disabled: true, editor: 'checkboxfield'},  
            
            //commented below code for DELQA-743 moved column to last
            //{text: 'Automatic Execution', dataIndex: 'autoExecute', width: 75, xtype: 'checkcolumn', tooltip: delta3.utils.Tooltips.studyGridAutoExecute,  disabled: true, editor: 'checkboxfield'},  
            
            {text: 'Send All Columns', dataIndex: 'sendAllColumns', width: 75, xtype: 'checkcolumn', tooltip: delta3.utils.Tooltips.studyGridSendAllColumns,  disabled: true, editor: 'checkboxfield'},         
            {text: 'Original Study', dataIndex: 'originalStudyId', width: 120, sortable: false, tooltip: delta3.utils.Tooltips.studyOrigName,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    var tableIndex = rec.data['originalStudyId'];
                    var study = thisGrid.store.findRecord('idStudy', tableIndex);
                    if (study === null)
                        return null;
                    else {
                        return study.get("name");
                    }
                }},
            {text: 'Original Study ID', dataIndex: 'originalStudyId', width: 100, sortable: false, tooltip: delta3.utils.Tooltips.studyOrigID},
            {text: 'Automatic Execution', dataIndex: 'autoExecute', width: 75, xtype: 'checkcolumn', tooltip: delta3.utils.Tooltips.studyGridAutoExecute,  disabled: true, editor: 'checkboxfield'},
        
            {text: 'Owner', dataIndex: 'createdBy', width: 90, sortable: true,filterInvisible: false,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', delta3.utils.GlobalVars.UserStore);
            }}
            
        
        ];
    },
    buildStore: function() {
        return delta3.store.StudyStore.create({groupId: Ext.ComponentQuery.query('#mainViewPort')[0].project});
        //return delta3.utils.GlobalVars.StudyStore;  
    },
    
    
    buildPlugins: function() {
        return [
            
            
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            itemId: 'studyGridEditor',
            listeners: {
                beforeedit: function(rowEditor, context, r) {
                    Ext.ComponentQuery.query('#studyGrid')[0].idEntity = context.record.data.idStudy;
                    delta3.utils.GlobalVars.FieldStore.clearFilter(true);
                    var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    thisGrid.modelStore.load();
                    thisGrid.modelProjectStore.load();
                },
                
                edit: function(rowEditor, context, eOpt) {
                    var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    // validate uniqueness of new study name                     
                    var valid = true;
                    var counter = 0;
                    
                    thisGrid.store.each(function (record, index) {
                        if (record.data.name === context.newValues.name) {
                            counter++;
                            if ( counter > 1 ) {
                                valid = false;
                                return;
                            }
                         }
                    });
                    
                    if ( valid === false ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Study names must be unique.');
                        return;                            
                    }   
                    
                    // set local/remote flags
                    var sm = thisGrid.getSelectionModel();                
                    var tableIndex = sm.getSelection()[0].data.idStat;
                    var statPackage = thisGrid.statStore.findRecord('idStat', tableIndex);
                    if (statPackage === null ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Statistical package not defined.');
                        return;
                    } else {
                        if ( statPackage.get("useProxy") === true ) {
                            context.newValues.localStatPackage = false;
                            context.newValues.remoteStatPackage = true;                            
                        } else {
                            context.newValues.localStatPackage = true;
                            context.newValues.remoteStatPackage = false;                               
                        }                     
                    }              
                
                    // save study
                    if ( (context.newValues.idModel !== null) && (context.originalValues.idModel !== 0) &&
                            (context.newValues.idModel !== context.originalValues.idModel) ) {
                        Ext.Msg.show({
                            title:'DELTA',
                            message: 'You are about to change Data Model for the existing Study.<br> Would you like to proceed?',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.Msg.QUESTION,
                            fn: function(btn) {
                                if (btn === 'yes') {
                                   thisGrid.store.sync();
                                } else {
                                   context.newValues.idModel = context.originalValues.idModel;
                                   thisGrid.store.load();
                                    Ext.ComponentQuery.query('#studyGrid')[0].getView().refresh(); 
                                }
                            }
                        });                             
                    } else 
                    
                    {
                        //delta3.utils.GlobalFunc.showDeltaMessage('It is always recommeneded to click on "Refresh" to get updated view');
                        
                        
                        /*
                        Ext.Msg.show({
                            closable: false,
                            msg: 'Study added successfully. It is always recommeneded to click on "Refresh" to get updated view.',
                            title: 'Project Change',
                            buttons: Ext.Msg.OK, 
                            width: 300,
                            fn: function(buttonId) {
                              if (buttonId === 'ok') {
         //                       console.log(btn);
                                var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                                thisGrid.methodStore.clearFilter(true);
                                thisGrid.methodStore.filter('active', true);              
                                thisGrid.store.load();
                                Ext.ComponentQuery.query('#studyGrid')[0].getView().refresh();
                              }
                            }
                          });*/
                        
                        thisGrid.checkAndSaveAdvancedParams(context, thisGrid);                    
                        Ext.ComponentQuery.query('#studyGrid')[0].getView().refresh(); 
                        thisGrid.store.sync();        
                    }
                },
                
                afterEdit : function(grid, row, field, rowIndex, columnIndex){
                    var thisGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    //thisGrid.store.load();
                    Ext.ComponentQuery.query('#studyGrid')[0].getView().refresh();
                }
                
                
                
                
            }
            })
        ];
    },
    
    refreshTasks: function(returnedValue) {
        var store = Ext.ComponentQuery.query('#studyGrid')[0].taskStore;
        if ( returnedValue === null || typeof returnValue !== 'undefined' ) {
            store.sync();
        } else {
            // check if this is an update or new record
            var index = store.find("idTask",returnedValue.tasks[0].idTask);
            if ( index !== -1 ) {
                store.insert(index, returnedValue.tasks[0]);
            } else {
                store.add(returnedValue.tasks[0]);
            }
        }
        Ext.ComponentQuery.query('#studyGrid')[0].getView().refresh(); 
    },
    paramPopupDispatcher: function(rec, thisGrid) {
        var win;
        var popupTypeIndex = thisGrid.methodStore.find('idMethod',rec.data.idMethod);
        if (popupTypeIndex === -1 ) {
            delta3.utils.GlobalFunc.showDeltaMessage('Please select Study method and basic paramters first.');
            return;
        } 
        switch ( thisGrid.methodStore.getAt(popupTypeIndex).get('name') ) {    
            
            case "logisticRegressionCreate":
                win = new delta3.view.study.popup.MethodParamPopup({
                   panelTitle: 'Logistic Regression Create',
                   fieldStore: delta3.utils.GlobalVars.FieldStore, 
                   methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                   selectedStudyRecord: rec, 
                   modelId: rec.data.idModel,
                   studyId: rec.data.idStudy});                
                win.show();
                break;            
                
                
            case "logisticRegression":
                var statTypeIndex = thisGrid.statStore.find('idStat',rec.data.idStat);                
                if ( thisGrid.statStore.getAt(statTypeIndex).get('shortName') === 'OCEANS' ) {   
                    win = new delta3.view.study.LogisticRegrContainer({
                        fieldStore: delta3.utils.GlobalVars.FieldStore, 
                        methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                        selectedStudyRecord: rec});                    
                } else {           
                    win = new delta3.view.study.popup.MethodParamPopup({
                       panelTitle: 'Logistic Regression 2 Step',
                       fieldStore: delta3.utils.GlobalVars.FieldStore, 
                       methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                       selectedStudyRecord: rec, 
                       modelId: rec.data.idModel,
                       studyId: rec.data.idStudy});                   
                }
                win.show();
                break;
                
            case "manualLogisticRegression":
                win = new delta3.view.study.popup.MethodParamPopup({
                    panelTitle: 'Logistic Regression Analysis',
                    fieldStore: delta3.utils.GlobalVars.FieldStore, 
                    methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                    selectedStudyRecord: rec, 
                    modelId: rec.data.idModel,
                    studyId: rec.data.idStudy});                 
                win.show();
                break;                
            case "propensityScoreAnalysis":
                win = new delta3.view.study.popup.MethodParamPopup({
                    panelTitle: 'Propensity Analysis',
                    fieldStore: delta3.utils.GlobalVars.FieldStore, 
                    methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                    selectedStudyRecord: rec, 
                    modelId: rec.data.idModel,
                    studyId: rec.data.idStudy});                    
                win.show();
                break;  
                
            case "propensityAnalysisCreate":
                win = new delta3.view.study.popup.MethodParamPopup({
                   panelTitle: 'Propensity Create',
                   fieldStore: delta3.utils.GlobalVars.FieldStore, 
                   methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                   selectedStudyRecord: rec, 
                   modelId: rec.data.idModel,
                   studyId: rec.data.idStudy});                 
                win.show();
                break;  
            
            case "iptw":
                win = new delta3.view.study.popup.MethodParamPopup({
                   panelTitle: 'IPTW',
                   fieldStore: delta3.utils.GlobalVars.FieldStore, 
                   methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                   selectedStudyRecord: rec, 
                   modelId: rec.data.idModel,
                   studyId: rec.data.idStudy});    
                win.show();
                break; 
            
            
            
            case "propensityScoreOutcomes":
                win = new delta3.view.study.popup.MethodParamPopup({
                   panelTitle: 'Propensity Outcomes',
                   fieldStore: delta3.utils.GlobalVars.FieldStore, 
                   methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                   selectedStudyRecord: rec, 
                   modelId: rec.data.idModel,
                   studyId: rec.data.idStudy});                 
                win.show();
                break;                 
            case "manualPropensityAnalysis":
                win = new delta3.view.study.popup.MethodParamPopup({
                    panelTitle: 'Propensity Analysis',
                    fieldStore: delta3.utils.GlobalVars.FieldStore, 
                    methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                    selectedStudyRecord: rec, 
                    modelId: rec.data.idModel,
                    studyId: rec.data.idStudy});                   
                win.show();
                break;                  
            case "riskAdjustedSPRT":
                win = new delta3.view.study.popup.MethodParamPopup({
                   panelTitle: 'Risk Adjusted SPRT',
                   fieldStore: delta3.utils.GlobalVars.FieldStore, 
                   methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                   selectedStudyRecord: rec, 
                   modelId: rec.data.idModel,
                   studyId: rec.data.idStudy});                  
                win.show();
                break;        
                
                
                
            case "logisticRegressionCombined":
                win = new delta3.view.study.LRCombinedPopup({
                    step: 0,
                    fieldStore: delta3.utils.GlobalVars.FieldStore, 
                    methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                    selectedStudyRecord: rec, 
                    modelId: rec.data.idModel,
                    studyId: rec.data.idStudy});
                win.show();
                break;      
                
            case "survivalAnalysisMatched":
                win = new delta3.view.study.popup.MethodParamPopup({
                   panelTitle: 'Survival Analysis Matched',
                   fieldStore: delta3.utils.GlobalVars.FieldStore, 
                   methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                   selectedStudyRecord: rec, 
                   modelId: rec.data.idModel,
                   studyId: rec.data.idStudy});                  
                win.show();
                break;        
            case "survivalAnalysisUnmatched":
                win = new delta3.view.study.popup.MethodParamPopup({
                   panelTitle: 'Survival Analysis Unmatched',
                   fieldStore: delta3.utils.GlobalVars.FieldStore, 
                   methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                   selectedStudyRecord: rec, 
                   modelId: rec.data.idModel,
                   studyId: rec.data.idStudy});                  
                win.show();
                break;
                
            case "logisticRegressionComplete":
                win = new delta3.view.study.popup.MethodParamPopup({
                   panelTitle: 'Logistic Regression Analysis',
                   fieldStore: delta3.utils.GlobalVars.FieldStore, 
                   methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                   selectedStudyRecord: rec, 
                   modelId: rec.data.idModel,
                   studyId: rec.data.idStudy});                  
                win.show();
                break;
             
              case "weightingByOdds":
                win = new delta3.view.study.popup.MethodParamPopup({
                   panelTitle: 'weightingByOdds',
                   fieldStore: delta3.utils.GlobalVars.FieldStore, 
                   methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                   selectedStudyRecord: rec, 
                   modelId: rec.data.idModel,
                   studyId: rec.data.idStudy});    
                win.show();
                break; 
             
             
                case "propensityStratification":
                win = new delta3.view.study.popup.MethodParamPopup({
                   panelTitle: 'PropensityStratification',
                   fieldStore: delta3.utils.GlobalVars.FieldStore, 
                   methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                   selectedStudyRecord: rec, 
                   modelId: rec.data.idModel,
                   studyId: rec.data.idStudy});    
                win.show();
                break; 
                
                
                case "logisticRegressionAnalysis":
                win = new delta3.view.study.popup.MethodParamPopup({
                   panelTitle: 'logisticRegressionAnalysis',
                   fieldStore: delta3.utils.GlobalVars.FieldStore, 
                   methodParams: JSON.parse(thisGrid.methodStore.getAt(popupTypeIndex).get('methodParams')),
                   selectedStudyRecord: rec, 
                   modelId: rec.data.idModel,
                   studyId: rec.data.idStudy});    
                win.show();
                break; 
                
                
                
             
            default:
                delta3.utils.GlobalFunc.showDeltaMessage(
                        'Method not supported: ' + thisGrid.methodStore.getAt(popupTypeIndex).get('name')
                        );
        }                            
    },      
    
    
    checkAndSaveAdvancedParams: function(context, theGrid) {
        var studyParams = context.record.data.methodParams;
        var params;
        if ( (typeof studyParams !== 'undefined') && (studyParams !== null) && (studyParams !== "") ) {
            // params previously saved for the study
            params = JSON.parse(studyParams);          
        } else {
            // empty params JSON - do nothing
            return;
        }        
        if ( typeof context.record.modified.startTS !== 'undefined' ) {
            var startDate = context.newValues.startTS.toISOString();
        } 
        if ( typeof context.record.modified.endTS !== 'undefined' ) {
            var endDate = context.newValues.endTS.toISOString();
        }
        if ( typeof context.record.modified.confidenceInterval !== 'undefined' ) {
            var ciValue = context.newValues.confidenceInterval;
        }  
        if ( typeof context.record.modified.confidenceInterval2 !== 'undefined' ) {
            var ci2Value = context.newValues.confidenceInterval2;
        }          
        if ( typeof context.record.modified.idOutcome !== 'undefined' ) {
            var outcomeRecord = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', context.newValues.idDate);       
            var outcomeValue = outcomeRecord.get("name");  
        }    
        
        
        if ( typeof context.record.modified.idKey !== 'undefined' ) {
            var uniqueRecord = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', context.newValues.idKey); 
            var uniqueValue = uniqueRecord.get("name");
        }   
        
        if ( typeof context.record.modified.idDate !== 'undefined' ) {
            var seqRecord = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', context.newValues.idDate);       
            var sequenceValue = seqRecord.get("name");   
        }    
        
        
        /*
        if ( typeof context.record.modified.idDate !== 'undefined' ) {
            var seqRecord = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', context.newValues.idDate);       
            var lastseenValue = seqRecord.get("name");   
        }*/   
        
        
        var popupTypeIndex = theGrid.methodStore.find('idMethod', context.record.data.idMethod);
        try {
            switch ( theGrid.methodStore.getAt(popupTypeIndex).get('name') ) {      
                case "logisticRegressionCreate":
                
                case "propensityAnalysisCreate":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.dependentVariableSelection.values[0] = outcomeValue;                                   
                    break;                
                
                case "logisticRegression":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.dependentVariableSelection.values[0] = outcomeValue;  
                    if ( typeof uniqueValue !== 'undefined' ) params[1].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[1].inputs.dependentVariableSelection.values[0] = outcomeValue;                  
                    if ( typeof startDate !== 'undefined' ) params[1].inputs.studyStartDate.values[0] = startDate;
                    if ( typeof endDate !== 'undefined' ) params[1].inputs.studyEndDate.values[0] = endDate;
                    if ( typeof sequenceValue !== 'undefined' ) params[1].inputs.sequencingVariableSelection.values[0] = sequenceValue;   
                    if ( typeof ciValue !== 'undefined' ) params[1].inputs.confidenceIntervalList.values[0].value = ciValue;;   
                    if ( typeof ci2Value !== 'undefined' ) params[1].inputs.confidenceIntervalList.values[1].value = ci2Value;                                  
                    break;
                case "manualLogisticRegression":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.dependentVariableSelection.values[0] = outcomeValue;                
                    if ( typeof startDate !== 'undefined' ) params[0].inputs.studyStartDate.values[0] = startDate;
                    if ( typeof endDate !== 'undefined' ) params[0].inputs.studyEndDate.values[0] = endDate;
                    if ( typeof sequenceValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = sequenceValue;   
                    if ( typeof ciValue !== 'undefined' ) params[0].inputs.confidenceIntervalList.values[0].value = ciValue;;   
                    if ( typeof ci2Value !== 'undefined' ) params[0].inputs.confidenceIntervalList.values[1].value = ci2Value;                                  
                    break;                    
                case "propensityScoreAnalysis":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.dependentVariableSelection.values[0] = outcomeValue;  
                    if ( typeof sequenceValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = sequenceValue;   
                    break;  
                case "manualPropensityAnalysis":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.dependentVariableSelection.values[0] = outcomeValue;  
                    if ( typeof sequenceValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = sequenceValue;   
                    break;                      
                case "riskAdjustedSPRT":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.dependentVariableSelection.values[0] = outcomeValue;  
                    if ( typeof sequenceValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = sequenceValue;   
                    break;   
        
                case "logisticRegressionCombined":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.dependentVariableSelection.values[0] = outcomeValue;                 
                    if ( typeof startDate !== 'undefined' ) params[0].inputs.studyStartDate.values[0] = startDate;
                    if ( typeof endDate !== 'undefined' ) params[0].inputs.studyEndDate.values[0] = endDate;
                    if ( typeof sequenceValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = sequenceValue;   
                    if ( typeof ciValue !== 'undefined' ) params[0].inputs.confidenceIntervalList.values[0].value = ciValue;;   
                    if ( typeof ci2Value !== 'undefined' ) params[0].inputs.confidenceIntervalList.values[1].value = ci2Value;                                  
                    break;    
                
                case "survivalAnalysisMatched":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.outcomeVariableSelection.values[0] = outcomeValue;               
                    if ( typeof sequenceValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = sequenceValue; 
                    //if ( typeof lastseenValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = lastseenValue; 
                    
                    break;     
                    
                case "survivalAnalysisUnmatched":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.outcomeVariableSelection.values[0] = outcomeValue;                
                    if ( typeof sequenceValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = sequenceValue;                                  
                    break;                  
                
                 case "logisticRegressionComplete":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.outcomeVariableSelection.values[0] = outcomeValue;                
                    if ( typeof sequenceValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = sequenceValue;                                  
                    break;                  
               
                case "propensityScoreOutcomes":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;                
                    if ( typeof sequenceValue !== 'undefined' ) params[0].inputs.sequencingVariableSelection.values[0] = sequenceValue;                                  
                    break;  
                
                case "iptw":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;
                    if ( typeof outcomeValue !== 'undefined' ) params[0].inputs.dependentVariableSelection.values[0] = outcomeValue;                                   
                    break;                
                
                case "propensityStratification":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;                                       
                    break;   
                    
                case "weightingByOdds":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;                                       
                    break;   
                    
                case "logisticRegressionAnalysis":
                    if ( typeof uniqueValue !== 'undefined' ) params[0].inputs.studyUniqueId.values[0] = uniqueValue;                                       
                    break;  
                
                
                default:
                    delta3.utils.GlobalFunc.showDeltaMessage(
                            'Method not supported: ' + theGrid.methodStore.getAt(popupTypeIndex).get('name')
                            );
            }  
        } catch (err) {
            delta3.utils.GlobalFunc.showDeltaMessage('Please clear Study parameters first.');
        }
        var newParams = JSON.stringify(params);
        context.record.data.methodParams = newParams;
    }
});

