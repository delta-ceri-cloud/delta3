/* 
 * Risk Adjusted SPRT Method Popup Window
 */

Ext.define('delta3.view.study.SPRTPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.methodSPRT',
    layout: 'fit',
    width: 500,
    height: 700,
    itemId: 'methodSPRTPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA',
    requires: [
          'delta3.model.IndependentVariableModel',
          'delta3.view.study.IndependentVariableSelector'
      ],    
    listeners: {
        beforedestroy: function(rowEditor, context, eOpt) {
            this.fieldStore.clearFilter(true);
        }
    },    
    labelWidth: 160,
    labelAlign: 'left',   
    step: 0,
    fieldStore: {},
    selectedStudyRecord: {},
    selectedFieldRecord: {},
    depVariable: {},
    params: {},
    fieldId: 0,
    newFieldId: {},
    modelId: {},
    studyId: {},
    items: [],
    buttons: [
        {
            text: 'Save',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodSPRTPopup')[0];
//                var genericIdCheckbox = Ext.ComponentQuery.query('#genericIdCheckbox')[0];                       
                var theIndepItems = Ext.ComponentQuery.query('#indepVariable')[0];        
                var listOfIndepVariables = theIndepItems.getRawValue();
                var arrayOfIndepVariables = listOfIndepVariables.split(',');          
                thisWin.params[thisWin.step].inputs.independentVariableSelection.values = [];              
                for (var i = 0; i < arrayOfIndepVariables.length; i++) {
                    thisWin.params[thisWin.step].inputs.independentVariableSelection.values[i] = arrayOfIndepVariables[i];
                }     
                var index = thisWin.selectedStudyRecord.get('idKey');
                thisWin.fieldStore.clearFilter(true);
                var uniqueField = thisWin.fieldStore.findRecord('idModelColumn', index);
//                if ( genericIdCheckbox.value === true ) {
//                    thisWin.params[thisWin.step].inputs.studyUniqueId.values[0] = 'id';
//                } else {
                    thisWin.params[thisWin.step].inputs.studyUniqueId.values[0] = uniqueField.get('name');
//                }
//                thisWin.selectedStudyRecord.set('isGenericId', genericIdCheckbox.value);                
                thisWin.params[thisWin.step].inputs.sequencingVariableSelection.values[0] = Ext.ComponentQuery.query('#seqVariable')[0].value;
                thisWin.params[thisWin.step].inputs.dataSplitOption.values[0] = Ext.ComponentQuery.query('#dataSplitOption')[0].value;               
                thisWin.params[thisWin.step].inputs.oddsRatio.values[0] = Ext.ComponentQuery.query('#oddsRatio')[0].value;                                        
                thisWin.params[thisWin.step].inputs.numberPeriods.values[0] = Ext.ComponentQuery.query('#numberPeriods')[0].value;
                thisWin.params[thisWin.step].inputs.alphaError.values[0] = Ext.ComponentQuery.query('#alphaError')[0].value;
                thisWin.params[thisWin.step].inputs.betaError.values[0] = Ext.ComponentQuery.query('#betaError')[0].value;  
                if ( typeof thisWin.params[thisWin.step].inputs.rollingWindowUse !== 'undefined' ) {
                    thisWin.params[thisWin.step].inputs.rollingWindowUse.values[0] = Ext.ComponentQuery.query('#rollingWindowUseCheckbox')[0].value;                       
                    if ( typeof thisWin.params[thisWin.step].inputs.rollingWindowTimeUnit !== 'undefined') {
                        thisWin.params[thisWin.step].inputs.rollingWindowTimeUnit.values[0] = Ext.ComponentQuery.query('#rollingWindowTimeUnit')[0].value; 
         
                    }              
                    if ( typeof thisWin.params[thisWin.step].inputs.rollingWindowUnitQuantity !== 'undefined' ) {   
                        thisWin.params[thisWin.step].inputs.rollingWindowUnitQuantity.values[0] = Ext.ComponentQuery.query('#rollingWindowUnitQuantity')[0].value;                 
                        
                    }
                }
                if ( typeof Ext.ComponentQuery.query('#avs')[0] !== 'undefined' ) {
                    thisWin.params[thisWin.step].inputs.automaticVariableSelection.values[0] = Ext.ComponentQuery.query('#avs')[0].value;  
                }
                thisWin.params[thisWin.step].inputs.dependentVariableSelection.values[0] = thisWin.depVariable;                    
                thisWin.selectedStudyRecord.set('idOutcome', delta3.utils.GlobalFunc.lookupFieldId(thisWin.depVariable));                   
                var newParams = JSON.stringify(thisWin.params);
                thisWin.selectedStudyRecord.set('methodParams', newParams);            
                var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                var rec = studyGrid.getStore().findRecord('idStudy', thisWin.studyId);      
                rec.set('status', 'New');                 
                studyGrid.getStore().sync();
                //thisWin.fieldStore.clearFilter(true);
                //thisWin.destroy();
                studyGrid.setLoading();
                // Clear the loading mask after 1.5 seconds
                setTimeout(function (target) {
                    target.setLoading(false);
                }, 1500, studyGrid);                
            }
        }, {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#methodSPRTPopup')[0];
                //thisWin.fieldStore.clearFilter(true);
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.fieldStore.clearFilter(true);   
        var methodComboBox = Ext.ComponentQuery.query('#methodComboBox')[0];
        var methodRec = methodComboBox.store.findRecord('idMethod', me.selectedStudyRecord.get('idMethod'));
        if ( me.modelId === 0 ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Data Model to the study first.");
            me.destroy();
        }         
        if ( methodRec === null || me.modelId === 0 ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign a Method to the study first.");
            me.fieldStore.clearFilter(true);
            me.destroy();
        }        
        me.selectedFieldRecord = me.fieldStore.findRecord('idModelColumn', me.fieldId);
        var index = me.selectedStudyRecord.get('idKey');
        var uniqueField = me.fieldStore.findRecord('idModelColumn', index); 
        if ( uniqueField === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Key Field to the study first.");
            me.destroy();
        } 
           
        index = me.selectedStudyRecord.get('idDate');
        if ( me.fieldStore.findRecord('idModelColumn', index) === null ) {
            delta3.utils.GlobalFunc.showDeltaMessage("Please assign Sequence Field to the study first.");
            me.destroy();            
        }     
        
        me.fieldStore.filter('fieldClass', 'Risk Factor');
        me.fieldStore.filter('idModel', me.modelId);
        me.fieldStore.filter('insertable', true);      
        var riskAdjustmentStore = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore,'name');

        var title = 'SPRT parameters for study "' + me.selectedStudyRecord.get('name') + '"';
        var studyParams = me.selectedStudyRecord.get('methodParams');
        var indepVariables = '';
        
        if ( (typeof studyParams !== 'undefined') && (studyParams !== null) && (studyParams !== "") ) {
            // params previously saved for the study
            me.params = JSON.parse(studyParams); 
            for (var i = 0; i < me.params[me.step].inputs.independentVariableSelection.values.length; i++) {
                if ( i > 0 ) {
                    indepVariables += ',';
                }
                indepVariables += me.params[me.step].inputs.independentVariableSelection.values[i];
            }           
        } else {
            // empty params string from template
            me.params = JSON.parse(methodRec.data.methodParams);
        }

        me.items[0] = new Ext.Panel(
                {
                    frame: true,                  
                    itemId: 'methodPropStep2Panel',
                    bodyStyle: 'padding:5px 5px 0',
                    title: title,
                    autoScroll: true,
                    defaultType: 'displayfield'
                }); 

        if ( me.params[me.step].inputs.studyUniqueId !== 'undefined' ) {
            me.items[0].add({
                allowBlank: !me.params[me.step].inputs.studyUniqueId.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                fieldLabel: me.params[me.step].inputs.studyUniqueId.inputText,
                itemId: 'studyId',
                allowBlank: true,
                value: uniqueField.get("name")
            });
//            me.items[0].add({
//                labelWidth: me.labelWidth,
//                labelAlign: me.labelAlign,                 
//                xtype: 'checkbox',
//                fieldLabel: 'Use generic Id',
//                inputValue: true,
//                value: me.selectedStudyRecord.get('isGenericId'),
//                itemId: 'genericIdCheckbox'                           
//            });
        }        
        if ( typeof me.params[me.step].inputs.dependentVariableSelection !== 'undefined' ) {
            me.depVariable = me.params[me.step].inputs.dependentVariableSelection.values[0];     
            me.fieldStore.filter('fieldClass', 'Outcome');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);         
            var outcomeDropdownValues = delta3.utils.GlobalFunc.createComboBoxStore(me.fieldStore, 'name');        
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.dependentVariableSelection.inputText,
                store: outcomeDropdownValues,
                itemId: 'depVariable',
                allowBlank: !me.params[me.step].inputs.dependentVariableSelection.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'value',
                valueField: 'value',
                queryMode: 'local',
                value: me.depVariable,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.depVariable = cmb.getValue();
                    }
                }
            });            
        }     
        
        me.fieldStore.clearFilter(true);        
        me.fieldStore.filter('fieldClass', 'Sequencer');
        me.fieldStore.filter('idModel', me.modelId);
        me.fieldStore.filter('insertable', true);          
        if ( me.params[me.step].inputs.sequencingVariableSelection !== 'undefined' ) {     
            var index = me.selectedStudyRecord.get('idDate');
            var sequenceField = me.fieldStore.findRecord('idModelColumn', index);     
            if ( sequenceField === null ) {
                delta3.utils.GlobalFunc.showDeltaMessage("Sequence Field is not a Sequencer class or does not exit.");
                me.destroy();                    
            }
            var currValue = sequenceField.get("name");                       
            me.items[0].add( {
                    fieldLabel: me.params[me.step].inputs.sequencingVariableSelection.inputText,
                    name: 'seqVariable',
                    itemId: 'seqVariable',
                    allowBlank: !me.params[me.step].inputs.sequencingVariableSelection.required,                            
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,                       
                    value: currValue
                });   
        }
        if ( me.params[me.step].inputs.oddsRatio !== 'undefined' ) {       
            var currValue = '';            
            if ( (typeof me.params[me.step].inputs.oddsRatio.values[0] !== 'undefined') && (me.params[me.step].inputs.oddsRatio.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.oddsRatio.values[0];
            }             
            me.items[0].add({
                xtype: 'textfield',
                fieldLabel: me.params[me.step].inputs.oddsRatio.inputText,
                tooltip: me.params[me.step].inputs.oddsRatio.decription, // this does not work
                itemId: 'oddsRatio',
                allowBlank: !me.params[me.step].inputs.oddsRatio.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: currValue
            });
        }        
        if ( me.params[me.step].inputs.alphaError !== 'undefined' ) {     
            var currValue = '';            
            if ( (typeof me.params[me.step].inputs.alphaError.values[0] !== 'undefined') && (me.params[me.step].inputs.alphaError.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.alphaError.values[0];
            }              
            me.items[0].add({
                xtype: 'textfield',
                fieldLabel: me.params[me.step].inputs.alphaError.inputText,
                itemId: 'alphaError',
                allowBlank: !me.params[me.step].inputs.alphaError.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: currValue
            });
        }       
        if ( me.params[me.step].inputs.betaError !== 'undefined' ) {    
            var currValue = '';            
            if ( (typeof me.params[me.step].inputs.betaError.values[0] !== 'undefined') && (me.params[me.step].inputs.betaError.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.betaError.values[0];
            }            
            me.items[0].add({
                xtype: 'textfield',
                fieldLabel: me.params[me.step].inputs.betaError.inputText,
                itemId: 'betaError',
                allowBlank: !me.params[me.step].inputs.betaError.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: currValue
            });
        }                
        if ( me.params[me.step].inputs.numberPeriods !== 'undefined' ) {                
            var currValue = '';            
            if ( (typeof me.params[me.step].inputs.numberPeriods.values[0] !== 'undefined') && (me.params[me.step].inputs.numberPeriods.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.numberPeriods.values[0];
            }             
            me.items[0].add({
                xtype: 'textfield',
                fieldLabel: me.params[me.step].inputs.numberPeriods.inputText,
                itemId: 'numberPeriods',
                allowBlank: !me.params[me.step].inputs.numberPeriods.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: currValue
            });
        }                     
        if ( me.params[me.step].inputs.dataSplitOption !== 'undefined' ) {    
            var currValue = '';
            var dropdownValues = {};
            if ( (typeof me.params[me.step].inputs.dataSplitOption.values[0] !== 'undefined') && (me.params[me.step].inputs.dataSplitOption.values[0] !== 'value0') ) {     
                currValue = me.params[me.step].inputs.dataSplitOption.values[0];
            }        
            if ( (me.params[me.step].inputs.dataSplitOption.type !== 'undefined') && (me.params[me.step].inputs.dataSplitOption.type === "MultipleUnique") ) {
                var values = me.params[me.step].inputs.dataSplitOption.availableValues;   
                var dataFields = [];
                var i = 0;
                while ( typeof values[i] !== 'undefined') {
                    var x = values[i][i+1];
                    dataFields[i++] = {period: x};
                }
                dropdownValues = Ext.create('Ext.data.Store', {              
                                fields: ['period'], 
                                data: dataFields 
                            });                
            }
            me.items[0].add({
                xtype: 'combobox',
                fieldLabel: me.params[me.step].inputs.dataSplitOption.inputText,
                store: dropdownValues,
                itemId: 'dataSplitOption',
                allowBlank: !me.params[me.step].inputs.dataSplitOption.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                displayField: 'period',
                valueField: 'period',
                queryMode: 'local',
                value: currValue,
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        me.newFieldId = cmb.getValue();
                    }
                }
            });
        }               
        if ( typeof me.params[me.step].inputs.independentVariableSelection !== 'undefined' ) {
            me.fieldStore.filter('fieldClass', 'Risk Factor');
            me.fieldStore.filter('idModel', me.modelId);
            me.fieldStore.filter('insertable', true);                 
            var riskFactorStore = delta3.utils.GlobalFunc.createLocalGridStore(me.fieldStore, delta3.model.IndependentVariableModel.getFields());
            me.items[0].add( 
               Ext.create('delta3.view.study.IndependentVariableSelector', {riskFactorStore: riskFactorStore, indepVariablesList: indepVariables})
            );        
        }           
        // following code for AVS allows study to retroactively pick this new parameter w/o clearing all params
        if ( typeof me.params[me.step].inputs.automaticVariableSelection !== 'undefined' ) {                
            var currValue = '';            
            if ( typeof me.params[me.step].inputs.automaticVariableSelection.values[0] !== 'undefined' ) {     
                currValue = me.params[me.step].inputs.automaticVariableSelection.values[0];
            }             
            me.items[0].add({
                xtype: 'checkbox',
                fieldLabel: me.params[me.step].inputs.automaticVariableSelection.inputText,
                itemId: 'avs',
                allowBlank: !me.params[me.step].inputs.automaticVariableSelection.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                   
                value: currValue
            });
        } else {
            if ( typeof me.methodParams[me.step].inputs.automaticVariableSelection !== 'undefined' ) {   
                me.params[me.step].inputs.automaticVariableSelection = me.methodParams[me.step].inputs.automaticVariableSelection;
                var currValue = '';            
                if ( typeof me.params[me.step].inputs.automaticVariableSelection.values[0] !== 'undefined' ) {     
                    currValue = me.params[me.step].inputs.automaticVariableSelection.values[0];
                }             
                me.items[0].add({
                    xtype: 'checkbox',
                    fieldLabel: me.params[me.step].inputs.automaticVariableSelection.inputText,
                    itemId: 'avs',
                    allowBlank: !me.params[me.step].inputs.automaticVariableSelection.required,                            
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,                   
                    value: currValue
                });                
            }
        }          
        if ( typeof me.params[me.step].inputs.rollingWindowUse !== 'undefined' ) {
            if ( (typeof me.params[me.step].inputs.rollingWindowTimeUnit === 'undefined')
                || (typeof me.params[me.step].inputs.rollingWindowUnitQuantity === 'undefined') ) {
                delta3.utils.GlobalFunc.showDeltaMessage('Rolling Window parameters are missing in the configuration file.');
                me.destroy(); 
            }
            me.items[0].add({
                allowBlank: !me.params[me.step].inputs.rollingWindowUse.required,                        
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,                 
                xtype: 'checkbox',
                fieldLabel: me.params[me.step].inputs.rollingWindowUse.inputText,
                inputValue: true,
                value: me.params[me.step].inputs.rollingWindowUse.values[0],
                listeners: {
                    render: function(c) {
                      new Ext.ToolTip({
                        target: c.getEl(),
                        html: me.params[me.step].inputs.rollingWindowUse.description
                      });
                    },
                    change: function(c) {
                        if ( c.getValue() === true ) {
                           Ext.ComponentQuery.query('#rollingWindowParams')[0].setVisible(true);  
                        } else {
                           Ext.ComponentQuery.query('#rollingWindowParams')[0].setVisible(false);  
                        }
                    }
                },                 
                itemId: 'rollingWindowUseCheckbox'                           
            });             
            me.items[0].add({
                xtype: 'fieldset', 
                layout: 'vbox', 
                itemId: 'rollingWindowParams',
                hidden: !Boolean(me.params[me.step].inputs.rollingWindowUse.values[0]),
                items:
                    [              
                        me.items[0].add(delta3.view.study.UtilFunc.multipleChoiceUI(me.params[me.step].inputs.rollingWindowTimeUnit,'rollingWindowTimeUnit', me)),
                        me.items[0].add(delta3.view.study.UtilFunc.numberUI(me.params[me.step].inputs.rollingWindowUnitQuantity, me, 'rollingWindowUnitQuantity'))
                    ]});   
        }         
        me.items[0].updateLayout();
        me.callParent();    
    }
}
);

