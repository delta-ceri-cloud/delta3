/* 
 * CERI 2015
 */

Ext.define('delta3.view.login.Login', {
    extend: 'Ext.window.Window',
    xtype: 'login',

    requires: [
        'delta3.controller.LoginController',
        'Ext.state.CookieProvider',
        'Ext.form.Panel',
        'Ext.state.Manager'
    ],

    controller: 'Login',
    title: 'DELTA Login',
    renderTo: 'loginDiv',
    bodyStyle: {
        padding: '30px 30px 30px'
    },       
    cls: 'login_style',
    width: 350,
    frame: false,
    header: true,
    layout: 'fit',
    fieldDefaults: {
        labelAlign: 'left',
        msgTarget: 'side'
    },
    closable: false,
    autoShow: true,
    listeners: {
        'render': function(p){ p.getKeyMap(); }
    },
    keys: {
         fn: function(key,e){
             this.target.component.controller.onLoginClick();
         },
         key: Ext.event.Event.ENTER            
    },
    items: {
        xtype: 'form',
        reference: 'form',
        items: [, {
            xtype: 'displayfield',
            hideEmptyLabel: false,
            value: 'Enter to authenticate'
        }, {
            xtype: 'textfield',
            name: 'UID',
            id: 'UID',
            margin: '5 5 5 5', 
            fieldLabel: 'User name',
            allowBlank: false
        }, {
            xtype: 'textfield',
            name: 'PWD',
            id: 'PWD',
            margin: '5 5 5 5', 
            inputType: 'password',
            fieldLabel: 'Password',
            allowBlank: false
        }],
        buttons: [{
            text: 'Login',
            formBind: true,
            listeners: {
                click: 'onLoginClick'
            }
        }]
    },
    initComponent: function() {
//        var cp = new Ext.state.CookieProvider({
//            path: "/Delta3/",
//            expires: new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 30)), //30 days
//            domain: "lahey.org"
//        });
//        Ext.state.Manager.setProvider(cp);  
        Ext.ComponentQuery.query('#deltaVersion')[0] = deltaVersion;
        Ext.ComponentQuery.query('#deltaDate')[0] = deltaDate;    
        this.callParent();
    }
});

