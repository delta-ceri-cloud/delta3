Ext.define('delta3.view.rv.common.PropDiffGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.propDiff',
    scrollable: true,
    viewConfig: {
        enableTextSelection: true,
        getRowClass: function(record, rowIndex, rowParams, store) {
            if (record.get('RowId') === "1")
                return 'firstRowBackground';
        }        
    },
    resultsData: {},
    height: delta3.utils.GlobalVars.tabHeight,
    requires: [
        'Ext.data.Store',
        'Ext.data.Model'
    ],
    layout: 'fit',
    width: '100%',
    columnLines: true,
    items: [],
    initComponent: function() {
        var me = this;
        var modelFields = delta3.view.rv.common.UtilFunc.buildGridModelFields('ProportionalDifferenceGraphOutput',me.resultsData);
        me.store = delta3.view.rv.common.UtilFunc.buildGridStore('ProportionalDifferenceGraphOutput',me.resultsData, modelFields);
        me.columns = eval("(" + delta3.view.rv.common.UtilFunc.buildGridColumns('ProportionalDifferenceGraphOutput',me.resultsData) + ")");
        me.tbar = []; 
        me.tbar[0] = {
            text: 'Export to CSV',
            iconCls: 'exportCSV-icon16',
            tooltip: delta3.utils.Tooltips.mbBtnExcel,                        
            handler: function(b, e) {
                b.up('grid').exportGrid('DELTA ' + me.title);
            }
        };        
        me.callParent();
    }
});


