/* 
 * set of functions to help render DELTA graphs
 */

Ext.define('delta3.view.rv.common.UtilFunc', {
    statics: {
        buildPropDiffChartStore: function(resultsData) {
           //var modelFields = '["id","time","low","medium","high","alert"]';  
           //var storeFields = JSON.parse(modelFields);                       
           var resultsArray = JSON.parse(resultsData);
           var resultStoreArray = [];
           var resultStore;
           var data;
           var dataRow;
           var resultCounter = 0;

           for (var i=0; i<resultsArray.results.length; i++) {      
               if (resultsArray.results[i].name === 'ProportionalDifferenceGraphOutput') { 
                    resultStore = {fields: []}; 
                    var modelFields = '["id","time","low","medium","high","alert"]'; 
                    var storeFields = JSON.parse(modelFields);     
                    storeFields[findColumn(resultsArray.results[i], 'rowid')] = "id";        
                    storeFields[findColumn(resultsArray.results[i], 'period')] = "time";                
                    storeFields[findColumn(resultsArray.results[i], 'upper')] = "high";
                    storeFields[findColumn(resultsArray.results[i], 'lower')] = "low";                   
                    storeFields[findColumn(resultsArray.results[i], 'proportional')] = "medium";     
                    data = '[';
                    for (var j=0; j<resultsArray.results[i].data.length; j++) {   
                         if ( j > 0 ) {
                             data += ',';
                         }
                         dataRow = '{';
                         for (var z=0; z<resultsArray.results[i].data[j].length; z++) {
                            if ( z > 0 ) {
                                dataRow += ',';
                            }  
                            if ( resultsArray.results[i].data[j][z] === 'NaN' ) {
                                resultsArray.results[i].data[j][z] = '""';
                            }                        
                            if  ( storeFields[z] === "time" ) {
                                dataRow += '"' + storeFields[z] + '":' + '"' + resultsArray.results[i].data[j][z] + '"';     
                            } else {
                                dataRow += '"' + storeFields[z] + '":' + resultsArray.results[i].data[j][z];  
                            }
                         }
                         dataRow += ',"zero":0,"open":0,"close":0,"alert":' + this.getVisualAlert(resultsArray.results[i].alerts, j) + '}';
                         data += dataRow;
                    }
                    data += ']';
                    var storeData = JSON.parse(data);
                    resultStore.data = storeData;
                    // following loop is to notify viewer, by changing color, about unusual record
                    // candle graph, used out of the box, requires this artificial operation 
                    // to be done before graph is instantiated
                    for (var z=0; z<resultStore.data.length; z++) {
                        if ( typeof resultStore.data[z].alert.type === 'undefined' ) {
                            resultStore.data[z].open = resultStore.data[z].medium - 0.001;
                            resultStore.data[z].close = resultStore.data[z].medium + 0.001;
                        } else {
                            resultStore.data[z].open = resultStore.data[z].medium + 0.001;
                            resultStore.data[z].close = resultStore.data[z].medium - 0.001;            
                        }          
                    }                     
                    resultStoreArray[resultCounter++] = resultStore;
               }
           }
           return resultStoreArray;        
           
           function findColumn(results, phrase) {
                for (var i=0; i<results.columns.length; i++) {
                    if ( results.columns[i].name.toLowerCase().indexOf(phrase) !== -1 )
                        return i;
                }
                return 0;
           }
        },
        buildObservedExpectedChartStore: function(resultsData) {                  
           var resultsArray = JSON.parse(resultsData);
           var resultStoreArray = [];           
           var resultStore;         
           var data;
           var dataRow;
           var resultCounter = 0;

           for (var i=0; i<resultsArray.results.length; i++) {      
               if (resultsArray.results[i].name === 'ObservedExpectedGraphOutput') { 
                    resultStore = {fields: []};   
                    var modelFields = '["rowId","time","obsCount","obsEvent","obsHigh","obsLow","obsMedium","expCount","expEvent","expHigh","expLow","expMedium","obs2High","obs2Low","exp2High","exp2Low"]';  
                    var storeFields = JSON.parse(modelFields);                       
                    storeFields[findColumn(resultsArray.results[i], 'rowid')] = "id";        
                    storeFields[findColumn(resultsArray.results[i], 'period')] = "time";                
                    storeFields[findColumn(resultsArray.results[i], 'observed count')] = "obsCount";
                    storeFields[findColumn(resultsArray.results[i], 'observed event')] = "obsEvent";                   
                    storeFields[findColumn(resultsArray.results[i], 'observed upper confidence interval')] = "obsHigh";    
                    storeFields[findColumn(resultsArray.results[i], 'observed lower confidence interval')] = "obsLow";    
                    storeFields[findColumn(resultsArray.results[i], 'observed proportion')] = "obsMedium";    
                    storeFields[findColumn(resultsArray.results[i], 'expected count')] = "expCount";
                    storeFields[findColumn(resultsArray.results[i], 'expected event')] = "expEvent";                   
                    storeFields[findColumn(resultsArray.results[i], 'expected upper confidence interval')] = "expHigh";    
                    storeFields[findColumn(resultsArray.results[i], 'expected lower confidence interval')] = "expLow";    
                    storeFields[findColumn(resultsArray.results[i], 'expected proportion')] = "expMedium";          

                    storeFields[findColumn(resultsArray.results[i], 'observed secondary upper confidence interval')] = "obs2High";    
                    storeFields[findColumn(resultsArray.results[i], 'observed secondary lower confidence interval')] = "obs2Low";    
                    storeFields[findColumn(resultsArray.results[i], 'expected secondary upper confidence interval')] = "exp2High";    
                    storeFields[findColumn(resultsArray.results[i], 'expected secondary lower confidence interval')] = "exp2Low";                        
                    data = '[';
                    for (var j=0; j<resultsArray.results[i].data.length; j++) {   
                         if ( j > 0 ) {
                             data += ',';
                         }
                         dataRow = '{';
                         for (var z=0; z<resultsArray.results[i].data[j].length; z++) {
                            if ( z > 0 ) {
                                dataRow += ',';
                            }  
                            if ( resultsArray.results[i].data[j][z] === 'NaN' ) {
                                resultsArray.results[i].data[j][z] = '""';
                            }                        
                            if  ( storeFields[z] === "time" ) {
                                dataRow += '"' + storeFields[z] + '":' + '"' + resultsArray.results[i].data[j][z] + '"';     
                            } else {
                                if ( storeFields[z] === "rowId" || storeFields[z] === "time" || storeFields[z] === "expCount" || storeFields[z] === "obsCount") {
                                    dataRow += '"' + storeFields[z] + '":' + resultsArray.results[i].data[j][z];  
                                } else {
                                    dataRow += '"' + storeFields[z] + '":' + Math.round(1000 * resultsArray.results[i].data[j][z])/10;  
                                }
                            }
                         }
                         dataRow += ',"zero":0,"open":0,"close":0,"alert":' + this.getVisualAlert(resultsArray.results[i].alerts, j) + '}';
                         data += dataRow;
                    }
                data += ']';

                var storeData = JSON.parse(data);
                resultStore.data = storeData;  
                // following loop is to notify viewer, by changing color, about unusual record
                // candle graph, used out of the box, requires this artificial operation 
                // to be done before graph is instantiated                
                for (var z=0; z<resultStore.data.length; z++) {
                    if ( typeof resultStore.data[z].alert.type === 'undefined' ) {
                        resultStore.data[z].open = resultStore.data[z].obsMedium - 0.001;
                        resultStore.data[z].close = resultStore.data[z].obsMedium + 0.001;
                    } else {
                        resultStore.data[z].open = resultStore.data[z].obsMedium + 0.001;
                        resultStore.data[z].close = resultStore.data[z].obsMedium - 0.001;            
                    }          
                }                
                resultStoreArray[resultCounter++] = resultStore; // this allows multiple results sets to be processed
               }
           }    
           return resultStoreArray;        
        },           
        buildRiskAdjustedSPRTChartStore: function(resultsData) {          
           var resultsArray = JSON.parse(resultsData);
           var resultStore;          
           var data;

            for (var i=0; i<resultsArray.results.length; i++) {               
               if (resultsArray.results[i].name === 'RaSprtGraphOutput') {  
                    resultStore = {fields: []};   
                    data = '[';
                    for (var j=0; j<resultsArray.results[i].data.length; j++) {   
                        if ( j > 0 ) {
                            data += ',';
                        }
                        data += '{"RowId":';
                        data += resultsArray.results[i].data[j][0];
                        var lower = resultsArray.results[i].columns[1].metaData.lowerBound;
                        var upper = resultsArray.results[i].columns[1].metaData.upperBound;
                        data += ',"riskAdjustedValue":' + resultsArray.results[i].data[j][1];
                        data += ',"upper":' + upper + ',"lower":' + lower + ',"alert":""}';
                    }
                    data += ']';
                    var storeData = JSON.parse(data);
                    resultStore.data = storeData;
               }
           }
           return resultStore;        
        },         
        checkIfResultsPresent: function(resultsData, resultType) {
           var resultsArray = JSON.parse(resultsData);     
           for (var i=0; i<resultsArray.results.length; i++) {      
               if (resultsArray.results[i].name === resultType) { 
                   return true;
               }
           }
           return false;             
        },
        buildGraphPanel: function(title,chart) {
            return new Ext.Panel(
                    {
                        frame: true,
                        labelAlign: 'right',                    
                        //itemId: 'cumPropGraphPanel',
                        bodyStyle: 'padding:5px 5px 0',
                        title: title,
                        autoScroll: true,
                        dockedItems: [{
                            xtype: 'toolbar',
                            docked: 'top',
                            items: [{
                                xtype: 'button',
                                iconCls: 'download-image-icon16',
                                text: 'Download PNG',
                                handler: function() {
                                    chart.download({filename: "DELTA " + title, format: "png"});
                                }
                            }, {
                                xtype: 'button',
                                iconCls: 'download-pdf-icon16',
                                text: 'Download JPEG',
                                handler: function() {
                                    chart.download({filename: "DELTA " + title, format: "jpeg"});
                                }
                            }, {
                                xtype: 'label',
                                width: 50,
                                text: 'Width:',
                                margin: '0 0 0 10'
                            }, {
                                xtype: 'textfield',
                                width: 50,
                                itemId: 'wipText',
                                name: 'wipText',
                                value: chart.chartWidth,
                                emptyText: 'Width in pixels'
                            },  {
                                xtype: 'label',
                                width: 50,
                                text: 'Height:',
                                margin: '0 0 0 10'
                            }, {
                                xtype: 'textfield',
                                width: 50,
                                itemId: 'hipText',
                                name: 'hipText',
                                value: chart.chartHeight,
                                emptyText: 'Height in pixels'
                            }, {
                                xtype: 'button',
                                text: 'Resize',
                                handler: function() {
                                    chart.chartWidth = this.up().down('#wipText').getValue();
                                    chart.chartHeight = this.up().down('#hipText').getValue();   
                                    //this.up().remove(chart);
                                    //this.up().add(chart);
                                    chart.getMarkup();
                                }
                            }]
                        }],
                        defaultType: 'displayfield'
                    }); 
        },
        getVisualAlert: function(alertArray, index) {
            if ( typeof alertArray === 'undefined' || alertArray.length === 0 )  return '""';
            var maxTypeOffset = 0;
            var maxType;
            var hit = false;
            for (var i=0; i<alertArray.length; i++) {
                // pick the highest type among all alert types for this row
                if ( alertArray[i].idRow === index ) {
                    if ( hit === false ) {
                        maxType = alertArray[i].type;
                        hit = true;
                    }
                    if ( alertArray[i].type >= maxType ) {
                        maxType = alertArray[i].type;
                        maxTypeOffset = i;
                    }
                }
            }
            if ( hit === true ) {
                return JSON.stringify(alertArray[maxTypeOffset]);
            } else {
                return '""';
            }
        },
    buildGridStore: function(type, resultsData, modelFields) {
        var resultsArray = JSON.parse(resultsData);
        var resultCounter = 0;
        var data = '{"dsData":[';
        var dataRow;

        for (var i = 0; i < resultsArray.results.length; i++) {
            if (resultsArray.results[i].name === type) {
                if ( resultCounter > 0 ) {
                    data += ',';
                }
                for (var j = 0; j < resultsArray.results[i].data.length; j++) {
                    if (j > 0) {
                        data += ',';
                    }
                    dataRow = '[';
                    for (var z = 0; z < resultsArray.results[i].data[j].length; z++) {
                        if (z > 0) {
                            dataRow += ',';
                        }
                        dataRow += '"' + resultsArray.results[i].data[j][z] + '"';
                    }
                    dataRow += ']';
                    data += dataRow;
                }
                resultCounter++;
            }
        }
        data += ']}';

        var fields = eval("(" + modelFields + ")");
        var model = Ext.define(null, {
            extend: 'Ext.data.Model',
            fields: fields
        });

        var store = new Ext.data.Store({
            autoLoad: true,
            model: model,
            data: {},
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'dsData'
                }
            }
        });

        store.loadRawData(eval("(" + data + ")")); // re-load data since metadata changed
        return store;
    },
    buildGridModelFields: function(type, resultsData) {
        var resultsArray = JSON.parse(resultsData);
        var j, i;
        for (j = 0; j < resultsArray.results.length; j++) {
            if (resultsArray.results[j].name === type) {
                var hString = '[';
                for (i = 0; i < resultsArray.results[j].columns.length; i++) {
                    if (i > 0) {
                        hString += ',';
                    }
                    hString += '{"name":"' + resultsArray.results[j].columns[i].name + '", "type":"string"}';
                }
                return hString += ']';
            }
        }
        return null;
    },
    buildGridColumns: function(type, resultsData) {
        var resultsArray = JSON.parse(resultsData);
        var hString = '[';
        var j, i;
        for (j = 0; j < resultsArray.results.length; j++) {
            if (resultsArray.results[j].name === type) {
                var hString = '[';
                for (i = 0; i < resultsArray.results[j].columns.length; i++) {
                    if (i > 0) {
                        hString += ',';
                    }
                    hString += '{"text":"' + resultsArray.results[j].columns[i].name + '", "dataIndex":"' + resultsArray.results[j].columns[i].name + '", "type":"string", "width": 130}';
                }
                return hString += ']';
            }
        }
        return null;
    },
    setDPI: function(canvas, dpi) {
        // Set up CSS size if it's not set up already
        if (!canvas.style.width)
            canvas.style.width = canvas.width + 'px';
        if (!canvas.style.height)
            canvas.style.height = canvas.height + 'px';

        var scaleFactor = dpi / 96;
        canvas.width = Math.ceil(canvas.width * scaleFactor);
        canvas.height = Math.ceil(canvas.height * scaleFactor);
        var ctx = canvas.getContext('2d');
        ctx.scale(scaleFactor, scaleFactor);
    }    
    }
});


