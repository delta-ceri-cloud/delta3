/**
 * Observed Expected Chart
 */
var tipWidthOE = 180;
var tipHeightOE = 46;

Ext.define('delta3.view.rv.extjs.ObservedExpectedChart', {
    animate: true,
    extend: 'Ext.chart.CartesianChart',
    requires: [       
        'Ext.chart.series.CandleStick',
        'Ext.chart.CartesianChart',
        'Ext.chart.series.Line',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.interactions.PanZoom',
        'Ext.chart.interactions.Crosshair'
    ],    
    height: 460,
    width: '100%',
    insetPadding: 20,
    innerPadding: 20,    
    yAxisTitle: "Cumulative ",
    sprites: [{
        id: 'spriteTitle',
        type  : 'text',
        //font: '14px Arial',
        text: 'Chart Title',
        x: 200,
        y: 16,
        width: 200,
        height: 40 
    }],        
    background: 'white',
    interactions: [
         {
             type: 'panzoom',
             enabled: false,
             zoomOnPanGesture: false,
             axes: {
                 left: {
                     allowPan: false,
                     allowZoom: false
                 },
                 bottom: {
                     allowPan: true,
                     allowZoom: true
                 }
             }
         },
         {
             type: 'crosshair'
         }
    ],             
    initComponent: function() {
        var me = this;
        me.turnOnAlerts(me.store);        
        me.axes[0].setTitle(me.yAxisTitle);
        var min = me.calcStoreMinYValue(me.store);
        var max = me.calcStoreMaxYValue(me.store)
        me.axes[0].setMinimum(min - min/10);
        me.axes[0].setMaximum(max + max/10);
        me.callParent();    
    },
    axes: [{
        type: 'numeric',
        position: 'left',
        fields: ['expCount', 'obsLow', 'obsMedium', 'obsHigh', 'expLow', 'expMedium', 'expHigh', 'exp2Low', 'exp2High'],
        grid: true
    }, {
        type: 'category',
        position: 'bottom',
        fields: ['time'],
        title: 'Time',
        grid: false
    }],
    series: [
        {
                type: 'line',               
                fill: false,
                style: {
                    smooth: true,
                    opacity: 0.7,
                    //fill: "#FF99CC",
                    stroke: "#FF99CC",
                    lineWidth: 1
                },
                title: 'Secondary Upper Confidence Limit',
                highlight: {
                    scale: 0.9
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidthOE,
                    height: tipHeightOE,
                    renderer: function (storeItem, item) {
                        displayExpObsTip(this, storeItem, item);
                    }
                },                            
                xField: 'time',
                yField: 'exp2High'
            },{
                type: 'line',
                fill: false,
                style: {
                    smooth: true,
                    opacity: 1,
                    //fill: "#FFFFFF",
                    stroke: "#FF99CC",                                
                    lineWidth: 1
                },
                title: 'Secondary Lower Confidence Limit',
                highlight: {
                    scale: 0.9
                },                                    
                xField: 'time',
                yField: 'exp2Low'
            }, {
                type: 'line',
                fill: false,
                style: {
                    smooth: true,
                    opacity: 0.7,
                    //fill: "#CCE6FF",
                    stroke: "#3399FF",
                    lineWidth: 1
                },
                title: 'Primary Upper Confidence Limit',
                highlight: {
                    scale: 0.9
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidthOE,
                    height: tipHeightOE,
                    renderer: function (storeItem, item) {
                        displayExpObsTip(this, storeItem, item);
                    }
                },                            
                xField: 'time',
                yField: 'expHigh'
            }, {
                type: 'line',
                fill: false,
                style: {
                    smooth: true,
                    opacity: 0.9,
                    //fill: "#DD99FF",
                    stroke: "#3399FF",
                    //'stroke-width': 3, // does not work according ro Sencha!
                    //'stroke-dasharray': 10,                     
                    lineWidth: 2
                },
                title: 'Expected Proportion',
                highlight: {
                    scale: 0.9
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidthOE,
                    height: tipHeightOE,
                    renderer: function (storeItem, item) {
                        displayExpObsTip(this, storeItem, item);
                    }
                },                            
                xField: 'time',
                yField: 'expMedium'
            }, {
                type: 'line',
                fill: false,
                style: {
                    smooth: true,
                    opacity: 0.7,
                    //fill: "#CCE6FF",
                    stroke: "#3399FF",
                    lineWidth: 1
                },
                title: 'Primary Lower Confidence Limit',
                highlight: {
                    scale: 0.9
                },
                tooltip: {
                    trackMouse: true,
                    width: tipWidthOE,
                    height: tipHeightOE,
                    renderer: function (storeItem, item) {
                        displayExpObsTip(this, storeItem, item);
                    }
                },                            
                xField: 'time',
                yField: 'expLow'
            }, /*{
                type: 'line',
                fill: false,
                style: {
                    smooth: true,
                    opacity: 0.9,
                    stroke: "none",
                    lineWidth: 1
                },
                title: 'Observed',
                highlight: {
                    scale: 0.9
                },
                marker: {
                    type: 'circle',              
                    r: 12,
                    fill: 'green',
                    scale: 0.7
                },                
                tooltip: {
                    trackMouse: true,
                    width: tipWidthOE,
                    height: tipHeightOE,
                    renderer: function (storeItem, item) {
                        displayExpObsTip(this, storeItem, item);
                    }
                },                            
                xField: 'time',
                yField: 'obsMedium'           
            }, */{
                type: 'candlestick',
                xField: 'time',
                openField: 'open',
                highField: 'obsHigh',
                lowField: 'obsLow',
                closeField: 'close',
                tooltip: {
                    trackMouse: true,
                    width: tipWidthOE,
                    height: tipHeightOE,
                    renderer: function (storeItem, item) {
                        displayExpObsTip(this, storeItem, item);
                    }
                },                 
                style: {
                    barWidth: 10,
                    barHeight: 10,
                    opacity: 0.9,
                    dropStyle: {
                        fill: '#ff0000',
                        stroke: '#ff0000',
                        'stroke-width': 2
                    },
                    raiseStyle: {
                        fill: '#333333',
                        stroke: '#333333',
                        'stroke-width': 2
                    }
                },                
                aggregator: {
                    strategy: 'time'
                }
            }
        ],
    calcStoreMinYValue: function (store) {
        var bottomMargin = 1;
        var tmp = store.getAt(0).data.obsLow;
        for (var i=0; i< store.data.length; i++) {
            if (store.getAt(i).data.obsLow < tmp) {
                tmp = store.getAt(i).data.obsLow;
            }         
            if (store.getAt(i).data.expLow < tmp) {
                tmp = store.getAt(i).data.expLow;
            }                  
        }
        return tmp - bottomMargin;
    },
    calcStoreMaxYValue: function (store) {
        var topMargin = 1;
        var tmp = store.getAt(0).data.obsHigh;
        for (var i=0; i< store.data.length; i++) {
            if (store.getAt(i).data.obsHigh > tmp) {
                tmp = store.getAt(i).data.obsHigh;
            }    
            if (store.getAt(i).data.expHigh > tmp) {
                tmp = store.getAt(i).data.expHigh;
            }             
        }
        return tmp + topMargin;
    },
    setGraphTitle: function(container) {
        var surface = this.getSurface('chart');
        var sprite = surface.get('spriteTitle');
        var title = container.name;
        if ( container.description !== '' && container.description !== null ) {
            title += ' - ' + container.description;
        }
        if ( container.filterName !== '' && container.filterName !== null ) {
            title += ' - Filter [' + container.filterName + ']';
        }
        sprite.setText(title);         
    },
    turnOnAlerts: function(store) {
       // following loop is to notify viewer, by changing color, about unusual record
       // candle graph, used out of the box, requires this artificial operation 
       // unfortunately it does not work when eveked from       
       for (var i=0; i<store.data.length; i++) {
            if ( typeof store.getAt(i).data.alert.type === 'undefined' ) {
                store.getAt(i).data.open = store.getAt(i).data.obsMedium - 0.001;
                store.getAt(i).data.close = store.getAt(i).data.obsMedium + 0.001;
            } else {
                store.getAt(i).data.open = store.getAt(i).data.obsMedium + 0.001;
                store.getAt(i).data.close = store.getAt(i).data.obsMedium - 0.001;            
            }          
        }        
    }
    });
  
function displayExpObsTip (tip, storeItem, item) {
    if (typeof storeItem.get('alert').description === 'undefined') {
        tip.height = tipHeightOE;        
        tip.setTitle(storeItem.get('time') + '<br>  Exp: ' 
               + storeItem.get('expLow') + ', '   
               + storeItem.get('expMedium') + ', '     
               + storeItem.get('expHigh') + ' '
               + '<br>  Obs: ' 
               + storeItem.get('obsLow') + ', '   
               + storeItem.get('obsMedium') + ', '     
               + storeItem.get('obsHigh') + ' '
           );
    } else {
        tip.height = tipHeightOE + 10;
        tip.setTitle(storeItem.get('alert').description + '<br>'
               + storeItem.get('time') + '<br>  Exp: ' 
               + storeItem.get('expLow') + ', '   
               + storeItem.get('expMedium') + ', '     
               + storeItem.get('expHigh') + ' '
               + '<br>  Obs: ' 
               + storeItem.get('obsLow') + ', '   
               + storeItem.get('obsMedium') + ', '     
               + storeItem.get('obsHigh') + ' '
           );        
    }
}