/* 
 * Stat Grid
 */

Ext.define('delta3.view.statPackage.StatGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.stat',
    itemId: 'statGrid',
    autoScroll: false,
    renderTo: document.body,
    minHeight: 170,    
    //height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'delta3.model.StatModel',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator'     
    ],
    listeners: {
        selectionchange: function (view, selections, options) {
            if ( typeof selections[0] !== 'undefined' ) {
                var methodGrid = Ext.ComponentQuery.query('#methodItemGrid')[0];
                methodGrid.setHidden(false);  
                methodGrid.setTitle('Methods for package ' + selections[0].data.name + ' ID ' + selections[0].data.idStat);
                var methodStore = methodGrid.getStore();
                var theFilter = new Ext.util.Filter({
                    property: 'idStat',
                    value   : selections[0].data.idStat,
                    anyMatch: true,
                    caseSensitive: false
                });
                methodStore.filter(theFilter);            
            }
        }
    },    
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                edit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#statGrid')[0];
                    thisGrid.store.save();
                }
            }
        })
    ],
    border: false,
    dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',            
            items: [{
                text: 'Edit',
                iconCls: 'spec-icon16',
                menu: Ext.create('Ext.menu.Menu', {
                    margin: '0 0 10 0',
                    items: [{
                        text: 'Add Stat Package',
                        //iconCls: 'add_new_config-icon16',
                        iconCls: 'add_new-icon16',
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#statGrid')[0];
                            thisGrid.plugins[0].cancelEdit();
                            // Create a new record instance
                            var r = delta3.model.StatModel.create({
                                name: '',
                                description: 'New Stat Package',
                                methodParams: '',
                                createdTS: '0000-00-00 00:00:00.0',
                                updatedTS: '0000-00-00 00:00:00.0'});
                            thisGrid.store.insert(0, r);
                            thisGrid.plugins[0].startEdit(0, 0);
                        }
                    }, {
                        text: 'Edit Stat Package',
                        iconCls: 'edit-icon16',
                        tooltip: delta3.utils.Tooltips.resultBtnEdit,                        
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#statGrid')[0];                           
                            var sel = thisGrid.getSelectionModel().getSelection()[0];
                            if (typeof sel === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Stat Package first.');
                                return;
                            }   
                            thisGrid.plugins[0].startEdit(sel, 0);
                        }
                    }, {
                        itemId: 'StatDelete',
                        text: 'Delete',
                        iconCls: 'delete-icon16',
                        tooltip: delta3.utils.Tooltips.resultBtnDelete,                     
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#statGrid')[0];                            
                            var sel = thisGrid.getSelectionModel().getSelection()[0];
                            if (typeof sel === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Stat Package first.');
                                return;
                            }                        
                            Ext.Msg.show({
                                title:'DELTA',
                                message: 'You are about to delete Stat Package. There maybe studies configured to use this Stat Package. Would you like to proceed?',
                                buttons: Ext.Msg.YESNO,
                                icon: Ext.Msg.QUESTION,
                                fn: function(btn) {                          
                                    if (btn === 'yes') {
                                        var sm = thisGrid.getSelectionModel();
                                        thisGrid.plugins[0].cancelEdit();
                                        thisGrid.store.remove(sm.getSelection());
                                        thisGrid.store.sync();
                                    }
                                }
                            });                    
                        }
                    }, {
                        itemId: 'recordInfo',
                        text: 'View Properties',
                        iconCls: 'recordInfo-icon16',
                        tooltip: delta3.utils.Tooltips.resultBtnProperties,                    
                        handler: function() {
                            var thisGrid = Ext.ComponentQuery.query('#statGrid')[0]
                            var sm = thisGrid.getSelectionModel();
                            thisGrid.plugins[0].cancelEdit();
                            if (typeof sm.getSelection()[0] === 'undefined') {
                                delta3.utils.GlobalFunc.showDeltaMessage('Please select Stat Package first.');
                                return;
                            }                            
                            var win = Ext.create('delta3.view.popup.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "Process"});
                            win.show();
                        }
                    }]
                })
            }, /*{
                itemId: 'initializeMethodRemote',
                text: 'Initialize Remote',
                iconCls: 'initialize_remote-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#statGrid')[0];                            
                    var sel = thisGrid.getSelectionModel().getSelection()[0];
                    if (typeof sel === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Stat Package first.');
                        return;
                    } 
                    if ( sel.data.shortName === 'DELTAlytics' ) {
                        delta3.utils.GlobalFunc.showDeltaMessage('Remote initialization of DELTAlytics not implemented');
                        return;                    
                    }                
                    Ext.Msg.show({
                        title:'DELTA',
                        message: 'You are about to initialize Stat Package configuration.<br>This may invalidate study paramaters. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                delta3.utils.GlobalFunc.doInitializeConfig("remote", sel.data.idStat, sel.data.shortName);
                            }
                        }
                    });                
                }
            }, */{
                itemId: 'initializeMethodLocal',
                text: 'Initialize',
                iconCls: 'initialize_local-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#statGrid')[0];                            
                    var sel = thisGrid.getSelectionModel().getSelection()[0];
                    if (typeof sel === 'undefined') {
                        delta3.utils.GlobalFunc.showDeltaMessage('Please select Stat Package first.');
                        return;
                    }                 
                    Ext.Msg.show({
                        title:'DELTA',
                        message: 'You are about to initialize DELTA configuration.<br>This may invalidate study paramaters. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                delta3.utils.GlobalFunc.doInitializeConfig("local", sel.data.idStat, sel.data.shortName, refreshBottomPanel);
                                function refreshBottomPanel() {
                                    var methodGrid = Ext.ComponentQuery.query('#methodItemGrid')[0];
                                    methodGrid.getStore().reload();
                                }
                            }
                        }
                    }); 
                }
            }, {
                itemId: 'refreshMethod',
                text: 'Refresh',
                iconCls: 'refresh-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#statGrid')[0]
                    thisGrid.store.load();
                }
            }]
        },{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No methods found',
            displayInfo: true
    }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.dockedItems[1].store = me.store;
        me.store.load();        
        me.columns = me.buildColumns();
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];  
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me})); 
    },
    buildColumns: function() {
        return [
            {text: 'ID', dataIndex: 'idStat', width: 40, locked: true},
            {text: 'Stat Package', dataIndex: 'shortName', width: 100, editor:
                new Ext.form.ComboBox({
                    store: delta3.utils.GlobalVars.statPackageComboBoxStore,
                    displayField: 'type',
                    valueField: 'type',
                    emptyText: '',
                    queryMode: 'local',
                    forceSelection: true,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            var h = cmb.getValue();
                            var thisGrid = Ext.ComponentQuery.query('#statGrid')[0];
                            var rec = thisGrid.store.getNewRecords();
                            if (rec.length > 0) { // process only new record
                                rec[0].data.type = h;
                            }                            
                        }
                    }
                })
            },           
            {text: 'Name', dataIndex: 'name', editor: 'textfield', width: 120, locked: true},
            {text: 'Description', dataIndex: 'description', editor: 'textfield', width: 120},
            {text: 'Config Version', dataIndex: 'configVersion', width: 100},            
            {text: 'Use Proxy', disabled: true, xtype: 'checkcolumn', dataIndex: 'useProxy',  disabled: true, editor: 'checkboxfield', width: 100},
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active',  disabled: true, editor: 'checkboxfield', width: 100},                  
            {text: 'Configuration', dataIndex: 'configuration', editor: 'textfield', width: 650}
        ];
    },
    setActiveRecord: function(record) {
        this.activeRecord = record;
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },
    buildStore: function() {
        return Ext.create('delta3.store.StatStore');
    }

});


