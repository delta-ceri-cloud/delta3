/* 
 * MethodItem Grid
 */

Ext.define('delta3.view.statPackage.MethodItemGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.methodItem',
    itemId: 'methodItemGrid',
    autoScroll: false,
    renderTo: document.body,
    minHeight: 170,    
    //height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator'     
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                edit: function(rowEditor, changes, r) {
                    var thisGrid = Ext.ComponentQuery.query('#methodItemGrid')[0];
                    thisGrid.store.save();
                }
            }
        })
    ],
    border: true,
//    tbar: [{
//            text: 'Add Method',
//            iconCls: 'add_new_config-icon16',
//            handler: function() {
//                var thisGrid = Ext.ComponentQuery.query('#methodGrid')[0];
//                thisGrid.plugins[0].cancelEdit();
//                // Create a new record instance
//                var r = delta3.model.MethodModel.create({
//                    name: '',
//                    description: 'New Method',
//                    methodParams: '',
//                    createdTS: '0000-00-00 00:00:00.0',
//                    updatedTS: '0000-00-00 00:00:00.0'});
//                thisGrid.store.insert(0, r);
//                thisGrid.plugins[0].startEdit(0, 0);
//            }
//        }, {
//            itemId: 'initializeMethodRemote',
//            text: 'Initialize Remote',
//            iconCls: 'initialize_remote-icon16',
//            handler: function() {
//                Ext.Msg.show({
//                    title:'DELTA',
//                    message: 'You are about to initialize DELTA configuration.<br>This may invalidate study paramaters. Would you like to proceed?',
//                    buttons: Ext.Msg.YESNO,
//                    icon: Ext.Msg.QUESTION,
//                    fn: function(btn) {
//                        if (btn === 'yes') {
//                            delta3.utils.GlobalFunc.doInitializeConfig("remote");
//                        }
//                    }
//                });                
//            }
//        }, {
//            itemId: 'initializeMethodLocal',
//            text: 'Initialize Local',
//            iconCls: 'initialize_local-icon16',
//            handler: function() {
//                Ext.Msg.show({
//                    title:'DELTA',
//                    message: 'You are about to initialize DELTA configuration.<br>This may invalidate study paramaters. Would you like to proceed?',
//                    buttons: Ext.Msg.YESNO,
//                    icon: Ext.Msg.QUESTION,
//                    fn: function(btn) {
//                        if (btn === 'yes') {
//                            delta3.utils.GlobalFunc.doInitializeConfig("local");
//                        }
//                    }
//                }); 
//            }
//        }, {
//            itemId: 'refreshMethod',
//            text: 'Refresh',
//            iconCls: 'refresh-icon16',
//            handler: function() {
//                var thisGrid = Ext.ComponentQuery.query('#methodGrid')[0]
//                thisGrid.store.load();
//            }
//        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No methods found',
            displayInfo: true
        }],
    initComponent: function() {
        var me = this;
        me.store = me.buildStore();
        me.dockedItems[0].store = me.store;
        me.columns = me.buildColumns();
//        var x = me.tbar.length;
//        me.tbar[x++] = delta3.utils.GridFilter.create({gridToSearch: me, local: true});   
//        me.tbar[x++] = delta3.utils.GridPaginator.create({gridToReload: me});         
        me.callParent();
        me.store.load();
    },
    buildColumns: function() {
        return [
            {text: 'ID', dataIndex: 'idMethod', width: 40, locked: true},
            {text: 'Name', dataIndex: 'shortName', editor: {xtype: 'textfield',maskRe: /[^<>%]/}, width: 120, locked: true},            
            {text: 'System ID', dataIndex: 'name', width: 120, locked: true},
            {text: 'Description', dataIndex: 'description', editor: {xtype: 'textfield',maskRe: /[^<>%]/}, width: 220},
            {text: 'Sequence #', dataIndex: 'sequenceNumber', width: 100,  editor: 'numberfield'},            
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active',  disabled: true, editor: 'checkboxfield', width: 60},
            {text: 'Parameters', dataIndex: 'methodParams', editor: 'textfield', width: 260},
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', delta3.utils.GlobalVars.UserStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function(val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', delta3.utils.GlobalVars.UserStore);
                }}
        ];
    },
    buildStore: function() {
        return Ext.create('delta3.store.MethodStore');
    }

});


