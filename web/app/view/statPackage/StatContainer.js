/* 
 * Method Container
 */

Ext.define('delta3.view.statPackage.StatContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.stats',
    requires: [
        'delta3.view.statPackage.MethodItemGrid',
        'delta3.model.MethodModel',
        'delta3.store.MethodStore'],        
    //height: delta3.utils.GlobalVars.gridMarginHeight+delta3.utils.GlobalVars.gridRowHeight*delta3.utils.GlobalVars.pageSize,       
    renderTo: Ext.getBody(),   
    initComponent:  function(){ 
        var me = this;
        me.items = [{
            title: 'Statistical Packages',
            xtype:  'grid.stat',
            region: 'north'
        },{
            title: 'Statistical Methods',
            xtype:  'grid.methodItem',
            hidden: true,
            height: 600,
            margin: '10 5 10 5',                
            region: 'south'            
        }], 
        me.callParent();
    }
});