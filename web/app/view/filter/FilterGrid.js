/* 
 * Filter Grid
 */

Ext.define('delta3.view.filter.FilterGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.filter',
    itemId: 'filterGrid',
    autoScroll: false,
    renderTo: document.body,
    minHeight: 150,
    height: '100%',
    selModel: {selType: 'checkboxmodel', mode: 'SINGLE'},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.grid.column.Check',
        'delta3.store.FilterStore',
        'delta3.view.filter.FilterFormulaPopup'
    ],
    border: false,
    buildViewConfig: function() {
        
        return {
            getRowClass: function(record, rowIndex, rowParams, store) {               
                var thisGrid = Ext.ComponentQuery.query('#filterGrid')[0];
                if ( thisGrid.isStudyMode === true ) {
                    var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    var r = studyGrid.store.findRecord('idStudy', thisGrid.studyId);
                    if (record.get('idFilter') === r.get('idFilter'))
                        return 'rowBold';
                }
            }
        };
    },
    dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',            
            items: []
        },{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No filters found',
            displayInfo: true
        }],
    isStudyMode: false,
    studyId: {},
    fieldStore: {},
    initComponent: function() {
        var me = this;
        me.viewConfig = me.buildViewConfig();
        me.store = new delta3.store.FilterStore({idModel: me.modelId})
        me.store.load();       
        me.dockedItems[1].store = me.store;
        me.columns = me.buildColumns();
        me.plugins = me.buildPlugins();
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];  
        tbr.add({
            itemId: 'addFilter',
            text: 'Add Filter',
            iconCls: 'add_new-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#filterGrid')[0];
                thisGrid.plugins[0].cancelEdit();
                var r = delta3.model.FilterModel.create({
                    active: true,
                    modelidModel: thisGrid.modelId,
                    organizationidOrganization: 0,
                    createdTS: '0000-00-00 00:00:00.0',
                    updatedTS: '0000-00-00 00:00:00.0'});
                thisGrid.store.insert(0, r);
                thisGrid.plugins[0].startEdit(0, 0);
            }
        });
        tbr.add({
            itemId: 'cloneFilter',
            text: 'Clone Filter',
            iconCls: 'cloneStudy-icon16',
            tooltip: delta3.utils.Tooltips.studyBtnClone,                       
            handler: function() {            
                var thisGrid = Ext.ComponentQuery.query('#filterGrid')[0];
                var sm = thisGrid.getSelectionModel();
                thisGrid.plugins[0].cancelEdit();
                if (typeof sm.getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select filter first.');
                    return;
                }   
                delta3.utils.GlobalFunc.doCloneFilter(sm.getSelection()[0].data, thisGrid);
            }
        });
        tbr.add({
            itemId: 'filterDefine',
            text: 'Edit',
            iconCls: 'define_sql-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#filterGrid')[0];
                var sm = thisGrid.getSelectionModel();
                thisGrid.plugins[0].cancelEdit();
                if (typeof sm.getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select filter first.');
                    return;
                }                         
                var win = new delta3.view.filter.FilterFormulaPopup({fieldStore: thisGrid.fieldStore, selectedStudyRecord: sm.getSelection()[0]});
                win.show();
            }
        });        
        if (me.isStudyMode === true) {
            tbr.add({
                itemId: 'assignStudy',
                text: 'Assign to Study',
                iconCls: 'assign-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#filterGrid')[0];
                    var sm = thisGrid.getSelectionModel();
                    //var newFilter = thisGrid.getStore().findRecord('idFilter', sm.selected.items[0].data.idFilter);
                    var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    var r = studyGrid.store.findRecord('idStudy', thisGrid.studyId);
                    //studyGrid.filterStore.add(newFilter);
                    r.set('idFilter', sm.selected.items[0].data.idFilter);
                    studyGrid.store.sync();
                    thisGrid.getView().refresh();
                }
            });
            tbr.add({
                itemId: 'clearFilter',
                text: 'Unassign Filter',
                iconCls: 'no_filter-icon16',
                handler: function() {
                    var thisGrid = Ext.ComponentQuery.query('#filterGrid')[0];
                    var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0];
                    var sm = thisGrid.getSelectionModel();
                    var record = sm.getSelection();
                    var r = studyGrid.store.findRecord('idStudy', thisGrid.studyId);
                    if (record[0].get('idFilter') === r.get('idFilter')) {
                        r.set('idFilter', 0);
                        studyGrid.store.sync();
                        thisGrid.getView().refresh();
                    }
                }
            });
        }
        tbr.add({
            itemId: 'recordInfo',
            text: 'View Properties',
            iconCls: 'recordInfo-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#filterGrid')[0]
                var sm = thisGrid.getSelectionModel();
                thisGrid.plugins[0].cancelEdit();
                if (typeof sm.getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select filter first.');
                    return;
                }                            
                var win = Ext.create('delta3.view.popup.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "Filter"});
                win.show();
            }
        });
        tbr.add({
            itemId: 'filterDelete',
            text: 'Delete',
            iconCls: 'delete-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#filterGrid')[0];
                thisGrid.getView().refresh();    
                var regrFieldPopup = Ext.ComponentQuery.query('#filterPopup')[0]; 
                regrFieldPopup.updateLayout();                  
                var sm = thisGrid.getSelectionModel();
                thisGrid.plugins[0].cancelEdit();
                if (typeof sm.getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select filter first.');
                    return;
                }                  
                delta3.utils.GlobalFunc.getFilterUsage(sm.getSelection()[0].data, thisGrid.deleteFilter, sm);
            }
        });
        tbr.add({
            itemId: 'recordInfo',
            text: 'View Properties',
            iconCls: 'recordInfo-icon16',
            tooltip: delta3.utils.Tooltips.studyBtnProperties,                       
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#filterGrid')[0]
                var sm = thisGrid.getSelectionModel();
                thisGrid.plugins[0].cancelEdit();
                if (typeof sm.getSelection()[0] === 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select Filter first.');
                    return;
                }                            
                var win = Ext.create('delta3.view.popup.RecordInfoPopup',{record: sm.getSelection()[0], recordType: "Filter"});
                win.show();
            }
        });
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me})); 
    },
    buildColumns: function() {
        return [
            {text: 'ID', dataIndex: 'idFilter', width: 30},
            {text: 'Name', dataIndex: 'name', width: 160, editor: 'textfield'},
            {text: 'Description', dataIndex: 'description', width: 270, editor: 'textfield'},
            {text: 'Record Count', dataIndex: 'resultCount', width: 100}              
            //{text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active', width: 50,  disabled: true, editor: 'checkboxfield'},
//            {text: 'Data Model', dataIndex: 'idModel', width: 100, sortable: false, tooltip: delta3.utils.Tooltips.studyGridModel,
//                renderer: function(val, cell, rec, r_idx, c_idx, store) {
//                    var tableIndex = rec.data['modelidModel'];
//                    var model = delta3.utils.GlobalVars.ModelStore.findRecord('idModel', tableIndex);
//                    if (model === null)
//                        return null;
//                    else {
//                        return model.get("name");
//                    }
//                }}            
        ];
    },
    buildPlugins: function() {
        return [Ext.create('Ext.grid.plugin.RowEditing', {
                clicksToEdit: 2,
                autoCancel: false,
                itemId: 'filterGridEditor',
                listeners: {
                    edit: function(rowEditor, changes, r) {
                        var thisGrid = Ext.ComponentQuery.query('#filterGrid')[0];         
                        thisGrid.store.sync();
                        if ( thisGrid.isStudyMode === true ) {
                            var studyGrid = Ext.ComponentQuery.query('#studyGrid')[0]; 
                            studyGrid.filterStore.load();
                        }
                    }
                }
            })];
    },
    deleteFilter: function(response, options, sm) {
        
        Ext.MessageBox.hide();
        var responseObject = JSON.parse(response.responseText);
        if (responseObject.studies.length === 0) {
            var thisGrid = Ext.ComponentQuery.query('#filterGrid')[0];
            thisGrid.store.remove(sm.getSelection());
            thisGrid.store.sync();
        } else {
            var idString = '';
            for (var i = 0; i < responseObject.studies.length; i++) {
                if (i > 0)
                    idString += ',';
                idString += responseObject.studies[i].name;
            }
            Ext.Msg.alert("DELTA", "Filter is used by Study: " + idString);
        }
    }
});

