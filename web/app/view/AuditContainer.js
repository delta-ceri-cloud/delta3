/* 
 * Audit Container
 */

Ext.define('delta3.view.AuditContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.audits',
    //height: delta3.utils.GlobalVars.gridMarginHeight+delta3.utils.GlobalVars.gridRowHeight*delta3.utils.GlobalVars.pageSize,       
    //layout: 'fit',
    initComponent:  function(){ 
        var me = this;
        me.items = {
                xtype:  'grid.audit',
                region: 'center'
        }, 
        me.callParent();
    }
});