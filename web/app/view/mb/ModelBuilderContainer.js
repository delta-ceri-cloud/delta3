/* 
 * ModelBuilder
 * CERI-LaheyLading
 */

Ext.define('delta3.view.mb.ModelBuilderContainer', {
    extend: 'Ext.container.Container',
    itemId: 'modelBuilderContainer',
    alias: 'widget.container.modelBuilder',
    requires: [
        'Ext.tree.*',
        'Ext.data.*',
        'Ext.tip.*',
        'Ext.dd.*',
        'Ext.data.StoreManager',
        'Ext.layout.container.HBox',
        'delta3.store.ModelData',
        'delta3.store.ModelTableComboStore',
        'delta3.store.ModelKeyComboStore',
        'delta3.store.ModelRelationshipTreeStore',
        'delta3.store.MetadataTreeStore',
        'delta3.view.mb.TableTreePanel',
        'delta3.store.ModelTableStore',
        'delta3.view.mb.RelationshipGrid',
        'delta3.store.RelationshipStore',
        'delta3.view.mb.FieldGrid',
        'delta3.store.ModelFieldStore',
        'delta3.view.mb.JoinPanel'
    ],
    layout: 'border',
    height: delta3.utils.GlobalVars.borderHeight,         
    deferredRender: false,
    autoDestroy: true,
    defaults: {
        collapsible: true,
        split: true,
        bodyStyle: 'padding:2px'
    },
    initComponent: function() {
        var me = this;
        lastFilterValue = "";

        /*
        me.columns = [{
                    xtype: 'treecolumn',
                    flex: 1,
                    dataIndex: 'text',
                    scope: me,
                    renderer: function(value) {
                        var searchString = this.searchField.getValue();
                        if (searchString.length > 0) {
                            console.log("inside renderer");
                            return this.strMarkRedPlus(searchString, value);
                        }
                        return value;
                    },
                   strMarkRedPlus: function(search, subject) {
                        console.log("inside strMarkRedPlus");
                        return subject.replace(
                                    new RegExp('(' + search + ')', "gi"), "<span style='color: red;'><b>$1</b></span>");
                   }
                            
                }];*/


        
        ModelData = Ext.create('delta3.store.ModelData');    // this is main holder of Model Builder data
        ModelData.modelSelected = this.modelSelected;
        ModelData.modelTableComboStore = Ext.create('delta3.store.ModelTableComboStore');
        ModelData.modelKey1ComboStore = Ext.create('delta3.store.ModelKeyComboStore');
        ModelData.modelKey2ComboStore = Ext.create('delta3.store.ModelKeyComboStore');
        ModelData.relationshipTreeStore = Ext.create('delta3.store.ModelRelationshipTreeStore');
        
        
        ModelData.tableTreePanel = 
                Ext.create('delta3.view.mb.TableTreePanel', 
                {
                    store: ModelData.relationshipTreeStore
                    
                });
        
        ModelData.tableStore = Ext.create('delta3.store.ModelTableStore');
        ModelData.relationshipGrid = Ext.create('delta3.view.mb.RelationshipGrid');
        
        ModelData.fieldGrid = Ext.create('delta3.view.mb.FieldGrid');
        ModelData.fieldGrid.setTitle(delta3.utils.GlobalFunc.emphasizeString("Status: " + ModelData.modelSelected.status));
        
        Ext.apply(this, {
            items: [{//-------------------------------------------------- left side tree panel with all db tables
                    title: 'Step 1A: Table Selection',
                    xtype: 'panel',
                    itemId: 'dataSourceTree',
                    region: 'west',
                    autoScroll: true,
                    margin: '1 0 0 0',                    
                    width: 252,
                    collapsible: true,
                    //maxSize: 252,    
                    tbar: [{
                            text: 'Add Table to Model',
                            iconCls: 'add_new-icon16',
                            tooltip: delta3.utils.Tooltips.dbsourceBtnAdd,                                 
                            handler: function() {
                                var theTree = Ext.ComponentQuery.query('#metadataTree')[0];
                                var rec = theTree.getSelection();
                                if ( (rec.length === 0) || (rec[0].data.id === 'root') ) {
                                    delta3.utils.GlobalFunc.showDeltaMessage('Please select table first');
                                } else {
                                    var r = delta3.model.ModelTableModel.create({
                                        name: rec[0].get('text'),
                                        physicalName: rec[0].get('text'),
                                        idModelTable: 0,
                                        active: true,
                                        modelidModel: ModelData.modelSelected.idModel,
                                        createdTS: '0000-00-00 00:00:00.0',
                                        updatedTS: '0000-00-00 00:00:00.0'
                                    });
                                    var root = ModelData.relationshipTreeStore.getRootNode();
                                    if ( root.childNodes.length === 0 ) {
                                        r.set('primaryTable', true);
                                    }                                    
                                    ModelData.tableStore.insert(0, r);
                                    ModelData.tableStore.sync();
                                }
                            }
                        }, {
                            text: 'Refresh',
                            iconCls: 'refresh-icon16',
                            tooltip: delta3.utils.Tooltips.relBtnRefresh,                           
                            handler: function() {
                                var theTree = Ext.ComponentQuery.query('#dataSourceTree')[0];
                                theTree.setLoading(true);                                
                                var theTree = Ext.ComponentQuery.query('#metadataTree')[0];
                                theTree.store.localRemoteFlag = 'remote';
                                theTree.store.load();
                            }
                        }],                    
                    items: [{
                        xtype: 'treepanel',
                        margin: '1 1 1 1',
                        layout: {
                            type: 'vbox',
                            align: 'left'
                        },
                        renderTo: Ext.getBody(),
                        itemId: 'metadataTree',
                        store: Ext.create('delta3.store.MetadataTreeStore'),
                        dataSource: this.dataSourceName,
                        viewConfig: {
                            allowCopy: true,
                            copy: true,
                            plugins: {
                                ptype: 'treeviewdragdrop',
                                dragGroup: 'treeTableDDGroup',
                                containerScroll: true
                            }
                        }}]
                }, 
                
                {
                    title: "Data Model:  " + ModelData.modelSelected.name, //------------------------------------------ middle tab pannel
                    xtype: 'tabpanel',
                    collapsible: false,
                    region: 'center',
                    //width: 250,                
                    margin: '1 0 0 0',
                    defaults: {autoScroll: true},                       
                    items: [ModelData.tableTreePanel, 
                            ModelData.relationshipGrid, 
                            Ext.create('delta3.view.mb.JoinPanel')
                        ]                    
                }, 
                
                {
                    title: 'Descriptive Statistics', //------------------------------------------ rigth tab pannel
                    itemId: 'descriptiveStatistics',
                    xtype: 'tabpanel',
                    collapsible: true,
                    collapsed: true,
                    region: 'east',
                    modelSelected: ModelData.modelSelected,
                    width: 660,
                    margin: '1 0 0 0'
                }, //--------------------------------------------------------- bottom grid with all model fields
                ModelData.fieldGrid
            ]});
        this.callParent();
        //data-qtip = 'gogog';
    }
    
});
