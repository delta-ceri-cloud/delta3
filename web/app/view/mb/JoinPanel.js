/* 
 * Export Popup Window
 */

Ext.define('delta3.view.mb.JoinPanel', {
    extend: 'Ext.Panel',
    layout: 'fit',
//    width: 450,
//    height: 360,
    itemId: 'joinPanel',
    title: 'SQL Preview',
    items: [{
            xtype: 'textareafield',
            width: 440,
            height: 340,            
            grow: true,
            itemId: 'joinSqlString',
            value: '',
            allowBlank: true
        }],
    buttons: [
        {
            text: 'Refresh',
            handler: function() {
                delta3.utils.GlobalFunc.doGetModelJoin(callback);
                function callback(sql) {
                    if ( sql.charAt(0) === 'M' && sql.charAt(1) === 'B' ) {
                        sql = "Insufficiant data provided or error.\n" + sql;
                    }
                    Ext.ComponentQuery.query('#joinSqlString')[0].setValue(sql);
                }
            }
        }],
    initComponent: function() {
        var me = this;
        delta3.utils.GlobalFunc.doGetModelJoin(callback);
        function callback(sql) {
            if ( sql.charAt(0) === 'M' && sql.charAt(1) === 'B' ) {
                sql = "Insufficiant data provided or error.\n" + sql;
            }            
            Ext.ComponentQuery.query('#joinSqlString')[0].setValue(sql);
        }
        me.callParent();        
    }
});


