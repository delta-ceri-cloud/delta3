/* 
 * Main View
 * responsible for loading files and general purpose data
 */

Ext.define('delta3.view.Viewport', {
    extend: 'Ext.container.Viewport',
    xtype: 'view-main',
    autoDestroy: true,
    requires: [
        'Ext.tab.Panel',
        'Ext.Toolbar',
        'Ext.window.MessageBox',
        'Ext.tip.ToolTip',
        'delta3.utils.GlobalVars',
        'delta3.controller.Main',
        'delta3.controller.Stats',        
        'delta3.controller.Methods',        
        'delta3.controller.Logregrs',
        'delta3.controller.Configurations',
        'delta3.controller.Processes',
        'delta3.controller.Studies',
        'delta3.controller.Models',
        'delta3.controller.Organizations',
        'delta3.controller.Permissions',
        'delta3.controller.Groups',
        'delta3.controller.Roles',
        'delta3.controller.Users',
        'delta3.controller.Events',
        'delta3.controller.Eventtemplates',
        'delta3.controller.Alerts',
        'delta3.controller.Notifications',
        'delta3.controller.Webservices',
        'delta3.controller.DAM',        
        'delta3.controller.HealthSurvey',
        'delta3.view.popup.PasswordPopup',
        'delta3.view.popup.HelpPopup',
        'delta3.store.OrganizationStore',
        'delta3.store.RoleStore',
        'delta3.store.FieldStore',
        'delta3.store.ProjectStore',
        'delta3.utils.GridPanel',
        'delta3.utils.Tooltips',
        'delta3.utils.GridFilter'],
    itemId: 'mainViewPort',
    title: 'D E L T A  3',
    projectStore: {},
    project: 0,
    periodicStatusCheck: {},
    
    
    
    initComponent: function () {
        var me = this;
        console.log('Initializing DELTA version ' + deltaVersion);
        //delta3.utils.GlobalFunc.getCurrentUser(me); no longer needed second call
        delta3.utils.GlobalVars.mainMenu = delta3.utils.GlobalFunc.getMenuByProject(0);
        
        me.projectStore = delta3.store.ProjectStore.create();
        me.projectStore.load();
        
        var rightTopMenu = Ext.create('Ext.menu.Menu', {
            items: [{
                    text: 'Change Password',
                    iconCls: 'password-icon16',
                    handler: function () {
                        var win = new delta3.view.popup.PasswordPopup();
                        win.show();
                    }
                }, {
                    text: 'Logout',
                    iconCls: 'logout-icon16',
                    handler: function () {
                        delta3.utils.GlobalFunc.doLogout();
                    }
                }, {
                    text: 'License',
                    iconCls: 'help-icon16',
                    handler: function () {
                        //var theTabs = Ext.ComponentQuery.query('#maintabs')[0];
                        //var t = theTabs.getActiveTab();
                        var win;
                        //if ((typeof t !== 'undefined') && (t !== null)) {
                        //    win = new delta3.view.popup.HelpPopup({helpSource: t.title + 'Help.html'});
                        //} else {
                            win = new delta3.view.popup.HelpPopup({helpSource: 'help2.html'});
                        //}
                        win.show();
                    }
                }]
        });
        var projectDropDown = new Ext.form.ComboBox({
            store: me.projectStore,
            itemId: 'projectComboBox',
            fieldLabel: 'Project',
            emptyText: 'Select a project..',
            labelAlign: 'right',
            queryMode: 'local',
            displayField: 'name',
            editable: false,
            valueField: 'idGroup',
            multiSelect: false,
            forceSelection: false,
            listeners: {
                
                'focus': function () {
                    me.projectStore.clearFilter(true);
                    me.projectStore.filter('active', true);      
                   
                    
                    if(delta3.utils.GlobalVars.currentUserAuthInfo.userAuth.projects[0].projectId===0)
                    {  
                        
                        var findrecord = me.projectStore.findRecord('name', 'All');
                        console.log("findrecord"+findrecord);
                        if(findrecord === null){
                             me.projectStore.add({'name': 'All', 'idGroup': 0, 'active': true});         
                        }
                        
                    }
                   
                   
                    me.projectStore.filterBy( function (record) {
                        for (var i=0; i<delta3.utils.GlobalVars.currentUserAuthInfo.userAuth.projects.length; i++) 
                        {
                            if ( record.get("idGroup") === delta3.utils.GlobalVars.currentUserAuthInfo.userAuth.projects[i].projectId ) 
                            {
                                return true;
                            }
                        }
                        return false;
                    });
                    
                },
                
                
                
                /* removed dirty change for above functionality
                
                'dirtychange': function () {
                    me.projectStore.add({'name': 'All', 'idGroup': 0, 'active': true}); // insert artificial selection option                   
                },*/
                
                
                
                'select': function (cmb, rec, idx) {
                    me.project = cmb.getValue();                 
                    me.updateTabs(me.project);
                    //me.updateTabs2(me.project);
                    delta3.utils.GlobalVars.mainMenu = delta3.utils.GlobalFunc.getMenuByProject(me.project);
                 
                    var mainMenu2 = Ext.create('Ext.button.Button', {
                        text: 'DELTA Menu',
                        iconCls: 'home-icon16',
                        itemId: 'startbutton',
                        menu: delta3.utils.GlobalVars.mainMenu
                    });
                    
                    
                    var mm = Ext.ComponentQuery.query('#mainmenu')[0];
                    mm.remove(0);
                    mm.insert(0, mainMenu2);                    
                }
            }
        });
        
        var loggingDropDown = null;
        if (delta3.utils.GlobalFunc.isPermitted("Logging")) {
            loggingDropDown = new Ext.form.ComboBox({
                store: delta3.utils.GlobalVars.loggingComboBoxStore,
                itemId: 'loggingComboBox',
                fieldLabel: 'Logging level',
                emptyText: 'Select a level..',
                labelAlign: 'right',
                queryMode: 'local',
                displayField: 'level',
                valueField: 'level',
                multiSelect: false,
                forceSelection: false,
                listeners: {
                    'select': function (cmb, rec, idx) {
                        var loggingLevel = cmb.getValue();
                        delta3.utils.GlobalFunc.doChangeLoggingLevel(loggingLevel);
                    }
                },
                receiveCurrentLoggingLevel: function (response) {
                    var x = response.loggingLevel[0];
                    //console.log("current DELTA server log level: " + x);
                    Ext.ComponentQuery.query('#loggingComboBox')[0].setValue(x);
                }
            });
            delta3.utils.GlobalFunc.doGetLoggingLevel(loggingDropDown.receiveCurrentLoggingLevel);
        }
        
        var mainMenu = Ext.create('Ext.button.Button', {
            text: 'DELTA Menu',
            iconCls: 'home-icon16',
            itemId: 'startbutton',
            menu: delta3.utils.GlobalVars.mainMenu
        });        
        me.items = [{
                xtype: 'panel',
                title: '<div style="text-align:center"><span style="font-size: bold">DELTA [ver. ' + deltaVersion + ']  Lahey-CERI Comparative Effectiveness Research Institute</span></div>',
                dockedItems: [{
                        xtype: 'toolbar',
                        docked: 'top',
                        itemId: 'mainmenu',
                        items: [mainMenu, projectDropDown, loggingDropDown,
                            {
                                xtype: 'tbfill'
                            }, {
                                xtype: 'button',
                                //html: '<div id="JIRA"><script type="text/javascript" src="https://ceri-lahey.atlassian.net/s/942d310bc0ffd4c91c9f62f34e7e1578-T/en_US-596pgm/71002/b6b48b2829824b869586ac216d119363/2.0.11/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=bf9f9ff4"></script></div>',
                                text: 'Feedback',
                                iconCls: 'feedback-icon16',
                                handler: function () {
                                    var win = window.open("feedback.html", "DELTA Feedback", "status=1,location=1,top=100,left=400,width=840,height=740");
                                    win.moveTo(200, 100);
                                }
                            }, {
                                xtype: 'button',
                                itemId: 'deltaStatPackageIndicator',
                                iconCls: 'bullet_black',
                                text: 'Stat Package',
                                listeners: {
                                    'click': function () {
                                        me.pingToGetVersionLocal(this);
                                    },
                                    'sudoClickLocal': function () {
                                        var thisButton = this;
                                        thisButton.setIconCls('bullet_black');
                                        thisButton.setDisabled(true);
                                        delta3.utils.GlobalFunc.callLocalStatPackage('start', updateUI);
                                        function updateUI(response, options) {
                                            thisButton.setDisabled(false);
                                            if (response.responseText.substring(0,27) === 'DELTAlytics started already') {
                                                thisButton.setIconCls('bullet_green');
                                                me.pingToGetVersionLocal(thisButton);
                                            } else {
                                                thisButton.setIconCls('bullet_red');
                                            }
                                        }
                                    }
                                }
                            }, {
                                xtype: 'button',
                                itemId: 'extPackageIndicator',
                                iconCls: 'bullet_black',
                                text: 'Stat Package',
                                listeners: {
                                    'click': function () {
                                        me.pingToGetVersionExt(this);
                                        Ext.Msg.show({
                                                title: 'DELTA v3.7 alternative stat package feature',
                                                msg: 'DELTA v3.7 is package default with DELTAlytics v2.0. Use of alternative stat package by using this feature is currently disabled. Please refer DELTA <a href="https://ceri-lahey.atlassian.net/wiki/spaces/DUM/overview">User Manual</a> for more information. Thank you for using DELTA.',
                                                buttons: Ext.MessageBox.OK,
                                            icon: 'statdisable'
                                            });
        
                                    },
                                    'sudoClickExt': function () {
                                        var thisButton = this;
                                        thisButton.setIconCls('bullet_black');
                                        thisButton.setDisabled(true);
                                        Ext.Ajax.request({
                                            url: '/Delta3/webresources/process/initializeStatPackage',
                                            method: "GET",
                                            disableCaching: true,
                                            success: function (response, options) {
                                                thisButton.setDisabled(false);
                                                if (response.responseText === 'OK') {
                                                    thisButton.setIconCls('bullet_green');
                                                    me.pingToGetVersionExt(thisButton);
                                                } else {
                                                    thisButton.setIconCls('bullet_red');
                                                }
                                            },
                                            failure: function (response) {
                                                console.log("call to OCEANS intit servlet failed");
                                                thisButton.setDisabled(false);
                                                thisButton.setIconCls('bullet_red');
                                            }
                                        });
                                    }
                                }
                            }, {
                                text: delta3.utils.GlobalVars.currentUser,
                                itemId: 'currentUserAlias',
                                menu: rightTopMenu,
                                icon: 'resources/images/user_suit.png'
                            }]
                    },
                    {
                        xtype: 'tabpanel',
                        itemId: 'maintabs',                    
                        layout: {
                            scrollable: true,
                            type: 'vbox'
                        },
                        hidden: true
                    }]
            }];
        me.callParent();
        me.periodicStatusCheck = setInterval(function ()
                {
                    me.pingToGetVersionExt(Ext.ComponentQuery.query('#extPackageIndicator')[0]);
                },
                120000); // every 120 sec            
        var bExt = me.down('#extPackageIndicator');
        bExt.fireEvent('sudoClickExt');
        me.periodicStatusCheckDelta = setInterval(function ()
                {
                    me.pingToGetVersionLocal(Ext.ComponentQuery.query('#deltaStatPackageIndicator')[0]);
                },
                180000); // every 180 sec            
        var bDelta = me.down('#deltaStatPackageIndicator');
        bDelta.fireEvent('sudoClickLocal');        
    },
    
    
    
    pingToGetVersionExt: function (thisButton) {
        thisButton.setIconCls('bullet_black');
        thisButton.setDisabled(true);
        
        
        /*
        Ext.Msg.show({
                title: 'DELTA v3.7 Feature Disable',
                msg: 'This feature is currently disable in DELTA v3.7. Please refer DELTA <a href="https://ceri-lahey.atlassian.net/wiki/spaces/DUM/overview">User Manual</a> for more information. Thank you for using DELTA.',
                buttons: Ext.MessageBox.OK,
            icon: 'statdisable'
            });
        */
        
        
        Ext.Ajax.request({
            url: '/Delta3/webresources/process/pingStatPackage',
            method: "GET",
            disableCaching: true,
            success: function (response, options) {
                thisButton.setDisabled(false);
                
                
                if (response.responseText !== '?') {    
                    var version = eval(response.responseText);
                    if (typeof version === 'undefined')
                        version = '';
                    thisButton.setText(version);
                    thisButton.setIconCls('bullet_green');
                    thisButton.setTooltip('Statistical package ready to process request');   
                    
                } else {
                    thisButton.setIconCls('bullet_red');
                     
                    
                }
            },
            failure: function (response) {
                thisButton.setDisabled(false);
                thisButton.setIconCls('bullet_red');
                Ext.Msg.show({
                title: 'DELTA v3.7 Feature Disable',
                msg: 'This feature is currently disable in DELTA v3.7. Please refer DELTA <a href="https://ceri-lahey.atlassian.net/wiki/spaces/DUM/overview">User Manual</a> for more information. Thank you for using DELTA.',
                buttons: Ext.MessageBox.OK,
                icon: 'statdisable'
                });
            }
        });
        
        
    },
    
    
    
    pingToGetVersionLocal: function (thisButton) {
        thisButton.setIconCls('bullet_black');
        thisButton.setDisabled(true);
        delta3.utils.GlobalFunc.callLocalStatPackage('pingStatPackage', updateUI);
        function updateUI(response) {
            thisButton.setDisabled(false);
            if (response.responseText !== '') {
                try {
                var version = eval(response.responseText);
                if (typeof version === 'undefined')
                    version = '';
                thisButton.setText(version);
                thisButton.setIconCls('bullet_green');
                thisButton.setTooltip('Statistical package ready to process request');               
            } catch (err) {
                thisButton.setTooltip(response.responseText);
            }
            } else {
                thisButton.setIconCls('bullet_red');
            }
        }        
    },
    
    updateTabs: function(projectId) {
        
        
        var maintabs = Ext.ComponentQuery.query('#maintabs')[0];     
         for (var i=maintabs.items.length-1; i >= 0; i--) {
               console.log("menu"+ maintabs.items.get(i));
       
            maintabs.items.get(i).destroy();
        }
       
       
        var theGrid = Ext.ComponentQuery.query('#studyGrid')[0];        
        if ( typeof theGrid !== 'undefined' ) {        
            if (!delta3.utils.GlobalFunc.isPermitted("Studies", projectId)) {
                theGrid.up().destroy();
            } else {
                theGrid.modelStore = Ext.create('delta3.store.ModelStore', {pageSize: delta3.utils.GlobalVars.largePageSize, groupId: projectId});
                theGrid.modelStore.load();                         
                theGrid.store.groupId = projectId;
                theGrid.store.loadPage(1);
                var modelComboBox = Ext.ComponentQuery.query('#modelComboBox')[0];
                if ( typeof modelComboBox !== 'undefined' ) {            
                    modelComboBox.setStore(theGrid.modelStore);
                }               
            }
        }    
        
        
        theGrid = Ext.ComponentQuery.query('#modelGrid')[0];
        updateGrid(theGrid, "Models", projectId);
       
        theGrid = Ext.ComponentQuery.query('#processGrid')[0];
        updateGrid(theGrid, "Processes", projectId);
        
        theGrid = Ext.ComponentQuery.query('#eventGrid')[0];
        updateGrid(theGrid, "Events", projectId);        
        
        theGrid = Ext.ComponentQuery.query('#notificationGrid')[0];
        updateGrid(theGrid, "Notifications", projectId);
        
        theGrid = Ext.ComponentQuery.query('#logregrGrid')[0];
        updateGrid(theGrid, "Logregrs", projectId);
        
        theGrid = Ext.ComponentQuery.query('#studyGrid')[0];
        updateGrid(theGrid, "Studies", projectId);
        
        theGrid = Ext.ComponentQuery.query('#userGrid')[0];
        updateGrid(theGrid, "Users", projectId);
        theGrid = Ext.ComponentQuery.query('#roleGrid')[0];
        updateGrid(theGrid, "Roles", projectId);
        theGrid = Ext.ComponentQuery.query('#groupGrid')[0];
        updateGrid(theGrid, "Groups", projectId);        
        theGrid = Ext.ComponentQuery.query('#organizationGrid')[0];
        updateGrid(theGrid, "Organizations", projectId);
        theGrid = Ext.ComponentQuery.query('#permissionGrid')[0];
        updateGrid(theGrid, "Permissions", projectId);      
        
        theGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
        updateGrid(theGrid, "Configurations", projectId);
        theGrid = Ext.ComponentQuery.query('#alertGrid')[0];
        
        updateGrid(theGrid, "Alerts", projectId);
        theGrid = Ext.ComponentQuery.query('#webserviceGrid')[0];
        
        updateGrid(theGrid, "Webservices", projectId);        
        theGrid = Ext.ComponentQuery.query('#auditGrid')[0];
        
        updateGrid(theGrid, "Audits", projectId);
        theGrid = Ext.ComponentQuery.query('#DAMGrid')[0];
        updateGrid(theGrid, "DAMPanel", projectId);    
        
        //theGrid = Ext.ComponentQuery.query('#statGrid')[0];
        //updateGrid(theGrid, "Stats", projectId);
        
        theGrid = Ext.ComponentQuery.query('#methodGrid')[0];
        updateGrid(theGrid, "Methods", projectId);              
        return;
        
        function updateGrid(grid, permission, projectId) {
            if ( typeof grid !== 'undefined' ) {        
                if (!delta3.utils.GlobalFunc.isPermitted(permission, projectId)) {
                    var gridTab = grid.up();    
                    var tb = grid.down('toolbar').removeAll();
                    grid.down('toolbar').destroy();
                    tb = null;
                    grid.removeAll();
                    grid.destroy();
                    gridTab.removeAll();
                    gridTab.destroy(); // remove tab this grid belonged to                   
                    grid = null;
                    gridTab = null;
                } else {
                    grid.store.groupId = projectId;
                    grid.store.loadPage(1);                
                }
            }            
        }
    
        
        //updateTabs2(projectId);
        
    },    
    
    
    
    updateTabs2: function(projectId) {
        
        
        var theGrid = Ext.ComponentQuery.query('#studyGrid')[0];        
        if ( typeof theGrid !== 'undefined' ) {        
            if (!delta3.utils.GlobalFunc.isPermitted("Studies", projectId)) {
                theGrid.up().destroy();
            } else {
                theGrid.modelStore = Ext.create('delta3.store.ModelStore', {pageSize: delta3.utils.GlobalVars.largePageSize, groupId: projectId});
                theGrid.modelStore.load();                         
                theGrid.store.groupId = projectId;
                theGrid.store.loadPage(1);
                var modelComboBox = Ext.ComponentQuery.query('#modelComboBox')[0];
                if ( typeof modelComboBox !== 'undefined' ) {            
                    modelComboBox.setStore(theGrid.modelStore);
                }               
            }
        }    
        
        
        theGrid = Ext.ComponentQuery.query('#modelGrid')[0];
        updateGrid(theGrid, "Models", projectId);
        theGrid = Ext.ComponentQuery.query('#processGrid')[0];
        updateGrid(theGrid, "Processes", projectId);
        theGrid = Ext.ComponentQuery.query('#eventGrid')[0];
        updateGrid(theGrid, "Events", projectId);        
        
        theGrid = Ext.ComponentQuery.query('#notificationGrid')[0];
        updateGrid(theGrid, "Notifications", projectId);
       
        theGrid = Ext.ComponentQuery.query('#logregrGrid')[0];
        updateGrid(theGrid, "Logregrs", projectId);
        
        theGrid = Ext.ComponentQuery.query('#studyGrid')[0];
        updateGrid(theGrid, "Studies", projectId);
        Ext.ComponentQuery.query('#studyGrid')[0].getView().refresh(); 
        theGrid.store.load();
        
        
        theGrid = Ext.ComponentQuery.query('#userGrid')[0];
        updateGrid(theGrid, "Users", projectId);
        theGrid = Ext.ComponentQuery.query('#roleGrid')[0];
        updateGrid(theGrid, "Roles", projectId);
        theGrid = Ext.ComponentQuery.query('#groupGrid')[0];
        updateGrid(theGrid, "Groups", projectId);        
        theGrid = Ext.ComponentQuery.query('#organizationGrid')[0];
        updateGrid(theGrid, "Organizations", projectId);
        
        theGrid = Ext.ComponentQuery.query('#permissionGrid')[0];
        updateGrid(theGrid, "Permissions", projectId);      
        
        theGrid = Ext.ComponentQuery.query('#configurationGrid')[0];
        updateGrid(theGrid, "Configurations", projectId);
        
        theGrid = Ext.ComponentQuery.query('#alertGrid')[0];
        updateGrid(theGrid, "Alerts", projectId);
        
        
        
        theGrid = Ext.ComponentQuery.query('#webserviceGrid')[0];
        updateGrid(theGrid, "Webservices", projectId);        
        theGrid = Ext.ComponentQuery.query('#auditGrid')[0];
        updateGrid(theGrid, "Audits", projectId);
        theGrid = Ext.ComponentQuery.query('#DAMGrid')[0];
        updateGrid(theGrid, "DAMPanel", projectId);    
        
        theGrid = Ext.ComponentQuery.query('#statGrid')[0];
        updateGrid(theGrid, "Stats", projectId);
        theGrid.store.load();
        
        theGrid = Ext.ComponentQuery.query('#methodGrid')[0];
        updateGrid(theGrid, "Methods", projectId);      
        theGrid.store.load();
        return;
        
        function updateGrid(grid, permission, projectId) {
            if ( typeof grid !== 'undefined' ) {        
                if (!delta3.utils.GlobalFunc.isPermitted(permission, projectId)) {
                    var gridTab = grid.up();    
                    var tb = grid.down('toolbar').removeAll();
                    grid.down('toolbar').destroy();
                    tb = null;
                    grid.removeAll();
                    grid.destroy();
                    gridTab.removeAll();
                    gridTab.destroy(); // remove tab this grid belonged to                   
                    grid = null;
                    gridTab = null;
                } else {
                    grid.store.groupId = projectId;
                    grid.store.loadPage(1);                
                }
            }            
        }
    }
});






