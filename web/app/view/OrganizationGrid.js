/* 
 * Organization Grid
 */

Ext.define('delta3.view.OrganizationGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.organization',
    itemId: 'organizationGrid',
    autoScroll: false,
    renderTo: document.body,
    minHeight: 170,    
    height: '100%',
    selModel: {mode: "SINGLE", allowDeselect: true},
    requires: [
        'Ext.data.Store',
        'Ext.toolbar.Paging',
        'Ext.grid.plugin.RowEditing',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'delta3.utils.GridFilter',
        'delta3.utils.GridPaginator',           
        'delta3.model.OrganizationModel'
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: false,
            listeners: {
                afteredit: function (rowEditor, changes, r, rowIndex) {
                    
                    /*-----commented code from v3.63--------------------------------
                    var thisGrid = Ext.ComponentQuery.query('#organizationGrid')[0];
                    thisGrid.store.save();
                    delta3.utils.GlobalFunc.doCloneStatFromRecord1(101);
                    */
                    
                    
                        //--------v3.64 added below code for resolution of bug DELQA-688 for save function
			//console.log("Calling sync Method");
			//thisGrid.store.sync();
                        var thisGrid = Ext.ComponentQuery.query('#organizationGrid')[0];
                        console.log("Calling save Method");
			thisGrid.store.save();
                        
                    //--------added/commeneted below code for resolution of bug DELQA-745 stopped calling anonymous function                 
                    //delta3.utils.GlobalFunc.doCloneStatFromRecord1(1);
                    
                    
                }
            }
        })
    ],
    border: false,
    dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',            
            items: [{
                text: 'Add Organization',
                iconCls: 'add_new-icon16',
                handler: function () {
                    var thisGrid = Ext.ComponentQuery.query('#organizationGrid')[0];
                    thisGrid.plugins[0].cancelEdit();
                    // Create a new record instance 
                    var r = delta3.model.OrganizationModel.create({
                        name: '',
                        longName: 'New Organization',
                        type: '',
                        createdTS: '0000-00-00 00:00:00.0',
                        updatedTS: '0000-00-00 00:00:00.0'});
                    thisGrid.store.insert(0, r);
                    thisGrid.plugins[0].startEdit(0, 0);
                }
            }, {
                itemId: 'removeOrganization',
                text: 'Remove Organization',
                iconCls: 'delete-icon16',
                handler: function () {
                    Ext.Msg.show({
                        title: 'DELTA',
                        message: 'You are about to delete Organization. Would you like to proceed?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function (btn) {
                            if (btn === 'yes') {
                                var thisGrid = Ext.ComponentQuery.query('#organizationGrid')[0]
                                var sm = thisGrid.getSelectionModel();
                                thisGrid.plugins[0].cancelEdit();
                                thisGrid.store.remove(sm.getSelection());
                                thisGrid.store.sync();
                                sm.select(0);
                            }
                        }
                    });
                }
            }, {
                itemId: 'refreshOrganization',
                text: 'Refresh',
                iconCls: 'refresh-icon16',
                handler: function () {
                    var thisGrid = Ext.ComponentQuery.query('#organizationGrid')[0]
                    thisGrid.store.load();
                }
            }]
        }, {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No organizations found',
            displayInfo: true
        }],
    initComponent: function () {
        var me = this;
        me.store = me.buildStore();
        me.store.load();
        me.dockedItems[1].store = me.store;
        me.columns = me.buildColumns();
        me.callParent();
        var tbr = me.getDockedItems('toolbar[dock="top"]')[0];  
        tbr.add(Ext.create(delta3.utils.GridFilter,{gridToSearch: me, local: false}));    
        tbr.add(Ext.create(delta3.utils.GridPaginator,{gridToReload: me}));  
    },
    buildColumns: function () {
        return [
            {text: 'ID', dataIndex: 'idOrganization', locked: true},
            {text: 'Name', dataIndex: 'name', editor: 'textfield'},
            {text: 'Long Name', dataIndex: 'longName', editor: 'textfield'},
            {text: 'Active', disabled: true, xtype: 'checkcolumn', dataIndex: 'active',  disabled: true, editor: 'checkboxfield'},
            // {text: 'Type', dataIndex: 'type', editor: 'textfield'},
            
            {text: 'Type', dataIndex: 'type', width: 100, editor:
                        new Ext.form.ComboBox({
                            store: delta3.utils.GlobalVars.orgTypeComboBoxStore,
                            displayField: 'type',
                            valueField: 'type',
                            emptyText: 'Not Assigned',
                            queryMode: 'local',
                            forceSelection: true
                            
                        })
            },  
            
            
            {text: 'GUID', dataIndex: 'guid'},
            {text: 'Key 1', dataIndex: 'key1'},
            {text: 'Key 2', dataIndex: 'key2'},
            {text: 'Parent ID', dataIndex: 'parentId', editor: 'numberfield'},
            {text: "Record Created TS", width: 120, sortable: true, dataIndex: 'createdTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Created By', dataIndex: 'createdBy', width: 100,
                renderer: function (val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'createdBy', delta3.utils.GlobalVars.UserStore);
                }},
            {text: "Record Updated TS", width: 120, sortable: true, dataIndex: 'updatedTS', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {text: 'Updated By', dataIndex: 'updatedBy', width: 100,
                renderer: function (val, cell, rec, r_idx, c_idx, store) {
                    return delta3.utils.GlobalFunc.lookupUserAlias(val, cell, rec, r_idx, c_idx, store, 'updatedBy', delta3.utils.GlobalVars.UserStore);
                }}
        ];
    },
    setActiveRecord: function (record) {
        this.activeRecord = record;
        if (record) {
            this.down('#save').enable();
            this.getForm().loadRecord(record);
        } else {
            this.down('#save').disable();
            this.getForm().reset();
        }
    },
    buildStore: function () {
        //return Ext.create('delta3.store.OrganizationStore');        
        return delta3.utils.GlobalVars.OrgStore;
    }
});



