/* 
 * Popup Window which allows pasitng JSON results and displaying results in tabs
 */

Ext.define('delta3.view.popup.ResultsPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.results',
    layout: 'vbox',
    width: 550,
    height: 600,
    itemId: 'resultPopup',
    modal: true,
    closeAction: 'destroy',
    packageName: 'd3',
    title: 'DELTA/OCEANS Results Import',
    items: [{
                xtype: 'combobox',
                margin: '5 5 5 5',
                fieldLabel: 'Select graphical package',
                itemId: 'reportingPackage',
                labelWidth: 200,
                labelAlign: 'left',                   
                store: delta3.utils.GlobalVars.graphPackageComboBoxStore,
                displayField: 'package',
                valueField: 'package',
                emptyText: 'd3',
                queryMode: 'local',
                forceSelection: true,
                multiSelect: false,
                tabIndex: 7,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        this.up('#resultPopup').packageName = cmb.getValue();
                    }
                }
            }, {
                xtype: 'textfield',
                margin: '5 5 5 5',                
                fieldLabel: 'Y Axis Label',
                itemId: 'outcomeName',
                labelWidth: 200,
                labelAlign: 'left',                   
                value: '',
                tabIndex: 1,
                allowBlank: true
            }, {
                xtype: 'textfield',
                margin: '5 5 5 5',                
                fieldLabel: 'Graph title',
                itemId: 'studyName',
                labelWidth: 200,
                labelAlign: 'left',                   
                value: '',
                tabIndex: 2,
                allowBlank: true
            }, {
                xtype: 'textfield',
                margin: '5 5 5 5',                
                fieldLabel: 'Graph title extended',
                itemId: 'description',
                labelWidth: 200,
                labelAlign: 'left',                   
                value: '',
                tabIndex: 3,
                allowBlank: true
            }, {
                xtype: 'textareafield',
                margin: '5 5 5 5',                  
                width: 528,
                height: 390,  
                emptyText: 'Paste here results in OCEANS JSON format...',
                submitEmptyText: false,
                grow: true,
                itemId: 'resultDataString',
                value: '',
                tabIndex: 4,
                allowBlank: true
            }],
    buttons: [
        {
            text: 'Show Results',
            tooltip: 'Save results from the text area and display',
            tabIndex: 5,
            handler: function() {
                var theString = Ext.ComponentQuery.query('#resultDataString')[0].value;
                if (typeof theString === 'undefined' || theString === '' ) {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please paste results JSON string into text area first.');
                    return;
                }                  
                var theStringUnescaped = theString.replace(/\\"/g,'"');   
                var theStringEscaped = theStringUnescaped.replace(/"/g,'\\"');
                delta3.utils.GlobalFunc.doSaveResults('{"processes":[{"idProcess":0,"idStudy":0,"idModel":0,"idFilter":0,"status":"Imported","progress":100,"active":true,"manualExecution":false,"description":"' 
                    + Ext.ComponentQuery.query('#description')[0].value
                    + '","name":"' + Ext.ComponentQuery.query('#studyName')[0].value
                    + '","message":"' + Ext.ComponentQuery.query('#outcomeName')[0].value
                    + '","data":"' + theStringEscaped + '"}]}'
                );
                var theTabs = Ext.ComponentQuery.query('#maintabs')[0];
                theTabs.add(Ext.create('delta3.view.rv.common.ResultsContainer', {
                    title: 'Imported from browser',
                    packageName: this.up('#resultPopup').packageName,
                    resultsData: theStringUnescaped,
                    processId: '',
                    name: Ext.ComponentQuery.query('#studyName')[0].value,
                    filterName: '',                                    
                    description: Ext.ComponentQuery.query('#description')[0].value,
                    outcomeName: Ext.ComponentQuery.query('#outcomeName')[0].value,
                    closable: true
                })).show();
                this.up('#resultPopup').destroy();                
                theTabs.updateLayout();
            }
        },
        {
            text: 'Close',
            tabIndex: 6,
            handler: function() {
                this.up('#resultPopup').destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});


