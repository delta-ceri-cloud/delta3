Ext.define('delta3.view.popup.FieldNameUniquePopup', {
    extend: 'Ext.Window',
    layout: 'fit',
    width: 400,
    height: 200,
    itemId: 'uniqueNamePopup',
    title: 'DELTA',
    modal: true,
    store: {},
    newName: '',
    closeAction: 'hide',
    items: new Ext.Panel(
            {
                frame: true,
                labelWidth: 180,
                labelAlign: 'right',
                title: 'Field name change',
                bodyStyle: 'padding:5px 5px 0',
                width: 400,
                height: 200,
                autoScroll: true,
                itemCls: 'form_row',
                defaultType: 'displayfield',
                buttons: [
                    {text: 'Accept',
                        handler: function() {
                            Ext.ComponentQuery.query('#uniqueNamePopup')[0].newName = Ext.ComponentQuery.query('#newFieldName')[0].value;
                            var thisGrid = Ext.ComponentQuery.query('#uniqueNamePopup')[0];
                            if (r.data.name.toUpperCase() !== Ext.ComponentQuery.query('#uniqueNamePopup')[0].newName.toUpperCase()) {
                                r.data.name = Ext.ComponentQuery.query('#uniqueNamePopup')[0].newName;
                                thisGrid.store.add(r);
                                thisGrid.store.sync();
                                setUsedIconInTableTreePanel(data.records[0].parentNode.data.text, data.records[0].get('text'));
                                thisGrid.setTitle(delta3.utils.GlobalFunc.emphasizeString("Status: needs processing"));
                                Ext.ComponentQuery.query('#uniqueNamePopup')[0].destroy();
                                return false;
                            } else {
                                delta3.utils.GlobalFunc.showDeltaMessage('Field names must be unique: ' + r.data.name);
                            }
                        }
                    },
                    {text: 'Cancel',
                        handler: function() {
                            Ext.ComponentQuery.query('#uniqueNamePopup')[0].destroy();
                            return false;
                        }
                    }
                ],
                items: [{
                        fieldLabel: 'Current field name',
                        name: 'currentFieldName',
                        allowBlank: false,
                        value: r.data.name
                    }, {
                        xtype: 'textfield',
                        fieldLabel: 'New field name',
                        itemId: 'newFieldName',
                        allowBlank: true,
                        value: Ext.ComponentQuery.query('#uniqueNamePopup')[0].newName
                    }
                ]
            })
});