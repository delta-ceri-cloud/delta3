/* 
 * Reccurence Popup
 */

Ext.define('delta3.view.popup.MemoPopup', {
    extend: 'Ext.Window',
    requires: ['Ext.form.FieldSet', 
            'Ext.form.field.Time',
            'Ext.form.field.Number',
            'Ext.form.field.Date'], 
    alias: 'widget.popup.memo',
    layout: 'fit',
    width: 400,
    height: 470,
    labelLength: 100,
    itemId: 'memoPopup',
    closeAction: 'destroy',
    title: 'DELTA Memo',
    items: [],
    initComponent: function() {
    }
});

