/* 
 * Popup Window which allows creation of relationship between model tables
 */

Ext.define('delta3.view.popup.RelationshipPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.relationship',
    layout: 'vbox',
    width: 360,
    height: 300,
    itemId: 'relationshipPopup',
    modal: true,
    labelWidth: 100,
    labelAlign: 'left',   
    dropdownWidth: 320,
    newFieldId: {},
    modelData: {},
    closeAction: 'destroy',
    title: 'DELTA Table Relationship',
    items: [],
    buttons: [
        {
            text: 'Save',
            tooltip: 'Save relationship in the Data Model',
            tabIndex: 5,
            handler: function() {
                var relTable1 = Ext.ComponentQuery.query('#relationshipTable1')[0].getSelection();
                var relKey1 = Ext.ComponentQuery.query('#relationshipKey1')[0].getSelection();
                var relTable2 = Ext.ComponentQuery.query('#relationshipTable2')[0].getSelection();
                var relKey2 = Ext.ComponentQuery.query('#relationshipKey2')[0].getSelection();
                var relType = Ext.ComponentQuery.query('#relationshipType')[0].getSelection();                
                if (relTable1 === null || relTable2 === null) {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select tables.');
                    return;
                }                   
                if (relKey1 === null || relKey2 === null) {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select table keys.');
                    return;
                }                 
                if (relType === null) {
                    delta3.utils.GlobalFunc.showDeltaMessage('Please select relationship type.');
                    return;
                }             
                if ( relKey1.data.type !== relKey2.data.type ) {
                    delta3.utils.GlobalFunc.showDeltaMessage('Cannot save: relationship key types have to match.');
                    return;                    
                }
                var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                var r = delta3.model.ModelRelationshipModel.create({
                            table1: relTable1.data.idModelTable,
                            key1: relKey1.data.idModelColumn,
                            table2: relTable2.data.idModelTable,
                            key2: relKey2.data.idModelColumn,
                            type: relType.data.value,
                            modelidModel: ModelData.modelSelected.idModel,
                            createdTS: '0000-00-00 00:00:00.0',
                            createdBy: '0',
                            updatedTS: '0000-00-00 00:00:00.0',
                            updatedBy: '0'
                        });
                thisGrid.store.insert(0, r);                
                thisGrid.store.sync();       
                this.up('#relationshipPopup').destroy();
            }
        },
        {
            text: 'Close',
            tabIndex: 6,
            handler: function() {
                this.up('#relationshipPopup').destroy();
            }
        }],
    initComponent: function() {
        var me = this;
            me.items[0] = new Ext.Panel(
            {
                width: 350,
                height: 280, 
                itemId: 'relationshipPanel',
                bodyStyle: 'padding:5px 5px 0',
                title: 'Relationship Definition',
                autoScroll: true,
                defaultType: 'displayfield'
            }); 
            var currValue = '';
            me.items[0].add(                
                {
                xtype: 'fieldset',                            
                layout: 'vbox',
                padding: '3,0,0,10',
                items: [{
                    xtype: 'combobox',
                    fieldLabel: 'Table',
                    store: me.modelData.modelTableComboStore,
                    itemId: 'relationshipTable1',
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,      
                    minWidth: me.dropdownWidth,
                    displayField: 'physicalName',
                    valueField: 'idModelTable',
                    queryMode: 'local',
                    value: currValue,
                    multiSelect: false,
                    forceSelection: false,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            var h = cmb.getValue();
                            me.modelData.modelKey1ComboStore.clearFilter(true);
                            me.modelData.modelKey1ComboStore.filter("modelTableidModelTable", h);
                            Ext.ComponentQuery.query('#relationshipKey1')[0].setSelection('');
                            Ext.ComponentQuery.query('#relationshipKey1')[0].setDisabled(false);
                            me.newFieldId = cmb.getValue();
                        }
                    }
                },{
                    xtype: 'combobox',
                    fieldLabel: 'Key',
                    store: me.modelData.modelKey1ComboStore,
                    itemId: 'relationshipKey1',
                    disabled: true,
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,     
                    minWidth: me.dropdownWidth,                    
                    displayField: 'name',
                    valueField: 'idModelColumn',
                    queryMode: 'local',
                    value: currValue,
                    multiSelect: false,
                    forceSelection: false,
                    renderTo: Ext.getBody(),
                    // Template for the dropdown menu.
                    // Note the use of the "x-list-plain" and "x-boundlist-item" class,
                    // this is required to make the items selectable.
                    tpl: Ext.create('Ext.XTemplate',
                        '<ul class="x-list-plain"><tpl for=".">',
                            '<li role="option" class="x-boundlist-item">{name} - {type}</li>',
                        '</tpl></ul>'
                    ),           
                    // template for the content inside text field
                    displayTpl: Ext.create('Ext.XTemplate',
                        '<tpl for=".">',
                            '{name} - {type}',
                        '</tpl>'
                    ),            
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            me.newFieldId = cmb.getValue();
                        }
                    }
                }]
            }); 
            me.items[0].add(                
                {
                xtype: 'fieldset',                            
                layout: 'vbox',
                padding: '3,0,0,10',
                items: [{
                    xtype: 'combobox',
                    fieldLabel: 'Table',
                    store: me.modelData.modelTableComboStore,
                    itemId: 'relationshipTable2',
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,    
                    minWidth: me.dropdownWidth,                    
                    displayField: 'physicalName',
                    valueField: 'idModelTable',
                    queryMode: 'local',
                    value: currValue,
                    multiSelect: false,
                    forceSelection: false,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            var h = cmb.getValue();
                            me.modelData.modelKey2ComboStore.clearFilter(true);
                            me.modelData.modelKey2ComboStore.filter("modelTableidModelTable", h);
                            Ext.ComponentQuery.query('#relationshipKey2')[0].setSelection('');
                            Ext.ComponentQuery.query('#relationshipKey2')[0].setDisabled(false);
                            me.newFieldId = cmb.getValue();
                        }
                    }
                },{
                    xtype: 'combobox',
                    fieldLabel: 'Key',
                    store: me.modelData.modelKey2ComboStore,
                    itemId: 'relationshipKey2',
                    disabled: true,
                    labelWidth: me.labelWidth,
                    labelAlign: me.labelAlign,     
                    minWidth: me.dropdownWidth,                    
                    displayField: 'name',
                    valueField: 'idModelColumn',
                    queryMode: 'local',
                    value: currValue,
                    multiSelect: false,
                    forceSelection: false,
                    renderTo: Ext.getBody(),
                    tpl: Ext.create('Ext.XTemplate',
                        '<ul class="x-list-plain"><tpl for=".">',
                            '<li role="option" class="x-boundlist-item">{name} - {type}</li>',
                        '</tpl></ul>'
                    ),           
                    displayTpl: Ext.create('Ext.XTemplate',
                        '<tpl for=".">',
                            '{name} - {type}',
                        '</tpl>'
                    ),                      
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            me.newFieldId = cmb.getValue();
                        }
                    }
                }]
            });      
            me.items[0].add({
                xtype: 'combobox',
                itemId: 'relationshipType',
                labelWidth: me.labelWidth,
                labelAlign: me.labelAlign,      
                padding: '0,0,0,46',
                fieldLabel: 'Type',
                store: delta3.utils.GlobalVars.relationshipComboBoxStore,
                displayField: 'type',
                valueField: 'value',
                emptyText: '',
                queryMode: 'local',
                forceSelection: true,
                listeners: {
                    'select': function(cmb, rec, idx) {
                        var h = cmb.getValue();
                        //var thisGrid = Ext.ComponentQuery.query('#relationshipGrid')[0];
                        //thisGrid.getSelectionModel().getSelection()[0].set('type', h);
                    }
                }
            });              
            me.items[0].updateLayout();
        me.callParent();
    }
});


