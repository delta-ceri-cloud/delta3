/* 
 * Import Model from file Popup Window
 */

Ext.define('delta3.view.popup.ImportModelFilePopup', {
    extend: 'Ext.Window',
    requires: ['Ext.form.field.File'], 
    alias: 'widget.popup.importModelFile',
    layout: 'fit',
    width: 470,
    height: 440,
    itemId: 'importModelFilePopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA Import',
    items: [],
    initComponent: function() {
        var me = this;
         me.items[0] = new Ext.form.Panel({
            title: 'Data Model Import',
            width: 450,
            bodyPadding: 10,
            frame: true,
            renderTo: Ext.getBody(),
            items: [{
                xtype: 'filefield',
                name: 'importFile',
                fieldLabel: 'File',
                labelWidth: 50,
                labelAlign: 'left',
                msgTarget: 'side',
                emptyText: 'Upload JSON File',
                allowBlank: false,
                // v3.64 Added and adjust css properties DELQA-524
                //buttonOnly: true,
                anchor: '100%',
                buttonText: 'Select file...'
            }, {
                xtype: 'checkbox',
                labelWidth: 150,
                labelAlign: 'left',                              
                fieldLabel: 'Process missing',
                inputValue: true,
                value: true,
                itemId: 'processMissingCheckbox'                           
            },{
                xtype: 'textareafield',
                width: 430,
                height: 300,            
                grow: true,
                itemId: 'importDataModelFeedback',
                value: '',
                allowBlank: true
            }],

            buttons: [{
                text: 'Upload',
                handler: function(b) {
                    var form = this.up('form').getForm();
                    if(form.isValid()){
                        form.submit({
                            url: '/Delta3/webresources/import/importFile',
                            waitMsg: 'Uploading import file...',
                            success: function(f, a) {
                                Ext.ComponentQuery.query('#importDataModelFeedback')[0].setValue(a.result.status.message);
                            },
                            failure: function(f,a){
                                Ext.ComponentQuery.query('#importDataModelFeedback')[0].setValue(a.response.responseText);
                            }                            
                        });
                    }
                }
            }, {
                text: 'Upload and Create Flat Table',
                handler: function(b) {
                    var form = this.up('form').getForm();
                    if(form.isValid()){
                        var theUrl = '/Delta3/webresources/import/importModelFileAndProcessAll';
                        if ( Ext.ComponentQuery.query('#processMissingCheckbox')[0].value === false ) {
                            theUrl = '/Delta3/webresources/import/importModelFileAndProcessNoMissing';
                        }
                        form.submit({
                            url: theUrl,
                            waitMsg: 'Uploading import file...',
                            success: function(f, a) {
                                delta3.utils.GlobalFunc.showDeltaMessage('Your file "' + a.result.file + '" has been uploaded and is processing.');
                            },
                            failure: function(f,a){
                                Ext.ComponentQuery.query('#importDataModelFeedback')[0].setValue(a.response.responseText);
                            }                            
                        });                        
                    }
                }
            }, {
                text: 'Close',
                handler: function() {
                    this.up('#importModelFilePopup').destroy();
                }
            }]
        }); 
        me.callParent();
    }
});


