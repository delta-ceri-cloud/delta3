/* 
 * Import Popup Window
 */
var maxParamWidth = 200;

Ext.define('delta3.view.popup.WebServicePopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.webservices',
    layout: 'vbox',
    width: 450,
    height: 720,
    itemId: 'webservicePopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA Web Service',
    requestHttpType: 'POST',
    items: [{
                xtype: 'fieldcontainer',                            
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    fieldLabel: 'URL',
                    itemId: 'wsName',
                    labelWidth: 30,
                    labelAlign: 'left',     
                    minWidth: 260,
                    margin: '5 5 5 5',
                    value: '',
                    allowBlank: true
                }, {
                    xtype: 'combobox',
                    margin: '5 5 5 5',
                    fieldLabel: 'HTTP Method',
                    itemId: 'httpMethod',
                    labelWidth: 80,
                    labelAlign: 'left',    
                    maxWidth: 150,
                    store: delta3.utils.GlobalVars.httpMethodComboBoxStore,
                    displayField: 'method',
                    valueField: 'method',
                    emptyText: 'POST',
                    queryMode: 'local',
                    forceSelection: true,
                    multiSelect: false,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            this.up('#webservicePopup').requestHttpType = cmb.getValue();
                        }
                    }
                }] 
            }, {
                xtype: 'fieldset',
                fieldLabel: '',
                title: 'HTTP Paramaters',
                hidden: false,
                width: 430,
                height: 134,
                labelAlign: 'left',       
                labelWidth: 40,
                layout: 'vbox',
                items: [{xtype: 'container',
                        layout: 'hbox',
                        items: [{
                                xtype: 'textfield',
                                fieldLabel: 'name',
                                itemId: 'httpName1',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',
                                value: ''
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'value',
                                itemId: 'httpValue1',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',  
                                value: ''
                            }]
                        }, {
                        xtype: 'container',
                        layout: 'hbox',
                        items: [{
                                xtype: 'textfield',
                                fieldLabel: 'name',
                                itemId: 'httpName2',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',
                                value: ''
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'value',
                                itemId: 'httpValue2',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2', 
                                value: ''
                            }]
                        }, {
                        xtype: 'container',
                        layout: 'hbox',
                        items: [{
                                xtype: 'textfield',
                                fieldLabel: 'name',
                                itemId: 'httpName3',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',
                                value: ''
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'value',
                                itemId: 'httpValue3',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',
                                value: ''
                            }]
                        }, {
                        xtype: 'container',
                        layout: 'hbox',
                        items: [{
                                xtype: 'textfield',
                                fieldLabel: 'name',
                                itemId: 'httpName4',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2', 
                                value: ''
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'value',
                                itemId: 'httpValue4',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',
                                value: ''
                            }]
                        }]
            }, {
                xtype: 'label',
                text: 'Type or paste in Request JSON',
                margin: '5 5 5 5'
            }, {
                xtype: 'textareafield',
                width: 425,
                height: 200,            
                margin: '5 5 5 5',
                grow: true,
                itemId: 'wsRequest',
                value: '',
                allowBlank: true
            }, {
                xtype: 'label',
                text: 'Response',
                margin: '5 5 5 5'
            }, {
                xtype: 'textareafield',
                width: 425,
                height: 200,         
                margin: '5 5 5 5',
                grow: true,
                itemId: 'wsResponse',
                value: '',
                allowBlank: true
            }],
    buttons: [
        {
            text: 'Call Web Service',
            handler: function() {
                var theRequest = '';
                var wsName = Ext.ComponentQuery.query('#wsName')[0].value;                
                if ( Ext.ComponentQuery.query('#webservicePopup')[0].requestHttpType === 'POST' ) {
                    theRequest = Ext.ComponentQuery.query('#wsRequest')[0].value;
                } else {
                    theRequest = '{';
                    if ( Ext.ComponentQuery.query('#httpName1')[0].value !== '' ) {
                        theRequest += '"' + Ext.ComponentQuery.query('#httpName1')[0].value + '":"' + Ext.ComponentQuery.query('#httpValue1')[0].value + '",';
                    }
                    if ( Ext.ComponentQuery.query('#httpName2')[0].value !== '' ) {
                        theRequest += '"' +Ext.ComponentQuery.query('#httpName2')[0].value + '":"' + Ext.ComponentQuery.query('#httpValue2')[0].value + '",';
                    }
                    if ( Ext.ComponentQuery.query('#httpName3')[0].value !== '' ) {
                        theRequest += '"' +Ext.ComponentQuery.query('#httpName3')[0].value + '":"' + Ext.ComponentQuery.query('#httpValue3')[0].value + '",';
                    }
                    if ( Ext.ComponentQuery.query('#httpName4')[0].value !== '' ) {
                        theRequest += '"' +Ext.ComponentQuery.query('#httpName4')[0].value + '":"' + Ext.ComponentQuery.query('#httpValue4')[0].value + '",';
                    }   
                    if ( theRequest !== '{' ) {
                        theRequest = theRequest.slice(0, -1);
                    }
                    theRequest += '}';
                    theRequest = JSON.parse(theRequest);
                }
                delta3.utils.GlobalFunc.doCallWebService(theRequest, wsName, Ext.ComponentQuery.query('#webservicePopup')[0].requestHttpType, displayResponseFunc);   
                function displayResponseFunc(response, options) {
                    if ( response.responseText !== 'ok') {
                        var win = window.open ("http://localhost:8080/Delta3/index.html",'_blank',"top=60, left=100, status=0, toolbar=1, scrollbars=1, height=880, width=800");
                        win.document.write(response.responseText);                     
                    }                    
                    var textArea = Ext.ComponentQuery.query('#wsResponse')[0];
                    textArea.setValue(response.responseText);                    
                }
            }
        },
        {
            text: 'Close',
            handler: function() {
                this.up('#webservicePopup').destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});


