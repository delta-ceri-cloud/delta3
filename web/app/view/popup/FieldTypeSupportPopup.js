Ext.define('delta3.view.popup.FieldTypeSupportPopup', {
    extend: 'Ext.Window',
    layout: 'fit',
    width: 400,
    height: 200,
    itemId: 'fieldTypeSupportPopup',
    title: 'DELTA',
    modal: true,
    store: {},
    record: {},
    data: {},
    newType: '',
    closeAction: 'hide',
    items: [],
    initComponent: function() {
        var me = this;
        me.items[0] = new Ext.Panel({
            frame: true,
            labelWidth: 180,
            labelAlign: 'right',
            title: 'Field type verification',
            bodyStyle: 'padding:5px 5px 0',
            width: 400,
            height: 200,
            autoScroll: true,
            itemCls: 'form_row',
            store: me.store,
            record: me.record,
            data: me.data,
            newType: me.newType,          
            defaultType: 'displayfield',
            buttons: [
                {text: 'Accept',
                    handler: function() {
                        var thisWin = Ext.ComponentQuery.query('#fieldTypeSupportPopup')[0];
                        if ( delta3.utils.GlobalFunc.isFieldTypeSupported(thisWin.newName.toUpperCase()) ) {
                            thisWin.record.data.type = thisWin.newType;
                            thisWin.store.add(thisWin.record);
                            thisWin.store.sync();
                            setUsedIconInTableTreePanel(thisWin.data.records[0].parentNode.data.text, thisWin.data.records[0].get('text'));
                            this.up().setTitle(delta3.utils.GlobalFunc.emphasizeString("Status: needs processing"));
                            Ext.ComponentQuery.query('#fieldTypeSupportPopup')[0].destroy();
                            return false;
                        } else {
                            delta3.utils.GlobalFunc.showDeltaMessage('Field type must be supported. Type ' + thisWin.newType + ' is not.');
                        }
                    }
                },
                {text: 'Cancel',
                    handler: function() {
                        Ext.ComponentQuery.query('#fieldTypeSupportPopup')[0].destroy();
                        return false;
                    }
                }
            ],
            items: [{
                    fieldLabel: 'Current field type',
                    name: 'currentFieldType',
                    allowBlank: false,
                    value: ''
                }, {
                    xtype: 'textfield',
                    fieldLabel: 'New field type',
                    itemId: 'newFieldType',
                    allowBlank: true,
                    value: ''
                }
            ],
            initComponent: function() {
                var me = this;
                me.items[0].value = me.record.data.type;
                me.items[1].value = me.newType;                       
                me.callParent();
            } 
        });
        me.callParent();        
    }
});