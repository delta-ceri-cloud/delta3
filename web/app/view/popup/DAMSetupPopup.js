/* 
 * Import Popup Window
 */
var maxParamWidth = 200;

Ext.define('delta3.view.popup.DAMSetupPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.damsetup',
    layout: 'vbox',
    width: 640,
    height: 460,
    bodyPadding: 12,
    itemId: 'damSetupPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTAlytics Setup',
    requestHttpType: 'POST',
    items: [{
                xtype: 'fieldset',                            
                layout: 'hbox',
                //bodyPadding: 20,
                margin: '10 10 10 10',
                title: 'DELTA REST Authentication',
                items: [{
                    xtype: 'textfield',
                    fieldLabel: 'User name',
                    itemId: 'restUser',
                    labelWidth: 70,
                    labelAlign: 'left',     
                    minWidth: 20,
                    //margin: '5 5 5 5',
                    value: '',
                    allowBlank: true
                }, {
                    xtype: 'textfield',
                    fieldLabel: '   Password',
                    itemId: 'restPassword',
                    labelWidth: 70,
                    labelAlign: 'left',     
                    minWidth: 20,
                    //margin: '5 5 5 5',
                    value: '',
                    allowBlank: true
                }, {
                    xtype: 'button',                    
                    text: 'Save',
                    margin: 5,
                    handler: function() {
                        var restUser = Ext.ComponentQuery.query('#restUser')[0].value;   
                        var restPassword = Ext.ComponentQuery.query('#restPassword')[0].value;                  
                        var authString = '{"Username": "' + restUser +'","Password": "' + restPassword + '"}';
                        delta3.utils.GlobalFunc.callLocalStatPackage('setupREST', callback, '?auth=' + btoa(authString));
                        function callback(response) {
                            delta3.utils.GlobalFunc.showDeltaMessage(response.responseText);                
                        }
                    }
                },  {
                        xtype: 'button',
                        text: 'Read',
                        margin: 5,
                        handler: function() {            
                            delta3.utils.GlobalFunc.callLocalStatPackage('readSetupData', callback, '?type=REST');
                            function callback(response) {  
                                try {
                                    var respTxt = atob(response.responseText);
                                    var authInfo = JSON.parse(respTxt);
                                    Ext.ComponentQuery.query('#restUser')[0].setValue(authInfo.Username);        
                                    Ext.ComponentQuery.query('#restPassword')[0].setValue(authInfo.Password);                                         
                                } catch (err) {
                                    Ext.ComponentQuery.query('#restUser')[0].setValue(response.responseText);                                       
                                }                                   
                            }
                        }
                    }] 
            }, {                
                xtype: 'fieldset',
                layout: 'hbox',
                //bodyPadding: 20,
                margin: '10 10 10 10',                
                title: 'DELTA REST URL',                
                items: [{
                        xtype: 'textfield',
                        width: 460,
                        height: 20,            
                        //margin: '5 5 5 5',
                        grow: true,
                        itemId: 'baseURL',
                        value: '',
                        allowBlank: true
                    },  {
                        xtype: 'button',
                        text: 'Save',
                        margin: 5,
                        handler: function() {
                            var url = Ext.ComponentQuery.query('#baseURL')[0].value;               
                            delta3.utils.GlobalFunc.callLocalStatPackage('setupURL', callback, '?url=' + btoa(url));
                            function callback(response) {  
                                delta3.utils.GlobalFunc.showDeltaMessage(response.responseText);
                            }
                        }
                    },  {
                        xtype: 'button',
                        text: 'Read',
                        margin: 5,
                        handler: function() {            
                            delta3.utils.GlobalFunc.callLocalStatPackage('readSetupData', callback, '?type=URL');
                            function callback(response) {  
                                 var respTxt;
                                try {
                                    respTxt = atob(response.responseText);
                                } catch (err) {
                                    respTxt = response.responseText;
                                }
                                Ext.ComponentQuery.query('#baseURL')[0].setValue(respTxt);                               
                            }
                        }
                    }]
            } , {                
                xtype: 'fieldset',
                layout: 'hbox',
                //bodyPadding: 20,
                margin: '10 10 10 10',                
                title: 'DELTAlytics Database Connection String',                
                items: [{
                        xtype: 'textfield',
                        width: 460,
                        height: 20,            
                        //margin: '5 5 5 5',
                        grow: true,
                        itemId: 'damDbConn',
                        value: '',
                        allowBlank: true
                    },  {
                        xtype: 'button',
                        text: 'Save',
                        margin: 5,
                        handler: function() {
                            var url = Ext.ComponentQuery.query('#damDbConn')[0].value;               
                            delta3.utils.GlobalFunc.callLocalStatPackage('setupDAMdb', callback, '?DAMdb=' + btoa(url));
                            function callback(response) {  
                                delta3.utils.GlobalFunc.showDeltaMessage(response.responseText);                                
                            }
                        }
                    },  {
                        xtype: 'button',
                        text: 'Read',
                        margin: 5,
                        handler: function() {            
                            delta3.utils.GlobalFunc.callLocalStatPackage('readSetupData', callback, '?type=DAMdb');
                            function callback(response) {  
                                var respTxt;
                                try {
                                    respTxt = atob(response.responseText);
                                } catch (err) {
                                    respTxt = response.responseText;
                                }
                                Ext.ComponentQuery.query('#damDbConn')[0].setValue(respTxt);
                            }
                        }
                    }]
            }, {                
                xtype: 'fieldset',
                layout: 'hbox',
                //bodyPadding: 20,
                margin: '10 10 10 10',                
                title: 'DELTA Database Connection String',                
                items: [{
                        xtype: 'textfield',
                        width: 460,
                        height: 20,            
                        //margin: '5 5 5 5',
                        grow: true,
                        itemId: 'deltaDbConn',
                        value: '',
                        allowBlank: true
                    },  {
                        xtype: 'button',
                        text: 'Save',
                        margin: 5,
                        handler: function() {
                            var url = Ext.ComponentQuery.query('#deltaDbConn')[0].value;               
                            delta3.utils.GlobalFunc.callLocalStatPackage('setupDELTAdb', callback, '?DELTAdb=' + btoa(url));
                            function callback(response) {  
                                delta3.utils.GlobalFunc.showDeltaMessage(response.responseText);                                
                            }
                        }
                    },  {
                        xtype: 'button',
                        text: 'Read',
                        margin: 5,
                        handler: function() {            
                            delta3.utils.GlobalFunc.callLocalStatPackage('readSetupData', callback, '?type=DELTAdb');
                            function callback(response) {  
                                var respTxt;
                                try {
                                    respTxt = atob(response.responseText);
                                } catch (err) {
                                    respTxt = response.responseText;
                                }
                                Ext.ComponentQuery.query('#deltaDbConn')[0].setValue(respTxt);                                
                            }
                        }
                    }]
            }, {                
                xtype: 'fieldset',
                layout: 'hbox',
                //bodyPadding: 20,
                margin: '10 10 10 10',                
                title: 'DELTA Data Connection String',                
                items: [{
                        xtype: 'textfield',
                        width: 460,
                        height: 20,            
                        //margin: '5 5 5 5',
                        grow: true,
                        itemId: 'dataDbConn',
                        value: '',
                        allowBlank: true
                    },  {
                        xtype: 'button',
                        text: 'Save',
                        margin: 5,
                        handler: function() {
                            var url = Ext.ComponentQuery.query('#dataDbConn')[0].value;               
                            delta3.utils.GlobalFunc.callLocalStatPackage('setupDatadb', callback, '?Datadb=' + btoa(url));
                            function callback(response) {  
                                delta3.utils.GlobalFunc.showDeltaMessage(response.responseText);                                
                            }
                        }
                    },  {
                        xtype: 'button',
                        text: 'Read',
                        margin: 5,
                        handler: function() {            
                            delta3.utils.GlobalFunc.callLocalStatPackage('readSetupData', callback, '?type=Datadb');
                            function callback(response) {  
                                var respTxt;
                                try {
                                    respTxt = atob(response.responseText);
                                } catch (err) {
                                    respTxt = response.responseText;
                                }
                                Ext.ComponentQuery.query('#dataDbConn')[0].setValue(respTxt);                                
                            }
                        }
                    }]
            }],
    buttons: [
        {
            text: 'Close',
            handler: function() {
                this.up('#damSetupPopup').destroy();
            }
        }]
});


