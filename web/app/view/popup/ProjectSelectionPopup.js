/* 
 * Project Selection Popup Window
 */

Ext.define('delta3.view.popup.ProjectSelectionPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.project',
    //layout: 'fit',
    width: 400,
    height: 200,
    itemId: 'projectPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA Project Selection',
    items: [],
    buttons: [
        {
            text: 'Save',
            handler: function() {          
                this.up('#projectPopup').destroy();
            }
        },
        {
            text: 'Cancel',
            handler: function() {
                this.up('#projectPopup').destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        me.store = Ext.create('delta3.store.ProjectStore');
        me.store.load();
        var projectDropDown = new Ext.form.ComboBox({
            store: me.store,
            itemId: 'projectComboBox',
            fieldLabel: 'Project',
            emptyText: 'Select a project..',
            labelAlign: 'right',
            queryMode: 'local',
            displayField: 'name',
            valueField: 'idGroup',
            multiSelect: false,
            forceSelection: false,
            listeners: {
                'focus': function() {
                    me.projectStore.filter('active', true);       
                },                
                'select': function(cmb, rec, idx) {
                    me.project = cmb.getValue();
                }
            }
        }); 
        me.items[0] = projectDropDown;
        me.callParent();
    }
});


