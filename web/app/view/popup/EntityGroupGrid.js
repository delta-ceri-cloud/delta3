/* 
 * Entity Group Grid,
 * can be specialized for different entities that need grouping
 */

Ext.define('delta3.view.popup.EntityGroupGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.grid.groupEntity',
    itemId: 'groupEntityGrid',
    autoScroll: false,
    renderTo: document.body,
    height: '100%',
    selType: 'checkboxmodel',
    requires: [
        'Ext.data.Store',
        'Ext.selection.RowModel',
        'Ext.grid.column.Column',
        'Ext.data.Model',
        'Ext.selection.RowModel',
        'delta3.model.FieldModel',
        'delta3.store.EntityGroupStore',
        'Ext.grid.column.Template'
    ],
    border: false,
    columnLines: true,
    hideHeaders: true,
    entityId: {},
    entityType: {},
    entityName: {},
    newGroupId: {},
    tbar: [{},
        {
            itemId: 'addEntity',
            text: 'Assign to Project',
            iconCls: 'add_new-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#groupEntityGrid')[0];
                if (thisGrid.newGroupId > 0) {
                    var r = Ext.ComponentQuery.query('#mainViewPort')[0].projectStore.findRecord('idGroup', thisGrid.newGroupId);                    
                    thisGrid.store.insert(0, r);
                    delta3.utils.GlobalFunc.saveRelationship(r.data.name, thisGrid.entityId, thisGrid.entityType, r.data.idGroup);
                    thisGrid.newGroupId = 0;
                }
                thisGrid.getView().refresh();
            }
        }, {
            itemId: 'entityGroupDelete',
            text: 'Delete',
            iconCls: 'delete-icon16',
            handler: function() {
                var thisGrid = Ext.ComponentQuery.query('#groupEntityGrid')[0];
                var sm = thisGrid.getSelectionModel();
                var selection = sm.getSelection();
                thisGrid.store.remove(selection);
                for (var i = 0; i < selection.length; i++) {
                    delta3.utils.GlobalFunc.removeRelationship(selection[i].data.name, thisGrid.entityId, thisGrid.entityType, selection[i].data.idGroup);
                    //thisGrid.removeRelationship(selection[i].data.name, thisGrid.entityId, selection[i].data.id);
                }
                thisGrid.getView().refresh();
            }
        }],
    dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            emptyMsg: 'No entities found',
            displayInfo: false
        }],
    initComponent: function() {
        var me = this;
        me.columns = me.buildColumns();
        me.store = me.buildStore(me.entityType, me.entityId);
        me.dockedItems[0].store = me.store;
        var dropDown = new Ext.form.ComboBox({
            store: Ext.ComponentQuery.query('#mainViewPort')[0].projectStore,
            itemId: 'groupEntityComboBox',
            displayField: 'name',
            valueField: 'idGroup',
            queryMode: 'local',
            multiSelect: false,
            forceSelection: true,
            listeners: {
                'select': function(cmb, rec, idx) {
                    me.newGroupId = cmb.getValue();
                }
            }
        });
        me.tbar[0] = dropDown;
        me.callParent();
        me.store.load();
    },
    buildColumns: function() {
        return [
            {text: 'Name', dataIndex: 'name', width: 126},
            //{text: 'Name',  xtype:'templatecolumn', tpl:'<b>{name}</b>', width: 110},           
            {text: 'Description', dataIndex: 'description', width: 204}
            //{text: 'Type', dataIndex: 'type', width: 68}
        ];
    },
    buildStore: function(type, entityId) {
        return new delta3.store.EntityGroupStore({entityType: type, entityId: entityId});
    }
});

