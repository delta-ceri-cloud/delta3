/* 
 * Group Popup Window, 
 * can be specialized for different entities that need grouping
 */

Ext.define('delta3.view.popup.GroupPopup', {
    extend: 'Ext.Window',
    alias: 'widget.popup.group',
    requires: ['delta3.view.popup.EntityGroupGrid'],
    layout: 'fit',
    width: 370,
    height: 260,
    itemId: 'groupPopup',
    modal: true,
    closeAction: 'destroy',
    title: 'DELTA',
    entityId: {},
    entityType: {},
    items: [],
    buttons: [
        {
            text: 'Close',
            handler: function() {
                var thisWin = Ext.ComponentQuery.query('#groupPopup')[0];
                var thisGrid1 = Ext.ComponentQuery.query('#modelGrid')[0];
                thisGrid1.store.load();  
                Ext.ComponentQuery.query('#modelGrid')[0].getView().refresh();
                
                thisWin.destroy();
            }
        }],
    initComponent: function() {
        var me = this;
        var title = 'Project selection for ' + me.entityType +  ': ' + me.selectedRecord.get('name');
        me.items[0] = new delta3.view.popup.EntityGroupGrid(
                {   title: title,
                    entityName: me.selectedRecord.get('name'),
                    entityType: me.entityType,
                    entityId: me.entityId
                });
        me.callParent();
    }
});
