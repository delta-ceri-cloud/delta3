/* 
 * DELTA analytics Module Container
 */

Ext.define('delta3.view.DAMContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.dAM',
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;   
        me.items = {
                xtype:  'panel.dam',
                region: 'center'
        },   
        me.callParent();
    }
});

