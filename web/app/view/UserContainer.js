/* 
 * User Container
 */

Ext.define('delta3.view.UserContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.users',
    autoDestroy: true,
    initComponent:  function(){ 
        var me = this;
        me.items = [{
                xtype:  'grid.user',
                region: 'center'
        }];    
        me.callParent();
    }
});

