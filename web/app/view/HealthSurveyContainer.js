/* 
 * Health Survey Container
 */

Ext.define('delta3.view.HealthSurveyContainer', {
    extend: 'Ext.container.Container',
    alias:  'widget.container.healthSurvey',
    //height: 84+21*delta3.utils.GlobalVars.pageSize,            
    layout: 'fit',
    initComponent:  function(){ 
        var me = this;   
        me.items = {
                xtype:  'panel.healthsurvey',
                region: 'center'
        },   
        me.callParent();
    }
});

