/* 
 * Import Popup Window
 */
var maxParamWidth = 200;

Ext.define('delta3.view.WebServicePanel', {
    extend: 'Ext.Panel',
    alias: 'widget.panel.webservices',
    layout: 'vbox',
    width: '100%',
    height: '100%',
    itemId: 'webservicePanel',
    requestHttpType: 'POST',
    tbar: [{
            text: 'Call Web Service',
            handler: function() {
                var theRequest = '';
                var wsName = Ext.ComponentQuery.query('#wsName')[0].value;                
                if ( Ext.ComponentQuery.query('#webservicePanel')[0].requestHttpType === 'POST' ) {
                    theRequest = Ext.ComponentQuery.query('#wsRequest')[0].value;
                } else {
                    theRequest = '{';
                    for (var i=0; i<5; i++ ) {
                        if ( Ext.ComponentQuery.query('#httpName' + i)[0].value !== '' ) {
                            theRequest += '"' + Ext.ComponentQuery.query('#httpName' + i)[0].value + '":"' + Ext.ComponentQuery.query('#httpValue' +i)[0].value + '",';
                        }
                    }             
                    if ( theRequest !== '{' ) {
                        theRequest = theRequest.slice(0, -1);
                    }
                    theRequest += '}';
                    theRequest = JSON.parse(theRequest);
                }
                delta3.utils.GlobalFunc.doCallWebService(theRequest, wsName, Ext.ComponentQuery.query('#webservicePanel')[0].requestHttpType, displayResponseFunc);   
                function displayResponseFunc(response, options) {         
                    var textArea = Ext.ComponentQuery.query('#wsResponse')[0];
                    textArea.setValue(response.responseText);                    
                }
            }
        }],
    items: [{
                xtype: 'fieldcontainer',                            
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    fieldLabel: 'URL',
                    itemId: 'wsName',
                    labelWidth: 30,
                    labelAlign: 'left',     
                    minWidth: 260,
                    margin: '5 5 5 5',
                    value: '',
                    allowBlank: true
                }, {
                    xtype: 'combobox',
                    margin: '5 5 5 5',
                    fieldLabel: 'HTTP Method',
                    itemId: 'httpMethod',
                    labelWidth: 80,
                    labelAlign: 'left',    
                    maxWidth: 150,
                    store: delta3.utils.GlobalVars.httpMethodComboBoxStore,
                    displayField: 'method',
                    valueField: 'method',
                    emptyText: 'POST',
                    queryMode: 'local',
                    forceSelection: true,
                    multiSelect: false,
                    listeners: {
                        'select': function(cmb, rec, idx) {
                            this.up('#webservicePanel').requestHttpType = cmb.getValue();
                        }
                    }
                }] 
            }, {
                xtype: 'fieldset',
                fieldLabel: '',
                title: 'HTTP Paramaters',
                hidden: false,
                width: 440,
                height: 150,
                labelAlign: 'left',       
                labelWidth: 40,
                margin: '5 5 5 5',
                layout: 'vbox',
                items: [{xtype: 'container',
                        layout: 'hbox',
                        items: [{
                                xtype: 'textfield',
                                fieldLabel: 'name',
                                itemId: 'httpName0',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',
                                value: ''
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'value',
                                itemId: 'httpValue0',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',  
                                value: ''
                            }]
                        }, {
                        xtype: 'container',
                        layout: 'hbox',
                        items: [{
                                xtype: 'textfield',
                                fieldLabel: 'name',
                                itemId: 'httpName1',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',
                                value: ''
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'value',
                                itemId: 'httpValue1',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2', 
                                value: ''
                            }]
                        }, {
                        xtype: 'container',
                        layout: 'hbox',
                        items: [{
                                xtype: 'textfield',
                                fieldLabel: 'name',
                                itemId: 'httpName2',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',
                                value: ''
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'value',
                                itemId: 'httpValue2',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',
                                value: ''
                            }]
                        }, {
                        xtype: 'container',
                        layout: 'hbox',
                        items: [{
                                xtype: 'textfield',
                                fieldLabel: 'name',
                                itemId: 'httpName3',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2', 
                                value: ''
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'value',
                                itemId: 'httpValue3',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',
                                value: ''
                            }]
                        }, {
                        xtype: 'container',
                        layout: 'hbox',
                        items: [{
                                xtype: 'textfield',
                                fieldLabel: 'name',
                                itemId: 'httpName4',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2', 
                                value: ''
                            }, {
                                xtype: 'textfield',
                                fieldLabel: 'value',
                                itemId: 'httpValue4',
                                labelWidth: 40,
                                maxWidth: maxParamWidth,
                                margin: '2 15 2 2',
                                value: ''
                            }]
                        }]
            }, {
                xtype: 'label',
                text: 'Type or paste in Request JSON',
                margin: '5 5 5 5'
            }, {
                xtype: 'textareafield',
                width: 800,
                height: 160,            
                margin: '5 5 5 5',
                grow: true,
                itemId: 'wsRequest',
                value: '',
                allowBlank: true
            }, {
                xtype: 'label',
                text: 'Response',
                margin: '5 5 5 5'
            }, {
                xtype: 'textareafield',
                width: 800,
                height: 160,         
                margin: '5 5 5 5',
                grow: true,
                itemId: 'wsResponse',
                value: '',
                allowBlank: true
            }]
});


