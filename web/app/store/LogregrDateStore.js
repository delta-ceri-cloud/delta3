/* 
 * Logistic Regression Date/Value Store
 */


Ext.define('delta3.store.LogregrDateStore', {
    extend: 'Ext.data.Store',
    alias: 'store.logregrdate',
    storeId: 'logregrDateStore',
    model: 'delta3.model.LogregrDateModel',
    required: ['Ext.data.field.Field'],
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    logregrName: {},
    logregrId: {},
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {logregr: '{"logregrs":[{"name":"' + this.logregrName
                                + '", "idLogregr":"' + JSON.stringify(this.logregrId) + '"}]}'};
                },
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);                                   
                            } else {
                                // STEP1: expand model of regregrFieldGrid with column data received from logregrData
                                var logregrFieldGrid = Ext.ComponentQuery.query('#logregrFieldGrid')[0];
                                logregrFieldGrid.width = delta3.utils.GlobalVars.initWidth;
                                logregrFieldGrid.height = delta3.utils.GlobalVars.initHeight;
                                var newFields = [];
                                var columnSize = this.getColumnSize(response);
                                this.removeDateColumns(logregrFieldGrid);
                                for (var i = 0; i < columnSize; i++) {
                                    var len = logregrFieldGrid.headerCt.gridDataColumns.length;
                                    var column = new Ext.grid.column.Column({
                                        text: response.logregrdates[i].name, 
                                        dataIndex: 'value' + i, 
                                        width: delta3.utils.GlobalVars.dateLength, 
                                        editor: 'textfield'});
                                    logregrFieldGrid.headerCt.insert(len, column);
                                    logregrFieldGrid.width += delta3.utils.GlobalVars.dateLength;
                                    newFields[i] = new Ext.data.field.Field({name: 'value' + i, type: 'string', persist: false});
                                    logregrFieldGrid.store.model.addFields(newFields);
                                }
                                // STEP2: update all local logregrFields recods with values received from logregrDates   
                                // this step may not be executed if logregrField store did not laod yet - see it's store load
                                for ( var i=0; i<logregrFieldGrid.store.data.length; i++ ) {
                                    logregrFieldGrid.height += 10;
                                    var z=0;
                                    for (var j=0; j<response.logregrdates.length; j++, z++) {                                        
                                        if ( z === columnSize ) {
                                            z=0; // reset column counter
                                        }                                        
                                        var currId = response.logregrdates[j].idLogregrField;
                                        if ( logregrFieldGrid.store.getAt(i).get("idLogregrField") === currId ) {                                        
                                            var recF = logregrFieldGrid.store.getAt(i);
                                            var newValue = response.logregrdates[j].value;
                                            recF.set('value' + z, newValue);                                        
                                        }
                                    }
                                }                                 
                                logregrFieldGrid.getView().refresh();
                                var regrFieldPopup = Ext.ComponentQuery.query('#logregrFieldPopup')[0]; 
                                regrFieldPopup.updateLayout();
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {                     
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            } 
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in LogregrDate WS call");
                }
            },
    getColumnSize: function(response) {
        // count occurance of the first LogregrField
        var theId = response.logregrdates[0].idLogregrField;
        var j = 0;
        for (var i=0; i<response.logregrdates.length; i++) {
            if ( theId === response.logregrdates[i].idLogregrField ) {
                j++;
            }
        }
        return j;
    },
    removeDateColumns: function (grid) {
        for (var i=grid.headerCt.gridDataColumns.length; i>delta3.utils.GlobalVars.dateColumnZero-1; i--) {
            grid.headerCt.remove(i);        
        }
    },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/study/createLogregrDates',
            read: 'webresources/study/getLogregrDates',
            update: 'webresources/study/updateLogregrDates',
            destroy: 'webresources/study/removeLogregrDates'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,            
            rootProperty: 'logregrdates'
        },
        writer: {
            writeAllFields: true
        }
    }
});


