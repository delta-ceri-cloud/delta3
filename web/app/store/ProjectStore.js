/* 
 * Project Store, based on Group model and backend
 */

Ext.define('delta3.store.ProjectStore', {
    extend: 'Ext.data.Store',
    alias: 'store.projects',
    model: 'delta3.model.GroupModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    orgId: 0,
    autoSave: false,
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {orgId: this.orgId};
                },                 
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getGroupsByOrg WS call");
                }
            },    
    proxy: {
        type: 'ajax',
        api: {
            read: 'webresources/admin/getGroupsByOrg'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,                  
            rootProperty: 'groups'
        },
        writer: {
            writeAllFields: true
        }
    }
});

