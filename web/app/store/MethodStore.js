/* 
 * Method Store
 */

Ext.define('delta3.store.MethodStore', {
    extend: 'Ext.data.Store',
    alias: 'store.method',
    model: 'delta3.model.MethodModel',
    pageSize: delta3.utils.GlobalVars.largePageSize,
    autoSave: false,
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            } 
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getMethodStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/study/createMethods',
            read: 'webresources/study/getMethods',
            update: 'webresources/study/updateMethods'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,
            rootProperty: 'methods'
        },
        writer: {
            writeAllFields: true
        }
    }
});

