/* 
 * Eventtemplate Store
 */

Ext.define('delta3.store.EventtemplateStore', {
    extend: 'Ext.data.Store',
    alias: 'store.eventtemplates',
    model: 'delta3.model.EventtemplateModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    idModel: {},
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            } 
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getEventtemplateStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/event/createEventtemplates',
            read: 'webresources/event/getEventtemplates',
            update: 'webresources/event/updateEventtemplates',
            destroy: 'webresources/event/removeEventtemplates'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,            
            rootProperty: 'eventtemplates'
        },
        writer: {
            writeAllFields: true
        }
    }
});

