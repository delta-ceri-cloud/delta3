/* 
 * CERI-Lahey
 * 
 */

Ext.define('delta3.store.ProjectRoleStore', {

    extend: 'Ext.data.Store',
    alias: 'store.projectroles',
    model: 'delta3.model.ProjectRoleModel',
    pageSize: delta3.utils.GlobalVars.largePageSize,
    userId: 0,
    autoSave: false,
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {userId: this.userId};
                },                 
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getProjectRole WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            read: 'webresources/admin/getProjectRoles'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,                  
            rootProperty: 'projectRoles'
        },
        writer: {
            writeAllFields: true
        }
    }
});
