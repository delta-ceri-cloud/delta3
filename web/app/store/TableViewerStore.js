/* 
 * TableViewer Store
 */


Ext.define('delta3.store.TableViewerStore', {
    extend: 'Ext.data.Store',
    alias: 'store.tableViewer',
    model: 'delta3.model.TableViewerModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    title: {},
    theGrid: {},
    listeners:
            {
                load: function(store, record, options) {
                    try {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (response.success === true) {
                            var me = this;
                            var theGrid = Ext.ComponentQuery.query('panel[title=' + me.title + ']');
                            var modelFields = JSON.parse(me.buildGridModel(response.metaData));
                            this.model.addFields(modelFields);
                            var columns = JSON.parse(me.buildGridColumns(response.metaData));
                            for (var i=0; i < columns.length; i++) {
                                if ( columns[i].tooltip === 'DATETIME' ) {
                                    columns[i].renderer = Ext.util.Format.dateRenderer('Y-m-d H:i:s');
                                }
                                if ( columns[i].tooltip === 'DATE' ) {
                                    columns[i].renderer = Ext.util.Format.dateRenderer('Y-m-d');
                                }                                
                            }
                            this.loadRawData(response); // re-load data since metadata changed
                            theGrid[0].reconfigure(me, columns);
                            //var theTabs = Ext.ComponentQuery.query('#maintabs')[0];    
                            //var theTab = Ext.ComponentQuery.query('container[title=' + me.title + ']');
                            //theTabs.setActiveTab(theTab[1]);                
                        } else {
                            delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                        }
                    }
                    } catch (err) {
                        console.log("exception1: " + err);
                    }
                },
                datachanged: function(store, record, options) {
                    try {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                    } catch (err) {
                        console.log("exception2: " + err);
                    }
                },                
                exception:
                        function(proxy, type, action, o, result, records) {
                            delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in generic table viewer WS call");
                        }
            },
    buildGridColumns: function(headers) {
        try {
            var hString = '[';
            for (var i = 0; i < headers.length; i++) {
                if (i > 0) {
                    hString += ',';
                }
                //       hString += '{"header":"' + headers[i].label + ', ' + headers[i].type + '", "dataIndex":"' + headers[i].label + '"';
                hString += '{"header":"' + headers[i].label + '", "tooltip": "' + headers[i].type + '", "dataIndex":"' + headers[i].label + '"';
                if (headers[i].type.indexOf("INT") !== -1) {
                    hString += ', "type":"int"';
                }
                if (headers[i].type.indexOf("LONG") !== -1) {
                    hString += ', "type":"int"';
                }
                if (headers[i].type.indexOf("DOUBLE") !== -1) {
                    hString += ', "type":"numberfield"';
                }
                if (headers[i].type.indexOf("FLOAT") !== -1) {
                    hString += ', "type":"numberfield"';
                }
//                if (headers[i].type.indexOf("DATE") !== -1) {
//                    hString += ', "type":"date", "dateFormat": "Y-m-d"';
//                }
//                if (headers[i].type.indexOf("TIMESTAMP") !== -1) {
//                    hString += ', "type":"date", "dateFormat": "Y-m-d H:i:s.u"';
//                }
//                if (headers[i].type.indexOf("DATETIME") !== -1) {
//                    hString += ', "type":"date"';
//                }            
                hString += "}"
            }
        } catch (err) {
            console.log("exception3: " + err);
        }        
        return hString += ']';
    },
    buildGridModel: function(headers) {
        try {
            var hString = '[';
            for (var i = 0; i < headers.length; i++) {
                if (i > 0) {
                    hString += ',';
                }
                hString += '{"name":"' + headers[i].label + '"';
                if (headers[i].type.indexOf("CHAR") !== -1) {
                    //hString += ', "type":"string"';
                }
                if (headers[i].type.indexOf("INT") !== -1) {
                    hString += ', "type":"int"';
                }
                if (headers[i].type.indexOf("LONG") !== -1) {
                    hString += ', "type":"int"';
                }
                if (headers[i].type.indexOf("DOUBLE") !== -1) {
                    hString += ', "type":"number"';
                }
                if (headers[i].type.indexOf("FLOAT") !== -1) {
                    hString += ', "type":"number"';
                }
                if (headers[i].type.indexOf("DATE") !== -1) {
                    hString += ', "type":"date", "dateFormat":"Y-m-d"';
                }
                if (headers[i].type.indexOf("TIMESTAMP") !== -1) {
                    hString += ', "type":"date", "dateFormat":"Y-m-d H:i:s.u"';
                }
                if (headers[i].type.indexOf("DATETIME") !== -1) {
                    hString += ', "type":"date", "dateFormat":"Y-m-d H:i:s.u"';
                }                
                hString += "}"
            }
        } catch (err) {
            console.log("exception4: " + err);
        }        
        return hString += ']';
    },
    proxy: {
        type: 'ajax',
        extraParams: {modelId: '0', append: '', orderBy: '', direction: '', filterId: '0'},
        api: {
            read: 'webresources/model/getDBObject'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,                  
            rootProperty: 'content'
        },
        writer: {
            writeAllFields: true
        }
    }
});





