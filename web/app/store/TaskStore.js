/* 
 * Task Store
 */

Ext.define('delta3.store.TaskStore', {
    extend: 'Ext.data.Store',
    alias: 'store.tasks',
    model: 'delta3.model.TaskModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    objectId: {},
    objectType: {},
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {group: '{"groups":[{"type":"' + this.groupType
                                + '", "idGroup":"' + JSON.stringify(this.groupId) + '"}]}'};
                },                
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getAlertStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/event/createTasks',
            read: 'webresources/event/getTasks',
            update: 'webresources/event/updateTasks',
            destroy: 'webresources/event/removeTasks'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,                  
            rootProperty: 'tasks'
        },
        writer: {
            writeAllFields: true
        }
    }
});

