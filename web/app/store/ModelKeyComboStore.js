/* 
 * Model Table for Combo lookup Store
 * Coping Systems, Inc.
 */

Ext.define('delta3.store.ModelKeyComboStore', {
    extend: 'Ext.data.Store',
    requires: [
        'Ext.data.*',
        'delta3.model.FieldModel'
    ],
    alias: 'store.modelKeyCombo',
    model: 'delta3.model.FieldModel'
});