/* 
 * Group Entity Store
 */

Ext.define('delta3.store.GroupEntityStore', {
    extend: 'Ext.data.Store',
    alias: 'store.groupEntity',
    itemId: 'groupEntityStore',
    model: 'delta3.model.FieldModel',
    groupId: {},
    groupType: {},
    pageSize: 5,
    autoSave: false,
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {group: '{"groups":[{"type":"' + this.groupType
                                + '", "idGroup":"' + JSON.stringify(this.groupId) + '"}]}'};
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in Group Relationship WS call");
                }
            },
    proxy: {
        type: 'ajax',
        actionMethods: {
            create: 'POST',
            read: 'GET',
            update: 'POST',
            destroy: 'POST'
        },
        api: {
            create: 'webresources/admin/addGroupEntity',
            read: 'webresources/admin/getGroupEntity',
            destroy: 'webresources/admin/removeGroupEntity'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,            
            rootProperty: 'entities'
        },
        writer: {
            writeAllFields: true
        }
    }
});

