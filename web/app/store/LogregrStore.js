/* 
 * Logistic Regression Store
 */


Ext.define('delta3.store.LogregrStore', {
    extend: 'Ext.data.Store',
    alias: 'store.logregr',
    model: 'delta3.model.LogregrModel',
    sorters: [{property: 'createdTS', direction: 'DESC'}],    
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    remoteFilter: true,
    remoteSort: true,      
    groupId: 0,
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {group: this.groupId};
                },                
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in Logregr WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/study/createLogregr',
            read: 'webresources/study/getLogregr',
            update: 'webresources/study/updateLogregr',
            destroy: 'webresources/study/removeLogregr'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,            
            rootProperty: 'logregrs'
        },
        writer: {
            writeAllFields: true
        }
    }
});


