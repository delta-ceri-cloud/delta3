/* 
 * Model Store
 */

Ext.define('delta3.store.ModelStore', {
    extend: 'Ext.data.Store',
    alias: 'store.models',
    model: 'delta3.model.ModelModel',
    sorters: [{property: 'createdTS', direction: 'DESC'}],
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    remoteFilter: true,
    remoteSort: true,         
    groupId: 0,
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {group: this.groupId};
                },
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    console.log('model grid exception');
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getModelStore WS call");
                }
            },
    //sorters: { property: 'name', direction : 'ASC' },             
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/model/createModels',
            read: 'webresources/model/getModels',
            update: 'webresources/model/updateModels',
            destroy: 'webresources/model/deleteModel'            
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,            
            rootProperty: 'models'
        },
        writer: {
            writeAllFields: true
        }
    }
});

