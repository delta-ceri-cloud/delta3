/* 
 * Notification Store
 */

Ext.define('delta3.store.NotificationStore', {
    extend: 'Ext.data.Store',
    alias: 'store.notifications',
    model: 'delta3.model.NotificationModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    idModel: {},
    groupId: 0,
    remoteSort: true,
    sorters: [{property: 'createdTS', direction: 'DESC'}],
    listeners:
            {
                beforeload: function(store, operation, options) {
                    this.getProxy().extraParams = {group: this.groupId};
                },                    
//                load: function(store, record, options) {
//                    var response = store.proxy.reader.rawData;
//                    if (typeof response !== 'undefined') {
//                        if (typeof response.success !== 'undefined') {
//                            if (response.success === false) {
//                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
//                            }
//                        }
//                    }
//                },
//                datachanged: function(store, record, options) {
//                    var response = store.proxy.reader.rawData;
//                    if (typeof response !== 'undefined') {
//                        if (typeof response.success !== 'undefined') {
//                            if (response.success === false) {
//                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
//                            } 
//                        }
//                    }
//                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in getNotificationStore WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/event/createNotifications',
            read: 'webresources/event/getNotifications',
            update: 'webresources/event/updateNotifications',
            destroy: 'webresources/event/removeNotifications'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,                  
            rootProperty: 'notifications'
        },
        writer: {
            writeAllFields: true
        }
    }
});

