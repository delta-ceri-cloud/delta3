/* 
 * Group Store
 */


Ext.define('delta3.store.GroupStore', {
    extend: 'Ext.data.Store',
    alias: 'store.groups',
    model: 'delta3.model.GroupModel',
    pageSize: delta3.utils.GlobalVars.pageSize,
    autoSave: false,
    listeners:
            {
                load: function(store, record, options) {
                    var response = store.proxy.reader.rawData;;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                datachanged: function(store, record, options) {
                    var response = store.proxy.reader.rawData;;
                    if (typeof response !== 'undefined') {
                        if (typeof response.success !== 'undefined') {
                            if (response.success === false) {
                                delta3.utils.GlobalFunc.popupErrorRedirect(response.status.message);
                            }
                        }
                    }
                },
                exception: function(proxy, type, action, o, result, records) {
                    delta3.utils.GlobalFunc.showRemoteException(proxy, type, action, o, result, records, "Error occured in Group WS call");
                }
            },
    proxy: {
        type: 'ajax',
        api: {
            create: 'webresources/admin/createGroups',
            read: 'webresources/admin/getGroups',
            update: 'webresources/admin/updateGroups',
            destroy: 'webresources/admin/deleteGroups'
        },
        reader: {
            totalProperty: 'totalCount',
            successProperty: "success",
            type: 'json',
            keepRawData: true,            
            rootProperty: 'groups'
        },
        writer: {
            writeAllFields: true
        }
    }
});

