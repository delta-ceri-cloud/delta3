/* by sdruker, Druck-I.T. */
/* global methodStore */

Ext.define('delta3.utils.GridPanel', {
    override: 'Ext.grid.GridPanel',
    requires: 'Ext.form.action.StandardSubmit',
    dateFormat : 'Y-m-d g:i',
    methodStore: {},
    exportGrid: function(title) {
        //console.log("This is grid title",title);
        if (!title) title = this.title;
        if (Ext.isIE) {
            //this._ieToExcel(this);
            var data = this._getCSV(this);
            var form = this.down('form#uploadForm');
            if (form) {
                form.destroy();
            }
            form = this.add({
                xtype: 'form',
                itemId: 'uploadForm',
                hidden: true,
                standardSubmit: true,
                url: 'http://webapps.figleaf.com/dataservices/Excel.cfc?method=echo&mimetype=application/vnd.ms-excel&filename=' + escape(title + ".csv"),
                items: [{
                    xtype: 'hiddenfield',
                    name: 'data',
                    value: data
                }]
            });
            form.getForm().submit();          
        } else {
            var data = this._getCSV(this);
            //window.location = 'data:text/csv;charset=utf8,' + encodeURIComponent(data);
            //console.log("This is at the grid pannel");
            var location = 'data:text/csv;charset=utf8,' + encodeURIComponent(data); 
            var gridEl = this.getEl(); 
            var el = Ext.DomHelper.append(gridEl, {
                tag: "a",
                download: title + "-" + Ext.Date.format(new Date(), 'Y-m-d Hi') + '.csv',
                href: location
            });
            el.click();
            Ext.fly(el).destroy();            
        }
    },

    _escapeForCSV: function(string) {
        if (string.match(/,/)) {
            if (!string.match(/"/)) {
                string = '"' + string + '"';
            } else {
                string = string.replace(/,/g, ''); // comma's and quotes-- sorry, just loose the commas
            }
        }
        return string;
    },





  /*---------original function-----------------------
    _getFieldText: function(fieldData) {
        console.log("This is field data",fieldData);
        var text;

        if (fieldData == null || fieldData == undefined) {
            text = '';
        } else if (fieldData._refObjectName && !fieldData.getMonth) {
            text = fieldData._refObjectName;
        } else if (fieldData instanceof Date) {
            text = Ext.Date.format(fieldData, this.dateFormat);
        } else if (typeof(fieldData) === "number" ) {
            
            text = '' + fieldData; 
        } else if (typeof(fieldData) === "boolean" ) { 
            text = '' + fieldData; 
        } else if (!fieldData.match || fieldData === '&#160;') { // not a string or object we recognize... or action column
            text = '';
        } else if (typeof(fieldData) === "string" ) { 
            text = fieldData;; 
        }
        
        else {
            text = fieldData;
        }

        return text;
    },*/
    
    
    
    
    
    
    
  
     _getFieldText: function(fieldData) {
        //console.log("This is field data",fieldData);
        
        var text;
        if (fieldData == null || fieldData == undefined) {
            //console.log("This is for null",fieldData);
            text = '';
        } 
        
        else if (fieldData._refObjectName && !fieldData.getMonth) {
           // console.log("This is ref object instance",fieldData);
            text = fieldData._refObjectName;
        } 
        else if (fieldData instanceof Date) {
            text = Ext.Date.format(fieldData, this.dateFormat);
            //console.log("This is date instance",text);
        } 
        
         else if (typeof(fieldData) === "number" ) {
            //console.log("This is number instance",fieldData );
            text = '' + fieldData; 
        }
        
        
         else if (typeof(fieldData) === "boolean" ) { 
            //console.log("This is for boolean",fieldData);
            text = '' + fieldData; 
            
            
        } else if (!fieldData.match || fieldData === '&#160;') { // not a string or object we recognize... or action column
            
             //console.log("This is at fieldData.match",fieldData);
             text = '';
        } 
        else {
            text = fieldData;
        }
        
        return text;
    },
    
    
    
    
    
    
    
    
    

    _getFieldTextAndEscape: function(fieldData) {
        var string  = this._getFieldText(fieldData);
        return '"' + string + '"';
        //return '"' + this._escapeForCSV(string) + '"'; // added "" to eliminate "SYLK" issue https://support.microsoft.com/kb/215591/EN-US
    },
    
    
    
    
    

    _getCSV: function (grid) {
        
        //console.log("This is at the grid _get CSV function grid :", grid);
        var cols    = grid.columns;
        var store   = grid.store;
        var data    = '';
        var that = this;
        Ext.Array.each(cols, function(col, index) {
           
            if (col.hidden != true) {
                data += that._getFieldTextAndEscape(col.text) + ',';
                //data += that._getFieldTextAndEscape(col.text) + '\t';                
            }
        });
        
        data += "\n";
        store.each(function(record) {
            
            //console.log("This is grid type",grid.xtype);
            var entry       = record.getData();
            Ext.Array.each(cols, function(col, index) {
                if (col.hidden != true) {
                    var fieldName   = col.dataIndex;
                    var text        = entry[fieldName];
    
                    //-------V3.64 added folowing code for export Fixes
                    //console.log("This is fieldName:", fieldName);
                    //console.log("This is text-----:", text);
                    //console.log("This is entry",entry);
                    
                   //-------V3.64 custom case string to date conversion for model grid
                    if (fieldName=="updatedTS" && typeof(text)!="undefined")
                    {
                       text = new Date(text);
                    }
                    
                    //-------V3.64 custom case string to date conversion for model grid last processed on updated with DELQA-612
                    if (fieldName=="processingId" && typeof(text)!="undefined")
                    {
                       text = new Date(text);
                    }
                    
                    
                    //-------V3.64 custom case string to date conversion for result grid
                     if (fieldName=="submittedTS" && typeof(text)!="undefined")
                    {
                       text = new Date(text);
                    }
                    
                    
                   
                     //-------V3.64 custom case string to date conversion for study grid
                     if (fieldName=="processedTS" && typeof(text)!="undefined")
                    {
                       //console.log("This is processedTS",text);
                       //console.log("This is text type", typeof(text));
                       text = new Date(text);
                    }
                    
                    
                    //-------V3.64 custom case for retriving for project name from group
                    if (fieldName=="idGroup")
                    {
                    var tableIndex = text;
                    var project = Ext.ComponentQuery.query('#mainViewPort')[0].projectStore.findRecord('idGroup', tableIndex);
                    if (project === null)
                        text = '' + text;
                    else {  
                        //console.log("This is project user group name:",project.get("name"));
                        text = '' + project.get("name");
                        }
                    }
                    
                    //-------V3.64 custom case ID column for model grid
                    if (fieldName=="idModel" && grid.xtype=="grid.model")
                    {
                        text = '' + text; 
                    }
                    
                     //-------V3.64 custom case ID column for result process grid                    
                    if (fieldName=="idModel" && grid.xtype=="grid.process")
                    {
                        //console.log("given text is model id at result");
                        var tableIndex = text;
                        var model = delta3.utils.GlobalVars.ModelStore.findRecord('idModel', tableIndex);
                         if (model === null)
                            text = '' + text; 
                         else {
                            //console.log("This is model name:",model.get("name"));
                            text = '' + model.get("name");
                        }
                    }
                    
                    
                     //-------V3.64 custom case ID column for study grid
                    if (fieldName=="idModel" && grid.xtype=="grid.study")
                    {
                        //console.log("given text is model id");
                        var tableIndex = text;
                        var model = delta3.utils.GlobalVars.ModelStore.findRecord('idModel', tableIndex);
                         if (model === null)
                            text = '' + text; 
                         else {
                            //console.log("This is model name:",model.get("name"));
                            text = '' + model.get("name");
                        }
                    }
                    
                    
                    //-------V3.64 custom case for retriving for statistical package name
                    if (fieldName=="idStat")
                    {
                       //console.log("given text is stacks id");
                       var tableIndex = text;   
                       var statPackage = grid.statStore.findRecord('idStat', tableIndex);
                       if (statPackage === null)
                           text = '' + text ;
                        else {
                             //console.log("This is staistical package name:",statPackage.get("shortName"));
                             text = statPackage.get("shortName");
                        }
                    }
                    
                     //-------V3.64 custom case for retriving for method name
                    if (fieldName=="idMethod")
                    {
                        //console.log("given text is method id");
                        var tableIndex = text;
                        var method = grid.methodStore.findRecord('idMethod', tableIndex);
                         if (method === null)
                            text = '' + text; 
                         else {
                            //console.log("This is method name:",method.get("shortName"));
                            text = '' + method.get("shortName");
                        }
                    }
                    
                    
                     //-------V3.64 custom case for retriving for keyfield
                    //console.log("This is keyId", entry.idKey);
                    
                    if (fieldName=="idKey")
                    {
                    var tableIndex = entry.idKey;
                        var field = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', tableIndex);
                        if (field === null)
                            text = '' + text; 
                        else {
                            //console.log("This is key field name:",field.get("name"));
                            text = '' + field.get("name");                            
                        }
                    }
                    
                    
                    

                    
                     //-------V3.64 custom case for retriving for sequenser
                    if (fieldName=="idDate")
                    {
                    
                        var tableIndex = entry.idDate;
                        var field = delta3.utils.GlobalVars.FieldStore.findRecord('idModelColumn', tableIndex);
                        if (field === null)
                            text = '' + text;
                        else {
                            //console.log("This is sequencer name:",field.get("name"));
                            text = '' + field.get("name");
                        }
                    }
                    
                   
                    
                    
                    
                    //-------V3.64 custom case for retriving for source in model grid
                    if (fieldName=="idConfiguration" && grid.xtype=="grid.model")
                    {
                       var tableIndex = text;
                        var confRec = delta3.utils.GlobalVars.ConfigurationStore.findRecord('idConfiguration', tableIndex);
                        if (confRec === null)
                            text = '' + text;
                        else {
                            text = '' + confRec.get("name");
                            }
                    }
                    
                     //-------V3.64 custom case for retriving for owner in model grid DELQA-292*/
                    if (fieldName=="createdBy" && grid.xtype=="grid.model")
                    {
                       
                       var tableIndex = text;
                       console.log("tableIndex",tableIndex);
                        var confRec = delta3.utils.GlobalVars.UserStore.findRecord('idUser', tableIndex);
                        console.log("confRec",confRec);
                        if (confRec === null)
                            text = '' + text;
                        else {
                            text = '' + confRec.get("alias");
                            }
                    }
                    
                    //-------V3.64 custom case for retriving for owner in model grid DELQA-292*/
                    if (fieldName=="tableName")
                    {
                      // console.log("This is tablename entry",entry.modelTableidModelTable);
                       var tableIndex = entry.modelTableidModelTable;
                       var confRec = ModelData.tableStore.findRecord('idModelTable', tableIndex);
                        if (confRec === null)
                            text = '' + text;
                        else {
                            text = '' + confRec.get("name");
                            }
                    }
                    
                    
                    
                    
                    data += that._getFieldTextAndEscape(text) + ',';
                    //data += that._getFieldTextAndEscape(text) + '\t';
                }
            });
            data += "\n";
        });

        return data;
    },












    //Newly added for foreign key export study
    _getFieldTextStudy: function(fieldData) {
        console.log("This is field data",fieldData);
        var text;
        var cnt=1;
        
        if (fieldData == null || fieldData == undefined) {
            text = '';
        } else if (fieldData._refObjectName && !fieldData.getMonth) {
            text = fieldData._refObjectName;
        } else if (fieldData instanceof Date) {
            text = Ext.Date.format(fieldData, this.dateFormat);
        } else if (typeof(fieldData) === "number" ) {
            
            if(cnt==1)
            { console.log("This is project",fieldData );}
            
            if(cnt==2)
            { console.log("This is model",fieldData );}
            
            text = '' + fieldData;
            cnt=cnt+1;
            
        } else if (typeof(fieldData) === "boolean" ) { 
            text = '' + fieldData; 
        } else if (!fieldData.match || fieldData === '&#160;') { // not a string or object we recognize... or action column
            text = '';
        } else if (typeof(fieldData) === "string" ) { 
            text = fieldData;; 
        }
        
        else {
            text = fieldData;
        }

        return text;
    },

































    _ieGetGridData : function(grid, sheet) {
        var that            = this;
        var resourceItems   = grid.store.data.items;
        var cols            = grid.columns;

        Ext.Array.each(cols, function(col, colIndex) {
            if (col.hidden != true) {
                sheet.cells(1,colIndex + 1).value = col.text;
            }
        });

        var rowIndex = 2;
        grid.store.each(function(record) {
            var entry   = record.getData();

            Ext.Array.each(cols, function(col, colIndex) {
                if (col.hidden != true) {
                    var fieldName   = col.dataIndex;
                    var text        = entry[fieldName];
                    var value       = that._getFieldText(text);

                    sheet.cells(rowIndex, colIndex+1).value = value;
                }
            });
            rowIndex++;
        });
    },

    _ieToExcel: function (grid) {
        if (window.ActiveXObject){
            var  xlApp, xlBook;
            try {
                xlApp = new ActiveXObject("Excel.Application"); 
                xlBook = xlApp.Workbooks.Add();
            } catch (e) {
                Ext.Msg.alert('Error', 'For the export to work in IE, you have to enable a security setting called "Initialize and script ActiveX control not marked as safe" from Internet Options -> Security -> Custom level..."');
                return;
            }

            xlBook.worksheets("Sheet1").activate;
            var XlSheet = xlBook.activeSheet;
            xlApp.visible = true; 

           this._ieGetGridData(grid, XlSheet);
           XlSheet.columns.autofit; 
        }
    }
});