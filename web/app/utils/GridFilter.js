/* 
 * Boston Advanced Analytics, Inc.
 * used to filter records in a grid
 */
Ext.define("delta3.utils.GridFilter", {
    extend: 'Ext.form.FieldContainer',
    layout: 'hbox',
    margin: "1 1 1 1",     
    gridToSearch: {},
    fieldToFilter: '',
    stringToSearch: {},
    local: true,
    filterInvisible: true,
    items: [{            
        }, {
            xtype: 'textfield',
            itemId: 'qfText',
            name: 'qfText',
            emptyText: 'Enter value to search..'
        }, {
            xtype: 'button',
            text: 'Filter',
            handler: function() {
                var lField = this.up().fieldToFilter;
                if ( (lField === '') || (typeof lField === 'undefined') ) return;   
                var text = this.up().down('#qfText').getValue();                
                text = this.up().reverseIdLookup(lField,text);
                if ( typeof text === 'undefined' ) return;                
                var lStore = this.up().gridToSearch.getStore();                
                var theFilter = new Ext.util.Filter({
                    property: lField,
                    value   : text,
                    anyMatch: true,
                    caseSensitive: false
                });
                lStore.filter(theFilter);
                var y = this.up().gridToSearch;
                var x = y.dockedItems.items[delta3.utils.GlobalFunc.getPagingComponentIndex(y.dockedItems)];
                if ( this.up().local === true ) {
                    x.disable();
                }
            }
        }, {
            xtype: 'button',
            text: 'Clear',
            handler: function() {
                var lStore = this.up().gridToSearch.getStore();
                lStore.clearFilter(false);
                this.up().down('#qfText').setValue('');
                var y = this.up().gridToSearch;
                var x = y.dockedItems.items[delta3.utils.GlobalFunc.getPagingComponentIndex(y.dockedItems)];
                if ( this.up().local === true ) {                
                    x.enable();                
                }
            }
        }],
    initComponent: function() {
        var me = this;
        var columnStore = Ext.create('Ext.data.Store', {
                fields: ['columnName','value'], 
                data: [] 
            });
        for ( var i=0; i < me.gridToSearch.getStore().model.fields.length; i++) {
            var columnName = me.getColumnName(me.gridToSearch.getStore().model.fields[i].name);
            
            
            if ( columnName === null ) {
                if ( me.filterInvisible === true ) {
                    continue;
                } else {
                    columnName = me.gridToSearch.getStore().model.fields[i].name;
                }
            }
            if ( me.isFieldSortable(me.gridToSearch.getStore().model.fields[i].name) === false ) {
                continue;
            }
            columnStore.add({columnName: columnName, value: me.gridToSearch.getStore().model.fields[i].name});
        }
        var pageOrBlank = '';
        if (me.local === true) pageOrBlank = 'Page ';
        var dropDown = new Ext.form.ComboBox({
            store: columnStore,
            itemId: 'qfColumnComboBox',
            fieldLabel: 'Grid ' + pageOrBlank + 'Filter',
            labelAlign: 'right',
            queryMode: 'local',
            displayField: 'columnName',
            valueField: 'value',
            multiSelect: false,
            forceSelection: false,
            listeners: {
                'select': function(cmb, rec, idx) {
                    me.fieldToFilter = cmb.getValue();
                }
            }
        }); 
        me.items[0] = dropDown;
        me.callParent();
    },
    isFieldSortable: function(fieldName) {
        for (var i=0; i<this.gridToSearch.columns.length; i++) {
            if ( this.gridToSearch.columns[i].dataIndex === fieldName ) 
                return this.gridToSearch.columns[i].sortable;
        }
        return false;
    },
    getColumnName: function(fieldName) {
        for (var i=0; i<this.gridToSearch.columns.length; i++) {
            if ( this.gridToSearch.columns[i].dataIndex === fieldName ) 
                return this.gridToSearch.columns[i].text;
        }
        return null;
    },
    // following method is to facilitate sorting on columns represented 
    // in the grid table by ids pointing to other tables
    reverseIdLookup: function(fieldName, value) {
        if ( (value === '') || (typeof value === 'undefined') ) return;                 
        switch (fieldName) {
            case 'Organization_idOrganization':
                var model = delta3.utils.GlobalVars.OrgStore.findRecord('name', value);
                var tmp = '0';
                if ( model !== null ) {
                    tmp = model.get("idOrganization");              
                }
                return tmp;
        }
        return value;
    }    
});

