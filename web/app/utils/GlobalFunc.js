Ext.define('Overrides.form.field.Base', {
    override: 'Ext.form.field.Base',

    getLabelableRenderData : function() {
        var me            = this,
            data          = me.callParent(),
            labelClsExtra = me.labelClsExtra;

        if (!me.allowBlank) {
            data.labelClsExtra = (labelClsExtra ? labelClsExtra + ' ' : '') + me.requiredCls;
        }

        return data;
    }
});

Ext.define('delta3.utils.GlobalFunc', {
        statics: {  
        getRandomColor: function(ranges){
            // Six levels of brightness from 0 to 5, 0 being the darkest
//            var rgb = [Math.random() * 256, Math.random() * 256, Math.random() * 256];
//            var mix = [brightness*51, brightness*51, brightness*51]; //51 => 255/5
//            var mixedrgb = [rgb[0] + mix[0], rgb[1] + mix[1], rgb[2] + mix[2]].map(function(x){ return Math.round(x/2.0)})
//            return "rgb(" + mixedrgb.join(",") + ")";
            if (!ranges) {
                ranges = [
                    [150,256],
                    [0, 190],
                    [0, 30]
                ];
            }
            var g = function() {
                //select random range and remove
                var range = ranges.splice(Math.floor(Math.random()*ranges.length), 1)[0];
                //pick a random number from within the range
                return Math.floor(Math.random() * (range[1] - range[0])) + range[0];
            }
            return "rgba(" + g() + "," + g() + "," + g() +",0.5)";
        },    
        DateStringISO: function(d){
            function pad(n){return n<10 ? '0'+n : n}
            return d.getUTCFullYear()+'-'
                + pad(d.getUTCMonth()+1)+'-'
                + pad(d.getUTCDate())+'T'
                + pad(d.getUTCHours())+':'
                + pad(d.getUTCMinutes())+':'
                + pad(d.getUTCSeconds())+'Z';
        },     
        DateStringYYYYMMDD: function(d){
            function pad(n){return n<10 ? '0'+n : n}
            return d.getFullYear()+'-'
                + pad(d.getMonth()+1)+'-'
                + pad(d.getDate());
        },  
        DateStringYmdHisu: function(d){
            function pad(n){return n<10 ? '0'+n : n}
            return d.getFullYear()+'-'
                + pad(d.getMonth()+1)+'-'
                + pad(d.getDate())+' '
                + pad(d.getHours())+':'
                + pad(d.getMinutes())+':'
                + pad(d.getSeconds())+'.000';
        },        
        jsonDeserializeHelper: function (key,value) {
          if ( typeof value === 'string' ) {
            var regexp;
            regexp = /^\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d.\d\d\dZ$/.exec(value);
            if ( regexp ) {
              return new Date(value);
            }
          }
          return value;
        },      
        checkJDBCFieldNameValid: function (value) {
          if ( typeof value === 'string' ) {
            var regexp;
            regexp = /^[a-zA-Z_][a-zA-Z0-9_]*$/.exec(value);
            if ( regexp ) {
              return true;
            }
          }
          return false;
        },         
        //--------------------------------------  general purpose messages
        popupErrorRedirect: function(message) {
            Ext.Msg.alert("DELTA Error", message, function() {
                if ( message === "Session timeout.\nPlease login again." ) {
                    window.location = 'index.html'; 
                }
            });        
        },  
        showRemoteException: function (proxy, type, action, o, result, records, message) {
                if ( type === 'remote' ) {
                    Ext.Msg.alert("DELTA. Could not " + action, result.raw.message);
                } else {
                    if ( type === 'response' ) {
                        Ext.Msg.alert("DELTA. Could not " + action, "Server's response could not be decoded");
                    } else {
                        Ext.Msg.alert("DELTA. Error", message);
                    }
                }
        },
        showDeltaMessage: function (message) {
                Ext.Msg.alert("DELTA", message);
        },     
        isFieldTypeSupported: function (type) {
            var n = type.indexOf("(");
            if ( n !== -1 ) type = type.substring(0,n);
            for (var i=0; i< delta3.utils.GlobalVars.supportedDBColumnTypes.length; i++) {
                if ( type.toUpperCase() === delta3.utils.GlobalVars.supportedDBColumnTypes[i] ) {
                    return true;
                }
            }
            return false;
        }, 
        lookupFieldId: function (fieldName) {
            var fieldRecord = delta3.utils.GlobalVars.FieldStore.findRecord('name', fieldName, 0 , false, false, true);
            var fieldId;
            if (fieldRecord === null)
                fieldId = 0;
            else {
                fieldId = fieldRecord.get("idModelColumn");    
            }  
            return fieldId;
        },
        //--------------------------------------  create generic comboBox store based on another store and filter
        createFilteredComboBoxStore: function(store, displayFieldId, valueFieldId, filterId, filterValue) {
            
            var data = '[';
            for ( var i=0; i<store.data.length; i++) {
                if ( store.getAt(i).get(filterId) === filterValue || filterValue === 0 ) {
                    if ( i > 0 ) {
                        data += ',';
                    }
                    data += '{"name":"' + store.getAt(i).get(displayFieldId) + '","idModel":"' + store.getAt(i).get(valueFieldId) + '"}';                  
                }
            }
            data += ']';
            var cbStore =  Ext.create('Ext.data.Store', {
                            fields: [displayFieldId,valueFieldId], 
                            //fields: ['name','idModel'],
                            data: JSON.parse(data) 
                        });
            return cbStore;
        },        
        //--------------------------------------  create generic comboBox store based on another store
        createComboBoxStore: function(store, fieldId) {
            var values = [];
            for ( var i=0; i<store.data.length; i++) {
                values[i] = {value: store.getAt(i).get(fieldId)};
            }
            var cbStore =  Ext.create('Ext.data.Store', {
                            fields: ['value'], 
                            data: values 
                        });
            return cbStore;
        },
        //--------------------------------------  create generic comboBox store based on another store
        createLocalGridStore: function(store, fieldArray) {
            var values = [];
            for ( var i=0; i<store.data.length; i++) {
                var valuesString = '{';
                for (var j=0; j<fieldArray.length; j++) {
                    if ( j > 0 ) {
                        valuesString += ',';
                    }
                    valuesString += '"' + fieldArray[j].getName() + '":"' + store.getAt(i).get(fieldArray[j].getName()) + '"';
                }
                valuesString += '}';
                values[i] = JSON.parse(valuesString);
            }
            var cbStore =  Ext.create('Ext.data.Store', {
                            fields: fieldArray,
                            data: values 
                        });
            return cbStore;
        },        
        //------------------------------------------------------------ call to initialize stat config
        doInitializeConfig: function (remoteLocal, packageId, packageName, callback)
        {		  
            var paramJSON = '{"type":' + remoteLocal + ',"id":' + packageId + ',"packageName":' + packageName + '}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/initStudyMethods',
                    method: "POST",
                    disableCaching: true,      
                    params: paramJSON,
                    success: doInitializeConfigSuccess,
                    failure: doInitializeConfigFailed
                }
            );
            Ext.MessageBox.wait('Initializing Study Config ...');   
            return;
            
            function doInitializeConfigSuccess(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA", response.responseText);
                } 
                callback();
            }

            function doInitializeConfigFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }  
        },
        //------------------------------------------------------------ call to create and populate Flat Table
        doGetModelProcessingId: function (modelRecord, callback)
        {		
            var modelJSON = '{"models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/getModelProcessingId',
                    method: "POST",
                    disableCaching: true,       
                    params: modelJSON,
                    success: doGetModelProcessingIdSuccess,
                    failure: doGetModelProcessingIdFailed
                }
            );
            return;
            
            function doGetModelProcessingIdSuccess(response, options)
            {
                Ext.MessageBox.hide();
                callback(response.responseText);
            }

            function doGetModelProcessingIdFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }      
        },         
        //------------------------------------------------------------ call to create and populate Flat Table
        doGetModelStatus: function (modelRecord)
        {		
            var modelJSON = '{"models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/getModelStatus',
                    method: "POST",
                    disableCaching: true,       
                    params: modelJSON,
                    success: doGetModelStatusSuccess,
                    failure: doGetModelStatusFailed
                }
            );
            return;
            
            function doGetModelStatusSuccess(response, options)
            {
                Ext.MessageBox.hide();
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doGetModelStatusFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }      
        }, 
        //------------------------------------------------------------ call to create and populate Flat Table
        doSetModelStatus: function (modelRecord)
        {		
            var modelJSON = '{"models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/setModelStatus',
                    method: "POST",
                    disableCaching: true,        
                    params: modelJSON,
                    success: doSetModelStatusSuccess,
                    failure: doSetModelStatusFailed
                }
            );
            Ext.MessageBox.wait('Updating Model Status ...');  
            return;
            
            function doSetModelStatusSuccess(response, options)
            {
                Ext.MessageBox.hide();
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doSetModelStatusFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }      
        },
        
        
        
        
        //----------------------------------------------------------------------
        doSetProjecttAssigned: function (modelRecord)
        {		
            var outputJSONString = '{"groups":[{"name":"' + groupName
                    + '", "idGroup":"' + idGroup
                    + '"}], "entities":[{"type":"' + type 
                    + '","idEntity":"' + JSON.stringify(idEntity)
                    + '"}]}';
            
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/setProjectAssigned',
                    method: "POST",
                    disableCaching: true,        
                    params: outputJSONString,
                    success: doSetProjectStatusSuccess,
                    failure: doSetProjectStatusFailed
                }
            );
            Ext.MessageBox.wait('Updating Model Status ...');  
            return;
            
            function doSetProjectStatusSuccess(response, options)
            {
                Ext.MessageBox.hide();
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doSetProjectStatusFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }      
        },
        
        
        
        //------------------------------------------------------------ call to save task ans schedule future events
        doSaveTaskAndScheduleEvents: function (taskJSON, callbackFunction) {
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/event/createTasksAndSchedule',
                    method: "POST",
                    disableCaching: true,
                    params: taskJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            function successFunction(response, options) {
                var returnValue = Ext.decode(response.responseText);
                var message;
                console.log("returnValue"+returnValue);
                if (typeof returnValue === 'undefined') {
                    message = 'General server error';
                } else {
                    message = returnValue.status.message;
                    console.log("message"+message);
                    console.log("returnValue"+returnValue.toString());
                    if(callbackFunction)
                        callbackFunction(returnValue);
                }                  
                delta3.utils.GlobalFunc.showDeltaMessage(message);  
            }
            function failedFunction(response, options) {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }              
            }            
        },

        //------------------------------------------------------------ call to delete Flat Table
        doDeleteFlatTable: function (modelRecord)
        {		
            var modelJSON = '{ "models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/deleteFlatTable',
                    method: "POST",
                    disableCaching: true,        
                    params: modelJSON,
                    success: doDeleteFlatTableSuccess,
                    failure: doDeleteFlatTableFailed
                }
            );
            Ext.MessageBox.wait('Deleting Flat Table ...');   
            return;
            
            function doDeleteFlatTableSuccess(response, options)
            {
                Ext.MessageBox.hide();  
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doDeleteFlatTableFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }  
        },
        //------------------------------------------------------------ call to delete Staging Table
        doDeleteStagingTable: function (modelRecord)
        {		
            var modelJSON = '{ "models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/deleteStagingTable',
                    method: "POST",
                    disableCaching: true,        
                    params: modelJSON,
                    success: doDeleteSuccess,
                    failure: doDeleteFailed
                }
            );
            Ext.MessageBox.wait('Deleting Staging Table ...');   
            return;
            
            function doDeleteSuccess(response, options)
            {
                Ext.MessageBox.hide();  
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doDeleteFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }  
        },       
 
        //------------------------------------------------------------ call to delete Flat Table
        doCloneStatFromRecord1: function (orgId)
        {		
            //var modelJSON = '{ "models":['+ JSON.stringify(modelRecord) + ']}';     
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/deleteFlatTable',
                    method: "POST",
                    disableCaching: true,        
                    params: orgId,
                    success: doSuccess,
                    failure: doFailed
                }
            );
            Ext.MessageBox.wait('Cloning Statistics Package ...');   
            return;
            
            function doSuccess(response, options)
            {
                Ext.MessageBox.hide();  
            }

            function doFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }  
        },       
       //------------------------------------------------------------ call to general Model processing servlet
        doProcessModel: function(requestType, modelRecord)
        {		
            var modelJSON = '{ "models":['+ JSON.stringify(modelRecord) + ']}';    
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/ProcessModelServlet',
                    method: "POST",
                    disableCaching: true,
                    params: { payload: modelJSON, requestType: requestType },
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Processing Model ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },  
       //------------------------------------------------------------ call to general Model processing servlet to stop processing
        cancelProcessModel: function(task)
        {		
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/ProcessModelServlet',
                    method: "GET",
                    disableCaching: true,
                    params: { payload: task },
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Cancelling processing Model ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },  
       //------------------------------------------------------------ call to general Study processing servlet
        doProcessStudy: function(requestType, studies)
        {		
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/ProcessStudyServlet',
                    method: "POST",
                    disableCaching: true,
                    params: { payload: studies, requestType: requestType },
                    success: successFunction,
                    failure: failedFunction
                }
            );
    
            Ext.MessageBox.wait('Initializing Study processing ...');              
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                
                Ext.Msg.alert("DELTA","Study submitted successfully. Please click on 'Refresh' button and see 'Stage Status' to verify progress of study. You also can navigate to 'Results' tab for results.");
                
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }            
        },  
       //------------------------------------------------------------ call to general Study processing servlet to stop processing
        cancelProcessStudy: function(task)
        {		
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/ProcessStudyServlet',
                    method: "GET",
                    disableCaching: true,
                    params: { payload: task },
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Cancelling processing Study ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },        
        //------------------------------------------------------------ call to create Flat Table
        doCreateFlatTable: function(modelRecord)
        {		
            var modelJSON = '{ "models":['+ JSON.stringify(modelRecord) + ']}';    
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/createFlatTable',
                    method: "POST",
                    disableCaching: true,
                    timeout       : 120000, //2 minutes, extra time needed
                    params: modelJSON,
                    success: doCreateFlatTableSuccess,
                    failure: doCreateFlatTableFailed
                }
            );
            Ext.MessageBox.wait('Creating Denormalized Study Table ...');  
            return;
            
            function doCreateFlatTableSuccess(response, options)
            {
                Ext.MessageBox.hide();  
                ModelData.fieldGrid.setTitle(
                        delta3.utils.GlobalFunc.emphasizeString("Status: " + response.responseText));
            }

            function doCreateFlatTableFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },                                             
        //------------------------------------------------------------ call to create DST table
        doVerifyStatement: function(n, f, secondary, callbackFunction, rowIndex)
        {		
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/verifyStatement',
                    method: "GET",
                    disableCaching: true,
                    params: {tableName: n, formula: f, secondary: secondary},                    
                    success: doVerifyStatementSuccess,
                    failure: doVerifyStatementFailed
                }
            );
            Ext.MessageBox.wait('Verifying formula statement ...');  
            return;
            
            function doVerifyStatementSuccess(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA", response.responseText);
                    callbackFunction(false, rowIndex);
                } else {
                    callbackFunction(true, rowIndex);
                }
            }

            function doVerifyStatementFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
                callbackFunction(false, rowIndex);
            }         
        },   
      //------------------------------------------------------------ call to export Model
        doExportDoubleArray: function(modelRecord)
        {		
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/exportDoubleArray',
                    method: "POST",
                    disableCaching: true,
                    params: modelRecord.idStudy,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Exporting double array ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText.indexOf('File not found') > -1 ) {
                    Ext.Msg.alert("DELTA", response.responseText);                              
                } else {
                    window.location = 'data:text/csv;charset=utf8,' + encodeURIComponent(response.responseText);                         
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },          
      //------------------------------------------------------------ call to export Study
        doExportStudy: function(modelRecord)
        {		
            var modelJSON = JSON.stringify(modelRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/exportStudy',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Exporting study ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    var win = Ext.create('delta3.view.popup.ExportPopup');
                    var textArea = win.down('#exportDataString');
                    textArea.setValue(response.responseText);
                    win.show();                                      
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },         
      //----------------------------------------- call to export Study Information in printable form
        doExportStudyInfo: function(modelRecord)
        {		
            var modelJSON = JSON.stringify(modelRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/exportStudyInfo',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Exporting study information ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    var win = window.open ("http://localhost:8080/Delta3/index.html",'_blank',"top=60, left=100, status=0, toolbar=1, scrollbars=1, height=880, width=800");
                    win.document.write(response.responseText);                                 
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },              
      //------------------------------------------------------------ call to export Model
        doExportModel: function(modelRecord)
        {		
            var modelJSON = JSON.stringify(modelRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/exportModel',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Exporting model ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    var win = Ext.create('delta3.view.popup.ExportPopup');
                    var textArea = win.down('#exportDataString');
                    textArea.setValue(response.responseText);
                    win.show();                                      
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },     
      //------------------------------------------------------------ call to import Model
        doImport: function(objectJSON)
        {		
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/import/importObjects',
                    method: "POST",
                    disableCaching: true,
                    params: objectJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Importing object(s) ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();           
                if ( response.responseText !== 'ok') {
                    var returnValue = Ext.decode(response.responseText);
                    Ext.Msg.alert("DELTA", returnValue.status.message);
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var returnValue = Ext.decode(response.responseText);
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", returnValue.status.message);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },     
      //------------------------------------------------------------ call Web Service
        doCallWebService: function(requestJSON, serviceName, method, callbackFunction)
        {		
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/' + serviceName,
                    method: method,
                    disableCaching: true,
                    params: requestJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Calling Web Service ' + serviceName + '...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                callbackFunction(response, options);             
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                callbackFunction(response, options);   
            }         
        },         
      //------------------------------------------------------------ call to clone Model
        doLockUnlockModel: function(modelRecord, theGrid)
        {		
            var modelJSON = JSON.stringify(modelRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/lockUnlockModel',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Locking/Unlocking model ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA", response.responseText);
                    theGrid.store.load();
                    theGrid.getView().refresh();
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },          
      //------------------------------------------------------------ call to clone Model
        doCloneModel: function(modelRecord, theGrid)
        {		
            modelRecord.name += ' Clone';
            var d = new Date();
            modelRecord.outputName = 'ft' + d.valueOf();
            var modelJSON = JSON.stringify(modelRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/cloneModel',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Cloning model ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA", response.responseText);
                    theGrid.store.load();
                    theGrid.getView().refresh();
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },  
      //------------------------------------------------------------ call to clone Filter
        doCloneFilter: function(filterRecord, theGrid)
        {		
            var filterJSON = JSON.stringify(filterRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/cloneFilter',
                    method: "POST",
                    disableCaching: true,
                    params: filterJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Cloning filter ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA", response.responseText);               
                } else {
                    theGrid.store.load();
                    theGrid.getView().refresh();                         
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },          
      //------------------------------------------------------------ call to clone Study
        doCloneStudy: function(studyRecord, theGrid)
        {		
            var modelJSON = JSON.stringify(studyRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/cloneStudy',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Cloning study ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA", response.responseText);
                    theGrid.store.load();
                    theGrid.getView().refresh();                    
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },         
      //------------------------------------------------------------ call to split Study
        doSplitStudy: function(studyRecord, newModelName)
        {		
            //modelRecord.name = newModelName;
            var modelJSON = JSON.stringify(studyRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/splitStudy',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Splitting studies ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA", response.responseText);
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },  
      //------------------------------------------------------------ call to execute Study
        doExecuteStudy: function(studyRecord, theGrid)
        {		
            var modelJSON = JSON.stringify(studyRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/executeStudy',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    timeout: 360000, //6 minutes, extra time needed for very large selects 
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Submitting study ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA", response.responseText);
                    theGrid.store.load();
                    theGrid.getView().refresh();
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },      
      //------------------------------------------------------------ call to advance Study status
        doAdvanceStudy: function(selRecord, theGrid)
        {		
            var modelJSON = JSON.stringify(selRecord);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/advanceStudy',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Executing study status advance ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                Ext.Msg.alert("DELTA", 'Study status update: ' + response.responseText);
                selRecord.status = response.responseText;
                theGrid.getView().refresh();   
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },              
      //------------------------------------------------------------ call to roll back Study status
        doRollbackStudy: function(selRecord, theGrid)
        {		
            var modelJSON = encodeURIComponent(JSON.stringify(selRecord));  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/rollbackStudy',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Executing study status rollback ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                Ext.Msg.alert("DELTA", 'Study status update: ' + response.responseText);
                selRecord.status = response.responseText;
                theGrid.getView().refresh();               
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },          
      //------------------------------------------------------------ call to save results
        doSaveResults: function(results)
        {		
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/process/saveResults',
                    method: "POST",
                    disableCaching: true,
                    params: results,
                    timeout: 60000, //1 minute
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Saving results ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA", response.responseText);
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },           
      //------------------------------------------------------------ call to process results
        doProcessResults: function(studyRecord)
        {		
            var modelJSON = encodeURIComponent(JSON.stringify(studyRecord));  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/process/processResults',
                    method: "POST",
                    disableCaching: true,
                    params: modelJSON,
                    timeout: 360000, //6 minutes, extra time needed for very large selects 
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Processing study results ...');  
            return;
            
            function successFunction(response, options)
            {
                Ext.MessageBox.hide();
                if ( response.responseText !== 'ok') {
                    Ext.Msg.alert("DELTA", response.responseText);
                    //theGrid.store.load();
                    //theGrid.getView().refresh();
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },           
        //------------------------------------------------------------- save group-entity relationship
        saveRelationship: function(groupName, idEntity, type, idGroup) {
            var outputJSONString = '{"groups":[{"name":"' + groupName
                    + '", "idGroup":"' + idGroup
                    + '"}], "entities":[{"type":"' + type 
                    + '","idEntity":"' + JSON.stringify(idEntity)
                    + '"}]}';
            Ext.Ajax.request
                    (
                            {
                                url: '/Delta3/webresources/admin/addGroupEntity',
                                method: "POST",
                                disableCaching: true,
                                params: outputJSONString,
                                success: saveRelationshipReturn,
                                failure: saveRelationshipReturn
                            }
                    );
            function saveRelationshipReturn(response, options)
            {
               
                var returnValue = Ext.decode(response.responseText);
                console.log("This is returnValue"+returnValue);
                
                if(type ==='models')
                {
                      Ext.Ajax.request
                        (
                            {
                                url: '/Delta3/webresources/model/setProjectAssigned',
                                method: "POST",
                                disableCaching: true,        
                                params: idEntity,
                                success: doSetProjectStatusSuccess,
                                failure: doSetProjectStatusFailed
                            }
                        );
                        function doSetProjectStatusSuccess(response, options){console.log("This is project Assigned status Success");}
                        function doSetProjectStatusFailed(response, options){console.log("This is project Assigned status failed");}
                }
                
                
                if (typeof returnValue === 'undefined') {
                    message = 'General server error';
                } else {
                    message = returnValue.status.message;
                }
                
                
                if (response.status !== 200 || returnValue.success === false) {
                    delta3.utils.GlobalVars.MsgBox.hide();
                    var thisGrid = Ext.ComponentQuery.query('#groupEntityGrid')[0];
                    thisGrid.store.removeAt(0);
                    thisGrid.getView().refresh();
                    delta3.utils.GlobalVars.MsgBox.show
                            (
                                    {
                                        title: 'DELTA Web Services',
                                        message: '<b style="color:#cc0000;">Call failed</b> </br> Message from server: </br>' + message,
                                        progressText: 'Checking...',
                                        width: 400,
                                        wait: false,
                                        closable: false,
                                        icon: delta3.utils.GlobalVars.MsgBox.ERROR,
                                        buttons: delta3.utils.GlobalVars.MsgBox.OK
                                    }
                            );
                }
            }
        },
        //------------------------------------------------------------- remove old and save new group-entity relationship
        removeAndAddRelationships: function(groupName, idEntity, type, idGroup) {
            var outputJSONString = '{"groups":[{"name":"' + groupName
                    + '", "idGroup":"' + idGroup
                    + '"}], "entities":[{"type":"' + type 
                    + '","idEntity":"' + JSON.stringify(idEntity)
                    + '"}]}';
            Ext.Ajax.request
                    (
                            {
                                url: '/Delta3/webresources/admin/removeAndAddGroupEntity',
                                method: "POST",
                                disableCaching: true,
                                params: outputJSONString,
                                success: removeAndAddReturn,
                                failure: removeAndAddReturn
                            }
                    );
            function removeAndAddReturn(response, options)
            {
                var returnValue = Ext.decode(response.responseText);
                if (typeof returnValue === 'undefined') {
                    message = 'General server error';
                } else {
                    message = returnValue.status.message;
                }
                if (response.status !== 200 || returnValue.success === false) {
                    delta3.utils.GlobalVars.MsgBox.hide();
                    delta3.utils.GlobalVars.MsgBox.show
                            (
                                    {
                                        title: 'DELTA Web Services',
                                        message: '<b style="color:#cc0000;">Call failed</b> </br> Message from server: </br>' + message,
                                        progressText: 'Checking...',
                                        width: 400,
                                        wait: false,
                                        closable: false,
                                        icon: delta3.utils.GlobalVars.MsgBox.ERROR,
                                        buttons: delta3.utils.GlobalVars.MsgBox.OK
                                    }
                            );
                }
            }
        },        
        //------------------------------------------------------------- remove group-entity relationship        
        removeRelationship: function(groupName, idEntity, type, idGroup) {
            var outputJSONString = '{"groups":[{"name":"' + groupName
                    + '", "idGroup":"' + idGroup
                    + '"}], "entities":[{"type":"' + type 
                    + '","idEntity":"' + JSON.stringify(idEntity)
                    + '"}]}';        

            Ext.Ajax.request
                    (
                            {
                                url: '/Delta3/webresources/admin/removeGroupEntity',
                                method: "POST",
                                disableCaching: true,
                                params: outputJSONString,
                                success: removeRelationshipReturn,
                                failure: removeRelationshipReturn
                            }
                    );
            function removeRelationshipReturn(response, options)
            {
                var message;
                
                if(type ==='models')
                {
                      Ext.Ajax.request
                        (
                            {
                                url: '/Delta3/webresources/model/resetProjectAssigned',
                                method: "POST",
                                disableCaching: true,        
                                params: idEntity,
                                success: doResetProjectStatusSuccess,
                                failure: doResetProjectStatusFailed
                            }
                        );
                        function doResetProjectStatusSuccess(response, options){console.log("This is project Assigned status Success");}
                        function doResetProjectStatusFailed(response, options){console.log("This is project Assigned status failed");}
                      
                }
                
                var returnValue = Ext.decode(response.responseText);
                if (typeof returnValue === 'undefined') {
                    message = 'General server error';
                } else {
                    message = returnValue.status.message;
                }
                console.log('calling removeGroupEntityRelationship returned ' + message);
                if (response.status !== 200 || returnValue.success === false) {
                    delta3.utils.GlobalVars.MsgBox.hide();
                    delta3.utils.GlobalVars.MsgBox.show
                            (
                                    {
                                        title: 'DELTA Web Services',
                                        message: '<b style="color:#cc0000;">Call failed</b> </br> Message from server: </br>' + message,
                                        progressText: 'Checking...',
                                        width: 400,
                                        wait: false,
                                        closable: false,
                                        icon: delta3.utils.GlobalVars.MsgBox.ERROR,
                                        buttons: delta3.utils.GlobalVars.MsgBox.OK
                                    }
                            );
                }
            }
        },        
      //------------------------------------------------------------ call to get Filter Usage (before delete for example)
        getFilterUsage: function(filter, callbackFunction, sm)
        {		
            var stringJSON = JSON.stringify(filter);  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getFilterUsage',
                    method: "GET",
                    disableCaching: true,
                    params: {filter: stringJSON},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting filter usage ...');  
            return;

            function successFunction(response, options)
            {
                callbackFunction(response, options, sm);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },   
      //------------------------------------------------------------ call to verify Filter syntax againt db
        validateFilter: function(filterId, filterName, sql, callbackFunction)
        {
            // passed sqlwhere clause is ignored by the server!!
            var stringJSON = '{"filterFormulas":[{"filter":{"idFilter":' 
                    + filterId + ',"name":"' 
                    + filterName + '"},"condition":"' + sql +'"}]}';  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/validateFilter',
                    method: "POST",
                    disableCaching: true,
                    params: stringJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Verifying filter ...');  
            return;

            function successFunction(response, options)
            {
                callbackFunction(response, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },            
        //------------------------------------------------------------ call to get array of <atchSet strings based on studyId
        getMatchSetForStudy: function(studyId, callbackFunction)
        {		          
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/process/getMatchSets',
                    method: "GET",
                    disableCaching: true,
                    params: {studyId: studyId},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Match Sets for Propensity Analysis ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },           
      //------------------------------------------------------------ call to get LRF string
        getLRF: function(logregrId, logregrName, callbackFunction)
        {		
            var stringJSON = '{"logregrs":[{"idLogregr":' + logregrId
                    + ',"name":"'  + logregrName + '"}]}';  
            
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getLRF',
                    method: "GET",
                    disableCaching: true,
                    params: {logregr: stringJSON},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Logistic Regression Formula ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp.status.message, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },      
        
      //------------------------------------------------------------ call to get array of LRF strings based on modelId
        getLRFforModel: function(modelId, callbackFunction)
        {		          
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getLRFforModel',
                    method: "GET",
                    disableCaching: true,
                    params: {modelId: modelId},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Logistic Regression Formulas ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },           
      //------------------------------------------------------------ call to get LRF string based on studyId
        getLRFforStudy: function(studyId, callbackFunction)
        {		          
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getLRFforStudy',
                    method: "GET",
                    disableCaching: true,
                    params: {studyId: studyId},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Logistic Regression Formula ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },      
      //------------------------------------------------------------ call to get LRF string based on processId
        getLRFforProcess: function(processId, callbackFunction)
        {		          
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getLRFforProcess',
                    method: "GET",
                    disableCaching: true,
                    params: {processId: processId},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Logistic Regression Formula ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },   
      //------------------------------------------------------------ call to get sudy details for studyId
        getStudyDetails: function(studyId, callbackFunction)
        {		          
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getStudyDetails',
                    method: "GET",
                    disableCaching: true,
                    params: {studyId: studyId},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Study details...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp, options);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },          
      //------------------------------------------------------------ call to get LRF string based on studyId
        getResultsforStudy: function(processData, studyId, callbackFunction)
        {		          
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getResultsforStudy',
                    method: "GET",
                    disableCaching: true,
                    params: {studyId: studyId},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting Results for Study ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                var resp = JSON.parse(response.responseText);
                callbackFunction(resp, processData);
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },   
      //------------------------------------------------------------ Open new tab and Show Study results - used to "zoom in" from Dashboard
        showResultsTab: function(result) {
            console.log("This is at result tb");
            var theTabs = Ext.ComponentQuery.query('#maintabs')[0];               
            var tab = Ext.getCmp('Results_' + result.idProcess);               
            if ( !Ext.isEmpty(tab) ) {
                theTabs.remove(tab);
            }          
            var newTab = Ext.create('Ext.tab.Panel', {
                title: 'Results [' + result.name + ', ID ' + result.idProcess + ']',
                height: 750,
                scrollable: 'y',
                id: 'Results_' + result.idProcess,
                cWidth: delta3.utils.GlobalVars.chartWidth,
                cHeight: delta3.utils.GlobalVars.chartHeigh, 
                cFontFamily: delta3.utils.GlobalVars.fontFamily,
                cFontSize: delta3.utils.GlobalVars.fontSize,
                closable: true       
            });    
            theTabs.add(newTab).show();
            delta3.utils.GlobalFunc.displayResultsSubtabs(result, newTab);              
            theTabs.updateLayout();                   
        },         
      //------------------------------------------------------------ Open new tab and Show Study results - used to "zoom in" from Dashboard
        showResultsTabZoom: function(idStudy, idProcess) {   
            var processGrid = Ext.ComponentQuery.query('#processGrid')[0];    
            if ( typeof processGrig !== 'undefined' ) {
                Ext.Msg.alert("DELTA", "Please open Result Grid to select and view detail resluts.");
            }
            var rec = processGrid.getStore().findRecord('idProcess', idProcess);    
            if ( rec === null ) { 
                Ext.Msg.alert("DELTA", "Please use Result Grid to select and view detail resluts.");
            }
            var result = rec.data;
            var theTabs = Ext.ComponentQuery.query('#maintabs')[0];                                            
            var tab = Ext.getCmp('Results_' + result.idProcess);               
            if ( !Ext.isEmpty(tab) ) {
                theTabs.remove(tab);
            }          
            var newTab = Ext.create('Ext.tab.Panel', {
                title: 'Results [' + result.name + ', ID ' + result.idProcess + ']',
                height: 750,
                scrollable: 'y',
                id: 'Results_' + result.idProcess,
                cWidth: delta3.utils.GlobalVars.chartWidth,
                cHeight: delta3.utils.GlobalVars.chartHeigh, 
                cFontFamily: delta3.utils.GlobalVars.fontFamily,
                cFontSize: delta3.utils.GlobalVars.fontSize,
                closable: true       
            });    
            theTabs.add(newTab).show();
            delta3.utils.GlobalFunc.displayResultsSubtabs(result, newTab);              
            theTabs.updateLayout();                   
        },           
      //------------------------------------------------------------ Open new tab and Show Study last results
        showLastResultsTab: function(idStudy) {   
            delta3.utils.GlobalFunc.getStudyDetails(idStudy, getResults);             
            function getResults(studyData) {
                // following call fetches last result for a given study
                delta3.utils.GlobalFunc.getResultsforStudy(studyData, idStudy, normalizeToFirstRecord);  
                function normalizeToFirstRecord(result, studyData) {
                    delta3.utils.GlobalFunc.displayDashboard(result.process[0], studyData);                    
                }
            }
        },   
      //------------------------------------------------------------ Display Dashboard
        displayDashboard: function (result, studyData) {
            var studyName = delta3.utils.GlobalVars.StudyStore.findRecord('idStudy', result.idStudy).data.name;            
            var theTabs = Ext.ComponentQuery.query('#maintabs')[0];                               
            var cell = 'dash' + delta3.utils.GlobalVars.resultId + result.idProcess;            
            var extendedResults = JSON.parse(result.data);  
            var inputParams = JSON.parse(result.input);   
            var Y_Label='';                
            if ( typeof studyData.studyDetails[0].outcome.name === 'undefined' ) {
                Y_Label = result.message;
            } else {
                Y_Label = studyData.studyDetails[0].outcome.name;
            }                
            var tab = Ext.getCmp('Dashboard_' + result.idProcess);               
            if ( !Ext.isEmpty(tab) ) {
                theTabs.remove(tab);
            }          
            theTabs.add(Ext.create('Ext.tab.Panel', {
                title: 'Dashboard [' + result.name + ', ID ' + result.idProcess + ']',
                height: 750,
                scrollable: 'y',
                id: 'Dashboard_' + result.idProcess,
                cWidthDash: delta3.utils.GlobalVars.chartWidthDash,
                cHeightDash: delta3.utils.GlobalVars.chartHeightDash,                
                cFontFamily: delta3.utils.GlobalVars.fontFamily,
                cFontSize: delta3.utils.GlobalVars.fontSize,               
                html: '<div id="' + cell + '"></div>',
                closable: true,
                dockedItems: [{
                    xtype: 'toolbar',
                    docked: 'top',
                    items: [{
                        xtype: 'label',
                        width: 50,
                        text: 'Width:',
                        margin: '0 0 0 10'
                    }, {
                        xtype: 'textfield',
                        width: 50,
                        itemId: 'wipText',
                        name: 'wipText',
                        value: delta3.utils.GlobalVars.chartWidthDash,
                        emptyText: 'Width in pixels'
                    },  {
                        xtype: 'label',
                        width: 50,
                        text: 'Height:',
                        margin: '0 0 0 10'
                    }, {
                        xtype: 'textfield',
                        width: 50,
                        itemId: 'hipText',
                        name: 'hipText',
                        value: delta3.utils.GlobalVars.chartHeightDash,
                        emptyText: 'Height in pixels'
                    }, {
                        xtype: 'button',
                        text: 'Resize',
                        iconCls: 'resize-icon16',                                
                        handler: function() {
                            var mTab = this.up().up();
                            mTab.cWidthDash = this.up().down('#wipText').getValue();
                            mTab.cHeightDash = this.up().down('#hipText').getValue();                                   
                            //delta3.utils.GlobalVars.fontFamily = this.up().down('#fontFamilyText').getValue();
                            //delta3.utils.GlobalVars.fontSize = this.up().down('#fontSizeText').getValue(); 
                            var panel = Ext.ComponentQuery.query('#Results_' + result.idProcess);
                            document.getElementById(cell).innerHTML = '';
                            panel.html = '<div id="' + cell + '"></div>';
                            extendedResults.params = {id: cell, 
                                idStudy: studyData.studyDetails[0].idStudy, 
                                outcome: Y_Label, 
                                width: mTab.cWidthDash, 
                                height: mTab.cHeightDash,
                                fontFamily: mTab.cFontFamily,
                                fontSize: mTab.cFontSize
                            };
                            showDashboard(extendedResults, inputParams, studyName, true, cell, result.idProcess);                             
                        }
                    }, {
                        xtype: 'combobox',
                        margin: '0 0 0 10',
                        fieldLabel: 'Font Family',
                        itemId: 'fontFamilyText',
                        labelWidth: 60,
                        labelAlign: 'left',    
                        minWidth: 290,
                        store: delta3.utils.GlobalVars.fontFamilyComboBoxStore,
                        displayField: 'fontFamily',
                        valueField: 'fontFamily',
                        emptyText: 'Font Family',
                        queryMode: 'local',
                        forceSelection: true,
                        multiSelect: false
                    }, {
                        xtype: 'combobox',
                        margin: '0 0 0 10',
                        fieldLabel: 'Font Size',
                        itemId: 'fontSizeText',
                        labelWidth: 50,
                        labelAlign: 'left',    
                        maxWidth: 120,
                        store: delta3.utils.GlobalVars.fontSizeComboBoxStore,
                        displayField: 'fontSize',
                        valueField: 'fontSize',
                        emptyText: 'Size px',
                        queryMode: 'local',
                        forceSelection: true,
                        multiSelect: false
                    }, {
                        xtype: 'button',
                        text: 'Change Font',
                        iconCls: 'font-icon16',                                
                        handler: function() {
                            var mTab = this.up().up();
                            //delta3.utils.GlobalVars.chartWidth = this.up().down('#wipText').getValue();
                            //delta3.utils.GlobalVars.chartHeigh = this.up().down('#hipText').getValue();                                   
                            mTab.cFontFamily = this.up().down('#fontFamilyText').getValue();
                            mTab.cFontSize = this.up().down('#fontSizeText').getValue();  
                            var panel = Ext.ComponentQuery.query('#Results_' + result.idProcess);
                            document.getElementById(cell).innerHTML = '';
                            panel.html = '<div id="' + cell + '"></div>';
                            extendedResults.params = {
                                id: delta3.utils.GlobalVars.resultId + result.idProcess, 
                                idStudy: studyData.studyDetails[0].idStudy,
                                outcome: Y_Label, 
                                width: mTab.cWidthDash, 
                                height: mTab.cHeightDash,
                                fontFamily: mTab.cFontFamily,
                                fontSize: mTab.cFontSize  
                            };
                            showDashboard(extendedResults, inputParams, studyName, true, cell, result.idProcess);                             
                        }
                    }, {
                            xtype: 'button',
                            text: 'Result Charts',
                            iconCls: 'show_results-icon16',                                
                            handler: function() {          
                                delta3.utils.GlobalFunc.showResultsTab(result); 
                            }
                        }]
                }]            
            })).show();    
            extendedResults.params = {id: cell, idStudy: studyData.studyDetails[0].idStudy, outcome: Y_Label, width: delta3.utils.GlobalVars.chartWidthDash, height: delta3.utils.GlobalVars.chartHeightDash};
            showDashboard(extendedResults, inputParams, studyName, true, cell, result.idProcess);                 
            theTabs.updateLayout();      
        },       
        
      //------------------------------------------------------------ Display subtabs of graphs and charts
        displayResultsSubtabs: function (result, masterTab) {  

            var extendedResults = JSON.parse(result.data);    
            var inputParams = JSON.parse(result.input);    
            var resultIndexMax = getOutcomesMaxNumber(extendedResults);
            var studyName = delta3.utils.GlobalVars.StudyStore.findRecord('idStudy', result.idStudy).data.name;
            var cellArray = [];
            var subTabTitle = '';
            /*--------------Previous code
            for ( var i = 0; i<extendedResults.results.length; i++) {
                if ( typeof extendedResults.results[i].Y_Axis !== 'undefined' 
                        && extendedResults.results[i].outputType === 'Chart' ) {
                    // pick the first one, like below. It is a temp fix - needs more precises code
                    subTabTitle = extendedResults.results[i].Y_Axis;
                    break;
                }
            }*/
            
            //--Added below code for V3.64 for https://ceri-lahey.atlassian.net/browse/DELQA-722
            var titleArray = [];
            for ( var i = 0; i<extendedResults.results.length; i++) {
                  if(extendedResults.results[i].outputType === 'Chart'){
                        //console.log(extendedResults.results[i].Y_Axis);
                        //subTabTitle = extendedResults.results[i].Y_Axis;
                        titleArray[i]= extendedResults.results[i].Y_Axis.trim();
                    }
               }

               var titleArrayUnique = titleArray.filter(function(item, index){
                    return titleArray.indexOf(item) >= index;
                });
            //--------------------------------------------------------------------------------
            
            for (var resultIndex=0; resultIndex<resultIndexMax; resultIndex++ ) {
                var cell = delta3.utils.GlobalVars.resultId + result.idProcess + resultIndex;  
                cellArray.push(cell);
                console.log("This is title array"+titleArrayUnique[resultIndex]);
                masterTab.add(Ext.create('Ext.tab.Panel', {
                    //title: 'Outcome ' + subTabTitle,
                    title: 'Outcome ' + titleArrayUnique[resultIndex],
                    height: 750,
                    scrollable: 'y',
                    id: 'Result_' + result.idProcess + '_' + resultIndex,
                    html: '<div id="' + cell + '"></div>',
                    closable: true,
                    dockedItems: [{
                        xtype: 'toolbar',
                        docked: 'top',
                        items: [{
                            xtype: 'label',
                            width: 50,
                            text: 'Width:',
                            margin: '0 0 0 10'
                        }, {
                            xtype: 'textfield',
                            width: 50,
                            itemId: 'wipText'+resultIndex,
                            name: 'wipText'+resultIndex,
                            value: delta3.utils.GlobalVars.chartWidth,
                            emptyText: 'Width in pixels'
                        },  {
                            xtype: 'label',
                            width: 50,
                            text: 'Height:',
                            margin: '0 0 0 10'
                        }, {
                            xtype: 'textfield',
                            width: 50,
                            itemId: 'hipText'+resultIndex,
                            name: 'hipText'+resultIndex,
                            value: delta3.utils.GlobalVars.chartHeight,
                            emptyText: 'Height in pixels'
                        }, {
                            xtype: 'button',
                            text: 'Resize',
                            iconCls: 'resize-icon16',                                
                            handler: function() {
                                var mTab = this.up().up();
                                var rIndex = parseInt(mTab.id.substring(mTab.id.length-1));
                                mTab.cWidth = this.up().down('#wipText'+rIndex).getValue();
                                mTab.cHeight = this.up().down('#hipText'+rIndex).getValue();                                   
                                //mTab.cFontFamily = this.up().down('#fontFamilyText').getValue();
                                //mTab.cFontSize = this.up().down('#fontSizeText').getValue();
                                var idString = this.up().up().id;
                                var index = parseInt(idString.substring(idString.length-1));
                                var panel = Ext.ComponentQuery.query('#Results_' + result.idProcess);
                                document.getElementById(cellArray[index]).innerHTML = '';
                                panel.html = '<div id="' + cellArray[index] + '"></div>';
                                extendedResults.params = {id: cellArray[index], 
                                    outcome: 'outcome', 
                                    width: mTab.cWidth, 
                                    height: mTab.cHeight,
                                    fontFamily: mTab.cFontFamily,
                                    fontSize: mTab.cFontSize
                                };
                                processResult(extendedResults, inputParams, studyName, true, cellArray[index], index);     
                            }
                        }, {
                            xtype: 'combobox',
                            margin: '0 0 0 10',
                            fieldLabel: 'Font Family',
                            itemId: 'fontFamilyText',
                            labelWidth: 60,
                            labelAlign: 'left',    
                            minWidth: 290,
                            store: delta3.utils.GlobalVars.fontFamilyComboBoxStore,
                            displayField: 'fontFamily',
                            valueField: 'fontFamily',
                            value: delta3.utils.GlobalVars.fontFamily,  
                            emptyText: 'Font Family',
                            queryMode: 'local',
                            forceSelection: true,
                            multiSelect: false
                        }, {
                            xtype: 'combobox',
                            margin: '0 0 0 10',
                            fieldLabel: 'Font Size',
                            itemId: 'fontSizeText',
                            labelWidth: 50,
                            labelAlign: 'left',    
                            maxWidth: 120,
                            store: delta3.utils.GlobalVars.fontSizeComboBoxStore,
                            displayField: 'fontSize',
                            valueField: 'fontSize',
                            value: delta3.utils.GlobalVars.fontSize,  
                            emptyText: 'Size px',
                            queryMode: 'local',
                            forceSelection: true,
                            multiSelect: false
                        }, {
                            xtype: 'button',
                            text: 'Change Font',
                            iconCls: 'font-icon16',                                
                            handler: function() {
                                var mTab = this.up().up();
                                //var rIndex = parseInt(mTab.id.substring(mTab.id.length-1));
                                //mTab.cWidth = this.up().down('#wipText'+rIndex).getValue();
                                //mTab.cHeight = this.up().down('#hipText'+rIndex).getValue();                                    
                                mTab.cFontFamily = this.up().down('#fontFamilyText').getValue();
                                mTab.cFontSize = this.up().down('#fontSizeText').getValue();   
                                var panel = Ext.ComponentQuery.query('#Results_' + result.idProcess);
                                var idString = this.up().up().id;
                                var index = parseInt(idString.substring(idString.length-1));
                                var panel = Ext.ComponentQuery.query('#Results_' + result.idProcess);
                                document.getElementById(cellArray[index]).innerHTML = '';
                                panel.html = '<div id="' + cellArray[index] + '"></div>';
                                extendedResults.params = {
                                    id: delta3.utils.GlobalVars.resultId + result.idProcess, 
                                    outcome: 'Outcome', 
                                    width: mTab.cWidth, 
                                    height: mTab.cHeight,
                                    fontFamily: mTab.cFontFamily,
                                    fontSize: mTab.cFontSize
                                };
                                processResult(extendedResults, inputParams, studyName, true, cellArray[index], index);                         
                            }
                        }]
                    }]            
                })).show();    
                extendedResults.params = {id: cell, outcome: 'outcome', width: delta3.utils.GlobalVars.chartWidth, height: delta3.utils.GlobalVars.chartHeight};
                processResult(extendedResults, inputParams, studyName, true, cell, resultIndex);     
            }
            masterTab.updateLayout();      
        },                                        
      //------------------------------------------------------------ call to clone LRF
        doCloneLRF: function(data, logregrName, theGrid)
        {		
            var stringJSON = '{"logregrs":[{"idLogregr":' + data.idLogregr
                    + ',"name":"'  + data.name + '"}]}';  
            
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/cloneLRF',
                    method: "POST",
                    disableCaching: true,
                    params: stringJSON,
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Cloning Logistic Regression Formula ...');  
            return;
            
            function successFunction(response, options)
            {
                var resp = JSON.parse(response.responseText);
                if (typeof resp.success !== 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage(resp.status.message);
                    theGrid.store.load();
                    theGrid.getView().refresh();                            
                }
            }

            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },       
        //------------------------------------------------------------- get where cluase string for Filter from backend
        getFilterWhereClauseFromServer: function(filterId, callbackFunction) {
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/study/getFilterWhereClause',
                    method: "GET",
                    disableCaching: true,
                    params: {filter: filterId},
                    success: successFunction,
                    failure: failedFunction
                }
            );
            Ext.MessageBox.wait('Getting filter where clause ...');  
            return;

            function successFunction(response, options)
            {
                Ext.MessageBox.hide();  
                callbackFunction(JSON.parse(response.responseText));
            }
            
            function failedFunction(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            } 
        },        
        //------------------------------------------------------------- generate where cluase string from Filter Fields
        // this function should no longer be used as backend is solely responsible for this evaluation
        getFilterWhereClause: function(formulas, fieldStore) {
            var counterMax = formulas.length;
            var orFlag = 0;
            var sqlOR = '';
            var sql = '';
            // build ORs first
            for (var i=0; i< counterMax; i++) {
                var r = formulas[i];
                if ( typeof r.data === 'undefined' ) {   
                    // this is to allow call to this function from Study Grid w/o reloading formulas
                    r.data = {};
                    r.data.idModelColumn = r.modelColumnidModelColumn;
                    r.data.formula = r.formula;
                    r.data.type = r.type;
                }
                if ( r.data.type === 'Include' ) {
                    var field = fieldStore.findRecord('idModelColumn', r.data.idModelColumn, 0, false, false, true);
                    if ( field === null ) {
                        sql += '';
                    } else {
                        if ( orFlag > 0 ) {
                            sql += ' OR ';
                        }
                        orFlag++;
                        sql += '(' + field.get("name") + ' ' + r.data.formula + ')';       
                    }
                }
            }
            if ( orFlag > 1 ) {
                sqlOR = '(' + sql + ')';
            } else {
                sqlOR = sql;
            }
            // now built negative ANDs
            var andFlag = 0;
            sql = '';
            for (var i=0; i< counterMax; i++) {
                var r = formulas[i];
                if ( typeof r.data === 'undefined' ) {   
                    // this is allow call to this function from Study Grid w/o reloading formulas
                    r.data = {};
                    r.data.idModelColumn = r.modelColumnidModelcColumn;
                    r.data.formula = r.formula;
                    r.data.type = r.type;                    
                }                
                if ( r.data.type === 'Exclude' ) {
                    var field = fieldStore.findRecord('idModelColumn', r.data.idModelColumn, 0, false, false, true);
                    if ( field === null ) {
                        sql += '';
                    } else {                    
                        if ( (orFlag > 0) || (andFlag > 0) ) {
                            sql += ' AND ';
                        }
                        andFlag++;
                        sql += 'NOT (' + field.get("name") + ' ' + r.data.formula + ')';      
                    }
                }
            }                    
            return sqlOR+sql;
        },
        //------------------------------------------------------------ call to "go pseudo"
        doPseudo: function(userAlias, idUser, callbackFunction)
        {
            var userAuthInfoJSONString = '{"userAuth":[{"alias":"' + userAlias 
            + '", "idUser":"' + idUser
            + '"}]}';
    
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/goPseudo',
                    method: "POST",
                    disableCaching: true,
                    params: userAuthInfoJSONString,                    
                    success: doPseudoSuccess,
                    failure: doPseudoFailed
                }
            );
            Ext.MessageBox.wait('Please wait ...');  
            return;
            
            function doPseudoSuccess(response, options)
            {
                Ext.MessageBox.hide();
                callbackFunction();
            }

            function doPseudoFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },
        //------------------------------------------------------------ call to get current logging level
        doGetLoggingLevel: function(callbackFunction)
        {
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/getLoggingLevel',
                    method: "GET",
                    disableCaching: true,               
                    success: callSuccess,
                    failure: callFailed
                }
            );
            Ext.MessageBox.wait('Please wait ...');  
            return;
            
            function callSuccess(response, options)
            {
                Ext.MessageBox.hide();
                callbackFunction(Ext.decode(response.responseText));
            }

            function callFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },        
        //------------------------------------------------------------ call to change logging level
        doChangeLoggingLevel: function(loggingLevel)
        {
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/changeLoggingLevel',
                    method: "POST",
                    disableCaching: true,
                    params: loggingLevel,                    
                    success: callSuccess,
                    failure: callFailed
                }
            );
            Ext.MessageBox.wait('Please wait ...');  
            return;
            
            function callSuccess(response, options)
            {
                Ext.MessageBox.hide();
                var returnValue = Ext.decode(response.responseText);
                var message;
                if (typeof returnValue === 'undefined') {
                    message = 'General server error';
                } else {
                    message = returnValue.status.message;
                } 
                Ext.Msg.alert("DELTA", message);
            }

            function callFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },       
        //----------------------------------------------------------------------         
        getCorrelationStats: function(modelRecord, filterselection, isFlatTable) 
        {
            MsgBox = Ext.MessageBox;
            var JSONString = '';

            JSONString = '{"dStats":[{"models":[' + JSON.stringify(modelRecord)
                    + '], "filters":[' + filterselection
                    + ']}]}';

            var serviceUrl;
            if ( isFlatTable === true ) {
                serviceUrl = '/Delta3/webresources/process/getCorrelationFlatTable';
            } else {
                serviceUrl = '/Delta3/webresources/process/getCorrelationStagingTable'; 
            }
            MsgBox.wait('Obtaining Correlation Statistics... Please wait...', 'DELTA');
            Ext.Ajax.request
                    (
                            {
                                url: serviceUrl,
                                method: "POST",
                                disableCaching: true,
                                params: JSONString,
                                timeout: 1200000, //20 minutes, extra time needed for very large calculations 
                                success: dStatsCallSuccess,
                                failure: dStatsCallFailed
                            }
                    );

            function dStatsCallSuccess(response, options) {
                try {
                    var returnValue = Ext.decode(response.responseText);
                } catch (err) {
                    // if server not wrapping messages correctly
                    dStatsCallFailed(response, options);
                    return;
                }
                if (returnValue.success === true) {
                    MsgBox.hide();
                    var thisGrid = Ext.ComponentQuery.query('#fieldGrid')[0];
                    var dsWindow = Ext.ComponentQuery.query('#descriptiveStatistics')[0];
                    dsWindow.expand(true);
                    // remove descriptive stats tab for this field if exists
                    var tab = Ext.getCmp(isFlatTable + 'corrTab');
                    if (tab !== null) {
                        dsWindow.remove(tab);
                    }
                    dsWindow.add(Ext.create('delta3.view.ds.DescriptiveStatsContainer', {
                        title: "Correlation",
                        id: isFlatTable + 'corrTab',
                        isFlatTable: isFlatTable,
                        selectedRecord: {},
                        dStats: returnValue.dStats,
                        fieldStore: thisGrid.store,
                        closable: true
                    })).show();
                    dsWindow.updateLayout();
                }
                else {
                    delta3.utils.GlobalFunc.showDeltaMessage(returnValue.status.message);
                }
            }

            function dStatsCallFailed(response, options) {
                Ext.MessageBox.hide();
                if (response.timedout === true) {
                    delta3.utils.GlobalFunc.showDeltaMessage(response.statusText);
                } else {
                    delta3.utils.GlobalFunc.showDeltaMessage(response.responseText);
                }
            }
        },                       
        //---------------------------------------------------------------------- 
        doGetModelJoin: function(callback)
        {
            var extraParams = {model: '{ "models":[' + JSON.stringify(ModelData.modelSelected) + ']}'};  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/getJoin',
                    method: "GET",
                    disableCaching: true,
                    params: extraParams,                    
                    success: doGetModelJoinSuccess,
                    failure: doGetModelJoinFailed
                }
            );
            //Ext.MessageBox.wait('Please wait ...');  
            return;
            
            function doGetModelJoinSuccess(response, options)
            {
                callback(response.responseText);
            }

            function doGetModelJoinFailed(response, options)
            {
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", "Timeout: " + response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        }, 
        //---------------------------------------------------------------------- 
        doUpdateModelDropdowns: function(callback)
        {
            var extraParams = {model: '{ "models":[' + JSON.stringify(ModelData.modelSelected) + ']}'};  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/getModelDetails',
                    method: "GET",
                    disableCaching: true,
                    params: extraParams,                    
                    success: doUpdateModelDropdownsSuccess,
                    failure: doUpdateModelDropdownsFailed
                }
            );
            Ext.MessageBox.wait('Please wait ...');  
            return;
            
            function doUpdateModelDropdownsSuccess(response, options)
            {
                Ext.MessageBox.hide();
                callback(Ext.decode(response.responseText));
            }

            function doUpdateModelDropdownsFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },        
        //----------------------------------------------------------------------
        doUpdateTableTree: function()
        {
            var extraParams = {model: '{ "models":[' + JSON.stringify(ModelData.modelSelected) + ']}'};  
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/model/getModelDetails',
                    method: "GET",
                    disableCaching: true,
                    params: extraParams,                    
                    success: doUpdateTableTreeSuccess,
                    failure: doUpdateTableTreeFailed
                }
            );
            Ext.MessageBox.wait('Please wait ...');  
            return;
            
            function doUpdateTableTreeSuccess(response, options)
            {
                Ext.MessageBox.hide();
                delta3.utils.GlobalFunc.updateTableTree(Ext.decode(response.responseText));
            }

            function doUpdateTableTreeFailed(response, options)
            {
                Ext.MessageBox.hide();  
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", response.responseText);      
                }
            }         
        },             
        //------------------------------------------------------------ call to update MB Table Tree        
        updateTableTree: function(response) {
            if ((typeof response.modelTables !== 'undefined') && (typeof response.modelColumns !== 'undefined')) {
                delta3.utils.GlobalFunc.updateModelBuilderDropdowns(ModelData, response);
                var treeModelString = '{"text":"' + ModelData.modelSelected.name + '","expanded":true,"children":[';
                //var treeModelString = '{"text":"' + ModelData.modelSelected.name + '","children":['; // collapsed version
                for (var i = 0; i < response.modelTables.length; i++) {
                    if (i !== 0) {
                        treeModelString += ',';
                    }
                    treeModelString += '{"text":"';
                    var newValue = response.modelTables[i].physicalName;
                    treeModelString += newValue + '","leaf": false';
                    if (response.modelTables[i].primaryTable === true) {
                        treeModelString += ',"iconCls":"primary-icon16","qtip":"primary table"';
                    }
                    treeModelString += ',"allowDrop":false,"expanded":false,"allowDrag":false}';
                    var id = ModelData.tableStore.findRecord('physicalName', newValue, 0, false, false, true); // store.findRecord
                    if ((typeof id !== 'undefined') && (id !== null)) {
                        id.data = response.modelTables[i];
                    }
                }
                treeModelString += ']}';
                var treeModel = JSON.parse(treeModelString);
                ModelData.relationshipTreeStore.setRootNode(treeModel);
                var digestedTreeModel = ModelData.relationshipTreeStore.getRootNode();
                var metadata = Ext.ComponentQuery.query('#metadataTree')[0].store.getRootNode();
                for (var x = 0; x < response.modelTables.length; x++) {
                    for (i = 0; i < metadata.childNodes.length; i++) {
                        if (response.modelTables[x].physicalName === metadata.childNodes[i].get('text')) {
                            for (j = 0; j < metadata.childNodes[i].childNodes.length; j++) {
                                digestedTreeModel.getChildAt(x).appendChild({
                                    text: metadata.childNodes[i].data.children[j].text,
                                    qtip: delta3.utils.GlobalFunc.normalizeDBTypes(metadata.childNodes[i].data.children[j].type),
                                    allowDrop: false,
                                    iconCls: columnUsedInModel(response.modelColumns, response.modelTables[x].physicalName, metadata.childNodes[i].data.children[j].text),
                                    checked: false,
                                    leaf: true
                                });
                            }
                        }
                    }
                }
            }
            function columnUsedInModel(modelColumns, tableName, columnName) {
                for (var i = 0; i < modelColumns.length; i++) {
                    if ((modelColumns[i].tableName === tableName) && (modelColumns[i].physicalName === columnName)) {
                        return 'field-used';
                    }
                }
                return '';
            }            
        },      
        //------------------------------------------------------------- remove stat package formula due to manual action
        clearLRFormula: function(logregrId) {
            var lrfGrid = Ext.ComponentQuery.query('#logregrGrid')[0];
            var rec = lrfGrid.getStore().findRecord('idLogregr',logregrId, 0, false, false, true);
            rec.set('formula','');
            lrfGrid.getStore().sync();
            var stats = Ext.ComponentQuery.query('#statsLRF')[0];
            if ( (typeof stats !== 'undefined') && (stats !== null) ) {
                Ext.ComponentQuery.query('#statsLRF')[0].removeAll(); 
            }
        },
        //------------------------------------------------------------- normlzed database types to My-SQL common denominator
        normalizeDBTypes: function(type) {
            if (type.toUpperCase() === 'SMALLDATETIME') {
                return 'DATETIME';
            }
            return type.toUpperCase();
        },
        mapFieldNamesToIDs: function(fieldStore, fieldList) {
            var arrayOfFieldNames = fieldList.split(",");  
            var listOfIDs = '';
            for (var i = 0; i < arrayOfFieldNames.length; i++) {
                if ( i > 0 ) listOfIDs += ',';
                var index = fieldStore.find('name',arrayOfFieldNames[i]);
                listOfIDs += fieldStore.getAt(index).get('idModelColumn');
            }          
            return listOfIDs;
        },
        //------------------------------------------------------------- call to fill in KeyComboBoxes and tableName(s)
        updateModelBuilderDropdowns: function (mbData, response) 
        {
            var comboItemString;
            mbData.modelTableComboStore.clearFilter(true);
            mbData.modelTableComboStore.removeAll(true); //silent
            for ( var j=0; j<response.modelTables.length; j++ ) {
                 // fill in table_name-->table_id map
                 mbData.tableStore.tableToIdMap[response.modelTables[j].physicalName] = response.modelTables[j].idModelTable;
                 comboItemString = '{"idModelTable":"' + response.modelTables[j].idModelTable 
                         + '","physicalName":"' + response.modelTables[j].physicalName + '"}';
                 mbData.modelTableComboStore.add(JSON.parse(comboItemString));                          
            }             
            mbData.modelKey1ComboStore.clearFilter(true);
            mbData.modelKey2ComboStore.clearFilter(true);
            mbData.modelKey1ComboStore.removeAll(true); //silent
            mbData.modelKey2ComboStore.removeAll(true); //silent         
            for ( var i=0; i<response.modelColumns.length; i++ ) {
                    // prepare 2 Key Combo Boxes for Relationships tab
                    if (response.modelColumns[i].keyField === true &&
                        response.modelColumns[i].active === true &&
                        response.modelColumns[i].atomic === true ) {
                        comboItemString = '{"idModelColumn":"' + response.modelColumns[i].idModelColumn 
                                + '","name":"' + response.modelColumns[i].name
                                + '","type":"' + response.modelColumns[i].type
                                + '","modelTableidModelTable":"' + response.modelColumns[i].modelTableidModelTable + '"}';
                        mbData.modelKey1ComboStore.add(JSON.parse(comboItemString));
                        mbData.modelKey2ComboStore.add(JSON.parse(comboItemString));                                            
                    }                                    
                   for ( j=0; j<response.modelTables.length; j++ ) {
                       // fill in tableName in columns based on tables
                       if ( response.modelTables[j].idModelTable === response.modelColumns[i].modelTableidModelTable  ) {
                          response.modelColumns[i].tableName = response.modelTables[j].physicalName;
                       }
                   }                                
             }    
        },
        getNumberOfParams: function (stats) {
            var i = 0;
            while ( ( typeof stats[1].statistics[i] !== 'undefined' ) && (stats[1].statistics[i].filter === "") ) i++;
            return i;
        },       
        openTableViewerTab: function(processRecord, appendString) {	
            var theTabs = Ext.ComponentQuery.query('#maintabs')[0];    
            var tab = Ext.getCmp('tableViewerTab' + appendString);
            if ( !Ext.isEmpty(tab) ) {
                theTabs.remove(tab);
            }
            var titleString = 'Flat Table: '
            if ( appendString === '' ) {
                titleString = 'Staging Table: ';
            }
            titleString += processRecord.name;
            theTabs.add(Ext.create('delta3.view.mb.TableViewerGrid',{
                idModel: processRecord.idModel,
                id: 'tableViewerTab' + appendString,
                title: titleString,
                append: appendString,
                autoShow: true,
                closable: true,
                region: 'center'
            })).show();        
            theTabs.updateLayout();
        },
        openModelTab: function(processRecord, dataSourceName){		
            var theTabs = Ext.ComponentQuery.query('#maintabs')[0];    
            var tab = Ext.getCmp('modelBuilderTab');
            if ( !Ext.isEmpty(tab) ) {
                tab.removeAll();
                theTabs.remove(tab);
            }
            var t = theTabs.add(Ext.create('delta3.view.mb.ModelBuilderContainer', { 
                        title: 'Model Builder: '+ processRecord.name,         
                        modelSelected: processRecord,
                        dataSourceName: dataSourceName,
                        id: 'modelBuilderTab',
                        closable: true
                    }));
            theTabs.updateLayout();                    
            t.show();   
        },   
        getPagingComponentIndex: function(dockedItems) {
            for (var i=0; i<dockedItems.length; i++) {
                if ( dockedItems.keys[i].indexOf("pagingtoolbar") > -1 ) {
                    return i;
                }
            } 
        },
        // this function is no longer used as IndepVariables are not loaded manually
        verifyIndependentVariableList: function(theWindow, arrayOfVariables) {
            var resultString = '';
            theWindow.fieldStore.filter('fieldClass', 'Risk Factor');
            theWindow.fieldStore.filter('idModel', theWindow.modelId);
            theWindow.fieldStore.filter('insertable', true);            
            for (var i = 0; i < arrayOfVariables.length; i++) {
                var found = false;
                for (var j = 0; j < theWindow.fieldStore.getCount(); j++) {
                    if ( theWindow.fieldStore.getAt(j).get('name') === arrayOfVariables[i] ) {
                        found = true;
                    }
                }
                if ( found === false ) {
                    resultString += arrayOfVariables[i] + ' ';
                }
            }       
            return resultString;
        },      
        //------------------------------------------------------------ call to initialize local stat config
        callLocalStatPackage: function (command, callbackFunction, data)
        {		
            //var extraParams = {cmd: '' + command + ''};
            var params;
            if ( typeof data !== 'undefined' ) {
                params = '{"cmd":"' + command.toString() + '","data":"' + data.toString() + '"}';
            } else {
                params = '{"cmd":"' + command.toString() + '"}';
            }
            var extraParams = JSON.parse(params);
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/process/callLocalStatPackage',
                    method: "GET",
                    disableCaching: true,      
                    params: extraParams,
                    success: successFunc,
                    failure: failureFunc
                }
            );
            return;
            
            function successFunc(response, options)
            {
                callbackFunction(response);
            }

            function failureFunc(response, options)
            {
                if ( response.timedout === true ) {
                    Ext.Msg.alert("DELTA", response.statusText);
                } else {
                    Ext.Msg.alert("DELTA", "DELTA server not responding.\nPlease close browser tab.");      
                }
            }  
        },        
//------------------------------------------------------------------------------                
        getJIRACollector: function() {
            Ext.Ajax.request
            (
                {
                    method: "GET",
                    disableCaching: true,
                    url: "https://ceri-lahey.atlassian.net/s/942d310bc0ffd4c91c9f62f34e7e1578-T/en_US-596pgm/71002/b6b48b2829824b869586ac216d119363/2.0.11/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-US&collectorId=bf9f9ff4",              
                    success: callSuccess,
                    failure: callFailed
                }
            );

            function callSuccess(response, options) {
               var view = Ext.ComponentQuery.query('#currentUserAlias')[0];
               console.log("JIRA code is here");
            }   

            function callFailed(response, options) {
                Ext.MessageBox.show
                (
                    {
                        title: 'DELTA',
                        msg: '<b style="color:#cc0000;">JIRA Collector button failed to load!</b>',
                        progressText: 'Checking...',
                        width: 400,
                        wait: false,
                        closable: false,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.MessageBox.OK,
                        fn: function() { }
                    }
                );    
                //viewPort.destroy();
            }
        },       
//------------------------------------------------------------------------------        
        
        /*
        getMenuByProject: function (projectId) 
        {
            
            //var menues = Ext.decode(delta3.utils.GlobalVars.currentUserAuthInfo.mainMenu);
            var menues = delta3.utils.GlobalVars.currentUserAuthInfo.mainMenu;  
             console.log("This is at getMenuByProject projectId" +projectId);
            
            var All_project_access;
            var cnt = delta3.utils.GlobalVars.currentUserAuthInfo.userAuth.projects.length;
            //console.log("total_project Assigned : "+cnt);
            var menues = delta3.utils.GlobalVars.currentUserAuthInfo.mainMenu;              
            for (var i=0; i<menues.length; i++) {
                if(menues[i].projectId ===0 && cnt===1)
                {        
                    All_project_access =true;
                }
                else
                {
                    All_project_access =false;
                }
                
                
               if(All_project_access ===true)
               {
                    return Ext.decode(menues[i].menu);
                }
                else{
                    if ( menues[i].projectId === projectId ) {
                    return Ext.decode(menues[i].menu);
                    }
                    
                }
            }
        },*/       
        
        
        
        
        
        
        //Below code is working without project Assigned functionality
        getMenuByProject: function (projectId) {
            //var menues1 = Ext.decode(delta3.utils.GlobalVars.currentUserAuthInfo.mainMenu);
            var menues = delta3.utils.GlobalVars.currentUserAuthInfo.mainMenu;  
            
            for (var i=0; i<menues.length; i++) {
                console.log("menues[i].menu"+menues[i].menu);               
                if ( menues[i].projectId === projectId ) {
                    //console.log("menues[i].projectId"+menues[i].projectId);
                    
                    return Ext.decode(menues[i].menu);
                }
            }
        },       
        
        
        
        
        
//------------------------------------------------------------------------------
        getCurrentUserAuthInfo: function (callbackSuccess, callbackFailed) {	
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/getCurrentUserAuthInfo',
                    method: "POST",
                    params: 1,          // this is App Id for DELTA              
                    disableCaching: true,
                    success: getCurrentUserAuthInfoSuccess,
                    failure: getCurrentUserAuthInfoFailed
                }
            );

            function getCurrentUserAuthInfoSuccess(response, options)
            {
                delta3.utils.GlobalVars.currentUserAuthInfo = Ext.decode(response.responseText);
                delta3.utils.GlobalVars.currentUser = delta3.utils.GlobalVars.currentUserAuthInfo.userAlias;
                if ( delta3.utils.GlobalVars.currentUserAuthInfo.success === true ) {
                    //Ext.create('delta3.view.Viewport'); 
                    callbackSuccess();
                } else {
                    //console.log(delta3.utils.GlobalVars.currentUserAuthInfo.status.message);
                    Ext.create({xtype: 'login'});    
//                    Ext.MessageBox.show
//                        (
//                            {
//                                title: 'DELTA',
//                                msg: delta3.utils.GlobalVars.currentUserAuthInfo.status.message,
//                                width: 400,
//                                wait: false,
//                                closable: false,
//                                icon: Ext.MessageBox.ERROR,
//                                buttons: Ext.MessageBox.OK,
//                                fn: function() { 
//                                    //delta3.utils.GlobalFunc.doLogout();
//                                    //window.location = 'index.html';   
//                                    Ext.create({xtype: 'login'});                                      
//                                }
//                            }
//                        );                       
                }
            }

            function getCurrentUserAuthInfoFailed(response, options)
            {
                //console.log(response.responseText);
                callbackFailed();                    
            }
        },
//------------------------------------------------------------------------------                
        getCurrentUser: function(viewPort) {
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/getCurrentUser',
                    method: "GET",
                    disableCaching: true,
                    success: callSuccess,
                    failure: callFailed
                }
            );

            function callSuccess(response, options) {
               var view = Ext.ComponentQuery.query('#currentUserAlias')[0];
               view.setText(response.responseText);
               delta3.utils.GlobalVars.currentUser = response.responseText;
            }   

            function callFailed(response, options) {
                Ext.MessageBox.show
                (
                    {
                        title: 'DELTA',
                        msg: '<b style="color:#cc0000;">Access denied!</b> Please check the credentials and try again<br/><br/><div style="padding-left:48px;">If you have forgotten your password, please click <a href="#">here</a>.<br/><br/>For any other assistance, <a href="mailto:admin@copingsystems.com">contact administrator</a> with your queries.',
                        progressText: 'Checking...',
                        width: 400,
                        wait: false,
                        closable: false,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.MessageBox.OK,
                        fn: function() { }
                    }
                );    
                viewPort.destroy();
            }
        },
//------------------------------------------------------------------------------
        getUserAuthInfo: function(userAlias, idUser, callback){		
            var userAuthInfoJSONString = '{"userAuth":[{"alias":"' + userAlias 
            + '", "idUser":"' + idUser
            + '"}]}';

            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/getUserAuthInfo',
                    method: "POST",
                    disableCaching: true,
                    params: userAuthInfoJSONString,
                    success: callback,
                    failure: getUserAuthInfoFailed
                }
            );

            function getUserAuthInfoFailed(response, options)
            {
                //console.log('calling getUserAuthInfo failed ' + response.responseText);
            }
        },
//------------------------------------------------------------------------------
        changePassword: function(oldPass, newPass){		
            var userAuthInfoJSONString = '{"op":"' + oldPass + '", "np":"' + newPass + '"}';
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/changePassword',
                    method: "POST",
                    disableCaching: true,
                    params: userAuthInfoJSONString,
                    success: changePasswordSuccess,
                    failure: changePasswordFailed
                }
            );
            function changePasswordSuccess(response, options)
            {
                var resp = JSON.parse(response.responseText);
                if (typeof resp.success !== 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage(resp.status.message);
                }
            }
            function changePasswordFailed(response, options)
            {
                //console.log('calling changePassword failed ' + response.responseText);
            }
        },    
//------------------------------------------------------------------------------
        resetPassword: function(alias){		
            //var userInfoJSONString = '{"user":"' + alias + '"}';
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/resetPassword',
                    method: "POST",
                    disableCaching: true,
                    params: alias,
                    success: resetPasswordSuccess,
                    failure: resetPasswordFailed
                }
            );
            function resetPasswordSuccess(response, options)
            {
                var resp = JSON.parse(response.responseText);
                if (typeof resp.success !== 'undefined') {
                    delta3.utils.GlobalFunc.showDeltaMessage(resp.status.message);
                }
            }
            function resetPasswordFailed(response, options)
            {
                //console.log('calling resetPassword failed ' + response.responseText);
            }
        },        
//------------------------------------------------------------------------------
        getRolePermissionInfo: function(name, idRole, callback){		
            var rolePermissionInfoJSONString = '{"rolePermissions":[{"name":"' + name 
            + '", "idRole":"' + idRole
            + '"}]}';

            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/getRolePermissionInfo',
                    method: "POST",
                    disableCaching: true,
                    params: rolePermissionInfoJSONString,
                    success: callback,
                    failure: getRolePermissionInfoFailed
                }
            );

            function getRolePermissionInfoFailed(response, options)
            {
                //console.log('calling getRolePermissionInfo failed ' + response.responseText);
            }
        }, 
//------------------------------------------------------------------------------
        getMatchSets: function(studyId, callback){		
            var params = '{"studyId":"' + studyId + '"}';
            var extraParams = JSON.parse(params);
            
            Ext.Ajax.request
            (
                {
                    url: '/Delta3/webresources/admin/getMatchSets',
                    method: "GET",
                    disableCaching: true,
                    params: extraParams,
                    success: callback,
                    failure: callFailed
                }
            );

            function callFailed(response, options)
            {
                console.log('calling getMatchSets failed ' + response.responseText);
            }
        },         
//------------------------------------------------------------------------------        
        doLogout: function()
        {
            Ext.Ajax.request
                    (
                            {
                                url: '/Delta3/webresources/admin/logout',
                                method: "POST",
                                disableCaching: true,
                                success: doLogoutSuccess,
                                failure: doLogoutFailed
                            }
                    );
            function doLogoutSuccess(response, options)
            {
                localStorage.removeItem('DeltaLoggedIn');
                console.log('logout success');   
                gotoLoginPage();
            }

            function doLogoutFailed(response, options)
            {
                setTimeout(gotoLoginPage, 1000);
            }

            function gotoLoginPage()
            {
                window.location = './index.html';
            }    
        },    
//------------------------------------------------------------------------------            
        lookupUserAlias: function(val, cell, rec, r_idx, c_idx, store, idString, lookupStore) {
            var tableIndex = rec.data[idString]; 
            var model = lookupStore.findRecord( 'idUser', tableIndex, 0, false, false, true);
            if ( model === null ) {
                if ( tableIndex === 0 ) 
                    return "system";
                else 
                    return null;
            }
            else  {
                return model.get("alias");
            }
        },
//------------------------------------------------------------------------------            
        lookupUserInfo: function(rec, idString, lookupStore) {
            var tableIndex = rec.data[idString]; 
            var model = lookupStore.findRecord( 'idUser', tableIndex, 0, false, false, true);
            if ( model === null ) 
                return null;
            else  {
                return model.get("alias") + ', ' + model.get("firstName") + ' ' + model.get("lastName");
            }   
        },        
//------------------------------------------------------------------------------            
        isPermitted: function(tag, project) {
        //console.log("project : "+project);
        //console.log("tag  : "+tag);
            
            
            if ( typeof project === 'undefined' ) {
                //console.log("This is at is permitted inside undefined"+project);
                project = 0;
            }
            
            
            var projects = delta3.utils.GlobalVars.currentUserAuthInfo.userAuth.projects;
            //console.log("projects" +projects);
            
            for (var j=0; j<projects.length; j++) {
                
                if ( projects[j].projectId === project ) 
                {   
                    //console.log("projects[j].projectId" +projects[j].projectId);
                    //console.log("project" +project);
                    var roles = projects[j].roles;
                    //console.log("This is at projects"+roles); 
                    
                    for (var i=0; i<roles.length; i++) {
                        //console.log("roles.length : "+roles.length);
                        
                        for (var z=0; z<roles[i].permissions.length; z++) {
                            //console.log("This is at projects"+roles[i].permissions.tag); 
                            //console.log("roles[i].permissions[z].tag"+roles[i].permissions);
                            //console.log("roles[i].permissions[z].tag" +roles[i].permissions[z].tag);
                            //console.log("tag" +tag);
                    
                            if ( roles[i].permissions[z].tag === tag) 
                            {
                                //console.log("role : "+roles[i].permissions[z].tag);
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        },
//------------------------------------------------------------------------------        
        standardDeviation: function(numbersArr) {
            //--CALCULATE AVAREGE--
            var total = 0;
            for(var key in numbersArr) 
               total += numbersArr[key];
            var meanVal = total / numbersArr.length;
            //--CALCULATE AVAREGE--

            //--CALCULATE STANDARD DEVIATION--
            var SDprep = 0;
            for(var key in numbersArr) 
               SDprep += Math.pow((parseFloat(numbersArr[key]) - meanVal),2);
            var SDresult = Math.sqrt(SDprep/numbersArr.length);
            //--CALCULATE STANDARD DEVIATION--
            return SDresult;

        },        
//------------------------------------------------------------------------------                  
        calcStandardDeviation: function(n, u, nsd) {
            var sd;
            var sd2;

            if (( n === null ) || ( u === null )) {
                return 0;
            }
            if ( nsd === null ) {
                nsd = 1;
            }
            sd2 = nsd*nsd;
            if ( (sd2 + 4*n*u*(1-u)) < 0 ) {
                return 0;
            }
            sd = 2*n*u + sd2 + nsd * Math.sqrt(sd2 + 4*n*u*(1-u));
            if ( (n + sd2) === 0 ) {
                return 0;
            }
            sd = sd/(2 * (n + sd2));
            if ( sd > 1 ) {
                sd = 1;
            }  
            return sd - u;
        },
//------------------------------------------------------------------------------  
        calcCorrelation: function(X, Y) {
            if ( X.length !== Y.length ) return "Array sizes have to be equal";
            var x_mean, y_mean, size = X.length, cov_xy, result;
            for (var i=0; i<size; i++) {
                x_mean += X[i];
            }
            x_mean = x_mean/size;
            for (var i=0; i<size; i++) {
                y_mean += Y[i];
            }
            y_mean = y_mean/size;        
            for (var i=0; i<size; i++) {
                cov_xy += (X[i]-x_mean) * (Y[i]-y_mean);
            }
            cov_xy = cov_xy/(size-1);
            result = cov_xy/calcStdDeviation(X,x_mean)*calcStdDeviation(Y,y_mean);
            return result;
        },
        calcStdDeviation: function(Z, z_mean) {
            var stdDev;
            for (var i=0; Z.length; i++) {
                stdDev = (Z[i]-z_mean)(Z[i]-z_mean);
            }
            stdDev = stdDev/(Z.lenght-1);
            stdDev = Math.sqrt(stdDev);
        },
//------------------------------------------------------------------------------          
        emphasizeString: function(string) {
            return '<div data-qtip="' + string + '"><span style="background-color: yellow; font-size: small">' + string + '</span></div>';
        },
        displayCentrallyBigString: function(string) {
            //return '<span style="color: blue; font-size: 300%; text-align: center">' + string + '</span>';
            return '<div data-qtip="' + string + '"><span id="centralInfo">' + string + '</span></div>';
        }           
    }  
});

