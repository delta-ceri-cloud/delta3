/* 
 * CERI-Lahey
 * used to change number of records in a grid
 */
Ext.define("delta3.utils.GridPaginator", {
    extend: 'Ext.form.FieldContainer',
    layout: 'hbox',
    margin: "1 1 1 1",   
    renderTo: Ext.getBody(),
    gridToReload: {},
    items: [{
            xtype: 'textfield',
            itemId: 'recNumText',
            name: 'recNumText',
            fieldLabel: 'Records per page',
            maxWidth: 240,
            labelWidth: 120,
            labelAlign: 'right',               
            emptyText: 'Enter number ..'
        }, {
            xtype: 'button',
            text: 'Change',
            handler: function() {
                var text = this.up().down('#recNumText').getValue();
                if ( (text === '') || (typeof text === 'undefined') ) return;                
                if ( isNaN(text) ) {
                    return;
                }
                this.up().down('#recNumText').setValue('');                
                var grid = this.up().gridToReload;       
                grid.dockedItems.items[0].pageSize = parseInt(text);
                grid.getStore().pageSize = parseInt(text);
                grid.getStore().reload({params: {
                    start: 0,
                    limit: text
                }}); 
            }
        }],
    initComponent: function() {
        var me = this;
        me.callParent();
    }
});

