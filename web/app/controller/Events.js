/* 
 * Event controller
 */

Ext.define('delta3.controller.Events', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Events',
    id: 'delta3.controller.Events',
    requires: [
        'delta3.view.event.EventGrid',
        'delta3.view.event.EventContainer',
        'delta3.model.EventModel',
        'delta3.store.EventStore'        
    ],    
    models: [
        'EventModel'
    ],
    stores: [
        'EventStore'
    ],    
    views: [
        'EventGrid',
        'EventContainer'
    ]  
});

