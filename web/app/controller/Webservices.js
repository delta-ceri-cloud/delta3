/* 
 * Web Services controller
 */

Ext.define('delta3.controller.Webservices', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Webservices',
    id: 'delta3.controller.Webservices',
    requires: [
        'delta3.view.WebServicePanel',
        'delta3.view.WebservicesContainer' 
    ],     
    views: [
        'WebServicePanel',
        'WebservicesContainer'
    ]  
});

