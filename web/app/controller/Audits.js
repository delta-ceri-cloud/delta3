/* 
 * Audit controller
 */


Ext.define('delta3.controller.Audits', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Audits',       
    id: 'delta3.controller.Audits',
    requires: [
        'delta3.view.AuditGrid',
        'delta3.view.AuditContainer',
        'delta3.model.AuditModel',
        'delta3.store.AuditStore'],           
    models: [
        'AuditModel'
    ],
    stores: [
        'AuditStore'
    ],
    views: [
        'AuditGrid',
        'AuditContainer'
    ]   
});