/* Main logic for dynamically supplying controllers behind manu clicks */

Ext.define('delta3.controller.Main', {
    extend: 'Ext.app.Controller',
    alias: 'controller.Main',    
    id: 'Main',
    init: function () {
        var me = this;
        me.control({
            'toolbar[itemId=mainmenu] button[itemId=startbutton] menuitem': {
                click: me.openModule
            }
        });
    },
    openModule: function (menuoption) {
        var me = this;
        var maintabs = Ext.ComponentQuery.query('#maintabs')[0];
        Ext.Msg.wait('Loading...');
        Ext.require(menuoption.controller, function () {
            Ext.Msg.hide();
            var controllerId = menuoption.controller.getId();
            var controller = me.getApplication().controllers.get(controllerId);
            if (!controller) {
                controller = Ext.create(controllerId, {
                    id: controllerId,
                    application: me.application
                });
                controller.container = me.createContainer(menuoption);
                //controller.container.closable = true,
                maintabs.add(controller.container);
                me.application.controllers.add(controller);
                controller.init();
                maintabs.show();
                maintabs.setActiveTab(controller.container);
                //controller.onLaunch(me.application);
            } else {
                if (controller.container.isDestroyed) {
                    controller.container = me.createContainer(menuoption);
                    maintabs.add(controller.container);
                }
            }
            controller.init()
            maintabs.setActiveTab(controller.container);
        });
    },
    createContainer: function (menuoption) {
        var name = menuoption.controller.type.split('.');
        var localXtype = 'container.' + name[2].charAt(0).toLowerCase() + name[2].slice(1);
        return Ext.widget({
            xtype: localXtype,
            title: menuoption.text,
            iconCls: menuoption.iconCls,
            closable: false,            
            layout: 'fit'
        });
    }
});
