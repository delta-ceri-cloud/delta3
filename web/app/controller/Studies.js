/* 
 * Study controller
 */

Ext.define('delta3.controller.Studies', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.Studies',
    id: 'delta3.controller.Studies',
    requires: [
        'delta3.view.study.StudyGrid',
        'delta3.view.study.StudyContainer',
        'delta3.model.StudyModel',
        'delta3.store.StudyStore'        
    ],    
    models: [
        'StudyModel'
    ],
    stores: [
        'StudyStore'
    ],    
    views: [
        'StudyGrid',
        'StudyContainer'
    ]  
});

