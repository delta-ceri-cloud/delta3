/* 
CERI 2015
 */

Ext.define('delta3.controller.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.Login',
    requires: [
        'delta3.utils.GlobalVars',
        'delta3.utils.GlobalFunc',
        'delta3.utils.Tooltips'
    ],      
    onLoginClick: function() {
        var me = this;
        var userAlias = Ext.getCmp('UID');
        var password = Ext.getCmp('PWD');
        
        
        
        Ext.Ajax.request
                (
                        {
                            url: '/Delta3/webresources/admin/login',
                            method: "GET",
                            disableCaching: true,
                            params: {Username: userAlias.getValue(), Password: password.getValue(), App: '1'},
                            success: loginSuccess,
                            failure: loginFailed
                        }
                );
        
        
        
        
        function loginSuccess(response, options)
        {
            var resp = JSON.parse(response.responseText);
            if(typeof resp.success !== 'undefined') {     
                if ( resp.success === false ) {
                    Ext.MessageBox.hide();
                    Ext.MessageBox.show
                        (
                            {
                                title: 'DELTA',
                                msg: '<b style="color:#cc0000;">' + resp.status.message + '</b>',
                                progressText: 'Checking...',
                                width: 400,
                                wait: false,
                                closable: false,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.MessageBox.OK
                            }
                        );
                } else {
                    Ext.create('delta3.utils.GlobalVars');
                    Ext.create('delta3.utils.GlobalFunc');
                    Ext.create('delta3.utils.Tooltips');    
                    // remove login window
                    me.getView().destroy(); 
                     //Ext.MessageBox.wait('DELTA Login Successful. Loading views for user ...');     
                    
                    
                    Ext.MessageBox.show({
                        title: 'DELTA Login Successful',
                        msg: 'User authenticated. Please wait while loading accessible modules...',
                        progressText: 'Loading user views...',
                        top:50,
                        width:600,
                        height:100,
                        progress:true,
                        closable:false,
                        aniEl:'mb6'
                      });
                    showProgressBar();
                    delta3.utils.GlobalFunc.getCurrentUserAuthInfo(me.initializeGlobalVariables, getCurrentUserAuthInfoFailed);
                }                  
            }         
        }
        
        function progressBar(v) {
               return function()	
               {
                  if(v === 10) {
                     Ext.MessageBox.hide();
                     //result();
                  } else {
                     var i = v/9;
                     Ext.MessageBox.updateProgress(i, Math.round(100*i)+'% completed');
                  }
               };
            };
            function showProgressBar() {
               for(var i = 1; i < 11; i++) {
                  setTimeout(progressBar(i), i*500);
               }
            }
            function result() {
               Ext.Msg.alert('status', '');
            }
        
        function getCurrentUserAuthInfoFailed(response, options) {
            console.log(response.responseText);
            delta3.utils.GlobalFunc.doLogout();             
            window.location = 'index.html';                     
        }

        function loginFailed(response, options)
        {
            Ext.MessageBox.hide();
            Ext.MessageBox.show
                (
                    {
                        title: 'DELTA',
                        msg: '<b style="color:#cc0000;">Access denied!</b> Please contact your System Administrator<br/><br/>',
                        progressText: 'Checking...',
                        width: 400,
                        wait: false,
                        closable: false,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.MessageBox.OK
                    }
                );
        }                  
    },
    
    
    
    
    initializeGlobalVariables: function() {
        // pre-load in-memory data stores for fast lookups
        delta3.utils.GlobalVars.OrgStore = Ext.create('delta3.store.OrganizationStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
        delta3.utils.GlobalVars.OrgStore.load();
        delta3.utils.GlobalVars.RoleStore = Ext.create('delta3.store.RoleStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
        delta3.utils.GlobalVars.RoleStore.load();
        delta3.utils.GlobalVars.PermissionStore = Ext.create('delta3.store.PermissionStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
        delta3.utils.GlobalVars.PermissionStore.load();
        delta3.utils.GlobalVars.FieldStore = Ext.create('delta3.store.FieldStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
        delta3.utils.GlobalVars.FieldStore.load();
        delta3.utils.GlobalVars.UserStore = Ext.create('delta3.store.UserStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
        delta3.utils.GlobalVars.UserStore.load();        
        delta3.utils.GlobalVars.StudyStore = Ext.create('delta3.store.StudyStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
        delta3.utils.GlobalVars.StudyStore.load();         
        delta3.utils.GlobalVars.ModelStore = Ext.create('delta3.store.ModelStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
        delta3.utils.GlobalVars.ModelStore.load();      
        delta3.utils.GlobalVars.ConfigurationStore = Ext.create('delta3.store.ConfigurationStore', {pageSize: delta3.utils.GlobalVars.largePageSize});
        delta3.utils.GlobalVars.ConfigurationStore.load();                         
        setTimeout(gotoMain, 300);   
        
        function gotoMain() {
            Ext.MessageBox.hide();      
            Ext.widget('view-main');
        }        
    }    
});
