/* 
 * Health Survey controller
 */

Ext.define('delta3.controller.HealthSurvey', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.delta3.controller.HealthSurvey',
    id: 'delta3.controller.HealthSurvey',
    requires: [
        'delta3.view.HealthSurveyPanel',
        'delta3.view.HealthSurveyContainer' 
    ],     
    views: [
        'HealthSurveyPanel',
        'HelathSurveyContainer'
    ]  
});
